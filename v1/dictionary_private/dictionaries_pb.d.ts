// package: fcp.dictionary.v1.dictionary_private
// file: v1/dictionary_private/dictionaries.proto

import * as jspb from "google-protobuf";
import * as v1_dictionary_cultures_group_pb from "../../v1/dictionary/cultures_group_pb";
import * as v1_dictionary_culture_pb from "../../v1/dictionary/culture_pb";
import * as v1_dictionary_product_pb from "../../v1/dictionary/product_pb";
import * as v1_dictionary_brand_pb from "../../v1/dictionary/brand_pb";
import * as v1_dictionary_packaging_pb from "../../v1/dictionary/packaging_pb";
import * as v1_dictionary_packaging_for_product_pb from "../../v1/dictionary/packaging_for_product_pb";
import * as v1_dictionary_active_substance_pb from "../../v1/dictionary/active_substance_pb";
import * as v1_dictionary_active_substance_for_product_pb from "../../v1/dictionary/active_substance_for_product_pb";
import * as v1_dictionary_vermin_group_pb from "../../v1/dictionary/vermin_group_pb";
import * as v1_dictionary_vermin_pb from "../../v1/dictionary/vermin_pb";
import * as v1_dictionary_unit_pb from "../../v1/dictionary/unit_pb";
import * as v1_dictionary_technology_crop_processing_pb from "../../v1/dictionary/technology_crop_processing_pb";
import * as v1_dictionary_microelement_pb from "../../v1/dictionary/microelement_pb";
import * as v1_dictionary_microelement_for_product_pb from "../../v1/dictionary/microelement_for_product_pb";
import * as v1_dictionary_hybrids_group_pb from "../../v1/dictionary/hybrids_group_pb";
import * as v1_dictionary_hybrids_pb from "../../v1/dictionary/hybrids_pb";
import * as v1_dictionary_hybrids_groups_for_culture_pb from "../../v1/dictionary/hybrids_groups_for_culture_pb";
import * as v1_dictionary_insect_row_pb from "../../v1/dictionary/insect_row_pb";
import * as v1_dictionary_insect_family_pb from "../../v1/dictionary/insect_family_pb";
import * as v1_dictionary_insect_info_pb from "../../v1/dictionary/insect_info_pb";
import * as v1_dictionary_vermin_for_crop_processing_pb from "../../v1/dictionary/vermin_for_crop_processing_pb";
import * as v1_dictionary_vermin_for_prep_pb from "../../v1/dictionary/vermin_for_prep_pb";
import * as v1_dictionary_vermin_for_active_substance_pb from "../../v1/dictionary/vermin_for_active_substance_pb";
import * as v1_dictionary_group_weeds_pb from "../../v1/dictionary/group_weeds_pb";
import * as v1_dictionary_group_weeds_for_weeds_pb from "../../v1/dictionary/group_weeds_for_weeds_pb";
import * as v1_dictionary_country_pd_pb from "../../v1/dictionary/country_pd_pb";
import * as v1_dictionary_culture_for_crop_processing_pb from "../../v1/dictionary/culture_for_crop_processing_pb";
import * as v1_dictionary_culture_grow_stage_pb from "../../v1/dictionary/culture_grow_stage_pb";
import * as v1_dictionary_family_weeds_pb from "../../v1/dictionary/family_weeds_pb";
import * as v1_dictionary_features_pb from "../../v1/dictionary/features_pb";
import * as v1_dictionary_features_for_culture_pb from "../../v1/dictionary/features_for_culture_pb";
import * as v1_dictionary_features_for_hybrid_pb from "../../v1/dictionary/features_for_hybrid_pb";
import * as v1_dictionary_features_values_pb from "../../v1/dictionary/features_values_pb";
import * as v1_dictionary_insect_type_pb from "../../v1/dictionary/insect_type_pb";
import * as v1_dictionary_insect_type_for_insect_pb from "../../v1/dictionary/insect_type_for_insect_pb";
import * as v1_dictionary_manufacturer_pb from "../../v1/dictionary/manufacturer_pb";
import * as v1_dictionary_perform_methods_pb from "../../v1/dictionary/perform_methods_pb";
import * as v1_dictionary_preparative_form_pb from "../../v1/dictionary/preparative_form_pb";
import * as v1_dictionary_product_class_pb from "../../v1/dictionary/product_class_pb";
import * as v1_dictionary_product_group_pb from "../../v1/dictionary/product_group_pb";
import * as v1_dictionary_product_in_grow_stage_pb from "../../v1/dictionary/product_in_grow_stage_pb";
import * as v1_dictionary_recomended_zone_pb from "../../v1/dictionary/recomended_zone_pb";
import * as v1_dictionary_zone_weed_pb from "../../v1/dictionary/zone_weed_pb";
import * as v1_dictionary_zone_for_hybrids_pb from "../../v1/dictionary/zone_for_hybrids_pb";
import * as v1_dictionary_zone_weed_for_weed_pb from "../../v1/dictionary/zone_weed_for_weed_pb";
import * as v1_dictionary_type_of_pathogen_pb from "../../v1/dictionary/type_of_pathogen_pb";
import * as v1_dictionary_vermin_description_pb from "../../v1/dictionary/vermin_description_pb";
import * as v1_dictionary_weeds_info_pb from "../../v1/dictionary/weeds_info_pb";
import * as v1_dictionary_product_kind_pb from "../../v1/dictionary/product_kind_pb";
import * as v1_dictionary_product_type_pb from "../../v1/dictionary/product_type_pb";
import * as v1_dictionary_product_type_to_kind_pb from "../../v1/dictionary/product_type_to_kind_pb";
import * as v1_dictionary_product_sub_group_pb from "../../v1/dictionary/product_sub_group_pb";
import * as v1_dictionary_product_group_to_kind_pb from "../../v1/dictionary/product_group_to_kind_pb";
import * as v1_dictionary_product_group_to_sub_group_pb from "../../v1/dictionary/product_group_to_sub_group_pb";

