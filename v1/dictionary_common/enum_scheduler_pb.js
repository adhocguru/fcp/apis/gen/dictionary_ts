// source: v1/dictionary_common/enum_scheduler.proto
/**
 * @fileoverview
 * @enhanceable
 * @suppress {messageConventions} JS Compiler reports an error if a variable or
 *     field starts with 'MSG_' and isn't a translatable message.
 * @public
 */
// GENERATED CODE -- DO NOT EDIT!

var jspb = require('google-protobuf');
var goog = jspb;
var global = Function('return this')();

goog.exportSymbol('proto.fcp.dictionary.v1.dictionary_common.SchedulerTaskPriority', null, global);
goog.exportSymbol('proto.fcp.dictionary.v1.dictionary_common.SchedulerTaskStatus', null, global);
goog.exportSymbol('proto.fcp.dictionary.v1.dictionary_common.SchedulerTaskType', null, global);
/**
 * @enum {number}
 */
proto.fcp.dictionary.v1.dictionary_common.SchedulerTaskStatus = {
  SHTS_UNSPECIFIED: 0,
  SHTS_WAITING: 1,
  SHTS_ERROR: 2,
  SHTS_LOCKED: 3,
  SHTS_WORK: 4,
  SHTS_DELETE: 5
};

/**
 * @enum {number}
 */
proto.fcp.dictionary.v1.dictionary_common.SchedulerTaskType = {
  SHTT_UNSPECIFIED: 0,
  SHTT_ONCE: 1,
  SHTT_PERIODIC: 2,
  SHTT_PERIODIC_BY_MODEL: 3
};

/**
 * @enum {number}
 */
proto.fcp.dictionary.v1.dictionary_common.SchedulerTaskPriority = {
  SHTP_UNSPECIFIED: 0,
  SHTP_CRITICAL: 1,
  SHTP_IMPORTANT: 2,
  SHTP_COMMON: 3,
  SHTP_MINOR: 4,
  SHTP_STUFF: 5
};

goog.object.extend(exports, proto.fcp.dictionary.v1.dictionary_common);
