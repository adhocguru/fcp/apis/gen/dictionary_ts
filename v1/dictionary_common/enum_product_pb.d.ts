// package: fcp.dictionary.v1.dictionary_common
// file: v1/dictionary_common/enum_product.proto

import * as jspb from "google-protobuf";

export class ProductProcessValue extends jspb.Message {
  getValue(): ProductProcessMap[keyof ProductProcessMap];
  setValue(value: ProductProcessMap[keyof ProductProcessMap]): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ProductProcessValue.AsObject;
  static toObject(includeInstance: boolean, msg: ProductProcessValue): ProductProcessValue.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ProductProcessValue, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ProductProcessValue;
  static deserializeBinaryFromReader(message: ProductProcessValue, reader: jspb.BinaryReader): ProductProcessValue;
}

export namespace ProductProcessValue {
  export type AsObject = {
    value: ProductProcessMap[keyof ProductProcessMap],
  }
}

export class ProductTypeValue extends jspb.Message {
  getValue(): ProductTypeMap[keyof ProductTypeMap];
  setValue(value: ProductTypeMap[keyof ProductTypeMap]): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ProductTypeValue.AsObject;
  static toObject(includeInstance: boolean, msg: ProductTypeValue): ProductTypeValue.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ProductTypeValue, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ProductTypeValue;
  static deserializeBinaryFromReader(message: ProductTypeValue, reader: jspb.BinaryReader): ProductTypeValue;
}

export namespace ProductTypeValue {
  export type AsObject = {
    value: ProductTypeMap[keyof ProductTypeMap],
  }
}

export interface ProductProcessMap {
  UNSPECIFIED: 0;
  RESOURCES: 1;
  SERVICES: 2;
  PRODUCTS: 3;
  GOODS: 4;
}

export const ProductProcess: ProductProcessMap;

export interface ProductTypeMap {
  PRODUCTTYPE_UNSPECIFIED: 0;
  RESOURCES_SEEDS: 100;
  RESOURCES_FERTILIZE: 101;
  RESOURCES_MICROFERTILIZERS: 102;
  RESOURCES_CHEMISTRY: 103;
  SERVICES_BANKING: 200;
  SERVICES_INSURANCE: 201;
  SERVICES_LEGAL: 202;
  PRODUCTS_FOOD: 300;
  GOODS_OVERALLS: 400;
}

export const ProductType: ProductTypeMap;

