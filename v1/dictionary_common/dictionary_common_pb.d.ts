// package: fcp.dictionary.v1.dictionary_common
// file: v1/dictionary_common/dictionary_common.proto

import * as jspb from "google-protobuf";
import * as google_protobuf_timestamp_pb from "google-protobuf/google/protobuf/timestamp_pb";

export class Sorting extends jspb.Message {
  getFieldName(): string;
  setFieldName(value: string): void;

  getOrder(): Sorting.OrderMap[keyof Sorting.OrderMap];
  setOrder(value: Sorting.OrderMap[keyof Sorting.OrderMap]): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): Sorting.AsObject;
  static toObject(includeInstance: boolean, msg: Sorting): Sorting.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: Sorting, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): Sorting;
  static deserializeBinaryFromReader(message: Sorting, reader: jspb.BinaryReader): Sorting;
}

export namespace Sorting {
  export type AsObject = {
    fieldName: string,
    order: Sorting.OrderMap[keyof Sorting.OrderMap],
  }

  export interface OrderMap {
    ASC: 0;
    DESC: 1;
  }

  export const Order: OrderMap;
}

export class PaginationRequest extends jspb.Message {
  getPageSize(): number;
  setPageSize(value: number): void;

  getCurrentPage(): number;
  setCurrentPage(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): PaginationRequest.AsObject;
  static toObject(includeInstance: boolean, msg: PaginationRequest): PaginationRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: PaginationRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): PaginationRequest;
  static deserializeBinaryFromReader(message: PaginationRequest, reader: jspb.BinaryReader): PaginationRequest;
}

export namespace PaginationRequest {
  export type AsObject = {
    pageSize: number,
    currentPage: number,
  }
}

export class PaginationResponse extends jspb.Message {
  getTotalItems(): number;
  setTotalItems(value: number): void;

  getTotalPages(): number;
  setTotalPages(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): PaginationResponse.AsObject;
  static toObject(includeInstance: boolean, msg: PaginationResponse): PaginationResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: PaginationResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): PaginationResponse;
  static deserializeBinaryFromReader(message: PaginationResponse, reader: jspb.BinaryReader): PaginationResponse;
}

export namespace PaginationResponse {
  export type AsObject = {
    totalItems: number,
    totalPages: number,
  }
}

export class TimeRange extends jspb.Message {
  hasFrom(): boolean;
  clearFrom(): void;
  getFrom(): google_protobuf_timestamp_pb.Timestamp | undefined;
  setFrom(value?: google_protobuf_timestamp_pb.Timestamp): void;

  hasTo(): boolean;
  clearTo(): void;
  getTo(): google_protobuf_timestamp_pb.Timestamp | undefined;
  setTo(value?: google_protobuf_timestamp_pb.Timestamp): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): TimeRange.AsObject;
  static toObject(includeInstance: boolean, msg: TimeRange): TimeRange.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: TimeRange, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): TimeRange;
  static deserializeBinaryFromReader(message: TimeRange, reader: jspb.BinaryReader): TimeRange;
}

export namespace TimeRange {
  export type AsObject = {
    from?: google_protobuf_timestamp_pb.Timestamp.AsObject,
    to?: google_protobuf_timestamp_pb.Timestamp.AsObject,
  }
}

export class PairInt64String extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  getName(): string;
  setName(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): PairInt64String.AsObject;
  static toObject(includeInstance: boolean, msg: PairInt64String): PairInt64String.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: PairInt64String, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): PairInt64String;
  static deserializeBinaryFromReader(message: PairInt64String, reader: jspb.BinaryReader): PairInt64String;
}

export namespace PairInt64String {
  export type AsObject = {
    id: number,
    name: string,
  }
}

export class PairStringString extends jspb.Message {
  getId(): string;
  setId(value: string): void;

  getName(): string;
  setName(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): PairStringString.AsObject;
  static toObject(includeInstance: boolean, msg: PairStringString): PairStringString.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: PairStringString, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): PairStringString;
  static deserializeBinaryFromReader(message: PairStringString, reader: jspb.BinaryReader): PairStringString;
}

export namespace PairStringString {
  export type AsObject = {
    id: string,
    name: string,
  }
}

export interface StateMap {
  ACTIVE: 0;
  INACTIVE: 1;
}

export const State: StateMap;

