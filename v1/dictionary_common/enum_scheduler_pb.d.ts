// package: fcp.dictionary.v1.dictionary_common
// file: v1/dictionary_common/enum_scheduler.proto

import * as jspb from "google-protobuf";

export interface SchedulerTaskStatusMap {
  SHTS_UNSPECIFIED: 0;
  SHTS_WAITING: 1;
  SHTS_ERROR: 2;
  SHTS_LOCKED: 3;
  SHTS_WORK: 4;
  SHTS_DELETE: 5;
}

export const SchedulerTaskStatus: SchedulerTaskStatusMap;

export interface SchedulerTaskTypeMap {
  SHTT_UNSPECIFIED: 0;
  SHTT_ONCE: 1;
  SHTT_PERIODIC: 2;
  SHTT_PERIODIC_BY_MODEL: 3;
}

export const SchedulerTaskType: SchedulerTaskTypeMap;

export interface SchedulerTaskPriorityMap {
  SHTP_UNSPECIFIED: 0;
  SHTP_CRITICAL: 1;
  SHTP_IMPORTANT: 2;
  SHTP_COMMON: 3;
  SHTP_MINOR: 4;
  SHTP_STUFF: 5;
}

export const SchedulerTaskPriority: SchedulerTaskPriorityMap;

