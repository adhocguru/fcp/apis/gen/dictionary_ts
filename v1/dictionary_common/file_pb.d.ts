// package: fcp.dictionary.v1.dictionary_common
// file: v1/dictionary_common/file.proto

import * as jspb from "google-protobuf";
import * as google_protobuf_timestamp_pb from "google-protobuf/google/protobuf/timestamp_pb";

export class Part extends jspb.Message {
  getNum(): number;
  setNum(value: number): void;

  getSize(): number;
  setSize(value: number): void;

  getContent(): Uint8Array | string;
  getContent_asU8(): Uint8Array;
  getContent_asB64(): string;
  setContent(value: Uint8Array | string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): Part.AsObject;
  static toObject(includeInstance: boolean, msg: Part): Part.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: Part, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): Part;
  static deserializeBinaryFromReader(message: Part, reader: jspb.BinaryReader): Part;
}

export namespace Part {
  export type AsObject = {
    num: number,
    size: number,
    content: Uint8Array | string,
  }
}

export class File extends jspb.Message {
  getUid(): string;
  setUid(value: string): void;

  getName(): string;
  setName(value: string): void;

  getSize(): number;
  setSize(value: number): void;

  getCompressType(): FileCompressTypeMap[keyof FileCompressTypeMap];
  setCompressType(value: FileCompressTypeMap[keyof FileCompressTypeMap]): void;

  getParts(): number;
  setParts(value: number): void;

  hasPart(): boolean;
  clearPart(): void;
  getPart(): Part | undefined;
  setPart(value?: Part): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): File.AsObject;
  static toObject(includeInstance: boolean, msg: File): File.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: File, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): File;
  static deserializeBinaryFromReader(message: File, reader: jspb.BinaryReader): File;
}

export namespace File {
  export type AsObject = {
    uid: string,
    name: string,
    size: number,
    compressType: FileCompressTypeMap[keyof FileCompressTypeMap],
    parts: number,
    part?: Part.AsObject,
  }
}

export interface FileCompressTypeMap {
  FCT_UNSPECIFIED: 0;
  FCT_ZIP: 1;
  FCT_GZIP: 2;
  FCT_TAR: 3;
}

export const FileCompressType: FileCompressTypeMap;

