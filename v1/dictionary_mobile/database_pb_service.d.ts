// package: fcp.dictionary.v1.dictionary_mobile
// file: v1/dictionary_mobile/database.proto

import * as v1_dictionary_mobile_database_pb from "../../v1/dictionary_mobile/database_pb";
import {grpc} from "@improbable-eng/grpc-web";

type MobileDbServiceGetDbVersion = {
  readonly methodName: string;
  readonly service: typeof MobileDbService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_dictionary_mobile_database_pb.GetDbVersionRequest;
  readonly responseType: typeof v1_dictionary_mobile_database_pb.GetDbVersionResponse;
};

type MobileDbServiceDownloadDb = {
  readonly methodName: string;
  readonly service: typeof MobileDbService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_dictionary_mobile_database_pb.DownloadDbRequest;
  readonly responseType: typeof v1_dictionary_mobile_database_pb.DownloadDbResponse;
};

type MobileDbServiceUploadDb = {
  readonly methodName: string;
  readonly service: typeof MobileDbService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_dictionary_mobile_database_pb.UploadDbRequest;
  readonly responseType: typeof v1_dictionary_mobile_database_pb.UploadDbResponse;
};

export class MobileDbService {
  static readonly serviceName: string;
  static readonly GetDbVersion: MobileDbServiceGetDbVersion;
  static readonly DownloadDb: MobileDbServiceDownloadDb;
  static readonly UploadDb: MobileDbServiceUploadDb;
}

export type ServiceError = { message: string, code: number; metadata: grpc.Metadata }
export type Status = { details: string, code: number; metadata: grpc.Metadata }

interface UnaryResponse {
  cancel(): void;
}
interface ResponseStream<T> {
  cancel(): void;
  on(type: 'data', handler: (message: T) => void): ResponseStream<T>;
  on(type: 'end', handler: (status?: Status) => void): ResponseStream<T>;
  on(type: 'status', handler: (status: Status) => void): ResponseStream<T>;
}
interface RequestStream<T> {
  write(message: T): RequestStream<T>;
  end(): void;
  cancel(): void;
  on(type: 'end', handler: (status?: Status) => void): RequestStream<T>;
  on(type: 'status', handler: (status: Status) => void): RequestStream<T>;
}
interface BidirectionalStream<ReqT, ResT> {
  write(message: ReqT): BidirectionalStream<ReqT, ResT>;
  end(): void;
  cancel(): void;
  on(type: 'data', handler: (message: ResT) => void): BidirectionalStream<ReqT, ResT>;
  on(type: 'end', handler: (status?: Status) => void): BidirectionalStream<ReqT, ResT>;
  on(type: 'status', handler: (status: Status) => void): BidirectionalStream<ReqT, ResT>;
}

export class MobileDbServiceClient {
  readonly serviceHost: string;

  constructor(serviceHost: string, options?: grpc.RpcOptions);
  getDbVersion(
    requestMessage: v1_dictionary_mobile_database_pb.GetDbVersionRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_mobile_database_pb.GetDbVersionResponse|null) => void
  ): UnaryResponse;
  getDbVersion(
    requestMessage: v1_dictionary_mobile_database_pb.GetDbVersionRequest,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_mobile_database_pb.GetDbVersionResponse|null) => void
  ): UnaryResponse;
  downloadDb(
    requestMessage: v1_dictionary_mobile_database_pb.DownloadDbRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_mobile_database_pb.DownloadDbResponse|null) => void
  ): UnaryResponse;
  downloadDb(
    requestMessage: v1_dictionary_mobile_database_pb.DownloadDbRequest,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_mobile_database_pb.DownloadDbResponse|null) => void
  ): UnaryResponse;
  uploadDb(
    requestMessage: v1_dictionary_mobile_database_pb.UploadDbRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_mobile_database_pb.UploadDbResponse|null) => void
  ): UnaryResponse;
  uploadDb(
    requestMessage: v1_dictionary_mobile_database_pb.UploadDbRequest,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_mobile_database_pb.UploadDbResponse|null) => void
  ): UnaryResponse;
}

