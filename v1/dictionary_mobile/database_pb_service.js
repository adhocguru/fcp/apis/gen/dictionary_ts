// package: fcp.dictionary.v1.dictionary_mobile
// file: v1/dictionary_mobile/database.proto

var v1_dictionary_mobile_database_pb = require("../../v1/dictionary_mobile/database_pb");
var grpc = require("@improbable-eng/grpc-web").grpc;

var MobileDbService = (function () {
  function MobileDbService() {}
  MobileDbService.serviceName = "fcp.dictionary.v1.dictionary_mobile.MobileDbService";
  return MobileDbService;
}());

MobileDbService.GetDbVersion = {
  methodName: "GetDbVersion",
  service: MobileDbService,
  requestStream: false,
  responseStream: false,
  requestType: v1_dictionary_mobile_database_pb.GetDbVersionRequest,
  responseType: v1_dictionary_mobile_database_pb.GetDbVersionResponse
};

MobileDbService.DownloadDb = {
  methodName: "DownloadDb",
  service: MobileDbService,
  requestStream: false,
  responseStream: false,
  requestType: v1_dictionary_mobile_database_pb.DownloadDbRequest,
  responseType: v1_dictionary_mobile_database_pb.DownloadDbResponse
};

MobileDbService.UploadDb = {
  methodName: "UploadDb",
  service: MobileDbService,
  requestStream: false,
  responseStream: false,
  requestType: v1_dictionary_mobile_database_pb.UploadDbRequest,
  responseType: v1_dictionary_mobile_database_pb.UploadDbResponse
};

exports.MobileDbService = MobileDbService;

function MobileDbServiceClient(serviceHost, options) {
  this.serviceHost = serviceHost;
  this.options = options || {};
}

MobileDbServiceClient.prototype.getDbVersion = function getDbVersion(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(MobileDbService.GetDbVersion, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

MobileDbServiceClient.prototype.downloadDb = function downloadDb(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(MobileDbService.DownloadDb, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

MobileDbServiceClient.prototype.uploadDb = function uploadDb(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(MobileDbService.UploadDb, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

exports.MobileDbServiceClient = MobileDbServiceClient;

