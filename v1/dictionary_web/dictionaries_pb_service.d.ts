// package: fcp.dictionary.v1.dictionary_web
// file: v1/dictionary_web/dictionaries.proto

import * as v1_dictionary_web_dictionaries_pb from "../../v1/dictionary_web/dictionaries_pb";
import * as v1_dictionary_cultures_group_pb from "../../v1/dictionary/cultures_group_pb";
import * as v1_dictionary_culture_pb from "../../v1/dictionary/culture_pb";
import * as v1_dictionary_product_pb from "../../v1/dictionary/product_pb";
import * as v1_dictionary_brand_pb from "../../v1/dictionary/brand_pb";
import * as v1_dictionary_packaging_pb from "../../v1/dictionary/packaging_pb";
import * as v1_dictionary_packaging_for_product_pb from "../../v1/dictionary/packaging_for_product_pb";
import * as v1_dictionary_active_substance_pb from "../../v1/dictionary/active_substance_pb";
import * as v1_dictionary_active_substance_for_product_pb from "../../v1/dictionary/active_substance_for_product_pb";
import * as v1_dictionary_vermin_group_pb from "../../v1/dictionary/vermin_group_pb";
import * as v1_dictionary_vermin_pb from "../../v1/dictionary/vermin_pb";
import * as v1_dictionary_unit_pb from "../../v1/dictionary/unit_pb";
import * as v1_dictionary_technology_crop_processing_pb from "../../v1/dictionary/technology_crop_processing_pb";
import * as v1_dictionary_microelement_pb from "../../v1/dictionary/microelement_pb";
import * as v1_dictionary_microelement_for_product_pb from "../../v1/dictionary/microelement_for_product_pb";
import * as v1_dictionary_hybrids_group_pb from "../../v1/dictionary/hybrids_group_pb";
import * as v1_dictionary_hybrids_pb from "../../v1/dictionary/hybrids_pb";
import * as v1_dictionary_hybrids_groups_for_culture_pb from "../../v1/dictionary/hybrids_groups_for_culture_pb";
import * as v1_dictionary_insect_row_pb from "../../v1/dictionary/insect_row_pb";
import * as v1_dictionary_insect_family_pb from "../../v1/dictionary/insect_family_pb";
import * as v1_dictionary_insect_info_pb from "../../v1/dictionary/insect_info_pb";
import * as v1_dictionary_vermin_for_crop_processing_pb from "../../v1/dictionary/vermin_for_crop_processing_pb";
import * as v1_dictionary_vermin_for_prep_pb from "../../v1/dictionary/vermin_for_prep_pb";
import * as v1_dictionary_vermin_for_active_substance_pb from "../../v1/dictionary/vermin_for_active_substance_pb";
import * as v1_dictionary_group_weeds_pb from "../../v1/dictionary/group_weeds_pb";
import * as v1_dictionary_group_weeds_for_weeds_pb from "../../v1/dictionary/group_weeds_for_weeds_pb";
import * as v1_dictionary_country_pd_pb from "../../v1/dictionary/country_pd_pb";
import * as v1_dictionary_culture_for_crop_processing_pb from "../../v1/dictionary/culture_for_crop_processing_pb";
import * as v1_dictionary_culture_grow_stage_pb from "../../v1/dictionary/culture_grow_stage_pb";
import * as v1_dictionary_family_weeds_pb from "../../v1/dictionary/family_weeds_pb";
import * as v1_dictionary_features_pb from "../../v1/dictionary/features_pb";
import * as v1_dictionary_features_for_culture_pb from "../../v1/dictionary/features_for_culture_pb";
import * as v1_dictionary_features_for_hybrid_pb from "../../v1/dictionary/features_for_hybrid_pb";
import * as v1_dictionary_features_values_pb from "../../v1/dictionary/features_values_pb";
import * as v1_dictionary_insect_type_pb from "../../v1/dictionary/insect_type_pb";
import * as v1_dictionary_insect_type_for_insect_pb from "../../v1/dictionary/insect_type_for_insect_pb";
import * as v1_dictionary_manufacturer_pb from "../../v1/dictionary/manufacturer_pb";
import * as v1_dictionary_perform_methods_pb from "../../v1/dictionary/perform_methods_pb";
import * as v1_dictionary_preparative_form_pb from "../../v1/dictionary/preparative_form_pb";
import * as v1_dictionary_product_class_pb from "../../v1/dictionary/product_class_pb";
import * as v1_dictionary_product_group_pb from "../../v1/dictionary/product_group_pb";
import * as v1_dictionary_product_in_grow_stage_pb from "../../v1/dictionary/product_in_grow_stage_pb";
import * as v1_dictionary_recomended_zone_pb from "../../v1/dictionary/recomended_zone_pb";
import * as v1_dictionary_zone_weed_pb from "../../v1/dictionary/zone_weed_pb";
import * as v1_dictionary_zone_for_hybrids_pb from "../../v1/dictionary/zone_for_hybrids_pb";
import * as v1_dictionary_zone_weed_for_weed_pb from "../../v1/dictionary/zone_weed_for_weed_pb";
import * as v1_dictionary_type_of_pathogen_pb from "../../v1/dictionary/type_of_pathogen_pb";
import * as v1_dictionary_vermin_description_pb from "../../v1/dictionary/vermin_description_pb";
import * as v1_dictionary_weeds_info_pb from "../../v1/dictionary/weeds_info_pb";
import * as v1_dictionary_product_kind_pb from "../../v1/dictionary/product_kind_pb";
import * as v1_dictionary_product_type_pb from "../../v1/dictionary/product_type_pb";
import * as v1_dictionary_product_type_to_kind_pb from "../../v1/dictionary/product_type_to_kind_pb";
import * as v1_dictionary_product_sub_group_pb from "../../v1/dictionary/product_sub_group_pb";
import * as v1_dictionary_product_group_to_kind_pb from "../../v1/dictionary/product_group_to_kind_pb";
import * as v1_dictionary_product_group_to_sub_group_pb from "../../v1/dictionary/product_group_to_sub_group_pb";
import {grpc} from "@improbable-eng/grpc-web";

type DictionaryServiceGetCulturesGroup = {
  readonly methodName: string;
  readonly service: typeof DictionaryService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_dictionary_cultures_group_pb.GetCulturesGroupRequest;
  readonly responseType: typeof v1_dictionary_cultures_group_pb.GetCulturesGroupResponse;
};

type DictionaryServiceListCulturesGroup = {
  readonly methodName: string;
  readonly service: typeof DictionaryService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_dictionary_cultures_group_pb.ListCulturesGroupRequest;
  readonly responseType: typeof v1_dictionary_cultures_group_pb.ListCulturesGroupResponse;
};

type DictionaryServiceGetCulture = {
  readonly methodName: string;
  readonly service: typeof DictionaryService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_dictionary_culture_pb.GetCultureRequest;
  readonly responseType: typeof v1_dictionary_culture_pb.GetCultureResponse;
};

type DictionaryServiceListCulture = {
  readonly methodName: string;
  readonly service: typeof DictionaryService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_dictionary_culture_pb.ListCultureRequest;
  readonly responseType: typeof v1_dictionary_culture_pb.ListCultureResponse;
};

type DictionaryServiceListCulturePlantProtection = {
  readonly methodName: string;
  readonly service: typeof DictionaryService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_dictionary_culture_pb.ListCultureRequest;
  readonly responseType: typeof v1_dictionary_culture_pb.ListCultureResponse;
};

type DictionaryServiceGetProduct = {
  readonly methodName: string;
  readonly service: typeof DictionaryService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_dictionary_product_pb.GetProductRequest;
  readonly responseType: typeof v1_dictionary_product_pb.GetProductResponse;
};

type DictionaryServiceListProduct = {
  readonly methodName: string;
  readonly service: typeof DictionaryService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_dictionary_product_pb.ListProductRequest;
  readonly responseType: typeof v1_dictionary_product_pb.ListProductResponse;
};

type DictionaryServiceListProductOfPlantProtectionByCulture = {
  readonly methodName: string;
  readonly service: typeof DictionaryService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_dictionary_product_pb.ListProductOfPlantProtectionByCultureRequest;
  readonly responseType: typeof v1_dictionary_product_pb.ListProductResponse;
};

type DictionaryServiceListProductAlternative = {
  readonly methodName: string;
  readonly service: typeof DictionaryService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_dictionary_product_pb.ListProductAlternativeRequest;
  readonly responseType: typeof v1_dictionary_product_pb.ListProductAlternativeResponse;
};

type DictionaryServiceGetBrand = {
  readonly methodName: string;
  readonly service: typeof DictionaryService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_dictionary_brand_pb.GetBrandRequest;
  readonly responseType: typeof v1_dictionary_brand_pb.GetBrandResponse;
};

type DictionaryServiceListBrand = {
  readonly methodName: string;
  readonly service: typeof DictionaryService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_dictionary_brand_pb.ListBrandRequest;
  readonly responseType: typeof v1_dictionary_brand_pb.ListBrandResponse;
};

type DictionaryServiceGetPackaging = {
  readonly methodName: string;
  readonly service: typeof DictionaryService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_dictionary_packaging_pb.GetPackagingRequest;
  readonly responseType: typeof v1_dictionary_packaging_pb.GetPackagingResponse;
};

type DictionaryServiceListPackaging = {
  readonly methodName: string;
  readonly service: typeof DictionaryService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_dictionary_packaging_pb.ListPackagingRequest;
  readonly responseType: typeof v1_dictionary_packaging_pb.ListPackagingResponse;
};

type DictionaryServiceGetPackagingForProduct = {
  readonly methodName: string;
  readonly service: typeof DictionaryService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_dictionary_packaging_for_product_pb.GetPackagingForProductRequest;
  readonly responseType: typeof v1_dictionary_packaging_for_product_pb.GetPackagingForProductResponse;
};

type DictionaryServiceListPackagingForProduct = {
  readonly methodName: string;
  readonly service: typeof DictionaryService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_dictionary_packaging_for_product_pb.ListPackagingForProductRequest;
  readonly responseType: typeof v1_dictionary_packaging_for_product_pb.ListPackagingForProductResponse;
};

type DictionaryServiceGetActiveSubstance = {
  readonly methodName: string;
  readonly service: typeof DictionaryService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_dictionary_active_substance_pb.GetActiveSubstanceRequest;
  readonly responseType: typeof v1_dictionary_active_substance_pb.GetActiveSubstanceResponse;
};

type DictionaryServiceListActiveSubstance = {
  readonly methodName: string;
  readonly service: typeof DictionaryService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_dictionary_active_substance_pb.ListActiveSubstanceRequest;
  readonly responseType: typeof v1_dictionary_active_substance_pb.ListActiveSubstanceResponse;
};

type DictionaryServiceGetActiveSubstanceForProduct = {
  readonly methodName: string;
  readonly service: typeof DictionaryService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_dictionary_active_substance_for_product_pb.GetActiveSubstanceForProductRequest;
  readonly responseType: typeof v1_dictionary_active_substance_for_product_pb.GetActiveSubstanceForProductResponse;
};

type DictionaryServiceListActiveSubstanceForProduct = {
  readonly methodName: string;
  readonly service: typeof DictionaryService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_dictionary_active_substance_for_product_pb.ListActiveSubstanceForProductRequest;
  readonly responseType: typeof v1_dictionary_active_substance_for_product_pb.ListActiveSubstanceForProductResponse;
};

type DictionaryServiceGetVerminGroup = {
  readonly methodName: string;
  readonly service: typeof DictionaryService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_dictionary_vermin_group_pb.GetVerminGroupRequest;
  readonly responseType: typeof v1_dictionary_vermin_group_pb.GetVerminGroupResponse;
};

type DictionaryServiceListVerminGroup = {
  readonly methodName: string;
  readonly service: typeof DictionaryService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_dictionary_vermin_group_pb.ListVerminGroupRequest;
  readonly responseType: typeof v1_dictionary_vermin_group_pb.ListVerminGroupResponse;
};

type DictionaryServiceGetVermin = {
  readonly methodName: string;
  readonly service: typeof DictionaryService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_dictionary_vermin_pb.GetVerminRequest;
  readonly responseType: typeof v1_dictionary_vermin_pb.GetVerminResponse;
};

type DictionaryServiceListVermin = {
  readonly methodName: string;
  readonly service: typeof DictionaryService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_dictionary_vermin_pb.ListVerminRequest;
  readonly responseType: typeof v1_dictionary_vermin_pb.ListVerminResponse;
};

type DictionaryServiceGetUnit = {
  readonly methodName: string;
  readonly service: typeof DictionaryService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_dictionary_unit_pb.GetUnitRequest;
  readonly responseType: typeof v1_dictionary_unit_pb.GetUnitResponse;
};

type DictionaryServiceListUnit = {
  readonly methodName: string;
  readonly service: typeof DictionaryService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_dictionary_unit_pb.ListUnitRequest;
  readonly responseType: typeof v1_dictionary_unit_pb.ListUnitResponse;
};

type DictionaryServiceGetTechnologyCropProcessing = {
  readonly methodName: string;
  readonly service: typeof DictionaryService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_dictionary_technology_crop_processing_pb.GetTechnologyCropProcessingRequest;
  readonly responseType: typeof v1_dictionary_technology_crop_processing_pb.GetTechnologyCropProcessingResponse;
};

type DictionaryServiceListTechnologyCropProcessing = {
  readonly methodName: string;
  readonly service: typeof DictionaryService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_dictionary_technology_crop_processing_pb.ListTechnologyCropProcessingRequest;
  readonly responseType: typeof v1_dictionary_technology_crop_processing_pb.ListTechnologyCropProcessingResponse;
};

type DictionaryServiceGetMicroelement = {
  readonly methodName: string;
  readonly service: typeof DictionaryService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_dictionary_microelement_pb.GetMicroelementRequest;
  readonly responseType: typeof v1_dictionary_microelement_pb.GetMicroelementResponse;
};

type DictionaryServiceListMicroelement = {
  readonly methodName: string;
  readonly service: typeof DictionaryService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_dictionary_microelement_pb.ListMicroelementRequest;
  readonly responseType: typeof v1_dictionary_microelement_pb.ListMicroelementResponse;
};

type DictionaryServiceGetMicroelementForProduct = {
  readonly methodName: string;
  readonly service: typeof DictionaryService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_dictionary_microelement_for_product_pb.GetMicroelementForProductRequest;
  readonly responseType: typeof v1_dictionary_microelement_for_product_pb.GetMicroelementForProductResponse;
};

type DictionaryServiceListMicroelementForProduct = {
  readonly methodName: string;
  readonly service: typeof DictionaryService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_dictionary_microelement_for_product_pb.ListMicroelementForProductRequest;
  readonly responseType: typeof v1_dictionary_microelement_for_product_pb.ListMicroelementForProductResponse;
};

type DictionaryServiceGetHybridsGroup = {
  readonly methodName: string;
  readonly service: typeof DictionaryService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_dictionary_hybrids_group_pb.GetHybridsGroupRequest;
  readonly responseType: typeof v1_dictionary_hybrids_group_pb.GetHybridsGroupResponse;
};

type DictionaryServiceListHybridsGroup = {
  readonly methodName: string;
  readonly service: typeof DictionaryService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_dictionary_hybrids_group_pb.ListHybridsGroupRequest;
  readonly responseType: typeof v1_dictionary_hybrids_group_pb.ListHybridsGroupResponse;
};

type DictionaryServiceGetHybrids = {
  readonly methodName: string;
  readonly service: typeof DictionaryService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_dictionary_hybrids_pb.GetHybridsRequest;
  readonly responseType: typeof v1_dictionary_hybrids_pb.GetHybridsResponse;
};

type DictionaryServiceListHybrids = {
  readonly methodName: string;
  readonly service: typeof DictionaryService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_dictionary_hybrids_pb.ListHybridsRequest;
  readonly responseType: typeof v1_dictionary_hybrids_pb.ListHybridsResponse;
};

type DictionaryServiceGetHybridsGroupsForCulture = {
  readonly methodName: string;
  readonly service: typeof DictionaryService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_dictionary_hybrids_groups_for_culture_pb.GetHybridsGroupsForCultureRequest;
  readonly responseType: typeof v1_dictionary_hybrids_groups_for_culture_pb.GetHybridsGroupsForCultureResponse;
};

type DictionaryServiceListHybridsGroupsForCulture = {
  readonly methodName: string;
  readonly service: typeof DictionaryService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_dictionary_hybrids_groups_for_culture_pb.ListHybridsGroupsForCultureRequest;
  readonly responseType: typeof v1_dictionary_hybrids_groups_for_culture_pb.ListHybridsGroupsForCultureResponse;
};

type DictionaryServiceGetInsectRow = {
  readonly methodName: string;
  readonly service: typeof DictionaryService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_dictionary_insect_row_pb.GetInsectRowRequest;
  readonly responseType: typeof v1_dictionary_insect_row_pb.GetInsectRowResponse;
};

type DictionaryServiceListInsectRow = {
  readonly methodName: string;
  readonly service: typeof DictionaryService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_dictionary_insect_row_pb.ListInsectRowRequest;
  readonly responseType: typeof v1_dictionary_insect_row_pb.ListInsectRowResponse;
};

type DictionaryServiceGetInsectFamily = {
  readonly methodName: string;
  readonly service: typeof DictionaryService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_dictionary_insect_family_pb.GetInsectFamilyRequest;
  readonly responseType: typeof v1_dictionary_insect_family_pb.GetInsectFamilyResponse;
};

type DictionaryServiceListInsectFamily = {
  readonly methodName: string;
  readonly service: typeof DictionaryService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_dictionary_insect_family_pb.ListInsectFamilyRequest;
  readonly responseType: typeof v1_dictionary_insect_family_pb.ListInsectFamilyResponse;
};

type DictionaryServiceGetInsectInfo = {
  readonly methodName: string;
  readonly service: typeof DictionaryService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_dictionary_insect_info_pb.GetInsectInfoRequest;
  readonly responseType: typeof v1_dictionary_insect_info_pb.GetInsectInfoResponse;
};

type DictionaryServiceListInsectInfo = {
  readonly methodName: string;
  readonly service: typeof DictionaryService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_dictionary_insect_info_pb.ListInsectInfoRequest;
  readonly responseType: typeof v1_dictionary_insect_info_pb.ListInsectInfoResponse;
};

type DictionaryServiceGetVerminForCropProcessing = {
  readonly methodName: string;
  readonly service: typeof DictionaryService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_dictionary_vermin_for_crop_processing_pb.GetVerminForCropProcessingRequest;
  readonly responseType: typeof v1_dictionary_vermin_for_crop_processing_pb.GetVerminForCropProcessingResponse;
};

type DictionaryServiceListVerminForCropProcessing = {
  readonly methodName: string;
  readonly service: typeof DictionaryService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_dictionary_vermin_for_crop_processing_pb.ListVerminForCropProcessingRequest;
  readonly responseType: typeof v1_dictionary_vermin_for_crop_processing_pb.ListVerminForCropProcessingResponse;
};

type DictionaryServiceGetVerminForPrep = {
  readonly methodName: string;
  readonly service: typeof DictionaryService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_dictionary_vermin_for_prep_pb.GetVerminForPrepRequest;
  readonly responseType: typeof v1_dictionary_vermin_for_prep_pb.GetVerminForPrepResponse;
};

type DictionaryServiceListVerminForPrep = {
  readonly methodName: string;
  readonly service: typeof DictionaryService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_dictionary_vermin_for_prep_pb.ListVerminForPrepRequest;
  readonly responseType: typeof v1_dictionary_vermin_for_prep_pb.ListVerminForPrepResponse;
};

type DictionaryServiceGetVerminForActiveSubstance = {
  readonly methodName: string;
  readonly service: typeof DictionaryService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_dictionary_vermin_for_active_substance_pb.GetVerminForActiveSubstanceRequest;
  readonly responseType: typeof v1_dictionary_vermin_for_active_substance_pb.GetVerminForActiveSubstanceResponse;
};

type DictionaryServiceListVerminForActiveSubstance = {
  readonly methodName: string;
  readonly service: typeof DictionaryService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_dictionary_vermin_for_active_substance_pb.ListVerminForActiveSubstanceRequest;
  readonly responseType: typeof v1_dictionary_vermin_for_active_substance_pb.ListVerminForActiveSubstanceResponse;
};

type DictionaryServiceGetGroupWeeds = {
  readonly methodName: string;
  readonly service: typeof DictionaryService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_dictionary_group_weeds_pb.GetGroupWeedsRequest;
  readonly responseType: typeof v1_dictionary_group_weeds_pb.GetGroupWeedsResponse;
};

type DictionaryServiceListGroupWeeds = {
  readonly methodName: string;
  readonly service: typeof DictionaryService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_dictionary_group_weeds_pb.ListGroupWeedsRequest;
  readonly responseType: typeof v1_dictionary_group_weeds_pb.ListGroupWeedsResponse;
};

type DictionaryServiceGetGroupWeedsForWeeds = {
  readonly methodName: string;
  readonly service: typeof DictionaryService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_dictionary_group_weeds_for_weeds_pb.GetGroupWeedsForWeedsRequest;
  readonly responseType: typeof v1_dictionary_group_weeds_for_weeds_pb.GetGroupWeedsForWeedsResponse;
};

type DictionaryServiceListGroupWeedsForWeeds = {
  readonly methodName: string;
  readonly service: typeof DictionaryService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_dictionary_group_weeds_for_weeds_pb.ListGroupWeedsForWeedsRequest;
  readonly responseType: typeof v1_dictionary_group_weeds_for_weeds_pb.ListGroupWeedsForWeedsResponse;
};

type DictionaryServiceGetCountryPd = {
  readonly methodName: string;
  readonly service: typeof DictionaryService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_dictionary_country_pd_pb.GetCountryPdRequest;
  readonly responseType: typeof v1_dictionary_country_pd_pb.GetCountryPdResponse;
};

type DictionaryServiceListCountryPd = {
  readonly methodName: string;
  readonly service: typeof DictionaryService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_dictionary_country_pd_pb.ListCountryPdRequest;
  readonly responseType: typeof v1_dictionary_country_pd_pb.ListCountryPdResponse;
};

type DictionaryServiceGetCultureForCropProcessing = {
  readonly methodName: string;
  readonly service: typeof DictionaryService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_dictionary_culture_for_crop_processing_pb.GetCultureForCropProcessingRequest;
  readonly responseType: typeof v1_dictionary_culture_for_crop_processing_pb.GetCultureForCropProcessingResponse;
};

type DictionaryServiceListCultureForCropProcessing = {
  readonly methodName: string;
  readonly service: typeof DictionaryService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_dictionary_culture_for_crop_processing_pb.ListCultureForCropProcessingRequest;
  readonly responseType: typeof v1_dictionary_culture_for_crop_processing_pb.ListCultureForCropProcessingResponse;
};

type DictionaryServiceGetCultureGrowStage = {
  readonly methodName: string;
  readonly service: typeof DictionaryService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_dictionary_culture_grow_stage_pb.GetCultureGrowStageRequest;
  readonly responseType: typeof v1_dictionary_culture_grow_stage_pb.GetCultureGrowStageResponse;
};

type DictionaryServiceListCultureGrowStage = {
  readonly methodName: string;
  readonly service: typeof DictionaryService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_dictionary_culture_grow_stage_pb.ListCultureGrowStageRequest;
  readonly responseType: typeof v1_dictionary_culture_grow_stage_pb.ListCultureGrowStageResponse;
};

type DictionaryServiceGetFamilyWeeds = {
  readonly methodName: string;
  readonly service: typeof DictionaryService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_dictionary_family_weeds_pb.GetFamilyWeedsRequest;
  readonly responseType: typeof v1_dictionary_family_weeds_pb.GetFamilyWeedsResponse;
};

type DictionaryServiceListFamilyWeeds = {
  readonly methodName: string;
  readonly service: typeof DictionaryService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_dictionary_family_weeds_pb.ListFamilyWeedsRequest;
  readonly responseType: typeof v1_dictionary_family_weeds_pb.ListFamilyWeedsResponse;
};

type DictionaryServiceGetFeatures = {
  readonly methodName: string;
  readonly service: typeof DictionaryService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_dictionary_features_pb.GetFeaturesRequest;
  readonly responseType: typeof v1_dictionary_features_pb.GetFeaturesResponse;
};

type DictionaryServiceListFeatures = {
  readonly methodName: string;
  readonly service: typeof DictionaryService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_dictionary_features_pb.ListFeaturesRequest;
  readonly responseType: typeof v1_dictionary_features_pb.ListFeaturesResponse;
};

type DictionaryServiceGetFeaturesForCulture = {
  readonly methodName: string;
  readonly service: typeof DictionaryService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_dictionary_features_for_culture_pb.GetFeaturesForCultureRequest;
  readonly responseType: typeof v1_dictionary_features_for_culture_pb.GetFeaturesForCultureResponse;
};

type DictionaryServiceListFeaturesForCulture = {
  readonly methodName: string;
  readonly service: typeof DictionaryService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_dictionary_features_for_culture_pb.ListFeaturesForCultureRequest;
  readonly responseType: typeof v1_dictionary_features_for_culture_pb.ListFeaturesForCultureResponse;
};

type DictionaryServiceGetFeaturesForHybrid = {
  readonly methodName: string;
  readonly service: typeof DictionaryService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_dictionary_features_for_hybrid_pb.GetFeaturesForHybridRequest;
  readonly responseType: typeof v1_dictionary_features_for_hybrid_pb.GetFeaturesForHybridResponse;
};

type DictionaryServiceListFeaturesForHybrid = {
  readonly methodName: string;
  readonly service: typeof DictionaryService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_dictionary_features_for_hybrid_pb.ListFeaturesForHybridRequest;
  readonly responseType: typeof v1_dictionary_features_for_hybrid_pb.ListFeaturesForHybridResponse;
};

type DictionaryServiceGetFeaturesValues = {
  readonly methodName: string;
  readonly service: typeof DictionaryService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_dictionary_features_values_pb.GetFeaturesValuesRequest;
  readonly responseType: typeof v1_dictionary_features_values_pb.GetFeaturesValuesResponse;
};

type DictionaryServiceListFeaturesValues = {
  readonly methodName: string;
  readonly service: typeof DictionaryService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_dictionary_features_values_pb.ListFeaturesValuesRequest;
  readonly responseType: typeof v1_dictionary_features_values_pb.ListFeaturesValuesResponse;
};

type DictionaryServiceGetInsectType = {
  readonly methodName: string;
  readonly service: typeof DictionaryService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_dictionary_insect_type_pb.GetInsectTypeRequest;
  readonly responseType: typeof v1_dictionary_insect_type_pb.GetInsectTypeResponse;
};

type DictionaryServiceListInsectType = {
  readonly methodName: string;
  readonly service: typeof DictionaryService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_dictionary_insect_type_pb.ListInsectTypeRequest;
  readonly responseType: typeof v1_dictionary_insect_type_pb.ListInsectTypeResponse;
};

type DictionaryServiceGetInsectTypeForInsect = {
  readonly methodName: string;
  readonly service: typeof DictionaryService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_dictionary_insect_type_for_insect_pb.GetInsectTypeForInsectRequest;
  readonly responseType: typeof v1_dictionary_insect_type_for_insect_pb.GetInsectTypeForInsectResponse;
};

type DictionaryServiceListInsectTypeForInsect = {
  readonly methodName: string;
  readonly service: typeof DictionaryService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_dictionary_insect_type_for_insect_pb.ListInsectTypeForInsectRequest;
  readonly responseType: typeof v1_dictionary_insect_type_for_insect_pb.ListInsectTypeForInsectResponse;
};

type DictionaryServiceGetManufacturer = {
  readonly methodName: string;
  readonly service: typeof DictionaryService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_dictionary_manufacturer_pb.GetManufacturerRequest;
  readonly responseType: typeof v1_dictionary_manufacturer_pb.GetManufacturerResponse;
};

type DictionaryServiceListManufacturer = {
  readonly methodName: string;
  readonly service: typeof DictionaryService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_dictionary_manufacturer_pb.ListManufacturerRequest;
  readonly responseType: typeof v1_dictionary_manufacturer_pb.ListManufacturerResponse;
};

type DictionaryServiceGetPerformMethods = {
  readonly methodName: string;
  readonly service: typeof DictionaryService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_dictionary_perform_methods_pb.GetPerformMethodsRequest;
  readonly responseType: typeof v1_dictionary_perform_methods_pb.GetPerformMethodsResponse;
};

type DictionaryServiceListPerformMethods = {
  readonly methodName: string;
  readonly service: typeof DictionaryService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_dictionary_perform_methods_pb.ListPerformMethodsRequest;
  readonly responseType: typeof v1_dictionary_perform_methods_pb.ListPerformMethodsResponse;
};

type DictionaryServiceGetPreparativeForm = {
  readonly methodName: string;
  readonly service: typeof DictionaryService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_dictionary_preparative_form_pb.GetPreparativeFormRequest;
  readonly responseType: typeof v1_dictionary_preparative_form_pb.GetPreparativeFormResponse;
};

type DictionaryServiceListPreparativeForm = {
  readonly methodName: string;
  readonly service: typeof DictionaryService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_dictionary_preparative_form_pb.ListPreparativeFormRequest;
  readonly responseType: typeof v1_dictionary_preparative_form_pb.ListPreparativeFormResponse;
};

type DictionaryServiceGetProductClass = {
  readonly methodName: string;
  readonly service: typeof DictionaryService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_dictionary_product_class_pb.GetProductClassRequest;
  readonly responseType: typeof v1_dictionary_product_class_pb.GetProductClassResponse;
};

type DictionaryServiceListProductClass = {
  readonly methodName: string;
  readonly service: typeof DictionaryService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_dictionary_product_class_pb.ListProductClassRequest;
  readonly responseType: typeof v1_dictionary_product_class_pb.ListProductClassResponse;
};

type DictionaryServiceGetProductGroup = {
  readonly methodName: string;
  readonly service: typeof DictionaryService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_dictionary_product_group_pb.GetProductGroupRequest;
  readonly responseType: typeof v1_dictionary_product_group_pb.GetProductGroupResponse;
};

type DictionaryServiceListProductGroup = {
  readonly methodName: string;
  readonly service: typeof DictionaryService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_dictionary_product_group_pb.ListProductGroupRequest;
  readonly responseType: typeof v1_dictionary_product_group_pb.ListProductGroupResponse;
};

type DictionaryServiceGetProductInGrowStage = {
  readonly methodName: string;
  readonly service: typeof DictionaryService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_dictionary_product_in_grow_stage_pb.GetProductInGrowStageRequest;
  readonly responseType: typeof v1_dictionary_product_in_grow_stage_pb.GetProductInGrowStageResponse;
};

type DictionaryServiceListProductInGrowStage = {
  readonly methodName: string;
  readonly service: typeof DictionaryService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_dictionary_product_in_grow_stage_pb.ListProductInGrowStageRequest;
  readonly responseType: typeof v1_dictionary_product_in_grow_stage_pb.ListProductInGrowStageResponse;
};

type DictionaryServiceGetRecomendedZone = {
  readonly methodName: string;
  readonly service: typeof DictionaryService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_dictionary_recomended_zone_pb.GetRecomendedZoneRequest;
  readonly responseType: typeof v1_dictionary_recomended_zone_pb.GetRecomendedZoneResponse;
};

type DictionaryServiceListRecomendedZone = {
  readonly methodName: string;
  readonly service: typeof DictionaryService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_dictionary_recomended_zone_pb.ListRecomendedZoneRequest;
  readonly responseType: typeof v1_dictionary_recomended_zone_pb.ListRecomendedZoneResponse;
};

type DictionaryServiceGetZoneWeed = {
  readonly methodName: string;
  readonly service: typeof DictionaryService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_dictionary_zone_weed_pb.GetZoneWeedRequest;
  readonly responseType: typeof v1_dictionary_zone_weed_pb.GetZoneWeedResponse;
};

type DictionaryServiceListZoneWeed = {
  readonly methodName: string;
  readonly service: typeof DictionaryService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_dictionary_zone_weed_pb.ListZoneWeedRequest;
  readonly responseType: typeof v1_dictionary_zone_weed_pb.ListZoneWeedResponse;
};

type DictionaryServiceGetZoneForHybrids = {
  readonly methodName: string;
  readonly service: typeof DictionaryService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_dictionary_zone_for_hybrids_pb.GetZoneForHybridsRequest;
  readonly responseType: typeof v1_dictionary_zone_for_hybrids_pb.GetZoneForHybridsResponse;
};

type DictionaryServiceListZoneForHybrids = {
  readonly methodName: string;
  readonly service: typeof DictionaryService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_dictionary_zone_for_hybrids_pb.ListZoneForHybridsRequest;
  readonly responseType: typeof v1_dictionary_zone_for_hybrids_pb.ListZoneForHybridsResponse;
};

type DictionaryServiceGetZoneWeedforWeed = {
  readonly methodName: string;
  readonly service: typeof DictionaryService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_dictionary_zone_weed_for_weed_pb.GetZoneWeedforWeedRequest;
  readonly responseType: typeof v1_dictionary_zone_weed_for_weed_pb.GetZoneWeedforWeedResponse;
};

type DictionaryServiceListZoneWeedforWeed = {
  readonly methodName: string;
  readonly service: typeof DictionaryService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_dictionary_zone_weed_for_weed_pb.ListZoneWeedforWeedRequest;
  readonly responseType: typeof v1_dictionary_zone_weed_for_weed_pb.ListZoneWeedforWeedResponse;
};

type DictionaryServiceGetTypeOfPathogen = {
  readonly methodName: string;
  readonly service: typeof DictionaryService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_dictionary_type_of_pathogen_pb.GetTypeOfPathogenRequest;
  readonly responseType: typeof v1_dictionary_type_of_pathogen_pb.GetTypeOfPathogenResponse;
};

type DictionaryServiceListTypeOfPathogen = {
  readonly methodName: string;
  readonly service: typeof DictionaryService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_dictionary_type_of_pathogen_pb.ListTypeOfPathogenRequest;
  readonly responseType: typeof v1_dictionary_type_of_pathogen_pb.ListTypeOfPathogenResponse;
};

type DictionaryServiceGetVerminDescription = {
  readonly methodName: string;
  readonly service: typeof DictionaryService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_dictionary_vermin_description_pb.GetVerminDescriptionRequest;
  readonly responseType: typeof v1_dictionary_vermin_description_pb.GetVerminDescriptionResponse;
};

type DictionaryServiceListVerminDescription = {
  readonly methodName: string;
  readonly service: typeof DictionaryService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_dictionary_vermin_description_pb.ListVerminDescriptionRequest;
  readonly responseType: typeof v1_dictionary_vermin_description_pb.ListVerminDescriptionResponse;
};

type DictionaryServiceGetWeedsInfo = {
  readonly methodName: string;
  readonly service: typeof DictionaryService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_dictionary_weeds_info_pb.GetWeedsInfoRequest;
  readonly responseType: typeof v1_dictionary_weeds_info_pb.GetWeedsInfoResponse;
};

type DictionaryServiceListWeedsInfo = {
  readonly methodName: string;
  readonly service: typeof DictionaryService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_dictionary_weeds_info_pb.ListWeedsInfoRequest;
  readonly responseType: typeof v1_dictionary_weeds_info_pb.ListWeedsInfoResponse;
};

type DictionaryServiceGetProductKind = {
  readonly methodName: string;
  readonly service: typeof DictionaryService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_dictionary_product_kind_pb.GetProductKindRequest;
  readonly responseType: typeof v1_dictionary_product_kind_pb.GetProductKindResponse;
};

type DictionaryServiceListProductKind = {
  readonly methodName: string;
  readonly service: typeof DictionaryService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_dictionary_product_kind_pb.ListProductKindRequest;
  readonly responseType: typeof v1_dictionary_product_kind_pb.ListProductKindResponse;
};

type DictionaryServiceGetProductType = {
  readonly methodName: string;
  readonly service: typeof DictionaryService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_dictionary_product_type_pb.GetProductTypeRequest;
  readonly responseType: typeof v1_dictionary_product_type_pb.GetProductTypeResponse;
};

type DictionaryServiceListProductType = {
  readonly methodName: string;
  readonly service: typeof DictionaryService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_dictionary_product_type_pb.ListProductTypeRequest;
  readonly responseType: typeof v1_dictionary_product_type_pb.ListProductTypeResponse;
};

type DictionaryServiceGetProductTypeToKind = {
  readonly methodName: string;
  readonly service: typeof DictionaryService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_dictionary_product_type_to_kind_pb.GetProductTypeToKindRequest;
  readonly responseType: typeof v1_dictionary_product_type_to_kind_pb.GetProductTypeToKindResponse;
};

type DictionaryServiceListProductTypeToKind = {
  readonly methodName: string;
  readonly service: typeof DictionaryService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_dictionary_product_type_to_kind_pb.ListProductTypeToKindRequest;
  readonly responseType: typeof v1_dictionary_product_type_to_kind_pb.ListProductTypeToKindResponse;
};

type DictionaryServiceGetProductSubGroup = {
  readonly methodName: string;
  readonly service: typeof DictionaryService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_dictionary_product_sub_group_pb.GetProductSubGroupRequest;
  readonly responseType: typeof v1_dictionary_product_sub_group_pb.GetProductSubGroupResponse;
};

type DictionaryServiceListProductSubGroup = {
  readonly methodName: string;
  readonly service: typeof DictionaryService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_dictionary_product_sub_group_pb.ListProductSubGroupRequest;
  readonly responseType: typeof v1_dictionary_product_sub_group_pb.ListProductSubGroupResponse;
};

type DictionaryServiceGetProductGroupToKind = {
  readonly methodName: string;
  readonly service: typeof DictionaryService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_dictionary_product_group_to_kind_pb.GetProductGroupToKindRequest;
  readonly responseType: typeof v1_dictionary_product_group_to_kind_pb.GetProductGroupToKindResponse;
};

type DictionaryServiceListProductGroupToKind = {
  readonly methodName: string;
  readonly service: typeof DictionaryService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_dictionary_product_group_to_kind_pb.ListProductGroupToKindRequest;
  readonly responseType: typeof v1_dictionary_product_group_to_kind_pb.ListProductGroupToKindResponse;
};

type DictionaryServiceGetProductGroupToSubGroup = {
  readonly methodName: string;
  readonly service: typeof DictionaryService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_dictionary_product_group_to_sub_group_pb.GetProductGroupToSubGroupRequest;
  readonly responseType: typeof v1_dictionary_product_group_to_sub_group_pb.GetProductGroupToSubGroupResponse;
};

type DictionaryServiceListProductGroupToSubGroup = {
  readonly methodName: string;
  readonly service: typeof DictionaryService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_dictionary_product_group_to_sub_group_pb.ListProductGroupToSubGroupRequest;
  readonly responseType: typeof v1_dictionary_product_group_to_sub_group_pb.ListProductGroupToSubGroupResponse;
};

export class DictionaryService {
  static readonly serviceName: string;
  static readonly GetCulturesGroup: DictionaryServiceGetCulturesGroup;
  static readonly ListCulturesGroup: DictionaryServiceListCulturesGroup;
  static readonly GetCulture: DictionaryServiceGetCulture;
  static readonly ListCulture: DictionaryServiceListCulture;
  static readonly ListCulturePlantProtection: DictionaryServiceListCulturePlantProtection;
  static readonly GetProduct: DictionaryServiceGetProduct;
  static readonly ListProduct: DictionaryServiceListProduct;
  static readonly ListProductOfPlantProtectionByCulture: DictionaryServiceListProductOfPlantProtectionByCulture;
  static readonly ListProductAlternative: DictionaryServiceListProductAlternative;
  static readonly GetBrand: DictionaryServiceGetBrand;
  static readonly ListBrand: DictionaryServiceListBrand;
  static readonly GetPackaging: DictionaryServiceGetPackaging;
  static readonly ListPackaging: DictionaryServiceListPackaging;
  static readonly GetPackagingForProduct: DictionaryServiceGetPackagingForProduct;
  static readonly ListPackagingForProduct: DictionaryServiceListPackagingForProduct;
  static readonly GetActiveSubstance: DictionaryServiceGetActiveSubstance;
  static readonly ListActiveSubstance: DictionaryServiceListActiveSubstance;
  static readonly GetActiveSubstanceForProduct: DictionaryServiceGetActiveSubstanceForProduct;
  static readonly ListActiveSubstanceForProduct: DictionaryServiceListActiveSubstanceForProduct;
  static readonly GetVerminGroup: DictionaryServiceGetVerminGroup;
  static readonly ListVerminGroup: DictionaryServiceListVerminGroup;
  static readonly GetVermin: DictionaryServiceGetVermin;
  static readonly ListVermin: DictionaryServiceListVermin;
  static readonly GetUnit: DictionaryServiceGetUnit;
  static readonly ListUnit: DictionaryServiceListUnit;
  static readonly GetTechnologyCropProcessing: DictionaryServiceGetTechnologyCropProcessing;
  static readonly ListTechnologyCropProcessing: DictionaryServiceListTechnologyCropProcessing;
  static readonly GetMicroelement: DictionaryServiceGetMicroelement;
  static readonly ListMicroelement: DictionaryServiceListMicroelement;
  static readonly GetMicroelementForProduct: DictionaryServiceGetMicroelementForProduct;
  static readonly ListMicroelementForProduct: DictionaryServiceListMicroelementForProduct;
  static readonly GetHybridsGroup: DictionaryServiceGetHybridsGroup;
  static readonly ListHybridsGroup: DictionaryServiceListHybridsGroup;
  static readonly GetHybrids: DictionaryServiceGetHybrids;
  static readonly ListHybrids: DictionaryServiceListHybrids;
  static readonly GetHybridsGroupsForCulture: DictionaryServiceGetHybridsGroupsForCulture;
  static readonly ListHybridsGroupsForCulture: DictionaryServiceListHybridsGroupsForCulture;
  static readonly GetInsectRow: DictionaryServiceGetInsectRow;
  static readonly ListInsectRow: DictionaryServiceListInsectRow;
  static readonly GetInsectFamily: DictionaryServiceGetInsectFamily;
  static readonly ListInsectFamily: DictionaryServiceListInsectFamily;
  static readonly GetInsectInfo: DictionaryServiceGetInsectInfo;
  static readonly ListInsectInfo: DictionaryServiceListInsectInfo;
  static readonly GetVerminForCropProcessing: DictionaryServiceGetVerminForCropProcessing;
  static readonly ListVerminForCropProcessing: DictionaryServiceListVerminForCropProcessing;
  static readonly GetVerminForPrep: DictionaryServiceGetVerminForPrep;
  static readonly ListVerminForPrep: DictionaryServiceListVerminForPrep;
  static readonly GetVerminForActiveSubstance: DictionaryServiceGetVerminForActiveSubstance;
  static readonly ListVerminForActiveSubstance: DictionaryServiceListVerminForActiveSubstance;
  static readonly GetGroupWeeds: DictionaryServiceGetGroupWeeds;
  static readonly ListGroupWeeds: DictionaryServiceListGroupWeeds;
  static readonly GetGroupWeedsForWeeds: DictionaryServiceGetGroupWeedsForWeeds;
  static readonly ListGroupWeedsForWeeds: DictionaryServiceListGroupWeedsForWeeds;
  static readonly GetCountryPd: DictionaryServiceGetCountryPd;
  static readonly ListCountryPd: DictionaryServiceListCountryPd;
  static readonly GetCultureForCropProcessing: DictionaryServiceGetCultureForCropProcessing;
  static readonly ListCultureForCropProcessing: DictionaryServiceListCultureForCropProcessing;
  static readonly GetCultureGrowStage: DictionaryServiceGetCultureGrowStage;
  static readonly ListCultureGrowStage: DictionaryServiceListCultureGrowStage;
  static readonly GetFamilyWeeds: DictionaryServiceGetFamilyWeeds;
  static readonly ListFamilyWeeds: DictionaryServiceListFamilyWeeds;
  static readonly GetFeatures: DictionaryServiceGetFeatures;
  static readonly ListFeatures: DictionaryServiceListFeatures;
  static readonly GetFeaturesForCulture: DictionaryServiceGetFeaturesForCulture;
  static readonly ListFeaturesForCulture: DictionaryServiceListFeaturesForCulture;
  static readonly GetFeaturesForHybrid: DictionaryServiceGetFeaturesForHybrid;
  static readonly ListFeaturesForHybrid: DictionaryServiceListFeaturesForHybrid;
  static readonly GetFeaturesValues: DictionaryServiceGetFeaturesValues;
  static readonly ListFeaturesValues: DictionaryServiceListFeaturesValues;
  static readonly GetInsectType: DictionaryServiceGetInsectType;
  static readonly ListInsectType: DictionaryServiceListInsectType;
  static readonly GetInsectTypeForInsect: DictionaryServiceGetInsectTypeForInsect;
  static readonly ListInsectTypeForInsect: DictionaryServiceListInsectTypeForInsect;
  static readonly GetManufacturer: DictionaryServiceGetManufacturer;
  static readonly ListManufacturer: DictionaryServiceListManufacturer;
  static readonly GetPerformMethods: DictionaryServiceGetPerformMethods;
  static readonly ListPerformMethods: DictionaryServiceListPerformMethods;
  static readonly GetPreparativeForm: DictionaryServiceGetPreparativeForm;
  static readonly ListPreparativeForm: DictionaryServiceListPreparativeForm;
  static readonly GetProductClass: DictionaryServiceGetProductClass;
  static readonly ListProductClass: DictionaryServiceListProductClass;
  static readonly GetProductGroup: DictionaryServiceGetProductGroup;
  static readonly ListProductGroup: DictionaryServiceListProductGroup;
  static readonly GetProductInGrowStage: DictionaryServiceGetProductInGrowStage;
  static readonly ListProductInGrowStage: DictionaryServiceListProductInGrowStage;
  static readonly GetRecomendedZone: DictionaryServiceGetRecomendedZone;
  static readonly ListRecomendedZone: DictionaryServiceListRecomendedZone;
  static readonly GetZoneWeed: DictionaryServiceGetZoneWeed;
  static readonly ListZoneWeed: DictionaryServiceListZoneWeed;
  static readonly GetZoneForHybrids: DictionaryServiceGetZoneForHybrids;
  static readonly ListZoneForHybrids: DictionaryServiceListZoneForHybrids;
  static readonly GetZoneWeedforWeed: DictionaryServiceGetZoneWeedforWeed;
  static readonly ListZoneWeedforWeed: DictionaryServiceListZoneWeedforWeed;
  static readonly GetTypeOfPathogen: DictionaryServiceGetTypeOfPathogen;
  static readonly ListTypeOfPathogen: DictionaryServiceListTypeOfPathogen;
  static readonly GetVerminDescription: DictionaryServiceGetVerminDescription;
  static readonly ListVerminDescription: DictionaryServiceListVerminDescription;
  static readonly GetWeedsInfo: DictionaryServiceGetWeedsInfo;
  static readonly ListWeedsInfo: DictionaryServiceListWeedsInfo;
  static readonly GetProductKind: DictionaryServiceGetProductKind;
  static readonly ListProductKind: DictionaryServiceListProductKind;
  static readonly GetProductType: DictionaryServiceGetProductType;
  static readonly ListProductType: DictionaryServiceListProductType;
  static readonly GetProductTypeToKind: DictionaryServiceGetProductTypeToKind;
  static readonly ListProductTypeToKind: DictionaryServiceListProductTypeToKind;
  static readonly GetProductSubGroup: DictionaryServiceGetProductSubGroup;
  static readonly ListProductSubGroup: DictionaryServiceListProductSubGroup;
  static readonly GetProductGroupToKind: DictionaryServiceGetProductGroupToKind;
  static readonly ListProductGroupToKind: DictionaryServiceListProductGroupToKind;
  static readonly GetProductGroupToSubGroup: DictionaryServiceGetProductGroupToSubGroup;
  static readonly ListProductGroupToSubGroup: DictionaryServiceListProductGroupToSubGroup;
}

export type ServiceError = { message: string, code: number; metadata: grpc.Metadata }
export type Status = { details: string, code: number; metadata: grpc.Metadata }

interface UnaryResponse {
  cancel(): void;
}
interface ResponseStream<T> {
  cancel(): void;
  on(type: 'data', handler: (message: T) => void): ResponseStream<T>;
  on(type: 'end', handler: (status?: Status) => void): ResponseStream<T>;
  on(type: 'status', handler: (status: Status) => void): ResponseStream<T>;
}
interface RequestStream<T> {
  write(message: T): RequestStream<T>;
  end(): void;
  cancel(): void;
  on(type: 'end', handler: (status?: Status) => void): RequestStream<T>;
  on(type: 'status', handler: (status: Status) => void): RequestStream<T>;
}
interface BidirectionalStream<ReqT, ResT> {
  write(message: ReqT): BidirectionalStream<ReqT, ResT>;
  end(): void;
  cancel(): void;
  on(type: 'data', handler: (message: ResT) => void): BidirectionalStream<ReqT, ResT>;
  on(type: 'end', handler: (status?: Status) => void): BidirectionalStream<ReqT, ResT>;
  on(type: 'status', handler: (status: Status) => void): BidirectionalStream<ReqT, ResT>;
}

export class DictionaryServiceClient {
  readonly serviceHost: string;

  constructor(serviceHost: string, options?: grpc.RpcOptions);
  getCulturesGroup(
    requestMessage: v1_dictionary_cultures_group_pb.GetCulturesGroupRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_cultures_group_pb.GetCulturesGroupResponse|null) => void
  ): UnaryResponse;
  getCulturesGroup(
    requestMessage: v1_dictionary_cultures_group_pb.GetCulturesGroupRequest,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_cultures_group_pb.GetCulturesGroupResponse|null) => void
  ): UnaryResponse;
  listCulturesGroup(
    requestMessage: v1_dictionary_cultures_group_pb.ListCulturesGroupRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_cultures_group_pb.ListCulturesGroupResponse|null) => void
  ): UnaryResponse;
  listCulturesGroup(
    requestMessage: v1_dictionary_cultures_group_pb.ListCulturesGroupRequest,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_cultures_group_pb.ListCulturesGroupResponse|null) => void
  ): UnaryResponse;
  getCulture(
    requestMessage: v1_dictionary_culture_pb.GetCultureRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_culture_pb.GetCultureResponse|null) => void
  ): UnaryResponse;
  getCulture(
    requestMessage: v1_dictionary_culture_pb.GetCultureRequest,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_culture_pb.GetCultureResponse|null) => void
  ): UnaryResponse;
  listCulture(
    requestMessage: v1_dictionary_culture_pb.ListCultureRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_culture_pb.ListCultureResponse|null) => void
  ): UnaryResponse;
  listCulture(
    requestMessage: v1_dictionary_culture_pb.ListCultureRequest,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_culture_pb.ListCultureResponse|null) => void
  ): UnaryResponse;
  listCulturePlantProtection(
    requestMessage: v1_dictionary_culture_pb.ListCultureRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_culture_pb.ListCultureResponse|null) => void
  ): UnaryResponse;
  listCulturePlantProtection(
    requestMessage: v1_dictionary_culture_pb.ListCultureRequest,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_culture_pb.ListCultureResponse|null) => void
  ): UnaryResponse;
  getProduct(
    requestMessage: v1_dictionary_product_pb.GetProductRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_product_pb.GetProductResponse|null) => void
  ): UnaryResponse;
  getProduct(
    requestMessage: v1_dictionary_product_pb.GetProductRequest,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_product_pb.GetProductResponse|null) => void
  ): UnaryResponse;
  listProduct(
    requestMessage: v1_dictionary_product_pb.ListProductRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_product_pb.ListProductResponse|null) => void
  ): UnaryResponse;
  listProduct(
    requestMessage: v1_dictionary_product_pb.ListProductRequest,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_product_pb.ListProductResponse|null) => void
  ): UnaryResponse;
  listProductOfPlantProtectionByCulture(
    requestMessage: v1_dictionary_product_pb.ListProductOfPlantProtectionByCultureRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_product_pb.ListProductResponse|null) => void
  ): UnaryResponse;
  listProductOfPlantProtectionByCulture(
    requestMessage: v1_dictionary_product_pb.ListProductOfPlantProtectionByCultureRequest,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_product_pb.ListProductResponse|null) => void
  ): UnaryResponse;
  listProductAlternative(
    requestMessage: v1_dictionary_product_pb.ListProductAlternativeRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_product_pb.ListProductAlternativeResponse|null) => void
  ): UnaryResponse;
  listProductAlternative(
    requestMessage: v1_dictionary_product_pb.ListProductAlternativeRequest,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_product_pb.ListProductAlternativeResponse|null) => void
  ): UnaryResponse;
  getBrand(
    requestMessage: v1_dictionary_brand_pb.GetBrandRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_brand_pb.GetBrandResponse|null) => void
  ): UnaryResponse;
  getBrand(
    requestMessage: v1_dictionary_brand_pb.GetBrandRequest,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_brand_pb.GetBrandResponse|null) => void
  ): UnaryResponse;
  listBrand(
    requestMessage: v1_dictionary_brand_pb.ListBrandRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_brand_pb.ListBrandResponse|null) => void
  ): UnaryResponse;
  listBrand(
    requestMessage: v1_dictionary_brand_pb.ListBrandRequest,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_brand_pb.ListBrandResponse|null) => void
  ): UnaryResponse;
  getPackaging(
    requestMessage: v1_dictionary_packaging_pb.GetPackagingRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_packaging_pb.GetPackagingResponse|null) => void
  ): UnaryResponse;
  getPackaging(
    requestMessage: v1_dictionary_packaging_pb.GetPackagingRequest,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_packaging_pb.GetPackagingResponse|null) => void
  ): UnaryResponse;
  listPackaging(
    requestMessage: v1_dictionary_packaging_pb.ListPackagingRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_packaging_pb.ListPackagingResponse|null) => void
  ): UnaryResponse;
  listPackaging(
    requestMessage: v1_dictionary_packaging_pb.ListPackagingRequest,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_packaging_pb.ListPackagingResponse|null) => void
  ): UnaryResponse;
  getPackagingForProduct(
    requestMessage: v1_dictionary_packaging_for_product_pb.GetPackagingForProductRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_packaging_for_product_pb.GetPackagingForProductResponse|null) => void
  ): UnaryResponse;
  getPackagingForProduct(
    requestMessage: v1_dictionary_packaging_for_product_pb.GetPackagingForProductRequest,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_packaging_for_product_pb.GetPackagingForProductResponse|null) => void
  ): UnaryResponse;
  listPackagingForProduct(
    requestMessage: v1_dictionary_packaging_for_product_pb.ListPackagingForProductRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_packaging_for_product_pb.ListPackagingForProductResponse|null) => void
  ): UnaryResponse;
  listPackagingForProduct(
    requestMessage: v1_dictionary_packaging_for_product_pb.ListPackagingForProductRequest,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_packaging_for_product_pb.ListPackagingForProductResponse|null) => void
  ): UnaryResponse;
  getActiveSubstance(
    requestMessage: v1_dictionary_active_substance_pb.GetActiveSubstanceRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_active_substance_pb.GetActiveSubstanceResponse|null) => void
  ): UnaryResponse;
  getActiveSubstance(
    requestMessage: v1_dictionary_active_substance_pb.GetActiveSubstanceRequest,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_active_substance_pb.GetActiveSubstanceResponse|null) => void
  ): UnaryResponse;
  listActiveSubstance(
    requestMessage: v1_dictionary_active_substance_pb.ListActiveSubstanceRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_active_substance_pb.ListActiveSubstanceResponse|null) => void
  ): UnaryResponse;
  listActiveSubstance(
    requestMessage: v1_dictionary_active_substance_pb.ListActiveSubstanceRequest,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_active_substance_pb.ListActiveSubstanceResponse|null) => void
  ): UnaryResponse;
  getActiveSubstanceForProduct(
    requestMessage: v1_dictionary_active_substance_for_product_pb.GetActiveSubstanceForProductRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_active_substance_for_product_pb.GetActiveSubstanceForProductResponse|null) => void
  ): UnaryResponse;
  getActiveSubstanceForProduct(
    requestMessage: v1_dictionary_active_substance_for_product_pb.GetActiveSubstanceForProductRequest,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_active_substance_for_product_pb.GetActiveSubstanceForProductResponse|null) => void
  ): UnaryResponse;
  listActiveSubstanceForProduct(
    requestMessage: v1_dictionary_active_substance_for_product_pb.ListActiveSubstanceForProductRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_active_substance_for_product_pb.ListActiveSubstanceForProductResponse|null) => void
  ): UnaryResponse;
  listActiveSubstanceForProduct(
    requestMessage: v1_dictionary_active_substance_for_product_pb.ListActiveSubstanceForProductRequest,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_active_substance_for_product_pb.ListActiveSubstanceForProductResponse|null) => void
  ): UnaryResponse;
  getVerminGroup(
    requestMessage: v1_dictionary_vermin_group_pb.GetVerminGroupRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_vermin_group_pb.GetVerminGroupResponse|null) => void
  ): UnaryResponse;
  getVerminGroup(
    requestMessage: v1_dictionary_vermin_group_pb.GetVerminGroupRequest,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_vermin_group_pb.GetVerminGroupResponse|null) => void
  ): UnaryResponse;
  listVerminGroup(
    requestMessage: v1_dictionary_vermin_group_pb.ListVerminGroupRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_vermin_group_pb.ListVerminGroupResponse|null) => void
  ): UnaryResponse;
  listVerminGroup(
    requestMessage: v1_dictionary_vermin_group_pb.ListVerminGroupRequest,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_vermin_group_pb.ListVerminGroupResponse|null) => void
  ): UnaryResponse;
  getVermin(
    requestMessage: v1_dictionary_vermin_pb.GetVerminRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_vermin_pb.GetVerminResponse|null) => void
  ): UnaryResponse;
  getVermin(
    requestMessage: v1_dictionary_vermin_pb.GetVerminRequest,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_vermin_pb.GetVerminResponse|null) => void
  ): UnaryResponse;
  listVermin(
    requestMessage: v1_dictionary_vermin_pb.ListVerminRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_vermin_pb.ListVerminResponse|null) => void
  ): UnaryResponse;
  listVermin(
    requestMessage: v1_dictionary_vermin_pb.ListVerminRequest,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_vermin_pb.ListVerminResponse|null) => void
  ): UnaryResponse;
  getUnit(
    requestMessage: v1_dictionary_unit_pb.GetUnitRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_unit_pb.GetUnitResponse|null) => void
  ): UnaryResponse;
  getUnit(
    requestMessage: v1_dictionary_unit_pb.GetUnitRequest,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_unit_pb.GetUnitResponse|null) => void
  ): UnaryResponse;
  listUnit(
    requestMessage: v1_dictionary_unit_pb.ListUnitRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_unit_pb.ListUnitResponse|null) => void
  ): UnaryResponse;
  listUnit(
    requestMessage: v1_dictionary_unit_pb.ListUnitRequest,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_unit_pb.ListUnitResponse|null) => void
  ): UnaryResponse;
  getTechnologyCropProcessing(
    requestMessage: v1_dictionary_technology_crop_processing_pb.GetTechnologyCropProcessingRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_technology_crop_processing_pb.GetTechnologyCropProcessingResponse|null) => void
  ): UnaryResponse;
  getTechnologyCropProcessing(
    requestMessage: v1_dictionary_technology_crop_processing_pb.GetTechnologyCropProcessingRequest,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_technology_crop_processing_pb.GetTechnologyCropProcessingResponse|null) => void
  ): UnaryResponse;
  listTechnologyCropProcessing(
    requestMessage: v1_dictionary_technology_crop_processing_pb.ListTechnologyCropProcessingRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_technology_crop_processing_pb.ListTechnologyCropProcessingResponse|null) => void
  ): UnaryResponse;
  listTechnologyCropProcessing(
    requestMessage: v1_dictionary_technology_crop_processing_pb.ListTechnologyCropProcessingRequest,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_technology_crop_processing_pb.ListTechnologyCropProcessingResponse|null) => void
  ): UnaryResponse;
  getMicroelement(
    requestMessage: v1_dictionary_microelement_pb.GetMicroelementRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_microelement_pb.GetMicroelementResponse|null) => void
  ): UnaryResponse;
  getMicroelement(
    requestMessage: v1_dictionary_microelement_pb.GetMicroelementRequest,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_microelement_pb.GetMicroelementResponse|null) => void
  ): UnaryResponse;
  listMicroelement(
    requestMessage: v1_dictionary_microelement_pb.ListMicroelementRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_microelement_pb.ListMicroelementResponse|null) => void
  ): UnaryResponse;
  listMicroelement(
    requestMessage: v1_dictionary_microelement_pb.ListMicroelementRequest,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_microelement_pb.ListMicroelementResponse|null) => void
  ): UnaryResponse;
  getMicroelementForProduct(
    requestMessage: v1_dictionary_microelement_for_product_pb.GetMicroelementForProductRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_microelement_for_product_pb.GetMicroelementForProductResponse|null) => void
  ): UnaryResponse;
  getMicroelementForProduct(
    requestMessage: v1_dictionary_microelement_for_product_pb.GetMicroelementForProductRequest,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_microelement_for_product_pb.GetMicroelementForProductResponse|null) => void
  ): UnaryResponse;
  listMicroelementForProduct(
    requestMessage: v1_dictionary_microelement_for_product_pb.ListMicroelementForProductRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_microelement_for_product_pb.ListMicroelementForProductResponse|null) => void
  ): UnaryResponse;
  listMicroelementForProduct(
    requestMessage: v1_dictionary_microelement_for_product_pb.ListMicroelementForProductRequest,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_microelement_for_product_pb.ListMicroelementForProductResponse|null) => void
  ): UnaryResponse;
  getHybridsGroup(
    requestMessage: v1_dictionary_hybrids_group_pb.GetHybridsGroupRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_hybrids_group_pb.GetHybridsGroupResponse|null) => void
  ): UnaryResponse;
  getHybridsGroup(
    requestMessage: v1_dictionary_hybrids_group_pb.GetHybridsGroupRequest,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_hybrids_group_pb.GetHybridsGroupResponse|null) => void
  ): UnaryResponse;
  listHybridsGroup(
    requestMessage: v1_dictionary_hybrids_group_pb.ListHybridsGroupRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_hybrids_group_pb.ListHybridsGroupResponse|null) => void
  ): UnaryResponse;
  listHybridsGroup(
    requestMessage: v1_dictionary_hybrids_group_pb.ListHybridsGroupRequest,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_hybrids_group_pb.ListHybridsGroupResponse|null) => void
  ): UnaryResponse;
  getHybrids(
    requestMessage: v1_dictionary_hybrids_pb.GetHybridsRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_hybrids_pb.GetHybridsResponse|null) => void
  ): UnaryResponse;
  getHybrids(
    requestMessage: v1_dictionary_hybrids_pb.GetHybridsRequest,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_hybrids_pb.GetHybridsResponse|null) => void
  ): UnaryResponse;
  listHybrids(
    requestMessage: v1_dictionary_hybrids_pb.ListHybridsRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_hybrids_pb.ListHybridsResponse|null) => void
  ): UnaryResponse;
  listHybrids(
    requestMessage: v1_dictionary_hybrids_pb.ListHybridsRequest,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_hybrids_pb.ListHybridsResponse|null) => void
  ): UnaryResponse;
  getHybridsGroupsForCulture(
    requestMessage: v1_dictionary_hybrids_groups_for_culture_pb.GetHybridsGroupsForCultureRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_hybrids_groups_for_culture_pb.GetHybridsGroupsForCultureResponse|null) => void
  ): UnaryResponse;
  getHybridsGroupsForCulture(
    requestMessage: v1_dictionary_hybrids_groups_for_culture_pb.GetHybridsGroupsForCultureRequest,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_hybrids_groups_for_culture_pb.GetHybridsGroupsForCultureResponse|null) => void
  ): UnaryResponse;
  listHybridsGroupsForCulture(
    requestMessage: v1_dictionary_hybrids_groups_for_culture_pb.ListHybridsGroupsForCultureRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_hybrids_groups_for_culture_pb.ListHybridsGroupsForCultureResponse|null) => void
  ): UnaryResponse;
  listHybridsGroupsForCulture(
    requestMessage: v1_dictionary_hybrids_groups_for_culture_pb.ListHybridsGroupsForCultureRequest,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_hybrids_groups_for_culture_pb.ListHybridsGroupsForCultureResponse|null) => void
  ): UnaryResponse;
  getInsectRow(
    requestMessage: v1_dictionary_insect_row_pb.GetInsectRowRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_insect_row_pb.GetInsectRowResponse|null) => void
  ): UnaryResponse;
  getInsectRow(
    requestMessage: v1_dictionary_insect_row_pb.GetInsectRowRequest,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_insect_row_pb.GetInsectRowResponse|null) => void
  ): UnaryResponse;
  listInsectRow(
    requestMessage: v1_dictionary_insect_row_pb.ListInsectRowRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_insect_row_pb.ListInsectRowResponse|null) => void
  ): UnaryResponse;
  listInsectRow(
    requestMessage: v1_dictionary_insect_row_pb.ListInsectRowRequest,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_insect_row_pb.ListInsectRowResponse|null) => void
  ): UnaryResponse;
  getInsectFamily(
    requestMessage: v1_dictionary_insect_family_pb.GetInsectFamilyRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_insect_family_pb.GetInsectFamilyResponse|null) => void
  ): UnaryResponse;
  getInsectFamily(
    requestMessage: v1_dictionary_insect_family_pb.GetInsectFamilyRequest,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_insect_family_pb.GetInsectFamilyResponse|null) => void
  ): UnaryResponse;
  listInsectFamily(
    requestMessage: v1_dictionary_insect_family_pb.ListInsectFamilyRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_insect_family_pb.ListInsectFamilyResponse|null) => void
  ): UnaryResponse;
  listInsectFamily(
    requestMessage: v1_dictionary_insect_family_pb.ListInsectFamilyRequest,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_insect_family_pb.ListInsectFamilyResponse|null) => void
  ): UnaryResponse;
  getInsectInfo(
    requestMessage: v1_dictionary_insect_info_pb.GetInsectInfoRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_insect_info_pb.GetInsectInfoResponse|null) => void
  ): UnaryResponse;
  getInsectInfo(
    requestMessage: v1_dictionary_insect_info_pb.GetInsectInfoRequest,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_insect_info_pb.GetInsectInfoResponse|null) => void
  ): UnaryResponse;
  listInsectInfo(
    requestMessage: v1_dictionary_insect_info_pb.ListInsectInfoRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_insect_info_pb.ListInsectInfoResponse|null) => void
  ): UnaryResponse;
  listInsectInfo(
    requestMessage: v1_dictionary_insect_info_pb.ListInsectInfoRequest,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_insect_info_pb.ListInsectInfoResponse|null) => void
  ): UnaryResponse;
  getVerminForCropProcessing(
    requestMessage: v1_dictionary_vermin_for_crop_processing_pb.GetVerminForCropProcessingRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_vermin_for_crop_processing_pb.GetVerminForCropProcessingResponse|null) => void
  ): UnaryResponse;
  getVerminForCropProcessing(
    requestMessage: v1_dictionary_vermin_for_crop_processing_pb.GetVerminForCropProcessingRequest,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_vermin_for_crop_processing_pb.GetVerminForCropProcessingResponse|null) => void
  ): UnaryResponse;
  listVerminForCropProcessing(
    requestMessage: v1_dictionary_vermin_for_crop_processing_pb.ListVerminForCropProcessingRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_vermin_for_crop_processing_pb.ListVerminForCropProcessingResponse|null) => void
  ): UnaryResponse;
  listVerminForCropProcessing(
    requestMessage: v1_dictionary_vermin_for_crop_processing_pb.ListVerminForCropProcessingRequest,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_vermin_for_crop_processing_pb.ListVerminForCropProcessingResponse|null) => void
  ): UnaryResponse;
  getVerminForPrep(
    requestMessage: v1_dictionary_vermin_for_prep_pb.GetVerminForPrepRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_vermin_for_prep_pb.GetVerminForPrepResponse|null) => void
  ): UnaryResponse;
  getVerminForPrep(
    requestMessage: v1_dictionary_vermin_for_prep_pb.GetVerminForPrepRequest,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_vermin_for_prep_pb.GetVerminForPrepResponse|null) => void
  ): UnaryResponse;
  listVerminForPrep(
    requestMessage: v1_dictionary_vermin_for_prep_pb.ListVerminForPrepRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_vermin_for_prep_pb.ListVerminForPrepResponse|null) => void
  ): UnaryResponse;
  listVerminForPrep(
    requestMessage: v1_dictionary_vermin_for_prep_pb.ListVerminForPrepRequest,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_vermin_for_prep_pb.ListVerminForPrepResponse|null) => void
  ): UnaryResponse;
  getVerminForActiveSubstance(
    requestMessage: v1_dictionary_vermin_for_active_substance_pb.GetVerminForActiveSubstanceRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_vermin_for_active_substance_pb.GetVerminForActiveSubstanceResponse|null) => void
  ): UnaryResponse;
  getVerminForActiveSubstance(
    requestMessage: v1_dictionary_vermin_for_active_substance_pb.GetVerminForActiveSubstanceRequest,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_vermin_for_active_substance_pb.GetVerminForActiveSubstanceResponse|null) => void
  ): UnaryResponse;
  listVerminForActiveSubstance(
    requestMessage: v1_dictionary_vermin_for_active_substance_pb.ListVerminForActiveSubstanceRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_vermin_for_active_substance_pb.ListVerminForActiveSubstanceResponse|null) => void
  ): UnaryResponse;
  listVerminForActiveSubstance(
    requestMessage: v1_dictionary_vermin_for_active_substance_pb.ListVerminForActiveSubstanceRequest,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_vermin_for_active_substance_pb.ListVerminForActiveSubstanceResponse|null) => void
  ): UnaryResponse;
  getGroupWeeds(
    requestMessage: v1_dictionary_group_weeds_pb.GetGroupWeedsRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_group_weeds_pb.GetGroupWeedsResponse|null) => void
  ): UnaryResponse;
  getGroupWeeds(
    requestMessage: v1_dictionary_group_weeds_pb.GetGroupWeedsRequest,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_group_weeds_pb.GetGroupWeedsResponse|null) => void
  ): UnaryResponse;
  listGroupWeeds(
    requestMessage: v1_dictionary_group_weeds_pb.ListGroupWeedsRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_group_weeds_pb.ListGroupWeedsResponse|null) => void
  ): UnaryResponse;
  listGroupWeeds(
    requestMessage: v1_dictionary_group_weeds_pb.ListGroupWeedsRequest,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_group_weeds_pb.ListGroupWeedsResponse|null) => void
  ): UnaryResponse;
  getGroupWeedsForWeeds(
    requestMessage: v1_dictionary_group_weeds_for_weeds_pb.GetGroupWeedsForWeedsRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_group_weeds_for_weeds_pb.GetGroupWeedsForWeedsResponse|null) => void
  ): UnaryResponse;
  getGroupWeedsForWeeds(
    requestMessage: v1_dictionary_group_weeds_for_weeds_pb.GetGroupWeedsForWeedsRequest,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_group_weeds_for_weeds_pb.GetGroupWeedsForWeedsResponse|null) => void
  ): UnaryResponse;
  listGroupWeedsForWeeds(
    requestMessage: v1_dictionary_group_weeds_for_weeds_pb.ListGroupWeedsForWeedsRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_group_weeds_for_weeds_pb.ListGroupWeedsForWeedsResponse|null) => void
  ): UnaryResponse;
  listGroupWeedsForWeeds(
    requestMessage: v1_dictionary_group_weeds_for_weeds_pb.ListGroupWeedsForWeedsRequest,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_group_weeds_for_weeds_pb.ListGroupWeedsForWeedsResponse|null) => void
  ): UnaryResponse;
  getCountryPd(
    requestMessage: v1_dictionary_country_pd_pb.GetCountryPdRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_country_pd_pb.GetCountryPdResponse|null) => void
  ): UnaryResponse;
  getCountryPd(
    requestMessage: v1_dictionary_country_pd_pb.GetCountryPdRequest,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_country_pd_pb.GetCountryPdResponse|null) => void
  ): UnaryResponse;
  listCountryPd(
    requestMessage: v1_dictionary_country_pd_pb.ListCountryPdRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_country_pd_pb.ListCountryPdResponse|null) => void
  ): UnaryResponse;
  listCountryPd(
    requestMessage: v1_dictionary_country_pd_pb.ListCountryPdRequest,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_country_pd_pb.ListCountryPdResponse|null) => void
  ): UnaryResponse;
  getCultureForCropProcessing(
    requestMessage: v1_dictionary_culture_for_crop_processing_pb.GetCultureForCropProcessingRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_culture_for_crop_processing_pb.GetCultureForCropProcessingResponse|null) => void
  ): UnaryResponse;
  getCultureForCropProcessing(
    requestMessage: v1_dictionary_culture_for_crop_processing_pb.GetCultureForCropProcessingRequest,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_culture_for_crop_processing_pb.GetCultureForCropProcessingResponse|null) => void
  ): UnaryResponse;
  listCultureForCropProcessing(
    requestMessage: v1_dictionary_culture_for_crop_processing_pb.ListCultureForCropProcessingRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_culture_for_crop_processing_pb.ListCultureForCropProcessingResponse|null) => void
  ): UnaryResponse;
  listCultureForCropProcessing(
    requestMessage: v1_dictionary_culture_for_crop_processing_pb.ListCultureForCropProcessingRequest,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_culture_for_crop_processing_pb.ListCultureForCropProcessingResponse|null) => void
  ): UnaryResponse;
  getCultureGrowStage(
    requestMessage: v1_dictionary_culture_grow_stage_pb.GetCultureGrowStageRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_culture_grow_stage_pb.GetCultureGrowStageResponse|null) => void
  ): UnaryResponse;
  getCultureGrowStage(
    requestMessage: v1_dictionary_culture_grow_stage_pb.GetCultureGrowStageRequest,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_culture_grow_stage_pb.GetCultureGrowStageResponse|null) => void
  ): UnaryResponse;
  listCultureGrowStage(
    requestMessage: v1_dictionary_culture_grow_stage_pb.ListCultureGrowStageRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_culture_grow_stage_pb.ListCultureGrowStageResponse|null) => void
  ): UnaryResponse;
  listCultureGrowStage(
    requestMessage: v1_dictionary_culture_grow_stage_pb.ListCultureGrowStageRequest,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_culture_grow_stage_pb.ListCultureGrowStageResponse|null) => void
  ): UnaryResponse;
  getFamilyWeeds(
    requestMessage: v1_dictionary_family_weeds_pb.GetFamilyWeedsRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_family_weeds_pb.GetFamilyWeedsResponse|null) => void
  ): UnaryResponse;
  getFamilyWeeds(
    requestMessage: v1_dictionary_family_weeds_pb.GetFamilyWeedsRequest,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_family_weeds_pb.GetFamilyWeedsResponse|null) => void
  ): UnaryResponse;
  listFamilyWeeds(
    requestMessage: v1_dictionary_family_weeds_pb.ListFamilyWeedsRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_family_weeds_pb.ListFamilyWeedsResponse|null) => void
  ): UnaryResponse;
  listFamilyWeeds(
    requestMessage: v1_dictionary_family_weeds_pb.ListFamilyWeedsRequest,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_family_weeds_pb.ListFamilyWeedsResponse|null) => void
  ): UnaryResponse;
  getFeatures(
    requestMessage: v1_dictionary_features_pb.GetFeaturesRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_features_pb.GetFeaturesResponse|null) => void
  ): UnaryResponse;
  getFeatures(
    requestMessage: v1_dictionary_features_pb.GetFeaturesRequest,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_features_pb.GetFeaturesResponse|null) => void
  ): UnaryResponse;
  listFeatures(
    requestMessage: v1_dictionary_features_pb.ListFeaturesRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_features_pb.ListFeaturesResponse|null) => void
  ): UnaryResponse;
  listFeatures(
    requestMessage: v1_dictionary_features_pb.ListFeaturesRequest,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_features_pb.ListFeaturesResponse|null) => void
  ): UnaryResponse;
  getFeaturesForCulture(
    requestMessage: v1_dictionary_features_for_culture_pb.GetFeaturesForCultureRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_features_for_culture_pb.GetFeaturesForCultureResponse|null) => void
  ): UnaryResponse;
  getFeaturesForCulture(
    requestMessage: v1_dictionary_features_for_culture_pb.GetFeaturesForCultureRequest,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_features_for_culture_pb.GetFeaturesForCultureResponse|null) => void
  ): UnaryResponse;
  listFeaturesForCulture(
    requestMessage: v1_dictionary_features_for_culture_pb.ListFeaturesForCultureRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_features_for_culture_pb.ListFeaturesForCultureResponse|null) => void
  ): UnaryResponse;
  listFeaturesForCulture(
    requestMessage: v1_dictionary_features_for_culture_pb.ListFeaturesForCultureRequest,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_features_for_culture_pb.ListFeaturesForCultureResponse|null) => void
  ): UnaryResponse;
  getFeaturesForHybrid(
    requestMessage: v1_dictionary_features_for_hybrid_pb.GetFeaturesForHybridRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_features_for_hybrid_pb.GetFeaturesForHybridResponse|null) => void
  ): UnaryResponse;
  getFeaturesForHybrid(
    requestMessage: v1_dictionary_features_for_hybrid_pb.GetFeaturesForHybridRequest,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_features_for_hybrid_pb.GetFeaturesForHybridResponse|null) => void
  ): UnaryResponse;
  listFeaturesForHybrid(
    requestMessage: v1_dictionary_features_for_hybrid_pb.ListFeaturesForHybridRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_features_for_hybrid_pb.ListFeaturesForHybridResponse|null) => void
  ): UnaryResponse;
  listFeaturesForHybrid(
    requestMessage: v1_dictionary_features_for_hybrid_pb.ListFeaturesForHybridRequest,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_features_for_hybrid_pb.ListFeaturesForHybridResponse|null) => void
  ): UnaryResponse;
  getFeaturesValues(
    requestMessage: v1_dictionary_features_values_pb.GetFeaturesValuesRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_features_values_pb.GetFeaturesValuesResponse|null) => void
  ): UnaryResponse;
  getFeaturesValues(
    requestMessage: v1_dictionary_features_values_pb.GetFeaturesValuesRequest,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_features_values_pb.GetFeaturesValuesResponse|null) => void
  ): UnaryResponse;
  listFeaturesValues(
    requestMessage: v1_dictionary_features_values_pb.ListFeaturesValuesRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_features_values_pb.ListFeaturesValuesResponse|null) => void
  ): UnaryResponse;
  listFeaturesValues(
    requestMessage: v1_dictionary_features_values_pb.ListFeaturesValuesRequest,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_features_values_pb.ListFeaturesValuesResponse|null) => void
  ): UnaryResponse;
  getInsectType(
    requestMessage: v1_dictionary_insect_type_pb.GetInsectTypeRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_insect_type_pb.GetInsectTypeResponse|null) => void
  ): UnaryResponse;
  getInsectType(
    requestMessage: v1_dictionary_insect_type_pb.GetInsectTypeRequest,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_insect_type_pb.GetInsectTypeResponse|null) => void
  ): UnaryResponse;
  listInsectType(
    requestMessage: v1_dictionary_insect_type_pb.ListInsectTypeRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_insect_type_pb.ListInsectTypeResponse|null) => void
  ): UnaryResponse;
  listInsectType(
    requestMessage: v1_dictionary_insect_type_pb.ListInsectTypeRequest,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_insect_type_pb.ListInsectTypeResponse|null) => void
  ): UnaryResponse;
  getInsectTypeForInsect(
    requestMessage: v1_dictionary_insect_type_for_insect_pb.GetInsectTypeForInsectRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_insect_type_for_insect_pb.GetInsectTypeForInsectResponse|null) => void
  ): UnaryResponse;
  getInsectTypeForInsect(
    requestMessage: v1_dictionary_insect_type_for_insect_pb.GetInsectTypeForInsectRequest,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_insect_type_for_insect_pb.GetInsectTypeForInsectResponse|null) => void
  ): UnaryResponse;
  listInsectTypeForInsect(
    requestMessage: v1_dictionary_insect_type_for_insect_pb.ListInsectTypeForInsectRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_insect_type_for_insect_pb.ListInsectTypeForInsectResponse|null) => void
  ): UnaryResponse;
  listInsectTypeForInsect(
    requestMessage: v1_dictionary_insect_type_for_insect_pb.ListInsectTypeForInsectRequest,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_insect_type_for_insect_pb.ListInsectTypeForInsectResponse|null) => void
  ): UnaryResponse;
  getManufacturer(
    requestMessage: v1_dictionary_manufacturer_pb.GetManufacturerRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_manufacturer_pb.GetManufacturerResponse|null) => void
  ): UnaryResponse;
  getManufacturer(
    requestMessage: v1_dictionary_manufacturer_pb.GetManufacturerRequest,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_manufacturer_pb.GetManufacturerResponse|null) => void
  ): UnaryResponse;
  listManufacturer(
    requestMessage: v1_dictionary_manufacturer_pb.ListManufacturerRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_manufacturer_pb.ListManufacturerResponse|null) => void
  ): UnaryResponse;
  listManufacturer(
    requestMessage: v1_dictionary_manufacturer_pb.ListManufacturerRequest,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_manufacturer_pb.ListManufacturerResponse|null) => void
  ): UnaryResponse;
  getPerformMethods(
    requestMessage: v1_dictionary_perform_methods_pb.GetPerformMethodsRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_perform_methods_pb.GetPerformMethodsResponse|null) => void
  ): UnaryResponse;
  getPerformMethods(
    requestMessage: v1_dictionary_perform_methods_pb.GetPerformMethodsRequest,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_perform_methods_pb.GetPerformMethodsResponse|null) => void
  ): UnaryResponse;
  listPerformMethods(
    requestMessage: v1_dictionary_perform_methods_pb.ListPerformMethodsRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_perform_methods_pb.ListPerformMethodsResponse|null) => void
  ): UnaryResponse;
  listPerformMethods(
    requestMessage: v1_dictionary_perform_methods_pb.ListPerformMethodsRequest,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_perform_methods_pb.ListPerformMethodsResponse|null) => void
  ): UnaryResponse;
  getPreparativeForm(
    requestMessage: v1_dictionary_preparative_form_pb.GetPreparativeFormRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_preparative_form_pb.GetPreparativeFormResponse|null) => void
  ): UnaryResponse;
  getPreparativeForm(
    requestMessage: v1_dictionary_preparative_form_pb.GetPreparativeFormRequest,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_preparative_form_pb.GetPreparativeFormResponse|null) => void
  ): UnaryResponse;
  listPreparativeForm(
    requestMessage: v1_dictionary_preparative_form_pb.ListPreparativeFormRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_preparative_form_pb.ListPreparativeFormResponse|null) => void
  ): UnaryResponse;
  listPreparativeForm(
    requestMessage: v1_dictionary_preparative_form_pb.ListPreparativeFormRequest,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_preparative_form_pb.ListPreparativeFormResponse|null) => void
  ): UnaryResponse;
  getProductClass(
    requestMessage: v1_dictionary_product_class_pb.GetProductClassRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_product_class_pb.GetProductClassResponse|null) => void
  ): UnaryResponse;
  getProductClass(
    requestMessage: v1_dictionary_product_class_pb.GetProductClassRequest,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_product_class_pb.GetProductClassResponse|null) => void
  ): UnaryResponse;
  listProductClass(
    requestMessage: v1_dictionary_product_class_pb.ListProductClassRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_product_class_pb.ListProductClassResponse|null) => void
  ): UnaryResponse;
  listProductClass(
    requestMessage: v1_dictionary_product_class_pb.ListProductClassRequest,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_product_class_pb.ListProductClassResponse|null) => void
  ): UnaryResponse;
  getProductGroup(
    requestMessage: v1_dictionary_product_group_pb.GetProductGroupRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_product_group_pb.GetProductGroupResponse|null) => void
  ): UnaryResponse;
  getProductGroup(
    requestMessage: v1_dictionary_product_group_pb.GetProductGroupRequest,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_product_group_pb.GetProductGroupResponse|null) => void
  ): UnaryResponse;
  listProductGroup(
    requestMessage: v1_dictionary_product_group_pb.ListProductGroupRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_product_group_pb.ListProductGroupResponse|null) => void
  ): UnaryResponse;
  listProductGroup(
    requestMessage: v1_dictionary_product_group_pb.ListProductGroupRequest,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_product_group_pb.ListProductGroupResponse|null) => void
  ): UnaryResponse;
  getProductInGrowStage(
    requestMessage: v1_dictionary_product_in_grow_stage_pb.GetProductInGrowStageRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_product_in_grow_stage_pb.GetProductInGrowStageResponse|null) => void
  ): UnaryResponse;
  getProductInGrowStage(
    requestMessage: v1_dictionary_product_in_grow_stage_pb.GetProductInGrowStageRequest,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_product_in_grow_stage_pb.GetProductInGrowStageResponse|null) => void
  ): UnaryResponse;
  listProductInGrowStage(
    requestMessage: v1_dictionary_product_in_grow_stage_pb.ListProductInGrowStageRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_product_in_grow_stage_pb.ListProductInGrowStageResponse|null) => void
  ): UnaryResponse;
  listProductInGrowStage(
    requestMessage: v1_dictionary_product_in_grow_stage_pb.ListProductInGrowStageRequest,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_product_in_grow_stage_pb.ListProductInGrowStageResponse|null) => void
  ): UnaryResponse;
  getRecomendedZone(
    requestMessage: v1_dictionary_recomended_zone_pb.GetRecomendedZoneRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_recomended_zone_pb.GetRecomendedZoneResponse|null) => void
  ): UnaryResponse;
  getRecomendedZone(
    requestMessage: v1_dictionary_recomended_zone_pb.GetRecomendedZoneRequest,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_recomended_zone_pb.GetRecomendedZoneResponse|null) => void
  ): UnaryResponse;
  listRecomendedZone(
    requestMessage: v1_dictionary_recomended_zone_pb.ListRecomendedZoneRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_recomended_zone_pb.ListRecomendedZoneResponse|null) => void
  ): UnaryResponse;
  listRecomendedZone(
    requestMessage: v1_dictionary_recomended_zone_pb.ListRecomendedZoneRequest,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_recomended_zone_pb.ListRecomendedZoneResponse|null) => void
  ): UnaryResponse;
  getZoneWeed(
    requestMessage: v1_dictionary_zone_weed_pb.GetZoneWeedRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_zone_weed_pb.GetZoneWeedResponse|null) => void
  ): UnaryResponse;
  getZoneWeed(
    requestMessage: v1_dictionary_zone_weed_pb.GetZoneWeedRequest,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_zone_weed_pb.GetZoneWeedResponse|null) => void
  ): UnaryResponse;
  listZoneWeed(
    requestMessage: v1_dictionary_zone_weed_pb.ListZoneWeedRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_zone_weed_pb.ListZoneWeedResponse|null) => void
  ): UnaryResponse;
  listZoneWeed(
    requestMessage: v1_dictionary_zone_weed_pb.ListZoneWeedRequest,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_zone_weed_pb.ListZoneWeedResponse|null) => void
  ): UnaryResponse;
  getZoneForHybrids(
    requestMessage: v1_dictionary_zone_for_hybrids_pb.GetZoneForHybridsRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_zone_for_hybrids_pb.GetZoneForHybridsResponse|null) => void
  ): UnaryResponse;
  getZoneForHybrids(
    requestMessage: v1_dictionary_zone_for_hybrids_pb.GetZoneForHybridsRequest,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_zone_for_hybrids_pb.GetZoneForHybridsResponse|null) => void
  ): UnaryResponse;
  listZoneForHybrids(
    requestMessage: v1_dictionary_zone_for_hybrids_pb.ListZoneForHybridsRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_zone_for_hybrids_pb.ListZoneForHybridsResponse|null) => void
  ): UnaryResponse;
  listZoneForHybrids(
    requestMessage: v1_dictionary_zone_for_hybrids_pb.ListZoneForHybridsRequest,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_zone_for_hybrids_pb.ListZoneForHybridsResponse|null) => void
  ): UnaryResponse;
  getZoneWeedforWeed(
    requestMessage: v1_dictionary_zone_weed_for_weed_pb.GetZoneWeedforWeedRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_zone_weed_for_weed_pb.GetZoneWeedforWeedResponse|null) => void
  ): UnaryResponse;
  getZoneWeedforWeed(
    requestMessage: v1_dictionary_zone_weed_for_weed_pb.GetZoneWeedforWeedRequest,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_zone_weed_for_weed_pb.GetZoneWeedforWeedResponse|null) => void
  ): UnaryResponse;
  listZoneWeedforWeed(
    requestMessage: v1_dictionary_zone_weed_for_weed_pb.ListZoneWeedforWeedRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_zone_weed_for_weed_pb.ListZoneWeedforWeedResponse|null) => void
  ): UnaryResponse;
  listZoneWeedforWeed(
    requestMessage: v1_dictionary_zone_weed_for_weed_pb.ListZoneWeedforWeedRequest,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_zone_weed_for_weed_pb.ListZoneWeedforWeedResponse|null) => void
  ): UnaryResponse;
  getTypeOfPathogen(
    requestMessage: v1_dictionary_type_of_pathogen_pb.GetTypeOfPathogenRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_type_of_pathogen_pb.GetTypeOfPathogenResponse|null) => void
  ): UnaryResponse;
  getTypeOfPathogen(
    requestMessage: v1_dictionary_type_of_pathogen_pb.GetTypeOfPathogenRequest,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_type_of_pathogen_pb.GetTypeOfPathogenResponse|null) => void
  ): UnaryResponse;
  listTypeOfPathogen(
    requestMessage: v1_dictionary_type_of_pathogen_pb.ListTypeOfPathogenRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_type_of_pathogen_pb.ListTypeOfPathogenResponse|null) => void
  ): UnaryResponse;
  listTypeOfPathogen(
    requestMessage: v1_dictionary_type_of_pathogen_pb.ListTypeOfPathogenRequest,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_type_of_pathogen_pb.ListTypeOfPathogenResponse|null) => void
  ): UnaryResponse;
  getVerminDescription(
    requestMessage: v1_dictionary_vermin_description_pb.GetVerminDescriptionRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_vermin_description_pb.GetVerminDescriptionResponse|null) => void
  ): UnaryResponse;
  getVerminDescription(
    requestMessage: v1_dictionary_vermin_description_pb.GetVerminDescriptionRequest,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_vermin_description_pb.GetVerminDescriptionResponse|null) => void
  ): UnaryResponse;
  listVerminDescription(
    requestMessage: v1_dictionary_vermin_description_pb.ListVerminDescriptionRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_vermin_description_pb.ListVerminDescriptionResponse|null) => void
  ): UnaryResponse;
  listVerminDescription(
    requestMessage: v1_dictionary_vermin_description_pb.ListVerminDescriptionRequest,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_vermin_description_pb.ListVerminDescriptionResponse|null) => void
  ): UnaryResponse;
  getWeedsInfo(
    requestMessage: v1_dictionary_weeds_info_pb.GetWeedsInfoRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_weeds_info_pb.GetWeedsInfoResponse|null) => void
  ): UnaryResponse;
  getWeedsInfo(
    requestMessage: v1_dictionary_weeds_info_pb.GetWeedsInfoRequest,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_weeds_info_pb.GetWeedsInfoResponse|null) => void
  ): UnaryResponse;
  listWeedsInfo(
    requestMessage: v1_dictionary_weeds_info_pb.ListWeedsInfoRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_weeds_info_pb.ListWeedsInfoResponse|null) => void
  ): UnaryResponse;
  listWeedsInfo(
    requestMessage: v1_dictionary_weeds_info_pb.ListWeedsInfoRequest,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_weeds_info_pb.ListWeedsInfoResponse|null) => void
  ): UnaryResponse;
  getProductKind(
    requestMessage: v1_dictionary_product_kind_pb.GetProductKindRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_product_kind_pb.GetProductKindResponse|null) => void
  ): UnaryResponse;
  getProductKind(
    requestMessage: v1_dictionary_product_kind_pb.GetProductKindRequest,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_product_kind_pb.GetProductKindResponse|null) => void
  ): UnaryResponse;
  listProductKind(
    requestMessage: v1_dictionary_product_kind_pb.ListProductKindRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_product_kind_pb.ListProductKindResponse|null) => void
  ): UnaryResponse;
  listProductKind(
    requestMessage: v1_dictionary_product_kind_pb.ListProductKindRequest,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_product_kind_pb.ListProductKindResponse|null) => void
  ): UnaryResponse;
  getProductType(
    requestMessage: v1_dictionary_product_type_pb.GetProductTypeRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_product_type_pb.GetProductTypeResponse|null) => void
  ): UnaryResponse;
  getProductType(
    requestMessage: v1_dictionary_product_type_pb.GetProductTypeRequest,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_product_type_pb.GetProductTypeResponse|null) => void
  ): UnaryResponse;
  listProductType(
    requestMessage: v1_dictionary_product_type_pb.ListProductTypeRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_product_type_pb.ListProductTypeResponse|null) => void
  ): UnaryResponse;
  listProductType(
    requestMessage: v1_dictionary_product_type_pb.ListProductTypeRequest,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_product_type_pb.ListProductTypeResponse|null) => void
  ): UnaryResponse;
  getProductTypeToKind(
    requestMessage: v1_dictionary_product_type_to_kind_pb.GetProductTypeToKindRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_product_type_to_kind_pb.GetProductTypeToKindResponse|null) => void
  ): UnaryResponse;
  getProductTypeToKind(
    requestMessage: v1_dictionary_product_type_to_kind_pb.GetProductTypeToKindRequest,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_product_type_to_kind_pb.GetProductTypeToKindResponse|null) => void
  ): UnaryResponse;
  listProductTypeToKind(
    requestMessage: v1_dictionary_product_type_to_kind_pb.ListProductTypeToKindRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_product_type_to_kind_pb.ListProductTypeToKindResponse|null) => void
  ): UnaryResponse;
  listProductTypeToKind(
    requestMessage: v1_dictionary_product_type_to_kind_pb.ListProductTypeToKindRequest,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_product_type_to_kind_pb.ListProductTypeToKindResponse|null) => void
  ): UnaryResponse;
  getProductSubGroup(
    requestMessage: v1_dictionary_product_sub_group_pb.GetProductSubGroupRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_product_sub_group_pb.GetProductSubGroupResponse|null) => void
  ): UnaryResponse;
  getProductSubGroup(
    requestMessage: v1_dictionary_product_sub_group_pb.GetProductSubGroupRequest,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_product_sub_group_pb.GetProductSubGroupResponse|null) => void
  ): UnaryResponse;
  listProductSubGroup(
    requestMessage: v1_dictionary_product_sub_group_pb.ListProductSubGroupRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_product_sub_group_pb.ListProductSubGroupResponse|null) => void
  ): UnaryResponse;
  listProductSubGroup(
    requestMessage: v1_dictionary_product_sub_group_pb.ListProductSubGroupRequest,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_product_sub_group_pb.ListProductSubGroupResponse|null) => void
  ): UnaryResponse;
  getProductGroupToKind(
    requestMessage: v1_dictionary_product_group_to_kind_pb.GetProductGroupToKindRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_product_group_to_kind_pb.GetProductGroupToKindResponse|null) => void
  ): UnaryResponse;
  getProductGroupToKind(
    requestMessage: v1_dictionary_product_group_to_kind_pb.GetProductGroupToKindRequest,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_product_group_to_kind_pb.GetProductGroupToKindResponse|null) => void
  ): UnaryResponse;
  listProductGroupToKind(
    requestMessage: v1_dictionary_product_group_to_kind_pb.ListProductGroupToKindRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_product_group_to_kind_pb.ListProductGroupToKindResponse|null) => void
  ): UnaryResponse;
  listProductGroupToKind(
    requestMessage: v1_dictionary_product_group_to_kind_pb.ListProductGroupToKindRequest,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_product_group_to_kind_pb.ListProductGroupToKindResponse|null) => void
  ): UnaryResponse;
  getProductGroupToSubGroup(
    requestMessage: v1_dictionary_product_group_to_sub_group_pb.GetProductGroupToSubGroupRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_product_group_to_sub_group_pb.GetProductGroupToSubGroupResponse|null) => void
  ): UnaryResponse;
  getProductGroupToSubGroup(
    requestMessage: v1_dictionary_product_group_to_sub_group_pb.GetProductGroupToSubGroupRequest,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_product_group_to_sub_group_pb.GetProductGroupToSubGroupResponse|null) => void
  ): UnaryResponse;
  listProductGroupToSubGroup(
    requestMessage: v1_dictionary_product_group_to_sub_group_pb.ListProductGroupToSubGroupRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_product_group_to_sub_group_pb.ListProductGroupToSubGroupResponse|null) => void
  ): UnaryResponse;
  listProductGroupToSubGroup(
    requestMessage: v1_dictionary_product_group_to_sub_group_pb.ListProductGroupToSubGroupRequest,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_product_group_to_sub_group_pb.ListProductGroupToSubGroupResponse|null) => void
  ): UnaryResponse;
}

