// package: fcp.dictionary.v1.dictionary_web
// file: v1/dictionary_web/dictionaries.proto

var v1_dictionary_web_dictionaries_pb = require("../../v1/dictionary_web/dictionaries_pb");
var v1_dictionary_cultures_group_pb = require("../../v1/dictionary/cultures_group_pb");
var v1_dictionary_culture_pb = require("../../v1/dictionary/culture_pb");
var v1_dictionary_product_pb = require("../../v1/dictionary/product_pb");
var v1_dictionary_brand_pb = require("../../v1/dictionary/brand_pb");
var v1_dictionary_packaging_pb = require("../../v1/dictionary/packaging_pb");
var v1_dictionary_packaging_for_product_pb = require("../../v1/dictionary/packaging_for_product_pb");
var v1_dictionary_active_substance_pb = require("../../v1/dictionary/active_substance_pb");
var v1_dictionary_active_substance_for_product_pb = require("../../v1/dictionary/active_substance_for_product_pb");
var v1_dictionary_vermin_group_pb = require("../../v1/dictionary/vermin_group_pb");
var v1_dictionary_vermin_pb = require("../../v1/dictionary/vermin_pb");
var v1_dictionary_unit_pb = require("../../v1/dictionary/unit_pb");
var v1_dictionary_technology_crop_processing_pb = require("../../v1/dictionary/technology_crop_processing_pb");
var v1_dictionary_microelement_pb = require("../../v1/dictionary/microelement_pb");
var v1_dictionary_microelement_for_product_pb = require("../../v1/dictionary/microelement_for_product_pb");
var v1_dictionary_hybrids_group_pb = require("../../v1/dictionary/hybrids_group_pb");
var v1_dictionary_hybrids_pb = require("../../v1/dictionary/hybrids_pb");
var v1_dictionary_hybrids_groups_for_culture_pb = require("../../v1/dictionary/hybrids_groups_for_culture_pb");
var v1_dictionary_insect_row_pb = require("../../v1/dictionary/insect_row_pb");
var v1_dictionary_insect_family_pb = require("../../v1/dictionary/insect_family_pb");
var v1_dictionary_insect_info_pb = require("../../v1/dictionary/insect_info_pb");
var v1_dictionary_vermin_for_crop_processing_pb = require("../../v1/dictionary/vermin_for_crop_processing_pb");
var v1_dictionary_vermin_for_prep_pb = require("../../v1/dictionary/vermin_for_prep_pb");
var v1_dictionary_vermin_for_active_substance_pb = require("../../v1/dictionary/vermin_for_active_substance_pb");
var v1_dictionary_group_weeds_pb = require("../../v1/dictionary/group_weeds_pb");
var v1_dictionary_group_weeds_for_weeds_pb = require("../../v1/dictionary/group_weeds_for_weeds_pb");
var v1_dictionary_country_pd_pb = require("../../v1/dictionary/country_pd_pb");
var v1_dictionary_culture_for_crop_processing_pb = require("../../v1/dictionary/culture_for_crop_processing_pb");
var v1_dictionary_culture_grow_stage_pb = require("../../v1/dictionary/culture_grow_stage_pb");
var v1_dictionary_family_weeds_pb = require("../../v1/dictionary/family_weeds_pb");
var v1_dictionary_features_pb = require("../../v1/dictionary/features_pb");
var v1_dictionary_features_for_culture_pb = require("../../v1/dictionary/features_for_culture_pb");
var v1_dictionary_features_for_hybrid_pb = require("../../v1/dictionary/features_for_hybrid_pb");
var v1_dictionary_features_values_pb = require("../../v1/dictionary/features_values_pb");
var v1_dictionary_insect_type_pb = require("../../v1/dictionary/insect_type_pb");
var v1_dictionary_insect_type_for_insect_pb = require("../../v1/dictionary/insect_type_for_insect_pb");
var v1_dictionary_manufacturer_pb = require("../../v1/dictionary/manufacturer_pb");
var v1_dictionary_perform_methods_pb = require("../../v1/dictionary/perform_methods_pb");
var v1_dictionary_preparative_form_pb = require("../../v1/dictionary/preparative_form_pb");
var v1_dictionary_product_class_pb = require("../../v1/dictionary/product_class_pb");
var v1_dictionary_product_group_pb = require("../../v1/dictionary/product_group_pb");
var v1_dictionary_product_in_grow_stage_pb = require("../../v1/dictionary/product_in_grow_stage_pb");
var v1_dictionary_recomended_zone_pb = require("../../v1/dictionary/recomended_zone_pb");
var v1_dictionary_zone_weed_pb = require("../../v1/dictionary/zone_weed_pb");
var v1_dictionary_zone_for_hybrids_pb = require("../../v1/dictionary/zone_for_hybrids_pb");
var v1_dictionary_zone_weed_for_weed_pb = require("../../v1/dictionary/zone_weed_for_weed_pb");
var v1_dictionary_type_of_pathogen_pb = require("../../v1/dictionary/type_of_pathogen_pb");
var v1_dictionary_vermin_description_pb = require("../../v1/dictionary/vermin_description_pb");
var v1_dictionary_weeds_info_pb = require("../../v1/dictionary/weeds_info_pb");
var v1_dictionary_product_kind_pb = require("../../v1/dictionary/product_kind_pb");
var v1_dictionary_product_type_pb = require("../../v1/dictionary/product_type_pb");
var v1_dictionary_product_type_to_kind_pb = require("../../v1/dictionary/product_type_to_kind_pb");
var v1_dictionary_product_sub_group_pb = require("../../v1/dictionary/product_sub_group_pb");
var v1_dictionary_product_group_to_kind_pb = require("../../v1/dictionary/product_group_to_kind_pb");
var v1_dictionary_product_group_to_sub_group_pb = require("../../v1/dictionary/product_group_to_sub_group_pb");
var grpc = require("@improbable-eng/grpc-web").grpc;

var DictionaryService = (function () {
  function DictionaryService() {}
  DictionaryService.serviceName = "fcp.dictionary.v1.dictionary_web.DictionaryService";
  return DictionaryService;
}());

DictionaryService.GetCulturesGroup = {
  methodName: "GetCulturesGroup",
  service: DictionaryService,
  requestStream: false,
  responseStream: false,
  requestType: v1_dictionary_cultures_group_pb.GetCulturesGroupRequest,
  responseType: v1_dictionary_cultures_group_pb.GetCulturesGroupResponse
};

DictionaryService.ListCulturesGroup = {
  methodName: "ListCulturesGroup",
  service: DictionaryService,
  requestStream: false,
  responseStream: false,
  requestType: v1_dictionary_cultures_group_pb.ListCulturesGroupRequest,
  responseType: v1_dictionary_cultures_group_pb.ListCulturesGroupResponse
};

DictionaryService.GetCulture = {
  methodName: "GetCulture",
  service: DictionaryService,
  requestStream: false,
  responseStream: false,
  requestType: v1_dictionary_culture_pb.GetCultureRequest,
  responseType: v1_dictionary_culture_pb.GetCultureResponse
};

DictionaryService.ListCulture = {
  methodName: "ListCulture",
  service: DictionaryService,
  requestStream: false,
  responseStream: false,
  requestType: v1_dictionary_culture_pb.ListCultureRequest,
  responseType: v1_dictionary_culture_pb.ListCultureResponse
};

DictionaryService.ListCulturePlantProtection = {
  methodName: "ListCulturePlantProtection",
  service: DictionaryService,
  requestStream: false,
  responseStream: false,
  requestType: v1_dictionary_culture_pb.ListCultureRequest,
  responseType: v1_dictionary_culture_pb.ListCultureResponse
};

DictionaryService.GetProduct = {
  methodName: "GetProduct",
  service: DictionaryService,
  requestStream: false,
  responseStream: false,
  requestType: v1_dictionary_product_pb.GetProductRequest,
  responseType: v1_dictionary_product_pb.GetProductResponse
};

DictionaryService.ListProduct = {
  methodName: "ListProduct",
  service: DictionaryService,
  requestStream: false,
  responseStream: false,
  requestType: v1_dictionary_product_pb.ListProductRequest,
  responseType: v1_dictionary_product_pb.ListProductResponse
};

DictionaryService.ListProductOfPlantProtectionByCulture = {
  methodName: "ListProductOfPlantProtectionByCulture",
  service: DictionaryService,
  requestStream: false,
  responseStream: false,
  requestType: v1_dictionary_product_pb.ListProductOfPlantProtectionByCultureRequest,
  responseType: v1_dictionary_product_pb.ListProductResponse
};

DictionaryService.ListProductAlternative = {
  methodName: "ListProductAlternative",
  service: DictionaryService,
  requestStream: false,
  responseStream: false,
  requestType: v1_dictionary_product_pb.ListProductAlternativeRequest,
  responseType: v1_dictionary_product_pb.ListProductAlternativeResponse
};

DictionaryService.GetBrand = {
  methodName: "GetBrand",
  service: DictionaryService,
  requestStream: false,
  responseStream: false,
  requestType: v1_dictionary_brand_pb.GetBrandRequest,
  responseType: v1_dictionary_brand_pb.GetBrandResponse
};

DictionaryService.ListBrand = {
  methodName: "ListBrand",
  service: DictionaryService,
  requestStream: false,
  responseStream: false,
  requestType: v1_dictionary_brand_pb.ListBrandRequest,
  responseType: v1_dictionary_brand_pb.ListBrandResponse
};

DictionaryService.GetPackaging = {
  methodName: "GetPackaging",
  service: DictionaryService,
  requestStream: false,
  responseStream: false,
  requestType: v1_dictionary_packaging_pb.GetPackagingRequest,
  responseType: v1_dictionary_packaging_pb.GetPackagingResponse
};

DictionaryService.ListPackaging = {
  methodName: "ListPackaging",
  service: DictionaryService,
  requestStream: false,
  responseStream: false,
  requestType: v1_dictionary_packaging_pb.ListPackagingRequest,
  responseType: v1_dictionary_packaging_pb.ListPackagingResponse
};

DictionaryService.GetPackagingForProduct = {
  methodName: "GetPackagingForProduct",
  service: DictionaryService,
  requestStream: false,
  responseStream: false,
  requestType: v1_dictionary_packaging_for_product_pb.GetPackagingForProductRequest,
  responseType: v1_dictionary_packaging_for_product_pb.GetPackagingForProductResponse
};

DictionaryService.ListPackagingForProduct = {
  methodName: "ListPackagingForProduct",
  service: DictionaryService,
  requestStream: false,
  responseStream: false,
  requestType: v1_dictionary_packaging_for_product_pb.ListPackagingForProductRequest,
  responseType: v1_dictionary_packaging_for_product_pb.ListPackagingForProductResponse
};

DictionaryService.GetActiveSubstance = {
  methodName: "GetActiveSubstance",
  service: DictionaryService,
  requestStream: false,
  responseStream: false,
  requestType: v1_dictionary_active_substance_pb.GetActiveSubstanceRequest,
  responseType: v1_dictionary_active_substance_pb.GetActiveSubstanceResponse
};

DictionaryService.ListActiveSubstance = {
  methodName: "ListActiveSubstance",
  service: DictionaryService,
  requestStream: false,
  responseStream: false,
  requestType: v1_dictionary_active_substance_pb.ListActiveSubstanceRequest,
  responseType: v1_dictionary_active_substance_pb.ListActiveSubstanceResponse
};

DictionaryService.GetActiveSubstanceForProduct = {
  methodName: "GetActiveSubstanceForProduct",
  service: DictionaryService,
  requestStream: false,
  responseStream: false,
  requestType: v1_dictionary_active_substance_for_product_pb.GetActiveSubstanceForProductRequest,
  responseType: v1_dictionary_active_substance_for_product_pb.GetActiveSubstanceForProductResponse
};

DictionaryService.ListActiveSubstanceForProduct = {
  methodName: "ListActiveSubstanceForProduct",
  service: DictionaryService,
  requestStream: false,
  responseStream: false,
  requestType: v1_dictionary_active_substance_for_product_pb.ListActiveSubstanceForProductRequest,
  responseType: v1_dictionary_active_substance_for_product_pb.ListActiveSubstanceForProductResponse
};

DictionaryService.GetVerminGroup = {
  methodName: "GetVerminGroup",
  service: DictionaryService,
  requestStream: false,
  responseStream: false,
  requestType: v1_dictionary_vermin_group_pb.GetVerminGroupRequest,
  responseType: v1_dictionary_vermin_group_pb.GetVerminGroupResponse
};

DictionaryService.ListVerminGroup = {
  methodName: "ListVerminGroup",
  service: DictionaryService,
  requestStream: false,
  responseStream: false,
  requestType: v1_dictionary_vermin_group_pb.ListVerminGroupRequest,
  responseType: v1_dictionary_vermin_group_pb.ListVerminGroupResponse
};

DictionaryService.GetVermin = {
  methodName: "GetVermin",
  service: DictionaryService,
  requestStream: false,
  responseStream: false,
  requestType: v1_dictionary_vermin_pb.GetVerminRequest,
  responseType: v1_dictionary_vermin_pb.GetVerminResponse
};

DictionaryService.ListVermin = {
  methodName: "ListVermin",
  service: DictionaryService,
  requestStream: false,
  responseStream: false,
  requestType: v1_dictionary_vermin_pb.ListVerminRequest,
  responseType: v1_dictionary_vermin_pb.ListVerminResponse
};

DictionaryService.GetUnit = {
  methodName: "GetUnit",
  service: DictionaryService,
  requestStream: false,
  responseStream: false,
  requestType: v1_dictionary_unit_pb.GetUnitRequest,
  responseType: v1_dictionary_unit_pb.GetUnitResponse
};

DictionaryService.ListUnit = {
  methodName: "ListUnit",
  service: DictionaryService,
  requestStream: false,
  responseStream: false,
  requestType: v1_dictionary_unit_pb.ListUnitRequest,
  responseType: v1_dictionary_unit_pb.ListUnitResponse
};

DictionaryService.GetTechnologyCropProcessing = {
  methodName: "GetTechnologyCropProcessing",
  service: DictionaryService,
  requestStream: false,
  responseStream: false,
  requestType: v1_dictionary_technology_crop_processing_pb.GetTechnologyCropProcessingRequest,
  responseType: v1_dictionary_technology_crop_processing_pb.GetTechnologyCropProcessingResponse
};

DictionaryService.ListTechnologyCropProcessing = {
  methodName: "ListTechnologyCropProcessing",
  service: DictionaryService,
  requestStream: false,
  responseStream: false,
  requestType: v1_dictionary_technology_crop_processing_pb.ListTechnologyCropProcessingRequest,
  responseType: v1_dictionary_technology_crop_processing_pb.ListTechnologyCropProcessingResponse
};

DictionaryService.GetMicroelement = {
  methodName: "GetMicroelement",
  service: DictionaryService,
  requestStream: false,
  responseStream: false,
  requestType: v1_dictionary_microelement_pb.GetMicroelementRequest,
  responseType: v1_dictionary_microelement_pb.GetMicroelementResponse
};

DictionaryService.ListMicroelement = {
  methodName: "ListMicroelement",
  service: DictionaryService,
  requestStream: false,
  responseStream: false,
  requestType: v1_dictionary_microelement_pb.ListMicroelementRequest,
  responseType: v1_dictionary_microelement_pb.ListMicroelementResponse
};

DictionaryService.GetMicroelementForProduct = {
  methodName: "GetMicroelementForProduct",
  service: DictionaryService,
  requestStream: false,
  responseStream: false,
  requestType: v1_dictionary_microelement_for_product_pb.GetMicroelementForProductRequest,
  responseType: v1_dictionary_microelement_for_product_pb.GetMicroelementForProductResponse
};

DictionaryService.ListMicroelementForProduct = {
  methodName: "ListMicroelementForProduct",
  service: DictionaryService,
  requestStream: false,
  responseStream: false,
  requestType: v1_dictionary_microelement_for_product_pb.ListMicroelementForProductRequest,
  responseType: v1_dictionary_microelement_for_product_pb.ListMicroelementForProductResponse
};

DictionaryService.GetHybridsGroup = {
  methodName: "GetHybridsGroup",
  service: DictionaryService,
  requestStream: false,
  responseStream: false,
  requestType: v1_dictionary_hybrids_group_pb.GetHybridsGroupRequest,
  responseType: v1_dictionary_hybrids_group_pb.GetHybridsGroupResponse
};

DictionaryService.ListHybridsGroup = {
  methodName: "ListHybridsGroup",
  service: DictionaryService,
  requestStream: false,
  responseStream: false,
  requestType: v1_dictionary_hybrids_group_pb.ListHybridsGroupRequest,
  responseType: v1_dictionary_hybrids_group_pb.ListHybridsGroupResponse
};

DictionaryService.GetHybrids = {
  methodName: "GetHybrids",
  service: DictionaryService,
  requestStream: false,
  responseStream: false,
  requestType: v1_dictionary_hybrids_pb.GetHybridsRequest,
  responseType: v1_dictionary_hybrids_pb.GetHybridsResponse
};

DictionaryService.ListHybrids = {
  methodName: "ListHybrids",
  service: DictionaryService,
  requestStream: false,
  responseStream: false,
  requestType: v1_dictionary_hybrids_pb.ListHybridsRequest,
  responseType: v1_dictionary_hybrids_pb.ListHybridsResponse
};

DictionaryService.GetHybridsGroupsForCulture = {
  methodName: "GetHybridsGroupsForCulture",
  service: DictionaryService,
  requestStream: false,
  responseStream: false,
  requestType: v1_dictionary_hybrids_groups_for_culture_pb.GetHybridsGroupsForCultureRequest,
  responseType: v1_dictionary_hybrids_groups_for_culture_pb.GetHybridsGroupsForCultureResponse
};

DictionaryService.ListHybridsGroupsForCulture = {
  methodName: "ListHybridsGroupsForCulture",
  service: DictionaryService,
  requestStream: false,
  responseStream: false,
  requestType: v1_dictionary_hybrids_groups_for_culture_pb.ListHybridsGroupsForCultureRequest,
  responseType: v1_dictionary_hybrids_groups_for_culture_pb.ListHybridsGroupsForCultureResponse
};

DictionaryService.GetInsectRow = {
  methodName: "GetInsectRow",
  service: DictionaryService,
  requestStream: false,
  responseStream: false,
  requestType: v1_dictionary_insect_row_pb.GetInsectRowRequest,
  responseType: v1_dictionary_insect_row_pb.GetInsectRowResponse
};

DictionaryService.ListInsectRow = {
  methodName: "ListInsectRow",
  service: DictionaryService,
  requestStream: false,
  responseStream: false,
  requestType: v1_dictionary_insect_row_pb.ListInsectRowRequest,
  responseType: v1_dictionary_insect_row_pb.ListInsectRowResponse
};

DictionaryService.GetInsectFamily = {
  methodName: "GetInsectFamily",
  service: DictionaryService,
  requestStream: false,
  responseStream: false,
  requestType: v1_dictionary_insect_family_pb.GetInsectFamilyRequest,
  responseType: v1_dictionary_insect_family_pb.GetInsectFamilyResponse
};

DictionaryService.ListInsectFamily = {
  methodName: "ListInsectFamily",
  service: DictionaryService,
  requestStream: false,
  responseStream: false,
  requestType: v1_dictionary_insect_family_pb.ListInsectFamilyRequest,
  responseType: v1_dictionary_insect_family_pb.ListInsectFamilyResponse
};

DictionaryService.GetInsectInfo = {
  methodName: "GetInsectInfo",
  service: DictionaryService,
  requestStream: false,
  responseStream: false,
  requestType: v1_dictionary_insect_info_pb.GetInsectInfoRequest,
  responseType: v1_dictionary_insect_info_pb.GetInsectInfoResponse
};

DictionaryService.ListInsectInfo = {
  methodName: "ListInsectInfo",
  service: DictionaryService,
  requestStream: false,
  responseStream: false,
  requestType: v1_dictionary_insect_info_pb.ListInsectInfoRequest,
  responseType: v1_dictionary_insect_info_pb.ListInsectInfoResponse
};

DictionaryService.GetVerminForCropProcessing = {
  methodName: "GetVerminForCropProcessing",
  service: DictionaryService,
  requestStream: false,
  responseStream: false,
  requestType: v1_dictionary_vermin_for_crop_processing_pb.GetVerminForCropProcessingRequest,
  responseType: v1_dictionary_vermin_for_crop_processing_pb.GetVerminForCropProcessingResponse
};

DictionaryService.ListVerminForCropProcessing = {
  methodName: "ListVerminForCropProcessing",
  service: DictionaryService,
  requestStream: false,
  responseStream: false,
  requestType: v1_dictionary_vermin_for_crop_processing_pb.ListVerminForCropProcessingRequest,
  responseType: v1_dictionary_vermin_for_crop_processing_pb.ListVerminForCropProcessingResponse
};

DictionaryService.GetVerminForPrep = {
  methodName: "GetVerminForPrep",
  service: DictionaryService,
  requestStream: false,
  responseStream: false,
  requestType: v1_dictionary_vermin_for_prep_pb.GetVerminForPrepRequest,
  responseType: v1_dictionary_vermin_for_prep_pb.GetVerminForPrepResponse
};

DictionaryService.ListVerminForPrep = {
  methodName: "ListVerminForPrep",
  service: DictionaryService,
  requestStream: false,
  responseStream: false,
  requestType: v1_dictionary_vermin_for_prep_pb.ListVerminForPrepRequest,
  responseType: v1_dictionary_vermin_for_prep_pb.ListVerminForPrepResponse
};

DictionaryService.GetVerminForActiveSubstance = {
  methodName: "GetVerminForActiveSubstance",
  service: DictionaryService,
  requestStream: false,
  responseStream: false,
  requestType: v1_dictionary_vermin_for_active_substance_pb.GetVerminForActiveSubstanceRequest,
  responseType: v1_dictionary_vermin_for_active_substance_pb.GetVerminForActiveSubstanceResponse
};

DictionaryService.ListVerminForActiveSubstance = {
  methodName: "ListVerminForActiveSubstance",
  service: DictionaryService,
  requestStream: false,
  responseStream: false,
  requestType: v1_dictionary_vermin_for_active_substance_pb.ListVerminForActiveSubstanceRequest,
  responseType: v1_dictionary_vermin_for_active_substance_pb.ListVerminForActiveSubstanceResponse
};

DictionaryService.GetGroupWeeds = {
  methodName: "GetGroupWeeds",
  service: DictionaryService,
  requestStream: false,
  responseStream: false,
  requestType: v1_dictionary_group_weeds_pb.GetGroupWeedsRequest,
  responseType: v1_dictionary_group_weeds_pb.GetGroupWeedsResponse
};

DictionaryService.ListGroupWeeds = {
  methodName: "ListGroupWeeds",
  service: DictionaryService,
  requestStream: false,
  responseStream: false,
  requestType: v1_dictionary_group_weeds_pb.ListGroupWeedsRequest,
  responseType: v1_dictionary_group_weeds_pb.ListGroupWeedsResponse
};

DictionaryService.GetGroupWeedsForWeeds = {
  methodName: "GetGroupWeedsForWeeds",
  service: DictionaryService,
  requestStream: false,
  responseStream: false,
  requestType: v1_dictionary_group_weeds_for_weeds_pb.GetGroupWeedsForWeedsRequest,
  responseType: v1_dictionary_group_weeds_for_weeds_pb.GetGroupWeedsForWeedsResponse
};

DictionaryService.ListGroupWeedsForWeeds = {
  methodName: "ListGroupWeedsForWeeds",
  service: DictionaryService,
  requestStream: false,
  responseStream: false,
  requestType: v1_dictionary_group_weeds_for_weeds_pb.ListGroupWeedsForWeedsRequest,
  responseType: v1_dictionary_group_weeds_for_weeds_pb.ListGroupWeedsForWeedsResponse
};

DictionaryService.GetCountryPd = {
  methodName: "GetCountryPd",
  service: DictionaryService,
  requestStream: false,
  responseStream: false,
  requestType: v1_dictionary_country_pd_pb.GetCountryPdRequest,
  responseType: v1_dictionary_country_pd_pb.GetCountryPdResponse
};

DictionaryService.ListCountryPd = {
  methodName: "ListCountryPd",
  service: DictionaryService,
  requestStream: false,
  responseStream: false,
  requestType: v1_dictionary_country_pd_pb.ListCountryPdRequest,
  responseType: v1_dictionary_country_pd_pb.ListCountryPdResponse
};

DictionaryService.GetCultureForCropProcessing = {
  methodName: "GetCultureForCropProcessing",
  service: DictionaryService,
  requestStream: false,
  responseStream: false,
  requestType: v1_dictionary_culture_for_crop_processing_pb.GetCultureForCropProcessingRequest,
  responseType: v1_dictionary_culture_for_crop_processing_pb.GetCultureForCropProcessingResponse
};

DictionaryService.ListCultureForCropProcessing = {
  methodName: "ListCultureForCropProcessing",
  service: DictionaryService,
  requestStream: false,
  responseStream: false,
  requestType: v1_dictionary_culture_for_crop_processing_pb.ListCultureForCropProcessingRequest,
  responseType: v1_dictionary_culture_for_crop_processing_pb.ListCultureForCropProcessingResponse
};

DictionaryService.GetCultureGrowStage = {
  methodName: "GetCultureGrowStage",
  service: DictionaryService,
  requestStream: false,
  responseStream: false,
  requestType: v1_dictionary_culture_grow_stage_pb.GetCultureGrowStageRequest,
  responseType: v1_dictionary_culture_grow_stage_pb.GetCultureGrowStageResponse
};

DictionaryService.ListCultureGrowStage = {
  methodName: "ListCultureGrowStage",
  service: DictionaryService,
  requestStream: false,
  responseStream: false,
  requestType: v1_dictionary_culture_grow_stage_pb.ListCultureGrowStageRequest,
  responseType: v1_dictionary_culture_grow_stage_pb.ListCultureGrowStageResponse
};

DictionaryService.GetFamilyWeeds = {
  methodName: "GetFamilyWeeds",
  service: DictionaryService,
  requestStream: false,
  responseStream: false,
  requestType: v1_dictionary_family_weeds_pb.GetFamilyWeedsRequest,
  responseType: v1_dictionary_family_weeds_pb.GetFamilyWeedsResponse
};

DictionaryService.ListFamilyWeeds = {
  methodName: "ListFamilyWeeds",
  service: DictionaryService,
  requestStream: false,
  responseStream: false,
  requestType: v1_dictionary_family_weeds_pb.ListFamilyWeedsRequest,
  responseType: v1_dictionary_family_weeds_pb.ListFamilyWeedsResponse
};

DictionaryService.GetFeatures = {
  methodName: "GetFeatures",
  service: DictionaryService,
  requestStream: false,
  responseStream: false,
  requestType: v1_dictionary_features_pb.GetFeaturesRequest,
  responseType: v1_dictionary_features_pb.GetFeaturesResponse
};

DictionaryService.ListFeatures = {
  methodName: "ListFeatures",
  service: DictionaryService,
  requestStream: false,
  responseStream: false,
  requestType: v1_dictionary_features_pb.ListFeaturesRequest,
  responseType: v1_dictionary_features_pb.ListFeaturesResponse
};

DictionaryService.GetFeaturesForCulture = {
  methodName: "GetFeaturesForCulture",
  service: DictionaryService,
  requestStream: false,
  responseStream: false,
  requestType: v1_dictionary_features_for_culture_pb.GetFeaturesForCultureRequest,
  responseType: v1_dictionary_features_for_culture_pb.GetFeaturesForCultureResponse
};

DictionaryService.ListFeaturesForCulture = {
  methodName: "ListFeaturesForCulture",
  service: DictionaryService,
  requestStream: false,
  responseStream: false,
  requestType: v1_dictionary_features_for_culture_pb.ListFeaturesForCultureRequest,
  responseType: v1_dictionary_features_for_culture_pb.ListFeaturesForCultureResponse
};

DictionaryService.GetFeaturesForHybrid = {
  methodName: "GetFeaturesForHybrid",
  service: DictionaryService,
  requestStream: false,
  responseStream: false,
  requestType: v1_dictionary_features_for_hybrid_pb.GetFeaturesForHybridRequest,
  responseType: v1_dictionary_features_for_hybrid_pb.GetFeaturesForHybridResponse
};

DictionaryService.ListFeaturesForHybrid = {
  methodName: "ListFeaturesForHybrid",
  service: DictionaryService,
  requestStream: false,
  responseStream: false,
  requestType: v1_dictionary_features_for_hybrid_pb.ListFeaturesForHybridRequest,
  responseType: v1_dictionary_features_for_hybrid_pb.ListFeaturesForHybridResponse
};

DictionaryService.GetFeaturesValues = {
  methodName: "GetFeaturesValues",
  service: DictionaryService,
  requestStream: false,
  responseStream: false,
  requestType: v1_dictionary_features_values_pb.GetFeaturesValuesRequest,
  responseType: v1_dictionary_features_values_pb.GetFeaturesValuesResponse
};

DictionaryService.ListFeaturesValues = {
  methodName: "ListFeaturesValues",
  service: DictionaryService,
  requestStream: false,
  responseStream: false,
  requestType: v1_dictionary_features_values_pb.ListFeaturesValuesRequest,
  responseType: v1_dictionary_features_values_pb.ListFeaturesValuesResponse
};

DictionaryService.GetInsectType = {
  methodName: "GetInsectType",
  service: DictionaryService,
  requestStream: false,
  responseStream: false,
  requestType: v1_dictionary_insect_type_pb.GetInsectTypeRequest,
  responseType: v1_dictionary_insect_type_pb.GetInsectTypeResponse
};

DictionaryService.ListInsectType = {
  methodName: "ListInsectType",
  service: DictionaryService,
  requestStream: false,
  responseStream: false,
  requestType: v1_dictionary_insect_type_pb.ListInsectTypeRequest,
  responseType: v1_dictionary_insect_type_pb.ListInsectTypeResponse
};

DictionaryService.GetInsectTypeForInsect = {
  methodName: "GetInsectTypeForInsect",
  service: DictionaryService,
  requestStream: false,
  responseStream: false,
  requestType: v1_dictionary_insect_type_for_insect_pb.GetInsectTypeForInsectRequest,
  responseType: v1_dictionary_insect_type_for_insect_pb.GetInsectTypeForInsectResponse
};

DictionaryService.ListInsectTypeForInsect = {
  methodName: "ListInsectTypeForInsect",
  service: DictionaryService,
  requestStream: false,
  responseStream: false,
  requestType: v1_dictionary_insect_type_for_insect_pb.ListInsectTypeForInsectRequest,
  responseType: v1_dictionary_insect_type_for_insect_pb.ListInsectTypeForInsectResponse
};

DictionaryService.GetManufacturer = {
  methodName: "GetManufacturer",
  service: DictionaryService,
  requestStream: false,
  responseStream: false,
  requestType: v1_dictionary_manufacturer_pb.GetManufacturerRequest,
  responseType: v1_dictionary_manufacturer_pb.GetManufacturerResponse
};

DictionaryService.ListManufacturer = {
  methodName: "ListManufacturer",
  service: DictionaryService,
  requestStream: false,
  responseStream: false,
  requestType: v1_dictionary_manufacturer_pb.ListManufacturerRequest,
  responseType: v1_dictionary_manufacturer_pb.ListManufacturerResponse
};

DictionaryService.GetPerformMethods = {
  methodName: "GetPerformMethods",
  service: DictionaryService,
  requestStream: false,
  responseStream: false,
  requestType: v1_dictionary_perform_methods_pb.GetPerformMethodsRequest,
  responseType: v1_dictionary_perform_methods_pb.GetPerformMethodsResponse
};

DictionaryService.ListPerformMethods = {
  methodName: "ListPerformMethods",
  service: DictionaryService,
  requestStream: false,
  responseStream: false,
  requestType: v1_dictionary_perform_methods_pb.ListPerformMethodsRequest,
  responseType: v1_dictionary_perform_methods_pb.ListPerformMethodsResponse
};

DictionaryService.GetPreparativeForm = {
  methodName: "GetPreparativeForm",
  service: DictionaryService,
  requestStream: false,
  responseStream: false,
  requestType: v1_dictionary_preparative_form_pb.GetPreparativeFormRequest,
  responseType: v1_dictionary_preparative_form_pb.GetPreparativeFormResponse
};

DictionaryService.ListPreparativeForm = {
  methodName: "ListPreparativeForm",
  service: DictionaryService,
  requestStream: false,
  responseStream: false,
  requestType: v1_dictionary_preparative_form_pb.ListPreparativeFormRequest,
  responseType: v1_dictionary_preparative_form_pb.ListPreparativeFormResponse
};

DictionaryService.GetProductClass = {
  methodName: "GetProductClass",
  service: DictionaryService,
  requestStream: false,
  responseStream: false,
  requestType: v1_dictionary_product_class_pb.GetProductClassRequest,
  responseType: v1_dictionary_product_class_pb.GetProductClassResponse
};

DictionaryService.ListProductClass = {
  methodName: "ListProductClass",
  service: DictionaryService,
  requestStream: false,
  responseStream: false,
  requestType: v1_dictionary_product_class_pb.ListProductClassRequest,
  responseType: v1_dictionary_product_class_pb.ListProductClassResponse
};

DictionaryService.GetProductGroup = {
  methodName: "GetProductGroup",
  service: DictionaryService,
  requestStream: false,
  responseStream: false,
  requestType: v1_dictionary_product_group_pb.GetProductGroupRequest,
  responseType: v1_dictionary_product_group_pb.GetProductGroupResponse
};

DictionaryService.ListProductGroup = {
  methodName: "ListProductGroup",
  service: DictionaryService,
  requestStream: false,
  responseStream: false,
  requestType: v1_dictionary_product_group_pb.ListProductGroupRequest,
  responseType: v1_dictionary_product_group_pb.ListProductGroupResponse
};

DictionaryService.GetProductInGrowStage = {
  methodName: "GetProductInGrowStage",
  service: DictionaryService,
  requestStream: false,
  responseStream: false,
  requestType: v1_dictionary_product_in_grow_stage_pb.GetProductInGrowStageRequest,
  responseType: v1_dictionary_product_in_grow_stage_pb.GetProductInGrowStageResponse
};

DictionaryService.ListProductInGrowStage = {
  methodName: "ListProductInGrowStage",
  service: DictionaryService,
  requestStream: false,
  responseStream: false,
  requestType: v1_dictionary_product_in_grow_stage_pb.ListProductInGrowStageRequest,
  responseType: v1_dictionary_product_in_grow_stage_pb.ListProductInGrowStageResponse
};

DictionaryService.GetRecomendedZone = {
  methodName: "GetRecomendedZone",
  service: DictionaryService,
  requestStream: false,
  responseStream: false,
  requestType: v1_dictionary_recomended_zone_pb.GetRecomendedZoneRequest,
  responseType: v1_dictionary_recomended_zone_pb.GetRecomendedZoneResponse
};

DictionaryService.ListRecomendedZone = {
  methodName: "ListRecomendedZone",
  service: DictionaryService,
  requestStream: false,
  responseStream: false,
  requestType: v1_dictionary_recomended_zone_pb.ListRecomendedZoneRequest,
  responseType: v1_dictionary_recomended_zone_pb.ListRecomendedZoneResponse
};

DictionaryService.GetZoneWeed = {
  methodName: "GetZoneWeed",
  service: DictionaryService,
  requestStream: false,
  responseStream: false,
  requestType: v1_dictionary_zone_weed_pb.GetZoneWeedRequest,
  responseType: v1_dictionary_zone_weed_pb.GetZoneWeedResponse
};

DictionaryService.ListZoneWeed = {
  methodName: "ListZoneWeed",
  service: DictionaryService,
  requestStream: false,
  responseStream: false,
  requestType: v1_dictionary_zone_weed_pb.ListZoneWeedRequest,
  responseType: v1_dictionary_zone_weed_pb.ListZoneWeedResponse
};

DictionaryService.GetZoneForHybrids = {
  methodName: "GetZoneForHybrids",
  service: DictionaryService,
  requestStream: false,
  responseStream: false,
  requestType: v1_dictionary_zone_for_hybrids_pb.GetZoneForHybridsRequest,
  responseType: v1_dictionary_zone_for_hybrids_pb.GetZoneForHybridsResponse
};

DictionaryService.ListZoneForHybrids = {
  methodName: "ListZoneForHybrids",
  service: DictionaryService,
  requestStream: false,
  responseStream: false,
  requestType: v1_dictionary_zone_for_hybrids_pb.ListZoneForHybridsRequest,
  responseType: v1_dictionary_zone_for_hybrids_pb.ListZoneForHybridsResponse
};

DictionaryService.GetZoneWeedforWeed = {
  methodName: "GetZoneWeedforWeed",
  service: DictionaryService,
  requestStream: false,
  responseStream: false,
  requestType: v1_dictionary_zone_weed_for_weed_pb.GetZoneWeedforWeedRequest,
  responseType: v1_dictionary_zone_weed_for_weed_pb.GetZoneWeedforWeedResponse
};

DictionaryService.ListZoneWeedforWeed = {
  methodName: "ListZoneWeedforWeed",
  service: DictionaryService,
  requestStream: false,
  responseStream: false,
  requestType: v1_dictionary_zone_weed_for_weed_pb.ListZoneWeedforWeedRequest,
  responseType: v1_dictionary_zone_weed_for_weed_pb.ListZoneWeedforWeedResponse
};

DictionaryService.GetTypeOfPathogen = {
  methodName: "GetTypeOfPathogen",
  service: DictionaryService,
  requestStream: false,
  responseStream: false,
  requestType: v1_dictionary_type_of_pathogen_pb.GetTypeOfPathogenRequest,
  responseType: v1_dictionary_type_of_pathogen_pb.GetTypeOfPathogenResponse
};

DictionaryService.ListTypeOfPathogen = {
  methodName: "ListTypeOfPathogen",
  service: DictionaryService,
  requestStream: false,
  responseStream: false,
  requestType: v1_dictionary_type_of_pathogen_pb.ListTypeOfPathogenRequest,
  responseType: v1_dictionary_type_of_pathogen_pb.ListTypeOfPathogenResponse
};

DictionaryService.GetVerminDescription = {
  methodName: "GetVerminDescription",
  service: DictionaryService,
  requestStream: false,
  responseStream: false,
  requestType: v1_dictionary_vermin_description_pb.GetVerminDescriptionRequest,
  responseType: v1_dictionary_vermin_description_pb.GetVerminDescriptionResponse
};

DictionaryService.ListVerminDescription = {
  methodName: "ListVerminDescription",
  service: DictionaryService,
  requestStream: false,
  responseStream: false,
  requestType: v1_dictionary_vermin_description_pb.ListVerminDescriptionRequest,
  responseType: v1_dictionary_vermin_description_pb.ListVerminDescriptionResponse
};

DictionaryService.GetWeedsInfo = {
  methodName: "GetWeedsInfo",
  service: DictionaryService,
  requestStream: false,
  responseStream: false,
  requestType: v1_dictionary_weeds_info_pb.GetWeedsInfoRequest,
  responseType: v1_dictionary_weeds_info_pb.GetWeedsInfoResponse
};

DictionaryService.ListWeedsInfo = {
  methodName: "ListWeedsInfo",
  service: DictionaryService,
  requestStream: false,
  responseStream: false,
  requestType: v1_dictionary_weeds_info_pb.ListWeedsInfoRequest,
  responseType: v1_dictionary_weeds_info_pb.ListWeedsInfoResponse
};

DictionaryService.GetProductKind = {
  methodName: "GetProductKind",
  service: DictionaryService,
  requestStream: false,
  responseStream: false,
  requestType: v1_dictionary_product_kind_pb.GetProductKindRequest,
  responseType: v1_dictionary_product_kind_pb.GetProductKindResponse
};

DictionaryService.ListProductKind = {
  methodName: "ListProductKind",
  service: DictionaryService,
  requestStream: false,
  responseStream: false,
  requestType: v1_dictionary_product_kind_pb.ListProductKindRequest,
  responseType: v1_dictionary_product_kind_pb.ListProductKindResponse
};

DictionaryService.GetProductType = {
  methodName: "GetProductType",
  service: DictionaryService,
  requestStream: false,
  responseStream: false,
  requestType: v1_dictionary_product_type_pb.GetProductTypeRequest,
  responseType: v1_dictionary_product_type_pb.GetProductTypeResponse
};

DictionaryService.ListProductType = {
  methodName: "ListProductType",
  service: DictionaryService,
  requestStream: false,
  responseStream: false,
  requestType: v1_dictionary_product_type_pb.ListProductTypeRequest,
  responseType: v1_dictionary_product_type_pb.ListProductTypeResponse
};

DictionaryService.GetProductTypeToKind = {
  methodName: "GetProductTypeToKind",
  service: DictionaryService,
  requestStream: false,
  responseStream: false,
  requestType: v1_dictionary_product_type_to_kind_pb.GetProductTypeToKindRequest,
  responseType: v1_dictionary_product_type_to_kind_pb.GetProductTypeToKindResponse
};

DictionaryService.ListProductTypeToKind = {
  methodName: "ListProductTypeToKind",
  service: DictionaryService,
  requestStream: false,
  responseStream: false,
  requestType: v1_dictionary_product_type_to_kind_pb.ListProductTypeToKindRequest,
  responseType: v1_dictionary_product_type_to_kind_pb.ListProductTypeToKindResponse
};

DictionaryService.GetProductSubGroup = {
  methodName: "GetProductSubGroup",
  service: DictionaryService,
  requestStream: false,
  responseStream: false,
  requestType: v1_dictionary_product_sub_group_pb.GetProductSubGroupRequest,
  responseType: v1_dictionary_product_sub_group_pb.GetProductSubGroupResponse
};

DictionaryService.ListProductSubGroup = {
  methodName: "ListProductSubGroup",
  service: DictionaryService,
  requestStream: false,
  responseStream: false,
  requestType: v1_dictionary_product_sub_group_pb.ListProductSubGroupRequest,
  responseType: v1_dictionary_product_sub_group_pb.ListProductSubGroupResponse
};

DictionaryService.GetProductGroupToKind = {
  methodName: "GetProductGroupToKind",
  service: DictionaryService,
  requestStream: false,
  responseStream: false,
  requestType: v1_dictionary_product_group_to_kind_pb.GetProductGroupToKindRequest,
  responseType: v1_dictionary_product_group_to_kind_pb.GetProductGroupToKindResponse
};

DictionaryService.ListProductGroupToKind = {
  methodName: "ListProductGroupToKind",
  service: DictionaryService,
  requestStream: false,
  responseStream: false,
  requestType: v1_dictionary_product_group_to_kind_pb.ListProductGroupToKindRequest,
  responseType: v1_dictionary_product_group_to_kind_pb.ListProductGroupToKindResponse
};

DictionaryService.GetProductGroupToSubGroup = {
  methodName: "GetProductGroupToSubGroup",
  service: DictionaryService,
  requestStream: false,
  responseStream: false,
  requestType: v1_dictionary_product_group_to_sub_group_pb.GetProductGroupToSubGroupRequest,
  responseType: v1_dictionary_product_group_to_sub_group_pb.GetProductGroupToSubGroupResponse
};

DictionaryService.ListProductGroupToSubGroup = {
  methodName: "ListProductGroupToSubGroup",
  service: DictionaryService,
  requestStream: false,
  responseStream: false,
  requestType: v1_dictionary_product_group_to_sub_group_pb.ListProductGroupToSubGroupRequest,
  responseType: v1_dictionary_product_group_to_sub_group_pb.ListProductGroupToSubGroupResponse
};

exports.DictionaryService = DictionaryService;

function DictionaryServiceClient(serviceHost, options) {
  this.serviceHost = serviceHost;
  this.options = options || {};
}

DictionaryServiceClient.prototype.getCulturesGroup = function getCulturesGroup(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(DictionaryService.GetCulturesGroup, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

DictionaryServiceClient.prototype.listCulturesGroup = function listCulturesGroup(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(DictionaryService.ListCulturesGroup, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

DictionaryServiceClient.prototype.getCulture = function getCulture(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(DictionaryService.GetCulture, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

DictionaryServiceClient.prototype.listCulture = function listCulture(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(DictionaryService.ListCulture, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

DictionaryServiceClient.prototype.listCulturePlantProtection = function listCulturePlantProtection(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(DictionaryService.ListCulturePlantProtection, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

DictionaryServiceClient.prototype.getProduct = function getProduct(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(DictionaryService.GetProduct, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

DictionaryServiceClient.prototype.listProduct = function listProduct(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(DictionaryService.ListProduct, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

DictionaryServiceClient.prototype.listProductOfPlantProtectionByCulture = function listProductOfPlantProtectionByCulture(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(DictionaryService.ListProductOfPlantProtectionByCulture, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

DictionaryServiceClient.prototype.listProductAlternative = function listProductAlternative(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(DictionaryService.ListProductAlternative, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

DictionaryServiceClient.prototype.getBrand = function getBrand(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(DictionaryService.GetBrand, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

DictionaryServiceClient.prototype.listBrand = function listBrand(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(DictionaryService.ListBrand, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

DictionaryServiceClient.prototype.getPackaging = function getPackaging(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(DictionaryService.GetPackaging, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

DictionaryServiceClient.prototype.listPackaging = function listPackaging(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(DictionaryService.ListPackaging, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

DictionaryServiceClient.prototype.getPackagingForProduct = function getPackagingForProduct(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(DictionaryService.GetPackagingForProduct, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

DictionaryServiceClient.prototype.listPackagingForProduct = function listPackagingForProduct(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(DictionaryService.ListPackagingForProduct, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

DictionaryServiceClient.prototype.getActiveSubstance = function getActiveSubstance(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(DictionaryService.GetActiveSubstance, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

DictionaryServiceClient.prototype.listActiveSubstance = function listActiveSubstance(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(DictionaryService.ListActiveSubstance, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

DictionaryServiceClient.prototype.getActiveSubstanceForProduct = function getActiveSubstanceForProduct(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(DictionaryService.GetActiveSubstanceForProduct, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

DictionaryServiceClient.prototype.listActiveSubstanceForProduct = function listActiveSubstanceForProduct(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(DictionaryService.ListActiveSubstanceForProduct, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

DictionaryServiceClient.prototype.getVerminGroup = function getVerminGroup(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(DictionaryService.GetVerminGroup, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

DictionaryServiceClient.prototype.listVerminGroup = function listVerminGroup(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(DictionaryService.ListVerminGroup, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

DictionaryServiceClient.prototype.getVermin = function getVermin(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(DictionaryService.GetVermin, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

DictionaryServiceClient.prototype.listVermin = function listVermin(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(DictionaryService.ListVermin, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

DictionaryServiceClient.prototype.getUnit = function getUnit(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(DictionaryService.GetUnit, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

DictionaryServiceClient.prototype.listUnit = function listUnit(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(DictionaryService.ListUnit, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

DictionaryServiceClient.prototype.getTechnologyCropProcessing = function getTechnologyCropProcessing(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(DictionaryService.GetTechnologyCropProcessing, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

DictionaryServiceClient.prototype.listTechnologyCropProcessing = function listTechnologyCropProcessing(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(DictionaryService.ListTechnologyCropProcessing, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

DictionaryServiceClient.prototype.getMicroelement = function getMicroelement(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(DictionaryService.GetMicroelement, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

DictionaryServiceClient.prototype.listMicroelement = function listMicroelement(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(DictionaryService.ListMicroelement, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

DictionaryServiceClient.prototype.getMicroelementForProduct = function getMicroelementForProduct(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(DictionaryService.GetMicroelementForProduct, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

DictionaryServiceClient.prototype.listMicroelementForProduct = function listMicroelementForProduct(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(DictionaryService.ListMicroelementForProduct, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

DictionaryServiceClient.prototype.getHybridsGroup = function getHybridsGroup(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(DictionaryService.GetHybridsGroup, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

DictionaryServiceClient.prototype.listHybridsGroup = function listHybridsGroup(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(DictionaryService.ListHybridsGroup, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

DictionaryServiceClient.prototype.getHybrids = function getHybrids(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(DictionaryService.GetHybrids, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

DictionaryServiceClient.prototype.listHybrids = function listHybrids(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(DictionaryService.ListHybrids, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

DictionaryServiceClient.prototype.getHybridsGroupsForCulture = function getHybridsGroupsForCulture(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(DictionaryService.GetHybridsGroupsForCulture, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

DictionaryServiceClient.prototype.listHybridsGroupsForCulture = function listHybridsGroupsForCulture(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(DictionaryService.ListHybridsGroupsForCulture, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

DictionaryServiceClient.prototype.getInsectRow = function getInsectRow(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(DictionaryService.GetInsectRow, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

DictionaryServiceClient.prototype.listInsectRow = function listInsectRow(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(DictionaryService.ListInsectRow, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

DictionaryServiceClient.prototype.getInsectFamily = function getInsectFamily(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(DictionaryService.GetInsectFamily, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

DictionaryServiceClient.prototype.listInsectFamily = function listInsectFamily(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(DictionaryService.ListInsectFamily, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

DictionaryServiceClient.prototype.getInsectInfo = function getInsectInfo(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(DictionaryService.GetInsectInfo, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

DictionaryServiceClient.prototype.listInsectInfo = function listInsectInfo(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(DictionaryService.ListInsectInfo, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

DictionaryServiceClient.prototype.getVerminForCropProcessing = function getVerminForCropProcessing(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(DictionaryService.GetVerminForCropProcessing, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

DictionaryServiceClient.prototype.listVerminForCropProcessing = function listVerminForCropProcessing(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(DictionaryService.ListVerminForCropProcessing, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

DictionaryServiceClient.prototype.getVerminForPrep = function getVerminForPrep(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(DictionaryService.GetVerminForPrep, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

DictionaryServiceClient.prototype.listVerminForPrep = function listVerminForPrep(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(DictionaryService.ListVerminForPrep, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

DictionaryServiceClient.prototype.getVerminForActiveSubstance = function getVerminForActiveSubstance(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(DictionaryService.GetVerminForActiveSubstance, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

DictionaryServiceClient.prototype.listVerminForActiveSubstance = function listVerminForActiveSubstance(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(DictionaryService.ListVerminForActiveSubstance, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

DictionaryServiceClient.prototype.getGroupWeeds = function getGroupWeeds(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(DictionaryService.GetGroupWeeds, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

DictionaryServiceClient.prototype.listGroupWeeds = function listGroupWeeds(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(DictionaryService.ListGroupWeeds, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

DictionaryServiceClient.prototype.getGroupWeedsForWeeds = function getGroupWeedsForWeeds(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(DictionaryService.GetGroupWeedsForWeeds, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

DictionaryServiceClient.prototype.listGroupWeedsForWeeds = function listGroupWeedsForWeeds(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(DictionaryService.ListGroupWeedsForWeeds, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

DictionaryServiceClient.prototype.getCountryPd = function getCountryPd(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(DictionaryService.GetCountryPd, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

DictionaryServiceClient.prototype.listCountryPd = function listCountryPd(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(DictionaryService.ListCountryPd, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

DictionaryServiceClient.prototype.getCultureForCropProcessing = function getCultureForCropProcessing(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(DictionaryService.GetCultureForCropProcessing, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

DictionaryServiceClient.prototype.listCultureForCropProcessing = function listCultureForCropProcessing(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(DictionaryService.ListCultureForCropProcessing, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

DictionaryServiceClient.prototype.getCultureGrowStage = function getCultureGrowStage(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(DictionaryService.GetCultureGrowStage, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

DictionaryServiceClient.prototype.listCultureGrowStage = function listCultureGrowStage(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(DictionaryService.ListCultureGrowStage, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

DictionaryServiceClient.prototype.getFamilyWeeds = function getFamilyWeeds(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(DictionaryService.GetFamilyWeeds, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

DictionaryServiceClient.prototype.listFamilyWeeds = function listFamilyWeeds(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(DictionaryService.ListFamilyWeeds, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

DictionaryServiceClient.prototype.getFeatures = function getFeatures(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(DictionaryService.GetFeatures, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

DictionaryServiceClient.prototype.listFeatures = function listFeatures(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(DictionaryService.ListFeatures, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

DictionaryServiceClient.prototype.getFeaturesForCulture = function getFeaturesForCulture(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(DictionaryService.GetFeaturesForCulture, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

DictionaryServiceClient.prototype.listFeaturesForCulture = function listFeaturesForCulture(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(DictionaryService.ListFeaturesForCulture, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

DictionaryServiceClient.prototype.getFeaturesForHybrid = function getFeaturesForHybrid(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(DictionaryService.GetFeaturesForHybrid, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

DictionaryServiceClient.prototype.listFeaturesForHybrid = function listFeaturesForHybrid(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(DictionaryService.ListFeaturesForHybrid, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

DictionaryServiceClient.prototype.getFeaturesValues = function getFeaturesValues(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(DictionaryService.GetFeaturesValues, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

DictionaryServiceClient.prototype.listFeaturesValues = function listFeaturesValues(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(DictionaryService.ListFeaturesValues, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

DictionaryServiceClient.prototype.getInsectType = function getInsectType(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(DictionaryService.GetInsectType, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

DictionaryServiceClient.prototype.listInsectType = function listInsectType(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(DictionaryService.ListInsectType, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

DictionaryServiceClient.prototype.getInsectTypeForInsect = function getInsectTypeForInsect(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(DictionaryService.GetInsectTypeForInsect, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

DictionaryServiceClient.prototype.listInsectTypeForInsect = function listInsectTypeForInsect(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(DictionaryService.ListInsectTypeForInsect, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

DictionaryServiceClient.prototype.getManufacturer = function getManufacturer(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(DictionaryService.GetManufacturer, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

DictionaryServiceClient.prototype.listManufacturer = function listManufacturer(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(DictionaryService.ListManufacturer, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

DictionaryServiceClient.prototype.getPerformMethods = function getPerformMethods(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(DictionaryService.GetPerformMethods, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

DictionaryServiceClient.prototype.listPerformMethods = function listPerformMethods(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(DictionaryService.ListPerformMethods, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

DictionaryServiceClient.prototype.getPreparativeForm = function getPreparativeForm(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(DictionaryService.GetPreparativeForm, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

DictionaryServiceClient.prototype.listPreparativeForm = function listPreparativeForm(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(DictionaryService.ListPreparativeForm, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

DictionaryServiceClient.prototype.getProductClass = function getProductClass(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(DictionaryService.GetProductClass, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

DictionaryServiceClient.prototype.listProductClass = function listProductClass(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(DictionaryService.ListProductClass, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

DictionaryServiceClient.prototype.getProductGroup = function getProductGroup(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(DictionaryService.GetProductGroup, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

DictionaryServiceClient.prototype.listProductGroup = function listProductGroup(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(DictionaryService.ListProductGroup, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

DictionaryServiceClient.prototype.getProductInGrowStage = function getProductInGrowStage(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(DictionaryService.GetProductInGrowStage, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

DictionaryServiceClient.prototype.listProductInGrowStage = function listProductInGrowStage(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(DictionaryService.ListProductInGrowStage, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

DictionaryServiceClient.prototype.getRecomendedZone = function getRecomendedZone(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(DictionaryService.GetRecomendedZone, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

DictionaryServiceClient.prototype.listRecomendedZone = function listRecomendedZone(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(DictionaryService.ListRecomendedZone, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

DictionaryServiceClient.prototype.getZoneWeed = function getZoneWeed(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(DictionaryService.GetZoneWeed, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

DictionaryServiceClient.prototype.listZoneWeed = function listZoneWeed(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(DictionaryService.ListZoneWeed, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

DictionaryServiceClient.prototype.getZoneForHybrids = function getZoneForHybrids(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(DictionaryService.GetZoneForHybrids, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

DictionaryServiceClient.prototype.listZoneForHybrids = function listZoneForHybrids(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(DictionaryService.ListZoneForHybrids, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

DictionaryServiceClient.prototype.getZoneWeedforWeed = function getZoneWeedforWeed(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(DictionaryService.GetZoneWeedforWeed, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

DictionaryServiceClient.prototype.listZoneWeedforWeed = function listZoneWeedforWeed(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(DictionaryService.ListZoneWeedforWeed, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

DictionaryServiceClient.prototype.getTypeOfPathogen = function getTypeOfPathogen(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(DictionaryService.GetTypeOfPathogen, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

DictionaryServiceClient.prototype.listTypeOfPathogen = function listTypeOfPathogen(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(DictionaryService.ListTypeOfPathogen, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

DictionaryServiceClient.prototype.getVerminDescription = function getVerminDescription(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(DictionaryService.GetVerminDescription, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

DictionaryServiceClient.prototype.listVerminDescription = function listVerminDescription(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(DictionaryService.ListVerminDescription, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

DictionaryServiceClient.prototype.getWeedsInfo = function getWeedsInfo(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(DictionaryService.GetWeedsInfo, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

DictionaryServiceClient.prototype.listWeedsInfo = function listWeedsInfo(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(DictionaryService.ListWeedsInfo, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

DictionaryServiceClient.prototype.getProductKind = function getProductKind(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(DictionaryService.GetProductKind, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

DictionaryServiceClient.prototype.listProductKind = function listProductKind(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(DictionaryService.ListProductKind, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

DictionaryServiceClient.prototype.getProductType = function getProductType(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(DictionaryService.GetProductType, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

DictionaryServiceClient.prototype.listProductType = function listProductType(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(DictionaryService.ListProductType, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

DictionaryServiceClient.prototype.getProductTypeToKind = function getProductTypeToKind(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(DictionaryService.GetProductTypeToKind, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

DictionaryServiceClient.prototype.listProductTypeToKind = function listProductTypeToKind(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(DictionaryService.ListProductTypeToKind, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

DictionaryServiceClient.prototype.getProductSubGroup = function getProductSubGroup(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(DictionaryService.GetProductSubGroup, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

DictionaryServiceClient.prototype.listProductSubGroup = function listProductSubGroup(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(DictionaryService.ListProductSubGroup, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

DictionaryServiceClient.prototype.getProductGroupToKind = function getProductGroupToKind(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(DictionaryService.GetProductGroupToKind, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

DictionaryServiceClient.prototype.listProductGroupToKind = function listProductGroupToKind(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(DictionaryService.ListProductGroupToKind, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

DictionaryServiceClient.prototype.getProductGroupToSubGroup = function getProductGroupToSubGroup(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(DictionaryService.GetProductGroupToSubGroup, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

DictionaryServiceClient.prototype.listProductGroupToSubGroup = function listProductGroupToSubGroup(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(DictionaryService.ListProductGroupToSubGroup, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

exports.DictionaryServiceClient = DictionaryServiceClient;

