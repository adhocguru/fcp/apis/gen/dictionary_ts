// package: fcp.dictionary.v1.dictionary
// file: v1/dictionary/recomended_zone.proto

import * as jspb from "google-protobuf";
import * as google_protobuf_wrappers_pb from "google-protobuf/google/protobuf/wrappers_pb";
import * as google_protobuf_timestamp_pb from "google-protobuf/google/protobuf/timestamp_pb";
import * as v1_dictionary_common_dictionary_common_pb from "../../v1/dictionary_common/dictionary_common_pb";

export class RecomendedZone extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  getName(): string;
  setName(value: string): void;

  hasHash(): boolean;
  clearHash(): void;
  getHash(): google_protobuf_wrappers_pb.StringValue | undefined;
  setHash(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasDeleted(): boolean;
  clearDeleted(): void;
  getDeleted(): google_protobuf_wrappers_pb.BoolValue | undefined;
  setDeleted(value?: google_protobuf_wrappers_pb.BoolValue): void;

  hasCreatedBy(): boolean;
  clearCreatedBy(): void;
  getCreatedBy(): google_protobuf_wrappers_pb.StringValue | undefined;
  setCreatedBy(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasCreatedAt(): boolean;
  clearCreatedAt(): void;
  getCreatedAt(): google_protobuf_timestamp_pb.Timestamp | undefined;
  setCreatedAt(value?: google_protobuf_timestamp_pb.Timestamp): void;

  hasUpdatedBy(): boolean;
  clearUpdatedBy(): void;
  getUpdatedBy(): google_protobuf_wrappers_pb.StringValue | undefined;
  setUpdatedBy(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasUpdatedAt(): boolean;
  clearUpdatedAt(): void;
  getUpdatedAt(): google_protobuf_timestamp_pb.Timestamp | undefined;
  setUpdatedAt(value?: google_protobuf_timestamp_pb.Timestamp): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): RecomendedZone.AsObject;
  static toObject(includeInstance: boolean, msg: RecomendedZone): RecomendedZone.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: RecomendedZone, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): RecomendedZone;
  static deserializeBinaryFromReader(message: RecomendedZone, reader: jspb.BinaryReader): RecomendedZone;
}

export namespace RecomendedZone {
  export type AsObject = {
    id: number,
    name: string,
    hash?: google_protobuf_wrappers_pb.StringValue.AsObject,
    deleted?: google_protobuf_wrappers_pb.BoolValue.AsObject,
    createdBy?: google_protobuf_wrappers_pb.StringValue.AsObject,
    createdAt?: google_protobuf_timestamp_pb.Timestamp.AsObject,
    updatedBy?: google_protobuf_wrappers_pb.StringValue.AsObject,
    updatedAt?: google_protobuf_timestamp_pb.Timestamp.AsObject,
  }
}

export class RecomendedZoneFilter extends jspb.Message {
  hasId(): boolean;
  clearId(): void;
  getId(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setId(value?: google_protobuf_wrappers_pb.Int64Value): void;

  hasName(): boolean;
  clearName(): void;
  getName(): google_protobuf_wrappers_pb.StringValue | undefined;
  setName(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasDeleted(): boolean;
  clearDeleted(): void;
  getDeleted(): google_protobuf_wrappers_pb.BoolValue | undefined;
  setDeleted(value?: google_protobuf_wrappers_pb.BoolValue): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): RecomendedZoneFilter.AsObject;
  static toObject(includeInstance: boolean, msg: RecomendedZoneFilter): RecomendedZoneFilter.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: RecomendedZoneFilter, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): RecomendedZoneFilter;
  static deserializeBinaryFromReader(message: RecomendedZoneFilter, reader: jspb.BinaryReader): RecomendedZoneFilter;
}

export namespace RecomendedZoneFilter {
  export type AsObject = {
    id?: google_protobuf_wrappers_pb.Int64Value.AsObject,
    name?: google_protobuf_wrappers_pb.StringValue.AsObject,
    deleted?: google_protobuf_wrappers_pb.BoolValue.AsObject,
  }
}

export class ListRecomendedZoneRequest extends jspb.Message {
  hasFilter(): boolean;
  clearFilter(): void;
  getFilter(): RecomendedZoneFilter | undefined;
  setFilter(value?: RecomendedZoneFilter): void;

  clearSortingsList(): void;
  getSortingsList(): Array<v1_dictionary_common_dictionary_common_pb.Sorting>;
  setSortingsList(value: Array<v1_dictionary_common_dictionary_common_pb.Sorting>): void;
  addSortings(value?: v1_dictionary_common_dictionary_common_pb.Sorting, index?: number): v1_dictionary_common_dictionary_common_pb.Sorting;

  hasPagination(): boolean;
  clearPagination(): void;
  getPagination(): v1_dictionary_common_dictionary_common_pb.PaginationRequest | undefined;
  setPagination(value?: v1_dictionary_common_dictionary_common_pb.PaginationRequest): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListRecomendedZoneRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ListRecomendedZoneRequest): ListRecomendedZoneRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListRecomendedZoneRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListRecomendedZoneRequest;
  static deserializeBinaryFromReader(message: ListRecomendedZoneRequest, reader: jspb.BinaryReader): ListRecomendedZoneRequest;
}

export namespace ListRecomendedZoneRequest {
  export type AsObject = {
    filter?: RecomendedZoneFilter.AsObject,
    sortingsList: Array<v1_dictionary_common_dictionary_common_pb.Sorting.AsObject>,
    pagination?: v1_dictionary_common_dictionary_common_pb.PaginationRequest.AsObject,
  }
}

export class ListRecomendedZoneResponse extends jspb.Message {
  clearItemsList(): void;
  getItemsList(): Array<RecomendedZone>;
  setItemsList(value: Array<RecomendedZone>): void;
  addItems(value?: RecomendedZone, index?: number): RecomendedZone;

  hasPagination(): boolean;
  clearPagination(): void;
  getPagination(): v1_dictionary_common_dictionary_common_pb.PaginationResponse | undefined;
  setPagination(value?: v1_dictionary_common_dictionary_common_pb.PaginationResponse): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListRecomendedZoneResponse.AsObject;
  static toObject(includeInstance: boolean, msg: ListRecomendedZoneResponse): ListRecomendedZoneResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListRecomendedZoneResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListRecomendedZoneResponse;
  static deserializeBinaryFromReader(message: ListRecomendedZoneResponse, reader: jspb.BinaryReader): ListRecomendedZoneResponse;
}

export namespace ListRecomendedZoneResponse {
  export type AsObject = {
    itemsList: Array<RecomendedZone.AsObject>,
    pagination?: v1_dictionary_common_dictionary_common_pb.PaginationResponse.AsObject,
  }
}

export class GetRecomendedZoneRequest extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetRecomendedZoneRequest.AsObject;
  static toObject(includeInstance: boolean, msg: GetRecomendedZoneRequest): GetRecomendedZoneRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetRecomendedZoneRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetRecomendedZoneRequest;
  static deserializeBinaryFromReader(message: GetRecomendedZoneRequest, reader: jspb.BinaryReader): GetRecomendedZoneRequest;
}

export namespace GetRecomendedZoneRequest {
  export type AsObject = {
    id: number,
  }
}

export class GetRecomendedZoneResponse extends jspb.Message {
  hasItem(): boolean;
  clearItem(): void;
  getItem(): RecomendedZone | undefined;
  setItem(value?: RecomendedZone): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetRecomendedZoneResponse.AsObject;
  static toObject(includeInstance: boolean, msg: GetRecomendedZoneResponse): GetRecomendedZoneResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetRecomendedZoneResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetRecomendedZoneResponse;
  static deserializeBinaryFromReader(message: GetRecomendedZoneResponse, reader: jspb.BinaryReader): GetRecomendedZoneResponse;
}

export namespace GetRecomendedZoneResponse {
  export type AsObject = {
    item?: RecomendedZone.AsObject,
  }
}

