// package: fcp.dictionary.v1.dictionary
// file: v1/dictionary/group_weeds.proto

import * as jspb from "google-protobuf";
import * as google_protobuf_wrappers_pb from "google-protobuf/google/protobuf/wrappers_pb";
import * as google_protobuf_timestamp_pb from "google-protobuf/google/protobuf/timestamp_pb";
import * as v1_dictionary_common_dictionary_common_pb from "../../v1/dictionary_common/dictionary_common_pb";

export class GroupWeeds extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  getName(): string;
  setName(value: string): void;

  hasHash(): boolean;
  clearHash(): void;
  getHash(): google_protobuf_wrappers_pb.StringValue | undefined;
  setHash(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasDeleted(): boolean;
  clearDeleted(): void;
  getDeleted(): google_protobuf_wrappers_pb.BoolValue | undefined;
  setDeleted(value?: google_protobuf_wrappers_pb.BoolValue): void;

  hasCreatedBy(): boolean;
  clearCreatedBy(): void;
  getCreatedBy(): google_protobuf_wrappers_pb.StringValue | undefined;
  setCreatedBy(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasCreatedAt(): boolean;
  clearCreatedAt(): void;
  getCreatedAt(): google_protobuf_timestamp_pb.Timestamp | undefined;
  setCreatedAt(value?: google_protobuf_timestamp_pb.Timestamp): void;

  hasUpdatedBy(): boolean;
  clearUpdatedBy(): void;
  getUpdatedBy(): google_protobuf_wrappers_pb.StringValue | undefined;
  setUpdatedBy(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasUpdatedAt(): boolean;
  clearUpdatedAt(): void;
  getUpdatedAt(): google_protobuf_timestamp_pb.Timestamp | undefined;
  setUpdatedAt(value?: google_protobuf_timestamp_pb.Timestamp): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GroupWeeds.AsObject;
  static toObject(includeInstance: boolean, msg: GroupWeeds): GroupWeeds.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GroupWeeds, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GroupWeeds;
  static deserializeBinaryFromReader(message: GroupWeeds, reader: jspb.BinaryReader): GroupWeeds;
}

export namespace GroupWeeds {
  export type AsObject = {
    id: number,
    name: string,
    hash?: google_protobuf_wrappers_pb.StringValue.AsObject,
    deleted?: google_protobuf_wrappers_pb.BoolValue.AsObject,
    createdBy?: google_protobuf_wrappers_pb.StringValue.AsObject,
    createdAt?: google_protobuf_timestamp_pb.Timestamp.AsObject,
    updatedBy?: google_protobuf_wrappers_pb.StringValue.AsObject,
    updatedAt?: google_protobuf_timestamp_pb.Timestamp.AsObject,
  }
}

export class GroupWeedsFilter extends jspb.Message {
  hasId(): boolean;
  clearId(): void;
  getId(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setId(value?: google_protobuf_wrappers_pb.Int64Value): void;

  hasName(): boolean;
  clearName(): void;
  getName(): google_protobuf_wrappers_pb.StringValue | undefined;
  setName(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasDeleted(): boolean;
  clearDeleted(): void;
  getDeleted(): google_protobuf_wrappers_pb.BoolValue | undefined;
  setDeleted(value?: google_protobuf_wrappers_pb.BoolValue): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GroupWeedsFilter.AsObject;
  static toObject(includeInstance: boolean, msg: GroupWeedsFilter): GroupWeedsFilter.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GroupWeedsFilter, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GroupWeedsFilter;
  static deserializeBinaryFromReader(message: GroupWeedsFilter, reader: jspb.BinaryReader): GroupWeedsFilter;
}

export namespace GroupWeedsFilter {
  export type AsObject = {
    id?: google_protobuf_wrappers_pb.Int64Value.AsObject,
    name?: google_protobuf_wrappers_pb.StringValue.AsObject,
    deleted?: google_protobuf_wrappers_pb.BoolValue.AsObject,
  }
}

export class ListGroupWeedsRequest extends jspb.Message {
  hasFilter(): boolean;
  clearFilter(): void;
  getFilter(): GroupWeedsFilter | undefined;
  setFilter(value?: GroupWeedsFilter): void;

  clearSortingsList(): void;
  getSortingsList(): Array<v1_dictionary_common_dictionary_common_pb.Sorting>;
  setSortingsList(value: Array<v1_dictionary_common_dictionary_common_pb.Sorting>): void;
  addSortings(value?: v1_dictionary_common_dictionary_common_pb.Sorting, index?: number): v1_dictionary_common_dictionary_common_pb.Sorting;

  hasPagination(): boolean;
  clearPagination(): void;
  getPagination(): v1_dictionary_common_dictionary_common_pb.PaginationRequest | undefined;
  setPagination(value?: v1_dictionary_common_dictionary_common_pb.PaginationRequest): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListGroupWeedsRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ListGroupWeedsRequest): ListGroupWeedsRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListGroupWeedsRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListGroupWeedsRequest;
  static deserializeBinaryFromReader(message: ListGroupWeedsRequest, reader: jspb.BinaryReader): ListGroupWeedsRequest;
}

export namespace ListGroupWeedsRequest {
  export type AsObject = {
    filter?: GroupWeedsFilter.AsObject,
    sortingsList: Array<v1_dictionary_common_dictionary_common_pb.Sorting.AsObject>,
    pagination?: v1_dictionary_common_dictionary_common_pb.PaginationRequest.AsObject,
  }
}

export class ListGroupWeedsResponse extends jspb.Message {
  clearItemsList(): void;
  getItemsList(): Array<GroupWeeds>;
  setItemsList(value: Array<GroupWeeds>): void;
  addItems(value?: GroupWeeds, index?: number): GroupWeeds;

  hasPagination(): boolean;
  clearPagination(): void;
  getPagination(): v1_dictionary_common_dictionary_common_pb.PaginationResponse | undefined;
  setPagination(value?: v1_dictionary_common_dictionary_common_pb.PaginationResponse): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListGroupWeedsResponse.AsObject;
  static toObject(includeInstance: boolean, msg: ListGroupWeedsResponse): ListGroupWeedsResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListGroupWeedsResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListGroupWeedsResponse;
  static deserializeBinaryFromReader(message: ListGroupWeedsResponse, reader: jspb.BinaryReader): ListGroupWeedsResponse;
}

export namespace ListGroupWeedsResponse {
  export type AsObject = {
    itemsList: Array<GroupWeeds.AsObject>,
    pagination?: v1_dictionary_common_dictionary_common_pb.PaginationResponse.AsObject,
  }
}

export class GetGroupWeedsRequest extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetGroupWeedsRequest.AsObject;
  static toObject(includeInstance: boolean, msg: GetGroupWeedsRequest): GetGroupWeedsRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetGroupWeedsRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetGroupWeedsRequest;
  static deserializeBinaryFromReader(message: GetGroupWeedsRequest, reader: jspb.BinaryReader): GetGroupWeedsRequest;
}

export namespace GetGroupWeedsRequest {
  export type AsObject = {
    id: number,
  }
}

export class GetGroupWeedsResponse extends jspb.Message {
  hasItem(): boolean;
  clearItem(): void;
  getItem(): GroupWeeds | undefined;
  setItem(value?: GroupWeeds): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetGroupWeedsResponse.AsObject;
  static toObject(includeInstance: boolean, msg: GetGroupWeedsResponse): GetGroupWeedsResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetGroupWeedsResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetGroupWeedsResponse;
  static deserializeBinaryFromReader(message: GetGroupWeedsResponse, reader: jspb.BinaryReader): GetGroupWeedsResponse;
}

export namespace GetGroupWeedsResponse {
  export type AsObject = {
    item?: GroupWeeds.AsObject,
  }
}

