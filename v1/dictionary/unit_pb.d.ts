// package: fcp.dictionary.v1.dictionary
// file: v1/dictionary/unit.proto

import * as jspb from "google-protobuf";
import * as google_protobuf_wrappers_pb from "google-protobuf/google/protobuf/wrappers_pb";
import * as google_protobuf_timestamp_pb from "google-protobuf/google/protobuf/timestamp_pb";
import * as v1_dictionary_common_dictionary_common_pb from "../../v1/dictionary_common/dictionary_common_pb";

export class Unit extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  getShortName(): string;
  setShortName(value: string): void;

  getName(): string;
  setName(value: string): void;

  hasHash(): boolean;
  clearHash(): void;
  getHash(): google_protobuf_wrappers_pb.StringValue | undefined;
  setHash(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasDeleted(): boolean;
  clearDeleted(): void;
  getDeleted(): google_protobuf_wrappers_pb.BoolValue | undefined;
  setDeleted(value?: google_protobuf_wrappers_pb.BoolValue): void;

  hasCreatedBy(): boolean;
  clearCreatedBy(): void;
  getCreatedBy(): google_protobuf_wrappers_pb.StringValue | undefined;
  setCreatedBy(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasCreatedAt(): boolean;
  clearCreatedAt(): void;
  getCreatedAt(): google_protobuf_timestamp_pb.Timestamp | undefined;
  setCreatedAt(value?: google_protobuf_timestamp_pb.Timestamp): void;

  hasUpdatedBy(): boolean;
  clearUpdatedBy(): void;
  getUpdatedBy(): google_protobuf_wrappers_pb.StringValue | undefined;
  setUpdatedBy(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasUpdatedAt(): boolean;
  clearUpdatedAt(): void;
  getUpdatedAt(): google_protobuf_timestamp_pb.Timestamp | undefined;
  setUpdatedAt(value?: google_protobuf_timestamp_pb.Timestamp): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): Unit.AsObject;
  static toObject(includeInstance: boolean, msg: Unit): Unit.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: Unit, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): Unit;
  static deserializeBinaryFromReader(message: Unit, reader: jspb.BinaryReader): Unit;
}

export namespace Unit {
  export type AsObject = {
    id: number,
    shortName: string,
    name: string,
    hash?: google_protobuf_wrappers_pb.StringValue.AsObject,
    deleted?: google_protobuf_wrappers_pb.BoolValue.AsObject,
    createdBy?: google_protobuf_wrappers_pb.StringValue.AsObject,
    createdAt?: google_protobuf_timestamp_pb.Timestamp.AsObject,
    updatedBy?: google_protobuf_wrappers_pb.StringValue.AsObject,
    updatedAt?: google_protobuf_timestamp_pb.Timestamp.AsObject,
  }
}

export class UnitFilter extends jspb.Message {
  hasId(): boolean;
  clearId(): void;
  getId(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setId(value?: google_protobuf_wrappers_pb.Int64Value): void;

  hasShortName(): boolean;
  clearShortName(): void;
  getShortName(): google_protobuf_wrappers_pb.StringValue | undefined;
  setShortName(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasName(): boolean;
  clearName(): void;
  getName(): google_protobuf_wrappers_pb.StringValue | undefined;
  setName(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasDeleted(): boolean;
  clearDeleted(): void;
  getDeleted(): google_protobuf_wrappers_pb.BoolValue | undefined;
  setDeleted(value?: google_protobuf_wrappers_pb.BoolValue): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): UnitFilter.AsObject;
  static toObject(includeInstance: boolean, msg: UnitFilter): UnitFilter.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: UnitFilter, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): UnitFilter;
  static deserializeBinaryFromReader(message: UnitFilter, reader: jspb.BinaryReader): UnitFilter;
}

export namespace UnitFilter {
  export type AsObject = {
    id?: google_protobuf_wrappers_pb.Int64Value.AsObject,
    shortName?: google_protobuf_wrappers_pb.StringValue.AsObject,
    name?: google_protobuf_wrappers_pb.StringValue.AsObject,
    deleted?: google_protobuf_wrappers_pb.BoolValue.AsObject,
  }
}

export class GetUnitRequest extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetUnitRequest.AsObject;
  static toObject(includeInstance: boolean, msg: GetUnitRequest): GetUnitRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetUnitRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetUnitRequest;
  static deserializeBinaryFromReader(message: GetUnitRequest, reader: jspb.BinaryReader): GetUnitRequest;
}

export namespace GetUnitRequest {
  export type AsObject = {
    id: number,
  }
}

export class GetUnitResponse extends jspb.Message {
  hasItem(): boolean;
  clearItem(): void;
  getItem(): Unit | undefined;
  setItem(value?: Unit): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetUnitResponse.AsObject;
  static toObject(includeInstance: boolean, msg: GetUnitResponse): GetUnitResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetUnitResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetUnitResponse;
  static deserializeBinaryFromReader(message: GetUnitResponse, reader: jspb.BinaryReader): GetUnitResponse;
}

export namespace GetUnitResponse {
  export type AsObject = {
    item?: Unit.AsObject,
  }
}

export class ListUnitRequest extends jspb.Message {
  hasFilter(): boolean;
  clearFilter(): void;
  getFilter(): UnitFilter | undefined;
  setFilter(value?: UnitFilter): void;

  clearSortingsList(): void;
  getSortingsList(): Array<v1_dictionary_common_dictionary_common_pb.Sorting>;
  setSortingsList(value: Array<v1_dictionary_common_dictionary_common_pb.Sorting>): void;
  addSortings(value?: v1_dictionary_common_dictionary_common_pb.Sorting, index?: number): v1_dictionary_common_dictionary_common_pb.Sorting;

  hasPagination(): boolean;
  clearPagination(): void;
  getPagination(): v1_dictionary_common_dictionary_common_pb.PaginationRequest | undefined;
  setPagination(value?: v1_dictionary_common_dictionary_common_pb.PaginationRequest): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListUnitRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ListUnitRequest): ListUnitRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListUnitRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListUnitRequest;
  static deserializeBinaryFromReader(message: ListUnitRequest, reader: jspb.BinaryReader): ListUnitRequest;
}

export namespace ListUnitRequest {
  export type AsObject = {
    filter?: UnitFilter.AsObject,
    sortingsList: Array<v1_dictionary_common_dictionary_common_pb.Sorting.AsObject>,
    pagination?: v1_dictionary_common_dictionary_common_pb.PaginationRequest.AsObject,
  }
}

export class ListUnitResponse extends jspb.Message {
  clearItemsList(): void;
  getItemsList(): Array<Unit>;
  setItemsList(value: Array<Unit>): void;
  addItems(value?: Unit, index?: number): Unit;

  hasPagination(): boolean;
  clearPagination(): void;
  getPagination(): v1_dictionary_common_dictionary_common_pb.PaginationResponse | undefined;
  setPagination(value?: v1_dictionary_common_dictionary_common_pb.PaginationResponse): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListUnitResponse.AsObject;
  static toObject(includeInstance: boolean, msg: ListUnitResponse): ListUnitResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListUnitResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListUnitResponse;
  static deserializeBinaryFromReader(message: ListUnitResponse, reader: jspb.BinaryReader): ListUnitResponse;
}

export namespace ListUnitResponse {
  export type AsObject = {
    itemsList: Array<Unit.AsObject>,
    pagination?: v1_dictionary_common_dictionary_common_pb.PaginationResponse.AsObject,
  }
}

