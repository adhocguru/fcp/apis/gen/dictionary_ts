// package: fcp.dictionary.v1.dictionary
// file: v1/dictionary/manufacturer.proto

import * as jspb from "google-protobuf";
import * as google_protobuf_wrappers_pb from "google-protobuf/google/protobuf/wrappers_pb";
import * as google_protobuf_timestamp_pb from "google-protobuf/google/protobuf/timestamp_pb";
import * as v1_dictionary_common_dictionary_common_pb from "../../v1/dictionary_common/dictionary_common_pb";

export class Manufacturer extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  getGuid(): string;
  setGuid(value: string): void;

  getName(): string;
  setName(value: string): void;

  hasHash(): boolean;
  clearHash(): void;
  getHash(): google_protobuf_wrappers_pb.StringValue | undefined;
  setHash(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasDeleted(): boolean;
  clearDeleted(): void;
  getDeleted(): google_protobuf_wrappers_pb.BoolValue | undefined;
  setDeleted(value?: google_protobuf_wrappers_pb.BoolValue): void;

  hasCreatedBy(): boolean;
  clearCreatedBy(): void;
  getCreatedBy(): google_protobuf_wrappers_pb.StringValue | undefined;
  setCreatedBy(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasCreatedAt(): boolean;
  clearCreatedAt(): void;
  getCreatedAt(): google_protobuf_timestamp_pb.Timestamp | undefined;
  setCreatedAt(value?: google_protobuf_timestamp_pb.Timestamp): void;

  hasUpdatedBy(): boolean;
  clearUpdatedBy(): void;
  getUpdatedBy(): google_protobuf_wrappers_pb.StringValue | undefined;
  setUpdatedBy(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasUpdatedAt(): boolean;
  clearUpdatedAt(): void;
  getUpdatedAt(): google_protobuf_timestamp_pb.Timestamp | undefined;
  setUpdatedAt(value?: google_protobuf_timestamp_pb.Timestamp): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): Manufacturer.AsObject;
  static toObject(includeInstance: boolean, msg: Manufacturer): Manufacturer.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: Manufacturer, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): Manufacturer;
  static deserializeBinaryFromReader(message: Manufacturer, reader: jspb.BinaryReader): Manufacturer;
}

export namespace Manufacturer {
  export type AsObject = {
    id: number,
    guid: string,
    name: string,
    hash?: google_protobuf_wrappers_pb.StringValue.AsObject,
    deleted?: google_protobuf_wrappers_pb.BoolValue.AsObject,
    createdBy?: google_protobuf_wrappers_pb.StringValue.AsObject,
    createdAt?: google_protobuf_timestamp_pb.Timestamp.AsObject,
    updatedBy?: google_protobuf_wrappers_pb.StringValue.AsObject,
    updatedAt?: google_protobuf_timestamp_pb.Timestamp.AsObject,
  }
}

export class ManufacturerFilter extends jspb.Message {
  hasId(): boolean;
  clearId(): void;
  getId(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setId(value?: google_protobuf_wrappers_pb.Int64Value): void;

  hasGuid(): boolean;
  clearGuid(): void;
  getGuid(): google_protobuf_wrappers_pb.StringValue | undefined;
  setGuid(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasName(): boolean;
  clearName(): void;
  getName(): google_protobuf_wrappers_pb.StringValue | undefined;
  setName(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasDeleted(): boolean;
  clearDeleted(): void;
  getDeleted(): google_protobuf_wrappers_pb.BoolValue | undefined;
  setDeleted(value?: google_protobuf_wrappers_pb.BoolValue): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ManufacturerFilter.AsObject;
  static toObject(includeInstance: boolean, msg: ManufacturerFilter): ManufacturerFilter.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ManufacturerFilter, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ManufacturerFilter;
  static deserializeBinaryFromReader(message: ManufacturerFilter, reader: jspb.BinaryReader): ManufacturerFilter;
}

export namespace ManufacturerFilter {
  export type AsObject = {
    id?: google_protobuf_wrappers_pb.Int64Value.AsObject,
    guid?: google_protobuf_wrappers_pb.StringValue.AsObject,
    name?: google_protobuf_wrappers_pb.StringValue.AsObject,
    deleted?: google_protobuf_wrappers_pb.BoolValue.AsObject,
  }
}

export class ListManufacturerRequest extends jspb.Message {
  hasFilter(): boolean;
  clearFilter(): void;
  getFilter(): ManufacturerFilter | undefined;
  setFilter(value?: ManufacturerFilter): void;

  clearSortingsList(): void;
  getSortingsList(): Array<v1_dictionary_common_dictionary_common_pb.Sorting>;
  setSortingsList(value: Array<v1_dictionary_common_dictionary_common_pb.Sorting>): void;
  addSortings(value?: v1_dictionary_common_dictionary_common_pb.Sorting, index?: number): v1_dictionary_common_dictionary_common_pb.Sorting;

  hasPagination(): boolean;
  clearPagination(): void;
  getPagination(): v1_dictionary_common_dictionary_common_pb.PaginationRequest | undefined;
  setPagination(value?: v1_dictionary_common_dictionary_common_pb.PaginationRequest): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListManufacturerRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ListManufacturerRequest): ListManufacturerRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListManufacturerRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListManufacturerRequest;
  static deserializeBinaryFromReader(message: ListManufacturerRequest, reader: jspb.BinaryReader): ListManufacturerRequest;
}

export namespace ListManufacturerRequest {
  export type AsObject = {
    filter?: ManufacturerFilter.AsObject,
    sortingsList: Array<v1_dictionary_common_dictionary_common_pb.Sorting.AsObject>,
    pagination?: v1_dictionary_common_dictionary_common_pb.PaginationRequest.AsObject,
  }
}

export class ListManufacturerResponse extends jspb.Message {
  clearItemsList(): void;
  getItemsList(): Array<Manufacturer>;
  setItemsList(value: Array<Manufacturer>): void;
  addItems(value?: Manufacturer, index?: number): Manufacturer;

  hasPagination(): boolean;
  clearPagination(): void;
  getPagination(): v1_dictionary_common_dictionary_common_pb.PaginationResponse | undefined;
  setPagination(value?: v1_dictionary_common_dictionary_common_pb.PaginationResponse): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListManufacturerResponse.AsObject;
  static toObject(includeInstance: boolean, msg: ListManufacturerResponse): ListManufacturerResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListManufacturerResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListManufacturerResponse;
  static deserializeBinaryFromReader(message: ListManufacturerResponse, reader: jspb.BinaryReader): ListManufacturerResponse;
}

export namespace ListManufacturerResponse {
  export type AsObject = {
    itemsList: Array<Manufacturer.AsObject>,
    pagination?: v1_dictionary_common_dictionary_common_pb.PaginationResponse.AsObject,
  }
}

export class GetManufacturerRequest extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetManufacturerRequest.AsObject;
  static toObject(includeInstance: boolean, msg: GetManufacturerRequest): GetManufacturerRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetManufacturerRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetManufacturerRequest;
  static deserializeBinaryFromReader(message: GetManufacturerRequest, reader: jspb.BinaryReader): GetManufacturerRequest;
}

export namespace GetManufacturerRequest {
  export type AsObject = {
    id: number,
  }
}

export class GetManufacturerResponse extends jspb.Message {
  hasItem(): boolean;
  clearItem(): void;
  getItem(): Manufacturer | undefined;
  setItem(value?: Manufacturer): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetManufacturerResponse.AsObject;
  static toObject(includeInstance: boolean, msg: GetManufacturerResponse): GetManufacturerResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetManufacturerResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetManufacturerResponse;
  static deserializeBinaryFromReader(message: GetManufacturerResponse, reader: jspb.BinaryReader): GetManufacturerResponse;
}

export namespace GetManufacturerResponse {
  export type AsObject = {
    item?: Manufacturer.AsObject,
  }
}

