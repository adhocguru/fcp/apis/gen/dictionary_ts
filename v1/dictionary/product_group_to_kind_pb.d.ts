// package: fcp.dictionary.v1.dictionary
// file: v1/dictionary/product_group_to_kind.proto

import * as jspb from "google-protobuf";
import * as google_protobuf_wrappers_pb from "google-protobuf/google/protobuf/wrappers_pb";
import * as google_protobuf_timestamp_pb from "google-protobuf/google/protobuf/timestamp_pb";
import * as v1_dictionary_common_dictionary_common_pb from "../../v1/dictionary_common/dictionary_common_pb";

export class ProductGroupToKind extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  getProductGroupId(): number;
  setProductGroupId(value: number): void;

  getProductGroupName(): string;
  setProductGroupName(value: string): void;

  getProductKindId(): number;
  setProductKindId(value: number): void;

  getProductKindName(): string;
  setProductKindName(value: string): void;

  hasHash(): boolean;
  clearHash(): void;
  getHash(): google_protobuf_wrappers_pb.StringValue | undefined;
  setHash(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasDeleted(): boolean;
  clearDeleted(): void;
  getDeleted(): google_protobuf_wrappers_pb.BoolValue | undefined;
  setDeleted(value?: google_protobuf_wrappers_pb.BoolValue): void;

  hasCreatedBy(): boolean;
  clearCreatedBy(): void;
  getCreatedBy(): google_protobuf_wrappers_pb.StringValue | undefined;
  setCreatedBy(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasCreatedAt(): boolean;
  clearCreatedAt(): void;
  getCreatedAt(): google_protobuf_timestamp_pb.Timestamp | undefined;
  setCreatedAt(value?: google_protobuf_timestamp_pb.Timestamp): void;

  hasUpdatedBy(): boolean;
  clearUpdatedBy(): void;
  getUpdatedBy(): google_protobuf_wrappers_pb.StringValue | undefined;
  setUpdatedBy(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasUpdatedAt(): boolean;
  clearUpdatedAt(): void;
  getUpdatedAt(): google_protobuf_timestamp_pb.Timestamp | undefined;
  setUpdatedAt(value?: google_protobuf_timestamp_pb.Timestamp): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ProductGroupToKind.AsObject;
  static toObject(includeInstance: boolean, msg: ProductGroupToKind): ProductGroupToKind.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ProductGroupToKind, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ProductGroupToKind;
  static deserializeBinaryFromReader(message: ProductGroupToKind, reader: jspb.BinaryReader): ProductGroupToKind;
}

export namespace ProductGroupToKind {
  export type AsObject = {
    id: number,
    productGroupId: number,
    productGroupName: string,
    productKindId: number,
    productKindName: string,
    hash?: google_protobuf_wrappers_pb.StringValue.AsObject,
    deleted?: google_protobuf_wrappers_pb.BoolValue.AsObject,
    createdBy?: google_protobuf_wrappers_pb.StringValue.AsObject,
    createdAt?: google_protobuf_timestamp_pb.Timestamp.AsObject,
    updatedBy?: google_protobuf_wrappers_pb.StringValue.AsObject,
    updatedAt?: google_protobuf_timestamp_pb.Timestamp.AsObject,
  }
}

export class ProductGroupToKindFilter extends jspb.Message {
  hasId(): boolean;
  clearId(): void;
  getId(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setId(value?: google_protobuf_wrappers_pb.Int64Value): void;

  hasProductGroupId(): boolean;
  clearProductGroupId(): void;
  getProductGroupId(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setProductGroupId(value?: google_protobuf_wrappers_pb.Int64Value): void;

  hasProductKindId(): boolean;
  clearProductKindId(): void;
  getProductKindId(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setProductKindId(value?: google_protobuf_wrappers_pb.Int64Value): void;

  hasDeleted(): boolean;
  clearDeleted(): void;
  getDeleted(): google_protobuf_wrappers_pb.BoolValue | undefined;
  setDeleted(value?: google_protobuf_wrappers_pb.BoolValue): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ProductGroupToKindFilter.AsObject;
  static toObject(includeInstance: boolean, msg: ProductGroupToKindFilter): ProductGroupToKindFilter.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ProductGroupToKindFilter, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ProductGroupToKindFilter;
  static deserializeBinaryFromReader(message: ProductGroupToKindFilter, reader: jspb.BinaryReader): ProductGroupToKindFilter;
}

export namespace ProductGroupToKindFilter {
  export type AsObject = {
    id?: google_protobuf_wrappers_pb.Int64Value.AsObject,
    productGroupId?: google_protobuf_wrappers_pb.Int64Value.AsObject,
    productKindId?: google_protobuf_wrappers_pb.Int64Value.AsObject,
    deleted?: google_protobuf_wrappers_pb.BoolValue.AsObject,
  }
}

export class ListProductGroupToKindRequest extends jspb.Message {
  hasFilter(): boolean;
  clearFilter(): void;
  getFilter(): ProductGroupToKindFilter | undefined;
  setFilter(value?: ProductGroupToKindFilter): void;

  clearSortingsList(): void;
  getSortingsList(): Array<v1_dictionary_common_dictionary_common_pb.Sorting>;
  setSortingsList(value: Array<v1_dictionary_common_dictionary_common_pb.Sorting>): void;
  addSortings(value?: v1_dictionary_common_dictionary_common_pb.Sorting, index?: number): v1_dictionary_common_dictionary_common_pb.Sorting;

  hasPagination(): boolean;
  clearPagination(): void;
  getPagination(): v1_dictionary_common_dictionary_common_pb.PaginationRequest | undefined;
  setPagination(value?: v1_dictionary_common_dictionary_common_pb.PaginationRequest): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListProductGroupToKindRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ListProductGroupToKindRequest): ListProductGroupToKindRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListProductGroupToKindRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListProductGroupToKindRequest;
  static deserializeBinaryFromReader(message: ListProductGroupToKindRequest, reader: jspb.BinaryReader): ListProductGroupToKindRequest;
}

export namespace ListProductGroupToKindRequest {
  export type AsObject = {
    filter?: ProductGroupToKindFilter.AsObject,
    sortingsList: Array<v1_dictionary_common_dictionary_common_pb.Sorting.AsObject>,
    pagination?: v1_dictionary_common_dictionary_common_pb.PaginationRequest.AsObject,
  }
}

export class ListProductGroupToKindResponse extends jspb.Message {
  clearItemsList(): void;
  getItemsList(): Array<ProductGroupToKind>;
  setItemsList(value: Array<ProductGroupToKind>): void;
  addItems(value?: ProductGroupToKind, index?: number): ProductGroupToKind;

  hasPagination(): boolean;
  clearPagination(): void;
  getPagination(): v1_dictionary_common_dictionary_common_pb.PaginationResponse | undefined;
  setPagination(value?: v1_dictionary_common_dictionary_common_pb.PaginationResponse): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListProductGroupToKindResponse.AsObject;
  static toObject(includeInstance: boolean, msg: ListProductGroupToKindResponse): ListProductGroupToKindResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListProductGroupToKindResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListProductGroupToKindResponse;
  static deserializeBinaryFromReader(message: ListProductGroupToKindResponse, reader: jspb.BinaryReader): ListProductGroupToKindResponse;
}

export namespace ListProductGroupToKindResponse {
  export type AsObject = {
    itemsList: Array<ProductGroupToKind.AsObject>,
    pagination?: v1_dictionary_common_dictionary_common_pb.PaginationResponse.AsObject,
  }
}

export class GetProductGroupToKindRequest extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetProductGroupToKindRequest.AsObject;
  static toObject(includeInstance: boolean, msg: GetProductGroupToKindRequest): GetProductGroupToKindRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetProductGroupToKindRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetProductGroupToKindRequest;
  static deserializeBinaryFromReader(message: GetProductGroupToKindRequest, reader: jspb.BinaryReader): GetProductGroupToKindRequest;
}

export namespace GetProductGroupToKindRequest {
  export type AsObject = {
    id: number,
  }
}

export class GetProductGroupToKindResponse extends jspb.Message {
  hasItem(): boolean;
  clearItem(): void;
  getItem(): ProductGroupToKind | undefined;
  setItem(value?: ProductGroupToKind): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetProductGroupToKindResponse.AsObject;
  static toObject(includeInstance: boolean, msg: GetProductGroupToKindResponse): GetProductGroupToKindResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetProductGroupToKindResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetProductGroupToKindResponse;
  static deserializeBinaryFromReader(message: GetProductGroupToKindResponse, reader: jspb.BinaryReader): GetProductGroupToKindResponse;
}

export namespace GetProductGroupToKindResponse {
  export type AsObject = {
    item?: ProductGroupToKind.AsObject,
  }
}

