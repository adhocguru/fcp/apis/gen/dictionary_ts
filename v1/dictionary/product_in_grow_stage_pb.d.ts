// package: fcp.dictionary.v1.dictionary
// file: v1/dictionary/product_in_grow_stage.proto

import * as jspb from "google-protobuf";
import * as google_protobuf_wrappers_pb from "google-protobuf/google/protobuf/wrappers_pb";
import * as google_protobuf_timestamp_pb from "google-protobuf/google/protobuf/timestamp_pb";
import * as v1_dictionary_common_dictionary_common_pb from "../../v1/dictionary_common/dictionary_common_pb";

export class ProductInGrowStage extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  getCultureGrowStageId(): number;
  setCultureGrowStageId(value: number): void;

  getCultureGrowStageName(): string;
  setCultureGrowStageName(value: string): void;

  getProductId(): number;
  setProductId(value: number): void;

  getProductName(): string;
  setProductName(value: string): void;

  hasHash(): boolean;
  clearHash(): void;
  getHash(): google_protobuf_wrappers_pb.StringValue | undefined;
  setHash(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasDeleted(): boolean;
  clearDeleted(): void;
  getDeleted(): google_protobuf_wrappers_pb.BoolValue | undefined;
  setDeleted(value?: google_protobuf_wrappers_pb.BoolValue): void;

  hasCreatedBy(): boolean;
  clearCreatedBy(): void;
  getCreatedBy(): google_protobuf_wrappers_pb.StringValue | undefined;
  setCreatedBy(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasCreatedAt(): boolean;
  clearCreatedAt(): void;
  getCreatedAt(): google_protobuf_timestamp_pb.Timestamp | undefined;
  setCreatedAt(value?: google_protobuf_timestamp_pb.Timestamp): void;

  hasUpdatedBy(): boolean;
  clearUpdatedBy(): void;
  getUpdatedBy(): google_protobuf_wrappers_pb.StringValue | undefined;
  setUpdatedBy(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasUpdatedAt(): boolean;
  clearUpdatedAt(): void;
  getUpdatedAt(): google_protobuf_timestamp_pb.Timestamp | undefined;
  setUpdatedAt(value?: google_protobuf_timestamp_pb.Timestamp): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ProductInGrowStage.AsObject;
  static toObject(includeInstance: boolean, msg: ProductInGrowStage): ProductInGrowStage.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ProductInGrowStage, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ProductInGrowStage;
  static deserializeBinaryFromReader(message: ProductInGrowStage, reader: jspb.BinaryReader): ProductInGrowStage;
}

export namespace ProductInGrowStage {
  export type AsObject = {
    id: number,
    cultureGrowStageId: number,
    cultureGrowStageName: string,
    productId: number,
    productName: string,
    hash?: google_protobuf_wrappers_pb.StringValue.AsObject,
    deleted?: google_protobuf_wrappers_pb.BoolValue.AsObject,
    createdBy?: google_protobuf_wrappers_pb.StringValue.AsObject,
    createdAt?: google_protobuf_timestamp_pb.Timestamp.AsObject,
    updatedBy?: google_protobuf_wrappers_pb.StringValue.AsObject,
    updatedAt?: google_protobuf_timestamp_pb.Timestamp.AsObject,
  }
}

export class ProductInGrowStageFilter extends jspb.Message {
  hasId(): boolean;
  clearId(): void;
  getId(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setId(value?: google_protobuf_wrappers_pb.Int64Value): void;

  hasCultureGrowStageId(): boolean;
  clearCultureGrowStageId(): void;
  getCultureGrowStageId(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setCultureGrowStageId(value?: google_protobuf_wrappers_pb.Int64Value): void;

  hasProductId(): boolean;
  clearProductId(): void;
  getProductId(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setProductId(value?: google_protobuf_wrappers_pb.Int64Value): void;

  hasDeleted(): boolean;
  clearDeleted(): void;
  getDeleted(): google_protobuf_wrappers_pb.BoolValue | undefined;
  setDeleted(value?: google_protobuf_wrappers_pb.BoolValue): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ProductInGrowStageFilter.AsObject;
  static toObject(includeInstance: boolean, msg: ProductInGrowStageFilter): ProductInGrowStageFilter.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ProductInGrowStageFilter, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ProductInGrowStageFilter;
  static deserializeBinaryFromReader(message: ProductInGrowStageFilter, reader: jspb.BinaryReader): ProductInGrowStageFilter;
}

export namespace ProductInGrowStageFilter {
  export type AsObject = {
    id?: google_protobuf_wrappers_pb.Int64Value.AsObject,
    cultureGrowStageId?: google_protobuf_wrappers_pb.Int64Value.AsObject,
    productId?: google_protobuf_wrappers_pb.Int64Value.AsObject,
    deleted?: google_protobuf_wrappers_pb.BoolValue.AsObject,
  }
}

export class ListProductInGrowStageRequest extends jspb.Message {
  hasFilter(): boolean;
  clearFilter(): void;
  getFilter(): ProductInGrowStageFilter | undefined;
  setFilter(value?: ProductInGrowStageFilter): void;

  clearSortingsList(): void;
  getSortingsList(): Array<v1_dictionary_common_dictionary_common_pb.Sorting>;
  setSortingsList(value: Array<v1_dictionary_common_dictionary_common_pb.Sorting>): void;
  addSortings(value?: v1_dictionary_common_dictionary_common_pb.Sorting, index?: number): v1_dictionary_common_dictionary_common_pb.Sorting;

  hasPagination(): boolean;
  clearPagination(): void;
  getPagination(): v1_dictionary_common_dictionary_common_pb.PaginationRequest | undefined;
  setPagination(value?: v1_dictionary_common_dictionary_common_pb.PaginationRequest): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListProductInGrowStageRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ListProductInGrowStageRequest): ListProductInGrowStageRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListProductInGrowStageRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListProductInGrowStageRequest;
  static deserializeBinaryFromReader(message: ListProductInGrowStageRequest, reader: jspb.BinaryReader): ListProductInGrowStageRequest;
}

export namespace ListProductInGrowStageRequest {
  export type AsObject = {
    filter?: ProductInGrowStageFilter.AsObject,
    sortingsList: Array<v1_dictionary_common_dictionary_common_pb.Sorting.AsObject>,
    pagination?: v1_dictionary_common_dictionary_common_pb.PaginationRequest.AsObject,
  }
}

export class ListProductInGrowStageResponse extends jspb.Message {
  clearItemsList(): void;
  getItemsList(): Array<ProductInGrowStage>;
  setItemsList(value: Array<ProductInGrowStage>): void;
  addItems(value?: ProductInGrowStage, index?: number): ProductInGrowStage;

  hasPagination(): boolean;
  clearPagination(): void;
  getPagination(): v1_dictionary_common_dictionary_common_pb.PaginationResponse | undefined;
  setPagination(value?: v1_dictionary_common_dictionary_common_pb.PaginationResponse): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListProductInGrowStageResponse.AsObject;
  static toObject(includeInstance: boolean, msg: ListProductInGrowStageResponse): ListProductInGrowStageResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListProductInGrowStageResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListProductInGrowStageResponse;
  static deserializeBinaryFromReader(message: ListProductInGrowStageResponse, reader: jspb.BinaryReader): ListProductInGrowStageResponse;
}

export namespace ListProductInGrowStageResponse {
  export type AsObject = {
    itemsList: Array<ProductInGrowStage.AsObject>,
    pagination?: v1_dictionary_common_dictionary_common_pb.PaginationResponse.AsObject,
  }
}

export class GetProductInGrowStageRequest extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetProductInGrowStageRequest.AsObject;
  static toObject(includeInstance: boolean, msg: GetProductInGrowStageRequest): GetProductInGrowStageRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetProductInGrowStageRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetProductInGrowStageRequest;
  static deserializeBinaryFromReader(message: GetProductInGrowStageRequest, reader: jspb.BinaryReader): GetProductInGrowStageRequest;
}

export namespace GetProductInGrowStageRequest {
  export type AsObject = {
    id: number,
  }
}

export class GetProductInGrowStageResponse extends jspb.Message {
  hasItem(): boolean;
  clearItem(): void;
  getItem(): ProductInGrowStage | undefined;
  setItem(value?: ProductInGrowStage): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetProductInGrowStageResponse.AsObject;
  static toObject(includeInstance: boolean, msg: GetProductInGrowStageResponse): GetProductInGrowStageResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetProductInGrowStageResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetProductInGrowStageResponse;
  static deserializeBinaryFromReader(message: GetProductInGrowStageResponse, reader: jspb.BinaryReader): GetProductInGrowStageResponse;
}

export namespace GetProductInGrowStageResponse {
  export type AsObject = {
    item?: ProductInGrowStage.AsObject,
  }
}

