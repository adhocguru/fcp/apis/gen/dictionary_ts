// package: fcp.dictionary.v1.dictionary
// file: v1/dictionary/vermin.proto

import * as jspb from "google-protobuf";
import * as google_protobuf_wrappers_pb from "google-protobuf/google/protobuf/wrappers_pb";
import * as google_protobuf_timestamp_pb from "google-protobuf/google/protobuf/timestamp_pb";
import * as v1_dictionary_common_dictionary_common_pb from "../../v1/dictionary_common/dictionary_common_pb";

export class Vermin extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  getGuid(): string;
  setGuid(value: string): void;

  getName(): string;
  setName(value: string): void;

  getVerminGroupId(): number;
  setVerminGroupId(value: number): void;

  getVerminGroupName(): string;
  setVerminGroupName(value: string): void;

  getPhoto(): string;
  setPhoto(value: string): void;

  getOrd(): string;
  setOrd(value: string): void;

  hasHash(): boolean;
  clearHash(): void;
  getHash(): google_protobuf_wrappers_pb.StringValue | undefined;
  setHash(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasDeleted(): boolean;
  clearDeleted(): void;
  getDeleted(): google_protobuf_wrappers_pb.BoolValue | undefined;
  setDeleted(value?: google_protobuf_wrappers_pb.BoolValue): void;

  hasCreatedBy(): boolean;
  clearCreatedBy(): void;
  getCreatedBy(): google_protobuf_wrappers_pb.StringValue | undefined;
  setCreatedBy(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasCreatedAt(): boolean;
  clearCreatedAt(): void;
  getCreatedAt(): google_protobuf_timestamp_pb.Timestamp | undefined;
  setCreatedAt(value?: google_protobuf_timestamp_pb.Timestamp): void;

  hasUpdatedBy(): boolean;
  clearUpdatedBy(): void;
  getUpdatedBy(): google_protobuf_wrappers_pb.StringValue | undefined;
  setUpdatedBy(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasUpdatedAt(): boolean;
  clearUpdatedAt(): void;
  getUpdatedAt(): google_protobuf_timestamp_pb.Timestamp | undefined;
  setUpdatedAt(value?: google_protobuf_timestamp_pb.Timestamp): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): Vermin.AsObject;
  static toObject(includeInstance: boolean, msg: Vermin): Vermin.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: Vermin, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): Vermin;
  static deserializeBinaryFromReader(message: Vermin, reader: jspb.BinaryReader): Vermin;
}

export namespace Vermin {
  export type AsObject = {
    id: number,
    guid: string,
    name: string,
    verminGroupId: number,
    verminGroupName: string,
    photo: string,
    ord: string,
    hash?: google_protobuf_wrappers_pb.StringValue.AsObject,
    deleted?: google_protobuf_wrappers_pb.BoolValue.AsObject,
    createdBy?: google_protobuf_wrappers_pb.StringValue.AsObject,
    createdAt?: google_protobuf_timestamp_pb.Timestamp.AsObject,
    updatedBy?: google_protobuf_wrappers_pb.StringValue.AsObject,
    updatedAt?: google_protobuf_timestamp_pb.Timestamp.AsObject,
  }
}

export class VerminFilter extends jspb.Message {
  hasId(): boolean;
  clearId(): void;
  getId(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setId(value?: google_protobuf_wrappers_pb.Int64Value): void;

  hasName(): boolean;
  clearName(): void;
  getName(): google_protobuf_wrappers_pb.StringValue | undefined;
  setName(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasVerminGroupId(): boolean;
  clearVerminGroupId(): void;
  getVerminGroupId(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setVerminGroupId(value?: google_protobuf_wrappers_pb.Int64Value): void;

  hasDeleted(): boolean;
  clearDeleted(): void;
  getDeleted(): google_protobuf_wrappers_pb.BoolValue | undefined;
  setDeleted(value?: google_protobuf_wrappers_pb.BoolValue): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): VerminFilter.AsObject;
  static toObject(includeInstance: boolean, msg: VerminFilter): VerminFilter.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: VerminFilter, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): VerminFilter;
  static deserializeBinaryFromReader(message: VerminFilter, reader: jspb.BinaryReader): VerminFilter;
}

export namespace VerminFilter {
  export type AsObject = {
    id?: google_protobuf_wrappers_pb.Int64Value.AsObject,
    name?: google_protobuf_wrappers_pb.StringValue.AsObject,
    verminGroupId?: google_protobuf_wrappers_pb.Int64Value.AsObject,
    deleted?: google_protobuf_wrappers_pb.BoolValue.AsObject,
  }
}

export class ListVerminRequest extends jspb.Message {
  hasFilter(): boolean;
  clearFilter(): void;
  getFilter(): VerminFilter | undefined;
  setFilter(value?: VerminFilter): void;

  clearSortingsList(): void;
  getSortingsList(): Array<v1_dictionary_common_dictionary_common_pb.Sorting>;
  setSortingsList(value: Array<v1_dictionary_common_dictionary_common_pb.Sorting>): void;
  addSortings(value?: v1_dictionary_common_dictionary_common_pb.Sorting, index?: number): v1_dictionary_common_dictionary_common_pb.Sorting;

  hasPagination(): boolean;
  clearPagination(): void;
  getPagination(): v1_dictionary_common_dictionary_common_pb.PaginationRequest | undefined;
  setPagination(value?: v1_dictionary_common_dictionary_common_pb.PaginationRequest): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListVerminRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ListVerminRequest): ListVerminRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListVerminRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListVerminRequest;
  static deserializeBinaryFromReader(message: ListVerminRequest, reader: jspb.BinaryReader): ListVerminRequest;
}

export namespace ListVerminRequest {
  export type AsObject = {
    filter?: VerminFilter.AsObject,
    sortingsList: Array<v1_dictionary_common_dictionary_common_pb.Sorting.AsObject>,
    pagination?: v1_dictionary_common_dictionary_common_pb.PaginationRequest.AsObject,
  }
}

export class ListVerminResponse extends jspb.Message {
  clearItemsList(): void;
  getItemsList(): Array<Vermin>;
  setItemsList(value: Array<Vermin>): void;
  addItems(value?: Vermin, index?: number): Vermin;

  hasPagination(): boolean;
  clearPagination(): void;
  getPagination(): v1_dictionary_common_dictionary_common_pb.PaginationResponse | undefined;
  setPagination(value?: v1_dictionary_common_dictionary_common_pb.PaginationResponse): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListVerminResponse.AsObject;
  static toObject(includeInstance: boolean, msg: ListVerminResponse): ListVerminResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListVerminResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListVerminResponse;
  static deserializeBinaryFromReader(message: ListVerminResponse, reader: jspb.BinaryReader): ListVerminResponse;
}

export namespace ListVerminResponse {
  export type AsObject = {
    itemsList: Array<Vermin.AsObject>,
    pagination?: v1_dictionary_common_dictionary_common_pb.PaginationResponse.AsObject,
  }
}

export class GetVerminRequest extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetVerminRequest.AsObject;
  static toObject(includeInstance: boolean, msg: GetVerminRequest): GetVerminRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetVerminRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetVerminRequest;
  static deserializeBinaryFromReader(message: GetVerminRequest, reader: jspb.BinaryReader): GetVerminRequest;
}

export namespace GetVerminRequest {
  export type AsObject = {
    id: number,
  }
}

export class GetVerminResponse extends jspb.Message {
  hasItem(): boolean;
  clearItem(): void;
  getItem(): Vermin | undefined;
  setItem(value?: Vermin): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetVerminResponse.AsObject;
  static toObject(includeInstance: boolean, msg: GetVerminResponse): GetVerminResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetVerminResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetVerminResponse;
  static deserializeBinaryFromReader(message: GetVerminResponse, reader: jspb.BinaryReader): GetVerminResponse;
}

export namespace GetVerminResponse {
  export type AsObject = {
    item?: Vermin.AsObject,
  }
}

