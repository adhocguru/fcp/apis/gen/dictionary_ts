// package: fcp.dictionary.v1.dictionary
// file: v1/dictionary/country.proto

var v1_dictionary_country_pb = require("../../v1/dictionary/country_pb");
var google_protobuf_empty_pb = require("google-protobuf/google/protobuf/empty_pb");
var grpc = require("@improbable-eng/grpc-web").grpc;

var CountryService = (function () {
  function CountryService() {}
  CountryService.serviceName = "fcp.dictionary.v1.dictionary.CountryService";
  return CountryService;
}());

CountryService.ListCountry = {
  methodName: "ListCountry",
  service: CountryService,
  requestStream: false,
  responseStream: false,
  requestType: v1_dictionary_country_pb.ListCountryRequest,
  responseType: v1_dictionary_country_pb.ListCountryResponse
};

CountryService.GetCountry = {
  methodName: "GetCountry",
  service: CountryService,
  requestStream: false,
  responseStream: false,
  requestType: v1_dictionary_country_pb.GetCountryRequest,
  responseType: v1_dictionary_country_pb.GetCountryResponse
};

CountryService.CreateCountry = {
  methodName: "CreateCountry",
  service: CountryService,
  requestStream: false,
  responseStream: false,
  requestType: v1_dictionary_country_pb.CreateCountryRequest,
  responseType: v1_dictionary_country_pb.CreateCountryResponse
};

CountryService.UpdateCountry = {
  methodName: "UpdateCountry",
  service: CountryService,
  requestStream: false,
  responseStream: false,
  requestType: v1_dictionary_country_pb.UpdateCountryRequest,
  responseType: v1_dictionary_country_pb.UpdateCountryResponse
};

CountryService.DeleteCountry = {
  methodName: "DeleteCountry",
  service: CountryService,
  requestStream: false,
  responseStream: false,
  requestType: v1_dictionary_country_pb.DeleteCountryRequest,
  responseType: google_protobuf_empty_pb.Empty
};

CountryService.RestoreCountry = {
  methodName: "RestoreCountry",
  service: CountryService,
  requestStream: false,
  responseStream: false,
  requestType: v1_dictionary_country_pb.RestoreCountryRequest,
  responseType: google_protobuf_empty_pb.Empty
};

exports.CountryService = CountryService;

function CountryServiceClient(serviceHost, options) {
  this.serviceHost = serviceHost;
  this.options = options || {};
}

CountryServiceClient.prototype.listCountry = function listCountry(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(CountryService.ListCountry, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

CountryServiceClient.prototype.getCountry = function getCountry(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(CountryService.GetCountry, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

CountryServiceClient.prototype.createCountry = function createCountry(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(CountryService.CreateCountry, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

CountryServiceClient.prototype.updateCountry = function updateCountry(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(CountryService.UpdateCountry, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

CountryServiceClient.prototype.deleteCountry = function deleteCountry(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(CountryService.DeleteCountry, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

CountryServiceClient.prototype.restoreCountry = function restoreCountry(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(CountryService.RestoreCountry, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

exports.CountryServiceClient = CountryServiceClient;

