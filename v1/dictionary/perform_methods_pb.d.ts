// package: fcp.dictionary.v1.dictionary
// file: v1/dictionary/perform_methods.proto

import * as jspb from "google-protobuf";
import * as google_protobuf_wrappers_pb from "google-protobuf/google/protobuf/wrappers_pb";
import * as google_protobuf_timestamp_pb from "google-protobuf/google/protobuf/timestamp_pb";
import * as v1_dictionary_common_dictionary_common_pb from "../../v1/dictionary_common/dictionary_common_pb";

export class PerformMethods extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  getGuid(): string;
  setGuid(value: string): void;

  getName(): string;
  setName(value: string): void;

  getOrd(): number;
  setOrd(value: number): void;

  hasHash(): boolean;
  clearHash(): void;
  getHash(): google_protobuf_wrappers_pb.StringValue | undefined;
  setHash(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasDeleted(): boolean;
  clearDeleted(): void;
  getDeleted(): google_protobuf_wrappers_pb.BoolValue | undefined;
  setDeleted(value?: google_protobuf_wrappers_pb.BoolValue): void;

  hasCreatedBy(): boolean;
  clearCreatedBy(): void;
  getCreatedBy(): google_protobuf_wrappers_pb.StringValue | undefined;
  setCreatedBy(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasCreatedAt(): boolean;
  clearCreatedAt(): void;
  getCreatedAt(): google_protobuf_timestamp_pb.Timestamp | undefined;
  setCreatedAt(value?: google_protobuf_timestamp_pb.Timestamp): void;

  hasUpdatedBy(): boolean;
  clearUpdatedBy(): void;
  getUpdatedBy(): google_protobuf_wrappers_pb.StringValue | undefined;
  setUpdatedBy(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasUpdatedAt(): boolean;
  clearUpdatedAt(): void;
  getUpdatedAt(): google_protobuf_timestamp_pb.Timestamp | undefined;
  setUpdatedAt(value?: google_protobuf_timestamp_pb.Timestamp): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): PerformMethods.AsObject;
  static toObject(includeInstance: boolean, msg: PerformMethods): PerformMethods.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: PerformMethods, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): PerformMethods;
  static deserializeBinaryFromReader(message: PerformMethods, reader: jspb.BinaryReader): PerformMethods;
}

export namespace PerformMethods {
  export type AsObject = {
    id: number,
    guid: string,
    name: string,
    ord: number,
    hash?: google_protobuf_wrappers_pb.StringValue.AsObject,
    deleted?: google_protobuf_wrappers_pb.BoolValue.AsObject,
    createdBy?: google_protobuf_wrappers_pb.StringValue.AsObject,
    createdAt?: google_protobuf_timestamp_pb.Timestamp.AsObject,
    updatedBy?: google_protobuf_wrappers_pb.StringValue.AsObject,
    updatedAt?: google_protobuf_timestamp_pb.Timestamp.AsObject,
  }
}

export class PerformMethodsFilter extends jspb.Message {
  hasId(): boolean;
  clearId(): void;
  getId(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setId(value?: google_protobuf_wrappers_pb.Int64Value): void;

  hasGuid(): boolean;
  clearGuid(): void;
  getGuid(): google_protobuf_wrappers_pb.StringValue | undefined;
  setGuid(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasName(): boolean;
  clearName(): void;
  getName(): google_protobuf_wrappers_pb.StringValue | undefined;
  setName(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasDeleted(): boolean;
  clearDeleted(): void;
  getDeleted(): google_protobuf_wrappers_pb.BoolValue | undefined;
  setDeleted(value?: google_protobuf_wrappers_pb.BoolValue): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): PerformMethodsFilter.AsObject;
  static toObject(includeInstance: boolean, msg: PerformMethodsFilter): PerformMethodsFilter.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: PerformMethodsFilter, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): PerformMethodsFilter;
  static deserializeBinaryFromReader(message: PerformMethodsFilter, reader: jspb.BinaryReader): PerformMethodsFilter;
}

export namespace PerformMethodsFilter {
  export type AsObject = {
    id?: google_protobuf_wrappers_pb.Int64Value.AsObject,
    guid?: google_protobuf_wrappers_pb.StringValue.AsObject,
    name?: google_protobuf_wrappers_pb.StringValue.AsObject,
    deleted?: google_protobuf_wrappers_pb.BoolValue.AsObject,
  }
}

export class ListPerformMethodsRequest extends jspb.Message {
  hasFilter(): boolean;
  clearFilter(): void;
  getFilter(): PerformMethodsFilter | undefined;
  setFilter(value?: PerformMethodsFilter): void;

  clearSortingsList(): void;
  getSortingsList(): Array<v1_dictionary_common_dictionary_common_pb.Sorting>;
  setSortingsList(value: Array<v1_dictionary_common_dictionary_common_pb.Sorting>): void;
  addSortings(value?: v1_dictionary_common_dictionary_common_pb.Sorting, index?: number): v1_dictionary_common_dictionary_common_pb.Sorting;

  hasPagination(): boolean;
  clearPagination(): void;
  getPagination(): v1_dictionary_common_dictionary_common_pb.PaginationRequest | undefined;
  setPagination(value?: v1_dictionary_common_dictionary_common_pb.PaginationRequest): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListPerformMethodsRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ListPerformMethodsRequest): ListPerformMethodsRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListPerformMethodsRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListPerformMethodsRequest;
  static deserializeBinaryFromReader(message: ListPerformMethodsRequest, reader: jspb.BinaryReader): ListPerformMethodsRequest;
}

export namespace ListPerformMethodsRequest {
  export type AsObject = {
    filter?: PerformMethodsFilter.AsObject,
    sortingsList: Array<v1_dictionary_common_dictionary_common_pb.Sorting.AsObject>,
    pagination?: v1_dictionary_common_dictionary_common_pb.PaginationRequest.AsObject,
  }
}

export class ListPerformMethodsResponse extends jspb.Message {
  clearItemsList(): void;
  getItemsList(): Array<PerformMethods>;
  setItemsList(value: Array<PerformMethods>): void;
  addItems(value?: PerformMethods, index?: number): PerformMethods;

  hasPagination(): boolean;
  clearPagination(): void;
  getPagination(): v1_dictionary_common_dictionary_common_pb.PaginationResponse | undefined;
  setPagination(value?: v1_dictionary_common_dictionary_common_pb.PaginationResponse): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListPerformMethodsResponse.AsObject;
  static toObject(includeInstance: boolean, msg: ListPerformMethodsResponse): ListPerformMethodsResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListPerformMethodsResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListPerformMethodsResponse;
  static deserializeBinaryFromReader(message: ListPerformMethodsResponse, reader: jspb.BinaryReader): ListPerformMethodsResponse;
}

export namespace ListPerformMethodsResponse {
  export type AsObject = {
    itemsList: Array<PerformMethods.AsObject>,
    pagination?: v1_dictionary_common_dictionary_common_pb.PaginationResponse.AsObject,
  }
}

export class GetPerformMethodsRequest extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetPerformMethodsRequest.AsObject;
  static toObject(includeInstance: boolean, msg: GetPerformMethodsRequest): GetPerformMethodsRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetPerformMethodsRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetPerformMethodsRequest;
  static deserializeBinaryFromReader(message: GetPerformMethodsRequest, reader: jspb.BinaryReader): GetPerformMethodsRequest;
}

export namespace GetPerformMethodsRequest {
  export type AsObject = {
    id: number,
  }
}

export class GetPerformMethodsResponse extends jspb.Message {
  hasItem(): boolean;
  clearItem(): void;
  getItem(): PerformMethods | undefined;
  setItem(value?: PerformMethods): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetPerformMethodsResponse.AsObject;
  static toObject(includeInstance: boolean, msg: GetPerformMethodsResponse): GetPerformMethodsResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetPerformMethodsResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetPerformMethodsResponse;
  static deserializeBinaryFromReader(message: GetPerformMethodsResponse, reader: jspb.BinaryReader): GetPerformMethodsResponse;
}

export namespace GetPerformMethodsResponse {
  export type AsObject = {
    item?: PerformMethods.AsObject,
  }
}

