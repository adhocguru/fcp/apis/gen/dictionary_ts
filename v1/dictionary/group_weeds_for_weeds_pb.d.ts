// package: fcp.dictionary.v1.dictionary
// file: v1/dictionary/group_weeds_for_weeds.proto

import * as jspb from "google-protobuf";
import * as google_protobuf_wrappers_pb from "google-protobuf/google/protobuf/wrappers_pb";
import * as google_protobuf_timestamp_pb from "google-protobuf/google/protobuf/timestamp_pb";
import * as v1_dictionary_common_dictionary_common_pb from "../../v1/dictionary_common/dictionary_common_pb";

export class GroupWeedsForWeeds extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  getVerminId(): number;
  setVerminId(value: number): void;

  getVerminName(): string;
  setVerminName(value: string): void;

  getGroupWeedsId(): number;
  setGroupWeedsId(value: number): void;

  getGroupWeedsName(): string;
  setGroupWeedsName(value: string): void;

  hasHash(): boolean;
  clearHash(): void;
  getHash(): google_protobuf_wrappers_pb.StringValue | undefined;
  setHash(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasDeleted(): boolean;
  clearDeleted(): void;
  getDeleted(): google_protobuf_wrappers_pb.BoolValue | undefined;
  setDeleted(value?: google_protobuf_wrappers_pb.BoolValue): void;

  hasCreatedBy(): boolean;
  clearCreatedBy(): void;
  getCreatedBy(): google_protobuf_wrappers_pb.StringValue | undefined;
  setCreatedBy(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasCreatedAt(): boolean;
  clearCreatedAt(): void;
  getCreatedAt(): google_protobuf_timestamp_pb.Timestamp | undefined;
  setCreatedAt(value?: google_protobuf_timestamp_pb.Timestamp): void;

  hasUpdatedBy(): boolean;
  clearUpdatedBy(): void;
  getUpdatedBy(): google_protobuf_wrappers_pb.StringValue | undefined;
  setUpdatedBy(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasUpdatedAt(): boolean;
  clearUpdatedAt(): void;
  getUpdatedAt(): google_protobuf_timestamp_pb.Timestamp | undefined;
  setUpdatedAt(value?: google_protobuf_timestamp_pb.Timestamp): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GroupWeedsForWeeds.AsObject;
  static toObject(includeInstance: boolean, msg: GroupWeedsForWeeds): GroupWeedsForWeeds.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GroupWeedsForWeeds, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GroupWeedsForWeeds;
  static deserializeBinaryFromReader(message: GroupWeedsForWeeds, reader: jspb.BinaryReader): GroupWeedsForWeeds;
}

export namespace GroupWeedsForWeeds {
  export type AsObject = {
    id: number,
    verminId: number,
    verminName: string,
    groupWeedsId: number,
    groupWeedsName: string,
    hash?: google_protobuf_wrappers_pb.StringValue.AsObject,
    deleted?: google_protobuf_wrappers_pb.BoolValue.AsObject,
    createdBy?: google_protobuf_wrappers_pb.StringValue.AsObject,
    createdAt?: google_protobuf_timestamp_pb.Timestamp.AsObject,
    updatedBy?: google_protobuf_wrappers_pb.StringValue.AsObject,
    updatedAt?: google_protobuf_timestamp_pb.Timestamp.AsObject,
  }
}

export class GroupWeedsForWeedsFilter extends jspb.Message {
  hasId(): boolean;
  clearId(): void;
  getId(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setId(value?: google_protobuf_wrappers_pb.Int64Value): void;

  hasVerminId(): boolean;
  clearVerminId(): void;
  getVerminId(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setVerminId(value?: google_protobuf_wrappers_pb.Int64Value): void;

  hasGroupWeedsId(): boolean;
  clearGroupWeedsId(): void;
  getGroupWeedsId(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setGroupWeedsId(value?: google_protobuf_wrappers_pb.Int64Value): void;

  hasDeleted(): boolean;
  clearDeleted(): void;
  getDeleted(): google_protobuf_wrappers_pb.BoolValue | undefined;
  setDeleted(value?: google_protobuf_wrappers_pb.BoolValue): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GroupWeedsForWeedsFilter.AsObject;
  static toObject(includeInstance: boolean, msg: GroupWeedsForWeedsFilter): GroupWeedsForWeedsFilter.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GroupWeedsForWeedsFilter, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GroupWeedsForWeedsFilter;
  static deserializeBinaryFromReader(message: GroupWeedsForWeedsFilter, reader: jspb.BinaryReader): GroupWeedsForWeedsFilter;
}

export namespace GroupWeedsForWeedsFilter {
  export type AsObject = {
    id?: google_protobuf_wrappers_pb.Int64Value.AsObject,
    verminId?: google_protobuf_wrappers_pb.Int64Value.AsObject,
    groupWeedsId?: google_protobuf_wrappers_pb.Int64Value.AsObject,
    deleted?: google_protobuf_wrappers_pb.BoolValue.AsObject,
  }
}

export class ListGroupWeedsForWeedsRequest extends jspb.Message {
  hasFilter(): boolean;
  clearFilter(): void;
  getFilter(): GroupWeedsForWeedsFilter | undefined;
  setFilter(value?: GroupWeedsForWeedsFilter): void;

  clearSortingsList(): void;
  getSortingsList(): Array<v1_dictionary_common_dictionary_common_pb.Sorting>;
  setSortingsList(value: Array<v1_dictionary_common_dictionary_common_pb.Sorting>): void;
  addSortings(value?: v1_dictionary_common_dictionary_common_pb.Sorting, index?: number): v1_dictionary_common_dictionary_common_pb.Sorting;

  hasPagination(): boolean;
  clearPagination(): void;
  getPagination(): v1_dictionary_common_dictionary_common_pb.PaginationRequest | undefined;
  setPagination(value?: v1_dictionary_common_dictionary_common_pb.PaginationRequest): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListGroupWeedsForWeedsRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ListGroupWeedsForWeedsRequest): ListGroupWeedsForWeedsRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListGroupWeedsForWeedsRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListGroupWeedsForWeedsRequest;
  static deserializeBinaryFromReader(message: ListGroupWeedsForWeedsRequest, reader: jspb.BinaryReader): ListGroupWeedsForWeedsRequest;
}

export namespace ListGroupWeedsForWeedsRequest {
  export type AsObject = {
    filter?: GroupWeedsForWeedsFilter.AsObject,
    sortingsList: Array<v1_dictionary_common_dictionary_common_pb.Sorting.AsObject>,
    pagination?: v1_dictionary_common_dictionary_common_pb.PaginationRequest.AsObject,
  }
}

export class ListGroupWeedsForWeedsResponse extends jspb.Message {
  clearItemsList(): void;
  getItemsList(): Array<GroupWeedsForWeeds>;
  setItemsList(value: Array<GroupWeedsForWeeds>): void;
  addItems(value?: GroupWeedsForWeeds, index?: number): GroupWeedsForWeeds;

  hasPagination(): boolean;
  clearPagination(): void;
  getPagination(): v1_dictionary_common_dictionary_common_pb.PaginationResponse | undefined;
  setPagination(value?: v1_dictionary_common_dictionary_common_pb.PaginationResponse): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListGroupWeedsForWeedsResponse.AsObject;
  static toObject(includeInstance: boolean, msg: ListGroupWeedsForWeedsResponse): ListGroupWeedsForWeedsResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListGroupWeedsForWeedsResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListGroupWeedsForWeedsResponse;
  static deserializeBinaryFromReader(message: ListGroupWeedsForWeedsResponse, reader: jspb.BinaryReader): ListGroupWeedsForWeedsResponse;
}

export namespace ListGroupWeedsForWeedsResponse {
  export type AsObject = {
    itemsList: Array<GroupWeedsForWeeds.AsObject>,
    pagination?: v1_dictionary_common_dictionary_common_pb.PaginationResponse.AsObject,
  }
}

export class GetGroupWeedsForWeedsRequest extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetGroupWeedsForWeedsRequest.AsObject;
  static toObject(includeInstance: boolean, msg: GetGroupWeedsForWeedsRequest): GetGroupWeedsForWeedsRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetGroupWeedsForWeedsRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetGroupWeedsForWeedsRequest;
  static deserializeBinaryFromReader(message: GetGroupWeedsForWeedsRequest, reader: jspb.BinaryReader): GetGroupWeedsForWeedsRequest;
}

export namespace GetGroupWeedsForWeedsRequest {
  export type AsObject = {
    id: number,
  }
}

export class GetGroupWeedsForWeedsResponse extends jspb.Message {
  hasItem(): boolean;
  clearItem(): void;
  getItem(): GroupWeedsForWeeds | undefined;
  setItem(value?: GroupWeedsForWeeds): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetGroupWeedsForWeedsResponse.AsObject;
  static toObject(includeInstance: boolean, msg: GetGroupWeedsForWeedsResponse): GetGroupWeedsForWeedsResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetGroupWeedsForWeedsResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetGroupWeedsForWeedsResponse;
  static deserializeBinaryFromReader(message: GetGroupWeedsForWeedsResponse, reader: jspb.BinaryReader): GetGroupWeedsForWeedsResponse;
}

export namespace GetGroupWeedsForWeedsResponse {
  export type AsObject = {
    item?: GroupWeedsForWeeds.AsObject,
  }
}

