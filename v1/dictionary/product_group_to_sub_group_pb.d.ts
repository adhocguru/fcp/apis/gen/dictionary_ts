// package: fcp.dictionary.v1.dictionary
// file: v1/dictionary/product_group_to_sub_group.proto

import * as jspb from "google-protobuf";
import * as google_protobuf_wrappers_pb from "google-protobuf/google/protobuf/wrappers_pb";
import * as google_protobuf_timestamp_pb from "google-protobuf/google/protobuf/timestamp_pb";
import * as v1_dictionary_common_dictionary_common_pb from "../../v1/dictionary_common/dictionary_common_pb";

export class ProductGroupToSubGroup extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  getProductGroupId(): number;
  setProductGroupId(value: number): void;

  getProductGroupName(): string;
  setProductGroupName(value: string): void;

  getProductSubGroupId(): number;
  setProductSubGroupId(value: number): void;

  getProductSubGroupName(): string;
  setProductSubGroupName(value: string): void;

  hasHash(): boolean;
  clearHash(): void;
  getHash(): google_protobuf_wrappers_pb.StringValue | undefined;
  setHash(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasDeleted(): boolean;
  clearDeleted(): void;
  getDeleted(): google_protobuf_wrappers_pb.BoolValue | undefined;
  setDeleted(value?: google_protobuf_wrappers_pb.BoolValue): void;

  hasCreatedBy(): boolean;
  clearCreatedBy(): void;
  getCreatedBy(): google_protobuf_wrappers_pb.StringValue | undefined;
  setCreatedBy(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasCreatedAt(): boolean;
  clearCreatedAt(): void;
  getCreatedAt(): google_protobuf_timestamp_pb.Timestamp | undefined;
  setCreatedAt(value?: google_protobuf_timestamp_pb.Timestamp): void;

  hasUpdatedBy(): boolean;
  clearUpdatedBy(): void;
  getUpdatedBy(): google_protobuf_wrappers_pb.StringValue | undefined;
  setUpdatedBy(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasUpdatedAt(): boolean;
  clearUpdatedAt(): void;
  getUpdatedAt(): google_protobuf_timestamp_pb.Timestamp | undefined;
  setUpdatedAt(value?: google_protobuf_timestamp_pb.Timestamp): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ProductGroupToSubGroup.AsObject;
  static toObject(includeInstance: boolean, msg: ProductGroupToSubGroup): ProductGroupToSubGroup.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ProductGroupToSubGroup, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ProductGroupToSubGroup;
  static deserializeBinaryFromReader(message: ProductGroupToSubGroup, reader: jspb.BinaryReader): ProductGroupToSubGroup;
}

export namespace ProductGroupToSubGroup {
  export type AsObject = {
    id: number,
    productGroupId: number,
    productGroupName: string,
    productSubGroupId: number,
    productSubGroupName: string,
    hash?: google_protobuf_wrappers_pb.StringValue.AsObject,
    deleted?: google_protobuf_wrappers_pb.BoolValue.AsObject,
    createdBy?: google_protobuf_wrappers_pb.StringValue.AsObject,
    createdAt?: google_protobuf_timestamp_pb.Timestamp.AsObject,
    updatedBy?: google_protobuf_wrappers_pb.StringValue.AsObject,
    updatedAt?: google_protobuf_timestamp_pb.Timestamp.AsObject,
  }
}

export class ProductGroupToSubGroupFilter extends jspb.Message {
  hasId(): boolean;
  clearId(): void;
  getId(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setId(value?: google_protobuf_wrappers_pb.Int64Value): void;

  hasProductGroupId(): boolean;
  clearProductGroupId(): void;
  getProductGroupId(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setProductGroupId(value?: google_protobuf_wrappers_pb.Int64Value): void;

  hasProductSubGroupId(): boolean;
  clearProductSubGroupId(): void;
  getProductSubGroupId(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setProductSubGroupId(value?: google_protobuf_wrappers_pb.Int64Value): void;

  hasDeleted(): boolean;
  clearDeleted(): void;
  getDeleted(): google_protobuf_wrappers_pb.BoolValue | undefined;
  setDeleted(value?: google_protobuf_wrappers_pb.BoolValue): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ProductGroupToSubGroupFilter.AsObject;
  static toObject(includeInstance: boolean, msg: ProductGroupToSubGroupFilter): ProductGroupToSubGroupFilter.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ProductGroupToSubGroupFilter, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ProductGroupToSubGroupFilter;
  static deserializeBinaryFromReader(message: ProductGroupToSubGroupFilter, reader: jspb.BinaryReader): ProductGroupToSubGroupFilter;
}

export namespace ProductGroupToSubGroupFilter {
  export type AsObject = {
    id?: google_protobuf_wrappers_pb.Int64Value.AsObject,
    productGroupId?: google_protobuf_wrappers_pb.Int64Value.AsObject,
    productSubGroupId?: google_protobuf_wrappers_pb.Int64Value.AsObject,
    deleted?: google_protobuf_wrappers_pb.BoolValue.AsObject,
  }
}

export class ListProductGroupToSubGroupRequest extends jspb.Message {
  hasFilter(): boolean;
  clearFilter(): void;
  getFilter(): ProductGroupToSubGroupFilter | undefined;
  setFilter(value?: ProductGroupToSubGroupFilter): void;

  clearSortingsList(): void;
  getSortingsList(): Array<v1_dictionary_common_dictionary_common_pb.Sorting>;
  setSortingsList(value: Array<v1_dictionary_common_dictionary_common_pb.Sorting>): void;
  addSortings(value?: v1_dictionary_common_dictionary_common_pb.Sorting, index?: number): v1_dictionary_common_dictionary_common_pb.Sorting;

  hasPagination(): boolean;
  clearPagination(): void;
  getPagination(): v1_dictionary_common_dictionary_common_pb.PaginationRequest | undefined;
  setPagination(value?: v1_dictionary_common_dictionary_common_pb.PaginationRequest): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListProductGroupToSubGroupRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ListProductGroupToSubGroupRequest): ListProductGroupToSubGroupRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListProductGroupToSubGroupRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListProductGroupToSubGroupRequest;
  static deserializeBinaryFromReader(message: ListProductGroupToSubGroupRequest, reader: jspb.BinaryReader): ListProductGroupToSubGroupRequest;
}

export namespace ListProductGroupToSubGroupRequest {
  export type AsObject = {
    filter?: ProductGroupToSubGroupFilter.AsObject,
    sortingsList: Array<v1_dictionary_common_dictionary_common_pb.Sorting.AsObject>,
    pagination?: v1_dictionary_common_dictionary_common_pb.PaginationRequest.AsObject,
  }
}

export class ListProductGroupToSubGroupResponse extends jspb.Message {
  clearItemsList(): void;
  getItemsList(): Array<ProductGroupToSubGroup>;
  setItemsList(value: Array<ProductGroupToSubGroup>): void;
  addItems(value?: ProductGroupToSubGroup, index?: number): ProductGroupToSubGroup;

  hasPagination(): boolean;
  clearPagination(): void;
  getPagination(): v1_dictionary_common_dictionary_common_pb.PaginationResponse | undefined;
  setPagination(value?: v1_dictionary_common_dictionary_common_pb.PaginationResponse): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListProductGroupToSubGroupResponse.AsObject;
  static toObject(includeInstance: boolean, msg: ListProductGroupToSubGroupResponse): ListProductGroupToSubGroupResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListProductGroupToSubGroupResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListProductGroupToSubGroupResponse;
  static deserializeBinaryFromReader(message: ListProductGroupToSubGroupResponse, reader: jspb.BinaryReader): ListProductGroupToSubGroupResponse;
}

export namespace ListProductGroupToSubGroupResponse {
  export type AsObject = {
    itemsList: Array<ProductGroupToSubGroup.AsObject>,
    pagination?: v1_dictionary_common_dictionary_common_pb.PaginationResponse.AsObject,
  }
}

export class GetProductGroupToSubGroupRequest extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetProductGroupToSubGroupRequest.AsObject;
  static toObject(includeInstance: boolean, msg: GetProductGroupToSubGroupRequest): GetProductGroupToSubGroupRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetProductGroupToSubGroupRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetProductGroupToSubGroupRequest;
  static deserializeBinaryFromReader(message: GetProductGroupToSubGroupRequest, reader: jspb.BinaryReader): GetProductGroupToSubGroupRequest;
}

export namespace GetProductGroupToSubGroupRequest {
  export type AsObject = {
    id: number,
  }
}

export class GetProductGroupToSubGroupResponse extends jspb.Message {
  hasItem(): boolean;
  clearItem(): void;
  getItem(): ProductGroupToSubGroup | undefined;
  setItem(value?: ProductGroupToSubGroup): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetProductGroupToSubGroupResponse.AsObject;
  static toObject(includeInstance: boolean, msg: GetProductGroupToSubGroupResponse): GetProductGroupToSubGroupResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetProductGroupToSubGroupResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetProductGroupToSubGroupResponse;
  static deserializeBinaryFromReader(message: GetProductGroupToSubGroupResponse, reader: jspb.BinaryReader): GetProductGroupToSubGroupResponse;
}

export namespace GetProductGroupToSubGroupResponse {
  export type AsObject = {
    item?: ProductGroupToSubGroup.AsObject,
  }
}

