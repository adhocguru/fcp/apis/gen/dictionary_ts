// package: fcp.dictionary.v1.dictionary
// file: v1/dictionary/insect_family.proto

import * as jspb from "google-protobuf";
import * as google_protobuf_wrappers_pb from "google-protobuf/google/protobuf/wrappers_pb";
import * as google_protobuf_timestamp_pb from "google-protobuf/google/protobuf/timestamp_pb";
import * as v1_dictionary_common_dictionary_common_pb from "../../v1/dictionary_common/dictionary_common_pb";

export class InsectFamily extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  getName(): string;
  setName(value: string): void;

  hasHash(): boolean;
  clearHash(): void;
  getHash(): google_protobuf_wrappers_pb.StringValue | undefined;
  setHash(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasDeleted(): boolean;
  clearDeleted(): void;
  getDeleted(): google_protobuf_wrappers_pb.BoolValue | undefined;
  setDeleted(value?: google_protobuf_wrappers_pb.BoolValue): void;

  hasCreatedBy(): boolean;
  clearCreatedBy(): void;
  getCreatedBy(): google_protobuf_wrappers_pb.StringValue | undefined;
  setCreatedBy(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasCreatedAt(): boolean;
  clearCreatedAt(): void;
  getCreatedAt(): google_protobuf_timestamp_pb.Timestamp | undefined;
  setCreatedAt(value?: google_protobuf_timestamp_pb.Timestamp): void;

  hasUpdatedBy(): boolean;
  clearUpdatedBy(): void;
  getUpdatedBy(): google_protobuf_wrappers_pb.StringValue | undefined;
  setUpdatedBy(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasUpdatedAt(): boolean;
  clearUpdatedAt(): void;
  getUpdatedAt(): google_protobuf_timestamp_pb.Timestamp | undefined;
  setUpdatedAt(value?: google_protobuf_timestamp_pb.Timestamp): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): InsectFamily.AsObject;
  static toObject(includeInstance: boolean, msg: InsectFamily): InsectFamily.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: InsectFamily, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): InsectFamily;
  static deserializeBinaryFromReader(message: InsectFamily, reader: jspb.BinaryReader): InsectFamily;
}

export namespace InsectFamily {
  export type AsObject = {
    id: number,
    name: string,
    hash?: google_protobuf_wrappers_pb.StringValue.AsObject,
    deleted?: google_protobuf_wrappers_pb.BoolValue.AsObject,
    createdBy?: google_protobuf_wrappers_pb.StringValue.AsObject,
    createdAt?: google_protobuf_timestamp_pb.Timestamp.AsObject,
    updatedBy?: google_protobuf_wrappers_pb.StringValue.AsObject,
    updatedAt?: google_protobuf_timestamp_pb.Timestamp.AsObject,
  }
}

export class InsectFamilyFilter extends jspb.Message {
  hasId(): boolean;
  clearId(): void;
  getId(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setId(value?: google_protobuf_wrappers_pb.Int64Value): void;

  hasName(): boolean;
  clearName(): void;
  getName(): google_protobuf_wrappers_pb.StringValue | undefined;
  setName(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasDeleted(): boolean;
  clearDeleted(): void;
  getDeleted(): google_protobuf_wrappers_pb.BoolValue | undefined;
  setDeleted(value?: google_protobuf_wrappers_pb.BoolValue): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): InsectFamilyFilter.AsObject;
  static toObject(includeInstance: boolean, msg: InsectFamilyFilter): InsectFamilyFilter.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: InsectFamilyFilter, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): InsectFamilyFilter;
  static deserializeBinaryFromReader(message: InsectFamilyFilter, reader: jspb.BinaryReader): InsectFamilyFilter;
}

export namespace InsectFamilyFilter {
  export type AsObject = {
    id?: google_protobuf_wrappers_pb.Int64Value.AsObject,
    name?: google_protobuf_wrappers_pb.StringValue.AsObject,
    deleted?: google_protobuf_wrappers_pb.BoolValue.AsObject,
  }
}

export class ListInsectFamilyRequest extends jspb.Message {
  hasFilter(): boolean;
  clearFilter(): void;
  getFilter(): InsectFamilyFilter | undefined;
  setFilter(value?: InsectFamilyFilter): void;

  clearSortingsList(): void;
  getSortingsList(): Array<v1_dictionary_common_dictionary_common_pb.Sorting>;
  setSortingsList(value: Array<v1_dictionary_common_dictionary_common_pb.Sorting>): void;
  addSortings(value?: v1_dictionary_common_dictionary_common_pb.Sorting, index?: number): v1_dictionary_common_dictionary_common_pb.Sorting;

  hasPagination(): boolean;
  clearPagination(): void;
  getPagination(): v1_dictionary_common_dictionary_common_pb.PaginationRequest | undefined;
  setPagination(value?: v1_dictionary_common_dictionary_common_pb.PaginationRequest): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListInsectFamilyRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ListInsectFamilyRequest): ListInsectFamilyRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListInsectFamilyRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListInsectFamilyRequest;
  static deserializeBinaryFromReader(message: ListInsectFamilyRequest, reader: jspb.BinaryReader): ListInsectFamilyRequest;
}

export namespace ListInsectFamilyRequest {
  export type AsObject = {
    filter?: InsectFamilyFilter.AsObject,
    sortingsList: Array<v1_dictionary_common_dictionary_common_pb.Sorting.AsObject>,
    pagination?: v1_dictionary_common_dictionary_common_pb.PaginationRequest.AsObject,
  }
}

export class ListInsectFamilyResponse extends jspb.Message {
  clearItemsList(): void;
  getItemsList(): Array<InsectFamily>;
  setItemsList(value: Array<InsectFamily>): void;
  addItems(value?: InsectFamily, index?: number): InsectFamily;

  hasPagination(): boolean;
  clearPagination(): void;
  getPagination(): v1_dictionary_common_dictionary_common_pb.PaginationResponse | undefined;
  setPagination(value?: v1_dictionary_common_dictionary_common_pb.PaginationResponse): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListInsectFamilyResponse.AsObject;
  static toObject(includeInstance: boolean, msg: ListInsectFamilyResponse): ListInsectFamilyResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListInsectFamilyResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListInsectFamilyResponse;
  static deserializeBinaryFromReader(message: ListInsectFamilyResponse, reader: jspb.BinaryReader): ListInsectFamilyResponse;
}

export namespace ListInsectFamilyResponse {
  export type AsObject = {
    itemsList: Array<InsectFamily.AsObject>,
    pagination?: v1_dictionary_common_dictionary_common_pb.PaginationResponse.AsObject,
  }
}

export class GetInsectFamilyRequest extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetInsectFamilyRequest.AsObject;
  static toObject(includeInstance: boolean, msg: GetInsectFamilyRequest): GetInsectFamilyRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetInsectFamilyRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetInsectFamilyRequest;
  static deserializeBinaryFromReader(message: GetInsectFamilyRequest, reader: jspb.BinaryReader): GetInsectFamilyRequest;
}

export namespace GetInsectFamilyRequest {
  export type AsObject = {
    id: number,
  }
}

export class GetInsectFamilyResponse extends jspb.Message {
  hasItem(): boolean;
  clearItem(): void;
  getItem(): InsectFamily | undefined;
  setItem(value?: InsectFamily): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetInsectFamilyResponse.AsObject;
  static toObject(includeInstance: boolean, msg: GetInsectFamilyResponse): GetInsectFamilyResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetInsectFamilyResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetInsectFamilyResponse;
  static deserializeBinaryFromReader(message: GetInsectFamilyResponse, reader: jspb.BinaryReader): GetInsectFamilyResponse;
}

export namespace GetInsectFamilyResponse {
  export type AsObject = {
    item?: InsectFamily.AsObject,
  }
}

