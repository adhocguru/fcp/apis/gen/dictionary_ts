// package: fcp.dictionary.v1.dictionary
// file: v1/dictionary/vermin_description.proto

import * as jspb from "google-protobuf";
import * as google_protobuf_wrappers_pb from "google-protobuf/google/protobuf/wrappers_pb";
import * as google_protobuf_timestamp_pb from "google-protobuf/google/protobuf/timestamp_pb";
import * as v1_dictionary_common_dictionary_common_pb from "../../v1/dictionary_common/dictionary_common_pb";

export class VerminDescription extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  getVerminId(): number;
  setVerminId(value: number): void;

  getVerminName(): string;
  setVerminName(value: string): void;

  getCultureId(): number;
  setCultureId(value: number): void;

  getCultureName(): string;
  setCultureName(value: string): void;

  getCulturesGroupId(): number;
  setCulturesGroupId(value: number): void;

  getCulturesGroupName(): string;
  setCulturesGroupName(value: string): void;

  getTxt(): string;
  setTxt(value: string): void;

  hasHash(): boolean;
  clearHash(): void;
  getHash(): google_protobuf_wrappers_pb.StringValue | undefined;
  setHash(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasDeleted(): boolean;
  clearDeleted(): void;
  getDeleted(): google_protobuf_wrappers_pb.BoolValue | undefined;
  setDeleted(value?: google_protobuf_wrappers_pb.BoolValue): void;

  hasCreatedBy(): boolean;
  clearCreatedBy(): void;
  getCreatedBy(): google_protobuf_wrappers_pb.StringValue | undefined;
  setCreatedBy(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasCreatedAt(): boolean;
  clearCreatedAt(): void;
  getCreatedAt(): google_protobuf_timestamp_pb.Timestamp | undefined;
  setCreatedAt(value?: google_protobuf_timestamp_pb.Timestamp): void;

  hasUpdatedBy(): boolean;
  clearUpdatedBy(): void;
  getUpdatedBy(): google_protobuf_wrappers_pb.StringValue | undefined;
  setUpdatedBy(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasUpdatedAt(): boolean;
  clearUpdatedAt(): void;
  getUpdatedAt(): google_protobuf_timestamp_pb.Timestamp | undefined;
  setUpdatedAt(value?: google_protobuf_timestamp_pb.Timestamp): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): VerminDescription.AsObject;
  static toObject(includeInstance: boolean, msg: VerminDescription): VerminDescription.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: VerminDescription, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): VerminDescription;
  static deserializeBinaryFromReader(message: VerminDescription, reader: jspb.BinaryReader): VerminDescription;
}

export namespace VerminDescription {
  export type AsObject = {
    id: number,
    verminId: number,
    verminName: string,
    cultureId: number,
    cultureName: string,
    culturesGroupId: number,
    culturesGroupName: string,
    txt: string,
    hash?: google_protobuf_wrappers_pb.StringValue.AsObject,
    deleted?: google_protobuf_wrappers_pb.BoolValue.AsObject,
    createdBy?: google_protobuf_wrappers_pb.StringValue.AsObject,
    createdAt?: google_protobuf_timestamp_pb.Timestamp.AsObject,
    updatedBy?: google_protobuf_wrappers_pb.StringValue.AsObject,
    updatedAt?: google_protobuf_timestamp_pb.Timestamp.AsObject,
  }
}

export class VerminDescriptionFilter extends jspb.Message {
  hasId(): boolean;
  clearId(): void;
  getId(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setId(value?: google_protobuf_wrappers_pb.Int64Value): void;

  hasVerminId(): boolean;
  clearVerminId(): void;
  getVerminId(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setVerminId(value?: google_protobuf_wrappers_pb.Int64Value): void;

  hasCultureId(): boolean;
  clearCultureId(): void;
  getCultureId(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setCultureId(value?: google_protobuf_wrappers_pb.Int64Value): void;

  hasCulturesGroupId(): boolean;
  clearCulturesGroupId(): void;
  getCulturesGroupId(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setCulturesGroupId(value?: google_protobuf_wrappers_pb.Int64Value): void;

  hasDeleted(): boolean;
  clearDeleted(): void;
  getDeleted(): google_protobuf_wrappers_pb.BoolValue | undefined;
  setDeleted(value?: google_protobuf_wrappers_pb.BoolValue): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): VerminDescriptionFilter.AsObject;
  static toObject(includeInstance: boolean, msg: VerminDescriptionFilter): VerminDescriptionFilter.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: VerminDescriptionFilter, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): VerminDescriptionFilter;
  static deserializeBinaryFromReader(message: VerminDescriptionFilter, reader: jspb.BinaryReader): VerminDescriptionFilter;
}

export namespace VerminDescriptionFilter {
  export type AsObject = {
    id?: google_protobuf_wrappers_pb.Int64Value.AsObject,
    verminId?: google_protobuf_wrappers_pb.Int64Value.AsObject,
    cultureId?: google_protobuf_wrappers_pb.Int64Value.AsObject,
    culturesGroupId?: google_protobuf_wrappers_pb.Int64Value.AsObject,
    deleted?: google_protobuf_wrappers_pb.BoolValue.AsObject,
  }
}

export class ListVerminDescriptionRequest extends jspb.Message {
  hasFilter(): boolean;
  clearFilter(): void;
  getFilter(): VerminDescriptionFilter | undefined;
  setFilter(value?: VerminDescriptionFilter): void;

  clearSortingsList(): void;
  getSortingsList(): Array<v1_dictionary_common_dictionary_common_pb.Sorting>;
  setSortingsList(value: Array<v1_dictionary_common_dictionary_common_pb.Sorting>): void;
  addSortings(value?: v1_dictionary_common_dictionary_common_pb.Sorting, index?: number): v1_dictionary_common_dictionary_common_pb.Sorting;

  hasPagination(): boolean;
  clearPagination(): void;
  getPagination(): v1_dictionary_common_dictionary_common_pb.PaginationRequest | undefined;
  setPagination(value?: v1_dictionary_common_dictionary_common_pb.PaginationRequest): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListVerminDescriptionRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ListVerminDescriptionRequest): ListVerminDescriptionRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListVerminDescriptionRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListVerminDescriptionRequest;
  static deserializeBinaryFromReader(message: ListVerminDescriptionRequest, reader: jspb.BinaryReader): ListVerminDescriptionRequest;
}

export namespace ListVerminDescriptionRequest {
  export type AsObject = {
    filter?: VerminDescriptionFilter.AsObject,
    sortingsList: Array<v1_dictionary_common_dictionary_common_pb.Sorting.AsObject>,
    pagination?: v1_dictionary_common_dictionary_common_pb.PaginationRequest.AsObject,
  }
}

export class ListVerminDescriptionResponse extends jspb.Message {
  clearItemsList(): void;
  getItemsList(): Array<VerminDescription>;
  setItemsList(value: Array<VerminDescription>): void;
  addItems(value?: VerminDescription, index?: number): VerminDescription;

  hasPagination(): boolean;
  clearPagination(): void;
  getPagination(): v1_dictionary_common_dictionary_common_pb.PaginationResponse | undefined;
  setPagination(value?: v1_dictionary_common_dictionary_common_pb.PaginationResponse): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListVerminDescriptionResponse.AsObject;
  static toObject(includeInstance: boolean, msg: ListVerminDescriptionResponse): ListVerminDescriptionResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListVerminDescriptionResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListVerminDescriptionResponse;
  static deserializeBinaryFromReader(message: ListVerminDescriptionResponse, reader: jspb.BinaryReader): ListVerminDescriptionResponse;
}

export namespace ListVerminDescriptionResponse {
  export type AsObject = {
    itemsList: Array<VerminDescription.AsObject>,
    pagination?: v1_dictionary_common_dictionary_common_pb.PaginationResponse.AsObject,
  }
}

export class GetVerminDescriptionRequest extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetVerminDescriptionRequest.AsObject;
  static toObject(includeInstance: boolean, msg: GetVerminDescriptionRequest): GetVerminDescriptionRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetVerminDescriptionRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetVerminDescriptionRequest;
  static deserializeBinaryFromReader(message: GetVerminDescriptionRequest, reader: jspb.BinaryReader): GetVerminDescriptionRequest;
}

export namespace GetVerminDescriptionRequest {
  export type AsObject = {
    id: number,
  }
}

export class GetVerminDescriptionResponse extends jspb.Message {
  hasItem(): boolean;
  clearItem(): void;
  getItem(): VerminDescription | undefined;
  setItem(value?: VerminDescription): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetVerminDescriptionResponse.AsObject;
  static toObject(includeInstance: boolean, msg: GetVerminDescriptionResponse): GetVerminDescriptionResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetVerminDescriptionResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetVerminDescriptionResponse;
  static deserializeBinaryFromReader(message: GetVerminDescriptionResponse, reader: jspb.BinaryReader): GetVerminDescriptionResponse;
}

export namespace GetVerminDescriptionResponse {
  export type AsObject = {
    item?: VerminDescription.AsObject,
  }
}

