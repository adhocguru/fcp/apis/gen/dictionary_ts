// package: fcp.dictionary.v1.dictionary
// file: v1/dictionary/technology_crop_processing.proto

import * as jspb from "google-protobuf";
import * as google_protobuf_wrappers_pb from "google-protobuf/google/protobuf/wrappers_pb";
import * as google_protobuf_timestamp_pb from "google-protobuf/google/protobuf/timestamp_pb";
import * as v1_dictionary_common_dictionary_common_pb from "../../v1/dictionary_common/dictionary_common_pb";

export class TechnologyCropProcessing extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  getProductId(): number;
  setProductId(value: number): void;

  getConsumptionNormMin(): string;
  setConsumptionNormMin(value: string): void;

  getConsumptionNormMax(): string;
  setConsumptionNormMax(value: string): void;

  getLastTreatmentTerm(): string;
  setLastTreatmentTerm(value: string): void;

  getMaxTreatmentCount(): string;
  setMaxTreatmentCount(value: string): void;

  getExperience(): string;
  setExperience(value: string): void;

  getAmountUnitText(): string;
  setAmountUnitText(value: string): void;

  getAmountUnitId(): number;
  setAmountUnitId(value: number): void;

  getAreaUnitText(): string;
  setAreaUnitText(value: string): void;

  getAreaUnitId(): number;
  setAreaUnitId(value: number): void;

  getConsumptionNormMinFluid(): string;
  setConsumptionNormMinFluid(value: string): void;

  getConsumptionNormMaxFluid(): string;
  setConsumptionNormMaxFluid(value: string): void;

  getAmountUnitFluid(): string;
  setAmountUnitFluid(value: string): void;

  getAreaUnitFluid(): string;
  setAreaUnitFluid(value: string): void;

  getWatingTime(): string;
  setWatingTime(value: string): void;

  getWatingTerms(): string;
  setWatingTerms(value: string): void;

  getFeatures(): string;
  setFeatures(value: string): void;

  hasHash(): boolean;
  clearHash(): void;
  getHash(): google_protobuf_wrappers_pb.StringValue | undefined;
  setHash(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasDeleted(): boolean;
  clearDeleted(): void;
  getDeleted(): google_protobuf_wrappers_pb.BoolValue | undefined;
  setDeleted(value?: google_protobuf_wrappers_pb.BoolValue): void;

  hasCreatedBy(): boolean;
  clearCreatedBy(): void;
  getCreatedBy(): google_protobuf_wrappers_pb.StringValue | undefined;
  setCreatedBy(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasCreatedAt(): boolean;
  clearCreatedAt(): void;
  getCreatedAt(): google_protobuf_timestamp_pb.Timestamp | undefined;
  setCreatedAt(value?: google_protobuf_timestamp_pb.Timestamp): void;

  hasUpdatedBy(): boolean;
  clearUpdatedBy(): void;
  getUpdatedBy(): google_protobuf_wrappers_pb.StringValue | undefined;
  setUpdatedBy(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasUpdatedAt(): boolean;
  clearUpdatedAt(): void;
  getUpdatedAt(): google_protobuf_timestamp_pb.Timestamp | undefined;
  setUpdatedAt(value?: google_protobuf_timestamp_pb.Timestamp): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): TechnologyCropProcessing.AsObject;
  static toObject(includeInstance: boolean, msg: TechnologyCropProcessing): TechnologyCropProcessing.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: TechnologyCropProcessing, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): TechnologyCropProcessing;
  static deserializeBinaryFromReader(message: TechnologyCropProcessing, reader: jspb.BinaryReader): TechnologyCropProcessing;
}

export namespace TechnologyCropProcessing {
  export type AsObject = {
    id: number,
    productId: number,
    consumptionNormMin: string,
    consumptionNormMax: string,
    lastTreatmentTerm: string,
    maxTreatmentCount: string,
    experience: string,
    amountUnitText: string,
    amountUnitId: number,
    areaUnitText: string,
    areaUnitId: number,
    consumptionNormMinFluid: string,
    consumptionNormMaxFluid: string,
    amountUnitFluid: string,
    areaUnitFluid: string,
    watingTime: string,
    watingTerms: string,
    features: string,
    hash?: google_protobuf_wrappers_pb.StringValue.AsObject,
    deleted?: google_protobuf_wrappers_pb.BoolValue.AsObject,
    createdBy?: google_protobuf_wrappers_pb.StringValue.AsObject,
    createdAt?: google_protobuf_timestamp_pb.Timestamp.AsObject,
    updatedBy?: google_protobuf_wrappers_pb.StringValue.AsObject,
    updatedAt?: google_protobuf_timestamp_pb.Timestamp.AsObject,
  }
}

export class TechnologyCropProcessingFilter extends jspb.Message {
  hasId(): boolean;
  clearId(): void;
  getId(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setId(value?: google_protobuf_wrappers_pb.Int64Value): void;

  hasProductId(): boolean;
  clearProductId(): void;
  getProductId(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setProductId(value?: google_protobuf_wrappers_pb.Int64Value): void;

  hasDeleted(): boolean;
  clearDeleted(): void;
  getDeleted(): google_protobuf_wrappers_pb.BoolValue | undefined;
  setDeleted(value?: google_protobuf_wrappers_pb.BoolValue): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): TechnologyCropProcessingFilter.AsObject;
  static toObject(includeInstance: boolean, msg: TechnologyCropProcessingFilter): TechnologyCropProcessingFilter.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: TechnologyCropProcessingFilter, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): TechnologyCropProcessingFilter;
  static deserializeBinaryFromReader(message: TechnologyCropProcessingFilter, reader: jspb.BinaryReader): TechnologyCropProcessingFilter;
}

export namespace TechnologyCropProcessingFilter {
  export type AsObject = {
    id?: google_protobuf_wrappers_pb.Int64Value.AsObject,
    productId?: google_protobuf_wrappers_pb.Int64Value.AsObject,
    deleted?: google_protobuf_wrappers_pb.BoolValue.AsObject,
  }
}

export class GetTechnologyCropProcessingRequest extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetTechnologyCropProcessingRequest.AsObject;
  static toObject(includeInstance: boolean, msg: GetTechnologyCropProcessingRequest): GetTechnologyCropProcessingRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetTechnologyCropProcessingRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetTechnologyCropProcessingRequest;
  static deserializeBinaryFromReader(message: GetTechnologyCropProcessingRequest, reader: jspb.BinaryReader): GetTechnologyCropProcessingRequest;
}

export namespace GetTechnologyCropProcessingRequest {
  export type AsObject = {
    id: number,
  }
}

export class GetTechnologyCropProcessingResponse extends jspb.Message {
  hasItem(): boolean;
  clearItem(): void;
  getItem(): TechnologyCropProcessing | undefined;
  setItem(value?: TechnologyCropProcessing): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetTechnologyCropProcessingResponse.AsObject;
  static toObject(includeInstance: boolean, msg: GetTechnologyCropProcessingResponse): GetTechnologyCropProcessingResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetTechnologyCropProcessingResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetTechnologyCropProcessingResponse;
  static deserializeBinaryFromReader(message: GetTechnologyCropProcessingResponse, reader: jspb.BinaryReader): GetTechnologyCropProcessingResponse;
}

export namespace GetTechnologyCropProcessingResponse {
  export type AsObject = {
    item?: TechnologyCropProcessing.AsObject,
  }
}

export class ListTechnologyCropProcessingRequest extends jspb.Message {
  hasFilter(): boolean;
  clearFilter(): void;
  getFilter(): TechnologyCropProcessingFilter | undefined;
  setFilter(value?: TechnologyCropProcessingFilter): void;

  clearSortingsList(): void;
  getSortingsList(): Array<v1_dictionary_common_dictionary_common_pb.Sorting>;
  setSortingsList(value: Array<v1_dictionary_common_dictionary_common_pb.Sorting>): void;
  addSortings(value?: v1_dictionary_common_dictionary_common_pb.Sorting, index?: number): v1_dictionary_common_dictionary_common_pb.Sorting;

  hasPagination(): boolean;
  clearPagination(): void;
  getPagination(): v1_dictionary_common_dictionary_common_pb.PaginationRequest | undefined;
  setPagination(value?: v1_dictionary_common_dictionary_common_pb.PaginationRequest): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListTechnologyCropProcessingRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ListTechnologyCropProcessingRequest): ListTechnologyCropProcessingRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListTechnologyCropProcessingRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListTechnologyCropProcessingRequest;
  static deserializeBinaryFromReader(message: ListTechnologyCropProcessingRequest, reader: jspb.BinaryReader): ListTechnologyCropProcessingRequest;
}

export namespace ListTechnologyCropProcessingRequest {
  export type AsObject = {
    filter?: TechnologyCropProcessingFilter.AsObject,
    sortingsList: Array<v1_dictionary_common_dictionary_common_pb.Sorting.AsObject>,
    pagination?: v1_dictionary_common_dictionary_common_pb.PaginationRequest.AsObject,
  }
}

export class ListTechnologyCropProcessingResponse extends jspb.Message {
  clearItemsList(): void;
  getItemsList(): Array<TechnologyCropProcessing>;
  setItemsList(value: Array<TechnologyCropProcessing>): void;
  addItems(value?: TechnologyCropProcessing, index?: number): TechnologyCropProcessing;

  hasPagination(): boolean;
  clearPagination(): void;
  getPagination(): v1_dictionary_common_dictionary_common_pb.PaginationResponse | undefined;
  setPagination(value?: v1_dictionary_common_dictionary_common_pb.PaginationResponse): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListTechnologyCropProcessingResponse.AsObject;
  static toObject(includeInstance: boolean, msg: ListTechnologyCropProcessingResponse): ListTechnologyCropProcessingResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListTechnologyCropProcessingResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListTechnologyCropProcessingResponse;
  static deserializeBinaryFromReader(message: ListTechnologyCropProcessingResponse, reader: jspb.BinaryReader): ListTechnologyCropProcessingResponse;
}

export namespace ListTechnologyCropProcessingResponse {
  export type AsObject = {
    itemsList: Array<TechnologyCropProcessing.AsObject>,
    pagination?: v1_dictionary_common_dictionary_common_pb.PaginationResponse.AsObject,
  }
}

