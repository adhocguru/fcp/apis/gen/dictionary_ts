// package: fcp.dictionary.v1.dictionary
// file: v1/dictionary/packaging.proto

import * as jspb from "google-protobuf";
import * as google_protobuf_wrappers_pb from "google-protobuf/google/protobuf/wrappers_pb";
import * as google_protobuf_timestamp_pb from "google-protobuf/google/protobuf/timestamp_pb";
import * as v1_dictionary_common_dictionary_common_pb from "../../v1/dictionary_common/dictionary_common_pb";

export class Packaging extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  getQuantity(): string;
  setQuantity(value: string): void;

  getUnitName(): string;
  setUnitName(value: string): void;

  getGuid(): string;
  setGuid(value: string): void;

  getSortOrder(): number;
  setSortOrder(value: number): void;

  hasHash(): boolean;
  clearHash(): void;
  getHash(): google_protobuf_wrappers_pb.StringValue | undefined;
  setHash(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasDeleted(): boolean;
  clearDeleted(): void;
  getDeleted(): google_protobuf_wrappers_pb.BoolValue | undefined;
  setDeleted(value?: google_protobuf_wrappers_pb.BoolValue): void;

  hasCreatedBy(): boolean;
  clearCreatedBy(): void;
  getCreatedBy(): google_protobuf_wrappers_pb.StringValue | undefined;
  setCreatedBy(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasCreatedAt(): boolean;
  clearCreatedAt(): void;
  getCreatedAt(): google_protobuf_timestamp_pb.Timestamp | undefined;
  setCreatedAt(value?: google_protobuf_timestamp_pb.Timestamp): void;

  hasUpdatedBy(): boolean;
  clearUpdatedBy(): void;
  getUpdatedBy(): google_protobuf_wrappers_pb.StringValue | undefined;
  setUpdatedBy(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasUpdatedAt(): boolean;
  clearUpdatedAt(): void;
  getUpdatedAt(): google_protobuf_timestamp_pb.Timestamp | undefined;
  setUpdatedAt(value?: google_protobuf_timestamp_pb.Timestamp): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): Packaging.AsObject;
  static toObject(includeInstance: boolean, msg: Packaging): Packaging.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: Packaging, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): Packaging;
  static deserializeBinaryFromReader(message: Packaging, reader: jspb.BinaryReader): Packaging;
}

export namespace Packaging {
  export type AsObject = {
    id: number,
    quantity: string,
    unitName: string,
    guid: string,
    sortOrder: number,
    hash?: google_protobuf_wrappers_pb.StringValue.AsObject,
    deleted?: google_protobuf_wrappers_pb.BoolValue.AsObject,
    createdBy?: google_protobuf_wrappers_pb.StringValue.AsObject,
    createdAt?: google_protobuf_timestamp_pb.Timestamp.AsObject,
    updatedBy?: google_protobuf_wrappers_pb.StringValue.AsObject,
    updatedAt?: google_protobuf_timestamp_pb.Timestamp.AsObject,
  }
}

export class PackagingFilter extends jspb.Message {
  hasId(): boolean;
  clearId(): void;
  getId(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setId(value?: google_protobuf_wrappers_pb.Int64Value): void;

  hasQuantity(): boolean;
  clearQuantity(): void;
  getQuantity(): google_protobuf_wrappers_pb.StringValue | undefined;
  setQuantity(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasUnitName(): boolean;
  clearUnitName(): void;
  getUnitName(): google_protobuf_wrappers_pb.StringValue | undefined;
  setUnitName(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasDeleted(): boolean;
  clearDeleted(): void;
  getDeleted(): google_protobuf_wrappers_pb.BoolValue | undefined;
  setDeleted(value?: google_protobuf_wrappers_pb.BoolValue): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): PackagingFilter.AsObject;
  static toObject(includeInstance: boolean, msg: PackagingFilter): PackagingFilter.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: PackagingFilter, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): PackagingFilter;
  static deserializeBinaryFromReader(message: PackagingFilter, reader: jspb.BinaryReader): PackagingFilter;
}

export namespace PackagingFilter {
  export type AsObject = {
    id?: google_protobuf_wrappers_pb.Int64Value.AsObject,
    quantity?: google_protobuf_wrappers_pb.StringValue.AsObject,
    unitName?: google_protobuf_wrappers_pb.StringValue.AsObject,
    deleted?: google_protobuf_wrappers_pb.BoolValue.AsObject,
  }
}

export class GetPackagingRequest extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetPackagingRequest.AsObject;
  static toObject(includeInstance: boolean, msg: GetPackagingRequest): GetPackagingRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetPackagingRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetPackagingRequest;
  static deserializeBinaryFromReader(message: GetPackagingRequest, reader: jspb.BinaryReader): GetPackagingRequest;
}

export namespace GetPackagingRequest {
  export type AsObject = {
    id: number,
  }
}

export class GetPackagingResponse extends jspb.Message {
  hasItem(): boolean;
  clearItem(): void;
  getItem(): Packaging | undefined;
  setItem(value?: Packaging): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetPackagingResponse.AsObject;
  static toObject(includeInstance: boolean, msg: GetPackagingResponse): GetPackagingResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetPackagingResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetPackagingResponse;
  static deserializeBinaryFromReader(message: GetPackagingResponse, reader: jspb.BinaryReader): GetPackagingResponse;
}

export namespace GetPackagingResponse {
  export type AsObject = {
    item?: Packaging.AsObject,
  }
}

export class ListPackagingRequest extends jspb.Message {
  hasFilter(): boolean;
  clearFilter(): void;
  getFilter(): PackagingFilter | undefined;
  setFilter(value?: PackagingFilter): void;

  clearSortingsList(): void;
  getSortingsList(): Array<v1_dictionary_common_dictionary_common_pb.Sorting>;
  setSortingsList(value: Array<v1_dictionary_common_dictionary_common_pb.Sorting>): void;
  addSortings(value?: v1_dictionary_common_dictionary_common_pb.Sorting, index?: number): v1_dictionary_common_dictionary_common_pb.Sorting;

  hasPagination(): boolean;
  clearPagination(): void;
  getPagination(): v1_dictionary_common_dictionary_common_pb.PaginationRequest | undefined;
  setPagination(value?: v1_dictionary_common_dictionary_common_pb.PaginationRequest): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListPackagingRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ListPackagingRequest): ListPackagingRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListPackagingRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListPackagingRequest;
  static deserializeBinaryFromReader(message: ListPackagingRequest, reader: jspb.BinaryReader): ListPackagingRequest;
}

export namespace ListPackagingRequest {
  export type AsObject = {
    filter?: PackagingFilter.AsObject,
    sortingsList: Array<v1_dictionary_common_dictionary_common_pb.Sorting.AsObject>,
    pagination?: v1_dictionary_common_dictionary_common_pb.PaginationRequest.AsObject,
  }
}

export class ListPackagingResponse extends jspb.Message {
  clearItemsList(): void;
  getItemsList(): Array<Packaging>;
  setItemsList(value: Array<Packaging>): void;
  addItems(value?: Packaging, index?: number): Packaging;

  hasPagination(): boolean;
  clearPagination(): void;
  getPagination(): v1_dictionary_common_dictionary_common_pb.PaginationResponse | undefined;
  setPagination(value?: v1_dictionary_common_dictionary_common_pb.PaginationResponse): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListPackagingResponse.AsObject;
  static toObject(includeInstance: boolean, msg: ListPackagingResponse): ListPackagingResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListPackagingResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListPackagingResponse;
  static deserializeBinaryFromReader(message: ListPackagingResponse, reader: jspb.BinaryReader): ListPackagingResponse;
}

export namespace ListPackagingResponse {
  export type AsObject = {
    itemsList: Array<Packaging.AsObject>,
    pagination?: v1_dictionary_common_dictionary_common_pb.PaginationResponse.AsObject,
  }
}

