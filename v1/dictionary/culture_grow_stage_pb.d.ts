// package: fcp.dictionary.v1.dictionary
// file: v1/dictionary/culture_grow_stage.proto

import * as jspb from "google-protobuf";
import * as google_protobuf_wrappers_pb from "google-protobuf/google/protobuf/wrappers_pb";
import * as google_protobuf_timestamp_pb from "google-protobuf/google/protobuf/timestamp_pb";
import * as v1_dictionary_common_dictionary_common_pb from "../../v1/dictionary_common/dictionary_common_pb";

export class CultureGrowStage extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  getName(): string;
  setName(value: string): void;

  getImage(): string;
  setImage(value: string): void;

  getStageOrder(): number;
  setStageOrder(value: number): void;

  getCultureId(): number;
  setCultureId(value: number): void;

  getCultureName(): string;
  setCultureName(value: string): void;

  hasHash(): boolean;
  clearHash(): void;
  getHash(): google_protobuf_wrappers_pb.StringValue | undefined;
  setHash(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasDeleted(): boolean;
  clearDeleted(): void;
  getDeleted(): google_protobuf_wrappers_pb.BoolValue | undefined;
  setDeleted(value?: google_protobuf_wrappers_pb.BoolValue): void;

  hasCreatedBy(): boolean;
  clearCreatedBy(): void;
  getCreatedBy(): google_protobuf_wrappers_pb.StringValue | undefined;
  setCreatedBy(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasCreatedAt(): boolean;
  clearCreatedAt(): void;
  getCreatedAt(): google_protobuf_timestamp_pb.Timestamp | undefined;
  setCreatedAt(value?: google_protobuf_timestamp_pb.Timestamp): void;

  hasUpdatedBy(): boolean;
  clearUpdatedBy(): void;
  getUpdatedBy(): google_protobuf_wrappers_pb.StringValue | undefined;
  setUpdatedBy(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasUpdatedAt(): boolean;
  clearUpdatedAt(): void;
  getUpdatedAt(): google_protobuf_timestamp_pb.Timestamp | undefined;
  setUpdatedAt(value?: google_protobuf_timestamp_pb.Timestamp): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): CultureGrowStage.AsObject;
  static toObject(includeInstance: boolean, msg: CultureGrowStage): CultureGrowStage.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: CultureGrowStage, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): CultureGrowStage;
  static deserializeBinaryFromReader(message: CultureGrowStage, reader: jspb.BinaryReader): CultureGrowStage;
}

export namespace CultureGrowStage {
  export type AsObject = {
    id: number,
    name: string,
    image: string,
    stageOrder: number,
    cultureId: number,
    cultureName: string,
    hash?: google_protobuf_wrappers_pb.StringValue.AsObject,
    deleted?: google_protobuf_wrappers_pb.BoolValue.AsObject,
    createdBy?: google_protobuf_wrappers_pb.StringValue.AsObject,
    createdAt?: google_protobuf_timestamp_pb.Timestamp.AsObject,
    updatedBy?: google_protobuf_wrappers_pb.StringValue.AsObject,
    updatedAt?: google_protobuf_timestamp_pb.Timestamp.AsObject,
  }
}

export class CultureGrowStageFilter extends jspb.Message {
  hasId(): boolean;
  clearId(): void;
  getId(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setId(value?: google_protobuf_wrappers_pb.Int64Value): void;

  hasName(): boolean;
  clearName(): void;
  getName(): google_protobuf_wrappers_pb.StringValue | undefined;
  setName(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasCultureId(): boolean;
  clearCultureId(): void;
  getCultureId(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setCultureId(value?: google_protobuf_wrappers_pb.Int64Value): void;

  hasDeleted(): boolean;
  clearDeleted(): void;
  getDeleted(): google_protobuf_wrappers_pb.BoolValue | undefined;
  setDeleted(value?: google_protobuf_wrappers_pb.BoolValue): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): CultureGrowStageFilter.AsObject;
  static toObject(includeInstance: boolean, msg: CultureGrowStageFilter): CultureGrowStageFilter.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: CultureGrowStageFilter, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): CultureGrowStageFilter;
  static deserializeBinaryFromReader(message: CultureGrowStageFilter, reader: jspb.BinaryReader): CultureGrowStageFilter;
}

export namespace CultureGrowStageFilter {
  export type AsObject = {
    id?: google_protobuf_wrappers_pb.Int64Value.AsObject,
    name?: google_protobuf_wrappers_pb.StringValue.AsObject,
    cultureId?: google_protobuf_wrappers_pb.Int64Value.AsObject,
    deleted?: google_protobuf_wrappers_pb.BoolValue.AsObject,
  }
}

export class ListCultureGrowStageRequest extends jspb.Message {
  hasFilter(): boolean;
  clearFilter(): void;
  getFilter(): CultureGrowStageFilter | undefined;
  setFilter(value?: CultureGrowStageFilter): void;

  clearSortingsList(): void;
  getSortingsList(): Array<v1_dictionary_common_dictionary_common_pb.Sorting>;
  setSortingsList(value: Array<v1_dictionary_common_dictionary_common_pb.Sorting>): void;
  addSortings(value?: v1_dictionary_common_dictionary_common_pb.Sorting, index?: number): v1_dictionary_common_dictionary_common_pb.Sorting;

  hasPagination(): boolean;
  clearPagination(): void;
  getPagination(): v1_dictionary_common_dictionary_common_pb.PaginationRequest | undefined;
  setPagination(value?: v1_dictionary_common_dictionary_common_pb.PaginationRequest): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListCultureGrowStageRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ListCultureGrowStageRequest): ListCultureGrowStageRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListCultureGrowStageRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListCultureGrowStageRequest;
  static deserializeBinaryFromReader(message: ListCultureGrowStageRequest, reader: jspb.BinaryReader): ListCultureGrowStageRequest;
}

export namespace ListCultureGrowStageRequest {
  export type AsObject = {
    filter?: CultureGrowStageFilter.AsObject,
    sortingsList: Array<v1_dictionary_common_dictionary_common_pb.Sorting.AsObject>,
    pagination?: v1_dictionary_common_dictionary_common_pb.PaginationRequest.AsObject,
  }
}

export class ListCultureGrowStageResponse extends jspb.Message {
  clearItemsList(): void;
  getItemsList(): Array<CultureGrowStage>;
  setItemsList(value: Array<CultureGrowStage>): void;
  addItems(value?: CultureGrowStage, index?: number): CultureGrowStage;

  hasPagination(): boolean;
  clearPagination(): void;
  getPagination(): v1_dictionary_common_dictionary_common_pb.PaginationResponse | undefined;
  setPagination(value?: v1_dictionary_common_dictionary_common_pb.PaginationResponse): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListCultureGrowStageResponse.AsObject;
  static toObject(includeInstance: boolean, msg: ListCultureGrowStageResponse): ListCultureGrowStageResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListCultureGrowStageResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListCultureGrowStageResponse;
  static deserializeBinaryFromReader(message: ListCultureGrowStageResponse, reader: jspb.BinaryReader): ListCultureGrowStageResponse;
}

export namespace ListCultureGrowStageResponse {
  export type AsObject = {
    itemsList: Array<CultureGrowStage.AsObject>,
    pagination?: v1_dictionary_common_dictionary_common_pb.PaginationResponse.AsObject,
  }
}

export class GetCultureGrowStageRequest extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetCultureGrowStageRequest.AsObject;
  static toObject(includeInstance: boolean, msg: GetCultureGrowStageRequest): GetCultureGrowStageRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetCultureGrowStageRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetCultureGrowStageRequest;
  static deserializeBinaryFromReader(message: GetCultureGrowStageRequest, reader: jspb.BinaryReader): GetCultureGrowStageRequest;
}

export namespace GetCultureGrowStageRequest {
  export type AsObject = {
    id: number,
  }
}

export class GetCultureGrowStageResponse extends jspb.Message {
  hasItem(): boolean;
  clearItem(): void;
  getItem(): CultureGrowStage | undefined;
  setItem(value?: CultureGrowStage): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetCultureGrowStageResponse.AsObject;
  static toObject(includeInstance: boolean, msg: GetCultureGrowStageResponse): GetCultureGrowStageResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetCultureGrowStageResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetCultureGrowStageResponse;
  static deserializeBinaryFromReader(message: GetCultureGrowStageResponse, reader: jspb.BinaryReader): GetCultureGrowStageResponse;
}

export namespace GetCultureGrowStageResponse {
  export type AsObject = {
    item?: CultureGrowStage.AsObject,
  }
}

