// package: fcp.dictionary.v1.dictionary
// file: v1/dictionary/insect_row.proto

import * as jspb from "google-protobuf";
import * as google_protobuf_wrappers_pb from "google-protobuf/google/protobuf/wrappers_pb";
import * as google_protobuf_timestamp_pb from "google-protobuf/google/protobuf/timestamp_pb";
import * as v1_dictionary_common_dictionary_common_pb from "../../v1/dictionary_common/dictionary_common_pb";

export class InsectRow extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  getName(): string;
  setName(value: string): void;

  hasHash(): boolean;
  clearHash(): void;
  getHash(): google_protobuf_wrappers_pb.StringValue | undefined;
  setHash(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasDeleted(): boolean;
  clearDeleted(): void;
  getDeleted(): google_protobuf_wrappers_pb.BoolValue | undefined;
  setDeleted(value?: google_protobuf_wrappers_pb.BoolValue): void;

  hasCreatedBy(): boolean;
  clearCreatedBy(): void;
  getCreatedBy(): google_protobuf_wrappers_pb.StringValue | undefined;
  setCreatedBy(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasCreatedAt(): boolean;
  clearCreatedAt(): void;
  getCreatedAt(): google_protobuf_timestamp_pb.Timestamp | undefined;
  setCreatedAt(value?: google_protobuf_timestamp_pb.Timestamp): void;

  hasUpdatedBy(): boolean;
  clearUpdatedBy(): void;
  getUpdatedBy(): google_protobuf_wrappers_pb.StringValue | undefined;
  setUpdatedBy(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasUpdatedAt(): boolean;
  clearUpdatedAt(): void;
  getUpdatedAt(): google_protobuf_timestamp_pb.Timestamp | undefined;
  setUpdatedAt(value?: google_protobuf_timestamp_pb.Timestamp): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): InsectRow.AsObject;
  static toObject(includeInstance: boolean, msg: InsectRow): InsectRow.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: InsectRow, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): InsectRow;
  static deserializeBinaryFromReader(message: InsectRow, reader: jspb.BinaryReader): InsectRow;
}

export namespace InsectRow {
  export type AsObject = {
    id: number,
    name: string,
    hash?: google_protobuf_wrappers_pb.StringValue.AsObject,
    deleted?: google_protobuf_wrappers_pb.BoolValue.AsObject,
    createdBy?: google_protobuf_wrappers_pb.StringValue.AsObject,
    createdAt?: google_protobuf_timestamp_pb.Timestamp.AsObject,
    updatedBy?: google_protobuf_wrappers_pb.StringValue.AsObject,
    updatedAt?: google_protobuf_timestamp_pb.Timestamp.AsObject,
  }
}

export class InsectRowFilter extends jspb.Message {
  hasId(): boolean;
  clearId(): void;
  getId(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setId(value?: google_protobuf_wrappers_pb.Int64Value): void;

  hasName(): boolean;
  clearName(): void;
  getName(): google_protobuf_wrappers_pb.StringValue | undefined;
  setName(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasDeleted(): boolean;
  clearDeleted(): void;
  getDeleted(): google_protobuf_wrappers_pb.BoolValue | undefined;
  setDeleted(value?: google_protobuf_wrappers_pb.BoolValue): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): InsectRowFilter.AsObject;
  static toObject(includeInstance: boolean, msg: InsectRowFilter): InsectRowFilter.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: InsectRowFilter, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): InsectRowFilter;
  static deserializeBinaryFromReader(message: InsectRowFilter, reader: jspb.BinaryReader): InsectRowFilter;
}

export namespace InsectRowFilter {
  export type AsObject = {
    id?: google_protobuf_wrappers_pb.Int64Value.AsObject,
    name?: google_protobuf_wrappers_pb.StringValue.AsObject,
    deleted?: google_protobuf_wrappers_pb.BoolValue.AsObject,
  }
}

export class ListInsectRowRequest extends jspb.Message {
  hasFilter(): boolean;
  clearFilter(): void;
  getFilter(): InsectRowFilter | undefined;
  setFilter(value?: InsectRowFilter): void;

  clearSortingsList(): void;
  getSortingsList(): Array<v1_dictionary_common_dictionary_common_pb.Sorting>;
  setSortingsList(value: Array<v1_dictionary_common_dictionary_common_pb.Sorting>): void;
  addSortings(value?: v1_dictionary_common_dictionary_common_pb.Sorting, index?: number): v1_dictionary_common_dictionary_common_pb.Sorting;

  hasPagination(): boolean;
  clearPagination(): void;
  getPagination(): v1_dictionary_common_dictionary_common_pb.PaginationRequest | undefined;
  setPagination(value?: v1_dictionary_common_dictionary_common_pb.PaginationRequest): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListInsectRowRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ListInsectRowRequest): ListInsectRowRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListInsectRowRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListInsectRowRequest;
  static deserializeBinaryFromReader(message: ListInsectRowRequest, reader: jspb.BinaryReader): ListInsectRowRequest;
}

export namespace ListInsectRowRequest {
  export type AsObject = {
    filter?: InsectRowFilter.AsObject,
    sortingsList: Array<v1_dictionary_common_dictionary_common_pb.Sorting.AsObject>,
    pagination?: v1_dictionary_common_dictionary_common_pb.PaginationRequest.AsObject,
  }
}

export class ListInsectRowResponse extends jspb.Message {
  clearItemsList(): void;
  getItemsList(): Array<InsectRow>;
  setItemsList(value: Array<InsectRow>): void;
  addItems(value?: InsectRow, index?: number): InsectRow;

  hasPagination(): boolean;
  clearPagination(): void;
  getPagination(): v1_dictionary_common_dictionary_common_pb.PaginationResponse | undefined;
  setPagination(value?: v1_dictionary_common_dictionary_common_pb.PaginationResponse): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListInsectRowResponse.AsObject;
  static toObject(includeInstance: boolean, msg: ListInsectRowResponse): ListInsectRowResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListInsectRowResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListInsectRowResponse;
  static deserializeBinaryFromReader(message: ListInsectRowResponse, reader: jspb.BinaryReader): ListInsectRowResponse;
}

export namespace ListInsectRowResponse {
  export type AsObject = {
    itemsList: Array<InsectRow.AsObject>,
    pagination?: v1_dictionary_common_dictionary_common_pb.PaginationResponse.AsObject,
  }
}

export class GetInsectRowRequest extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetInsectRowRequest.AsObject;
  static toObject(includeInstance: boolean, msg: GetInsectRowRequest): GetInsectRowRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetInsectRowRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetInsectRowRequest;
  static deserializeBinaryFromReader(message: GetInsectRowRequest, reader: jspb.BinaryReader): GetInsectRowRequest;
}

export namespace GetInsectRowRequest {
  export type AsObject = {
    id: number,
  }
}

export class GetInsectRowResponse extends jspb.Message {
  hasItem(): boolean;
  clearItem(): void;
  getItem(): InsectRow | undefined;
  setItem(value?: InsectRow): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetInsectRowResponse.AsObject;
  static toObject(includeInstance: boolean, msg: GetInsectRowResponse): GetInsectRowResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetInsectRowResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetInsectRowResponse;
  static deserializeBinaryFromReader(message: GetInsectRowResponse, reader: jspb.BinaryReader): GetInsectRowResponse;
}

export namespace GetInsectRowResponse {
  export type AsObject = {
    item?: InsectRow.AsObject,
  }
}

