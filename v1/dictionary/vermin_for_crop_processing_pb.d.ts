// package: fcp.dictionary.v1.dictionary
// file: v1/dictionary/vermin_for_crop_processing.proto

import * as jspb from "google-protobuf";
import * as google_protobuf_wrappers_pb from "google-protobuf/google/protobuf/wrappers_pb";
import * as google_protobuf_timestamp_pb from "google-protobuf/google/protobuf/timestamp_pb";
import * as v1_dictionary_common_dictionary_common_pb from "../../v1/dictionary_common/dictionary_common_pb";

export class VerminForCropProcessing extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  getVerminId(): number;
  setVerminId(value: number): void;

  getVerminName(): string;
  setVerminName(value: string): void;

  getVerminGroupId(): number;
  setVerminGroupId(value: number): void;

  getVerminGroupName(): string;
  setVerminGroupName(value: string): void;

  getTechnologyCropProcessingId(): number;
  setTechnologyCropProcessingId(value: number): void;

  hasHash(): boolean;
  clearHash(): void;
  getHash(): google_protobuf_wrappers_pb.StringValue | undefined;
  setHash(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasDeleted(): boolean;
  clearDeleted(): void;
  getDeleted(): google_protobuf_wrappers_pb.BoolValue | undefined;
  setDeleted(value?: google_protobuf_wrappers_pb.BoolValue): void;

  hasCreatedBy(): boolean;
  clearCreatedBy(): void;
  getCreatedBy(): google_protobuf_wrappers_pb.StringValue | undefined;
  setCreatedBy(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasCreatedAt(): boolean;
  clearCreatedAt(): void;
  getCreatedAt(): google_protobuf_timestamp_pb.Timestamp | undefined;
  setCreatedAt(value?: google_protobuf_timestamp_pb.Timestamp): void;

  hasUpdatedBy(): boolean;
  clearUpdatedBy(): void;
  getUpdatedBy(): google_protobuf_wrappers_pb.StringValue | undefined;
  setUpdatedBy(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasUpdatedAt(): boolean;
  clearUpdatedAt(): void;
  getUpdatedAt(): google_protobuf_timestamp_pb.Timestamp | undefined;
  setUpdatedAt(value?: google_protobuf_timestamp_pb.Timestamp): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): VerminForCropProcessing.AsObject;
  static toObject(includeInstance: boolean, msg: VerminForCropProcessing): VerminForCropProcessing.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: VerminForCropProcessing, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): VerminForCropProcessing;
  static deserializeBinaryFromReader(message: VerminForCropProcessing, reader: jspb.BinaryReader): VerminForCropProcessing;
}

export namespace VerminForCropProcessing {
  export type AsObject = {
    id: number,
    verminId: number,
    verminName: string,
    verminGroupId: number,
    verminGroupName: string,
    technologyCropProcessingId: number,
    hash?: google_protobuf_wrappers_pb.StringValue.AsObject,
    deleted?: google_protobuf_wrappers_pb.BoolValue.AsObject,
    createdBy?: google_protobuf_wrappers_pb.StringValue.AsObject,
    createdAt?: google_protobuf_timestamp_pb.Timestamp.AsObject,
    updatedBy?: google_protobuf_wrappers_pb.StringValue.AsObject,
    updatedAt?: google_protobuf_timestamp_pb.Timestamp.AsObject,
  }
}

export class VerminForCropProcessingFilter extends jspb.Message {
  hasId(): boolean;
  clearId(): void;
  getId(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setId(value?: google_protobuf_wrappers_pb.Int64Value): void;

  hasVerminId(): boolean;
  clearVerminId(): void;
  getVerminId(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setVerminId(value?: google_protobuf_wrappers_pb.Int64Value): void;

  hasVerminGroupId(): boolean;
  clearVerminGroupId(): void;
  getVerminGroupId(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setVerminGroupId(value?: google_protobuf_wrappers_pb.Int64Value): void;

  hasTechnologyCropProcessingId(): boolean;
  clearTechnologyCropProcessingId(): void;
  getTechnologyCropProcessingId(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setTechnologyCropProcessingId(value?: google_protobuf_wrappers_pb.Int64Value): void;

  hasDeleted(): boolean;
  clearDeleted(): void;
  getDeleted(): google_protobuf_wrappers_pb.BoolValue | undefined;
  setDeleted(value?: google_protobuf_wrappers_pb.BoolValue): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): VerminForCropProcessingFilter.AsObject;
  static toObject(includeInstance: boolean, msg: VerminForCropProcessingFilter): VerminForCropProcessingFilter.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: VerminForCropProcessingFilter, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): VerminForCropProcessingFilter;
  static deserializeBinaryFromReader(message: VerminForCropProcessingFilter, reader: jspb.BinaryReader): VerminForCropProcessingFilter;
}

export namespace VerminForCropProcessingFilter {
  export type AsObject = {
    id?: google_protobuf_wrappers_pb.Int64Value.AsObject,
    verminId?: google_protobuf_wrappers_pb.Int64Value.AsObject,
    verminGroupId?: google_protobuf_wrappers_pb.Int64Value.AsObject,
    technologyCropProcessingId?: google_protobuf_wrappers_pb.Int64Value.AsObject,
    deleted?: google_protobuf_wrappers_pb.BoolValue.AsObject,
  }
}

export class ListVerminForCropProcessingRequest extends jspb.Message {
  hasFilter(): boolean;
  clearFilter(): void;
  getFilter(): VerminForCropProcessingFilter | undefined;
  setFilter(value?: VerminForCropProcessingFilter): void;

  clearSortingsList(): void;
  getSortingsList(): Array<v1_dictionary_common_dictionary_common_pb.Sorting>;
  setSortingsList(value: Array<v1_dictionary_common_dictionary_common_pb.Sorting>): void;
  addSortings(value?: v1_dictionary_common_dictionary_common_pb.Sorting, index?: number): v1_dictionary_common_dictionary_common_pb.Sorting;

  hasPagination(): boolean;
  clearPagination(): void;
  getPagination(): v1_dictionary_common_dictionary_common_pb.PaginationRequest | undefined;
  setPagination(value?: v1_dictionary_common_dictionary_common_pb.PaginationRequest): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListVerminForCropProcessingRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ListVerminForCropProcessingRequest): ListVerminForCropProcessingRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListVerminForCropProcessingRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListVerminForCropProcessingRequest;
  static deserializeBinaryFromReader(message: ListVerminForCropProcessingRequest, reader: jspb.BinaryReader): ListVerminForCropProcessingRequest;
}

export namespace ListVerminForCropProcessingRequest {
  export type AsObject = {
    filter?: VerminForCropProcessingFilter.AsObject,
    sortingsList: Array<v1_dictionary_common_dictionary_common_pb.Sorting.AsObject>,
    pagination?: v1_dictionary_common_dictionary_common_pb.PaginationRequest.AsObject,
  }
}

export class ListVerminForCropProcessingResponse extends jspb.Message {
  clearItemsList(): void;
  getItemsList(): Array<VerminForCropProcessing>;
  setItemsList(value: Array<VerminForCropProcessing>): void;
  addItems(value?: VerminForCropProcessing, index?: number): VerminForCropProcessing;

  hasPagination(): boolean;
  clearPagination(): void;
  getPagination(): v1_dictionary_common_dictionary_common_pb.PaginationResponse | undefined;
  setPagination(value?: v1_dictionary_common_dictionary_common_pb.PaginationResponse): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListVerminForCropProcessingResponse.AsObject;
  static toObject(includeInstance: boolean, msg: ListVerminForCropProcessingResponse): ListVerminForCropProcessingResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListVerminForCropProcessingResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListVerminForCropProcessingResponse;
  static deserializeBinaryFromReader(message: ListVerminForCropProcessingResponse, reader: jspb.BinaryReader): ListVerminForCropProcessingResponse;
}

export namespace ListVerminForCropProcessingResponse {
  export type AsObject = {
    itemsList: Array<VerminForCropProcessing.AsObject>,
    pagination?: v1_dictionary_common_dictionary_common_pb.PaginationResponse.AsObject,
  }
}

export class GetVerminForCropProcessingRequest extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetVerminForCropProcessingRequest.AsObject;
  static toObject(includeInstance: boolean, msg: GetVerminForCropProcessingRequest): GetVerminForCropProcessingRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetVerminForCropProcessingRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetVerminForCropProcessingRequest;
  static deserializeBinaryFromReader(message: GetVerminForCropProcessingRequest, reader: jspb.BinaryReader): GetVerminForCropProcessingRequest;
}

export namespace GetVerminForCropProcessingRequest {
  export type AsObject = {
    id: number,
  }
}

export class GetVerminForCropProcessingResponse extends jspb.Message {
  hasItem(): boolean;
  clearItem(): void;
  getItem(): VerminForCropProcessing | undefined;
  setItem(value?: VerminForCropProcessing): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetVerminForCropProcessingResponse.AsObject;
  static toObject(includeInstance: boolean, msg: GetVerminForCropProcessingResponse): GetVerminForCropProcessingResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetVerminForCropProcessingResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetVerminForCropProcessingResponse;
  static deserializeBinaryFromReader(message: GetVerminForCropProcessingResponse, reader: jspb.BinaryReader): GetVerminForCropProcessingResponse;
}

export namespace GetVerminForCropProcessingResponse {
  export type AsObject = {
    item?: VerminForCropProcessing.AsObject,
  }
}

