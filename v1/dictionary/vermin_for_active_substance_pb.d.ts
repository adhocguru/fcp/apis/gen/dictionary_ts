// package: fcp.dictionary.v1.dictionary
// file: v1/dictionary/vermin_for_active_substance.proto

import * as jspb from "google-protobuf";
import * as google_protobuf_wrappers_pb from "google-protobuf/google/protobuf/wrappers_pb";
import * as google_protobuf_timestamp_pb from "google-protobuf/google/protobuf/timestamp_pb";
import * as v1_dictionary_common_dictionary_common_pb from "../../v1/dictionary_common/dictionary_common_pb";

export class VerminForActiveSubstance extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  getVerminForPrepId(): number;
  setVerminForPrepId(value: number): void;

  getVerminId(): number;
  setVerminId(value: number): void;

  getVerminName(): string;
  setVerminName(value: string): void;

  getActiveSubstanceId(): number;
  setActiveSubstanceId(value: number): void;

  getActiveSubstanceName(): string;
  setActiveSubstanceName(value: string): void;

  hasHash(): boolean;
  clearHash(): void;
  getHash(): google_protobuf_wrappers_pb.StringValue | undefined;
  setHash(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasDeleted(): boolean;
  clearDeleted(): void;
  getDeleted(): google_protobuf_wrappers_pb.BoolValue | undefined;
  setDeleted(value?: google_protobuf_wrappers_pb.BoolValue): void;

  hasCreatedBy(): boolean;
  clearCreatedBy(): void;
  getCreatedBy(): google_protobuf_wrappers_pb.StringValue | undefined;
  setCreatedBy(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasCreatedAt(): boolean;
  clearCreatedAt(): void;
  getCreatedAt(): google_protobuf_timestamp_pb.Timestamp | undefined;
  setCreatedAt(value?: google_protobuf_timestamp_pb.Timestamp): void;

  hasUpdatedBy(): boolean;
  clearUpdatedBy(): void;
  getUpdatedBy(): google_protobuf_wrappers_pb.StringValue | undefined;
  setUpdatedBy(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasUpdatedAt(): boolean;
  clearUpdatedAt(): void;
  getUpdatedAt(): google_protobuf_timestamp_pb.Timestamp | undefined;
  setUpdatedAt(value?: google_protobuf_timestamp_pb.Timestamp): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): VerminForActiveSubstance.AsObject;
  static toObject(includeInstance: boolean, msg: VerminForActiveSubstance): VerminForActiveSubstance.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: VerminForActiveSubstance, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): VerminForActiveSubstance;
  static deserializeBinaryFromReader(message: VerminForActiveSubstance, reader: jspb.BinaryReader): VerminForActiveSubstance;
}

export namespace VerminForActiveSubstance {
  export type AsObject = {
    id: number,
    verminForPrepId: number,
    verminId: number,
    verminName: string,
    activeSubstanceId: number,
    activeSubstanceName: string,
    hash?: google_protobuf_wrappers_pb.StringValue.AsObject,
    deleted?: google_protobuf_wrappers_pb.BoolValue.AsObject,
    createdBy?: google_protobuf_wrappers_pb.StringValue.AsObject,
    createdAt?: google_protobuf_timestamp_pb.Timestamp.AsObject,
    updatedBy?: google_protobuf_wrappers_pb.StringValue.AsObject,
    updatedAt?: google_protobuf_timestamp_pb.Timestamp.AsObject,
  }
}

export class VerminForActiveSubstanceFilter extends jspb.Message {
  hasId(): boolean;
  clearId(): void;
  getId(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setId(value?: google_protobuf_wrappers_pb.Int64Value): void;

  hasVerminForPrepId(): boolean;
  clearVerminForPrepId(): void;
  getVerminForPrepId(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setVerminForPrepId(value?: google_protobuf_wrappers_pb.Int64Value): void;

  hasVerminId(): boolean;
  clearVerminId(): void;
  getVerminId(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setVerminId(value?: google_protobuf_wrappers_pb.Int64Value): void;

  hasActiveSubstanceId(): boolean;
  clearActiveSubstanceId(): void;
  getActiveSubstanceId(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setActiveSubstanceId(value?: google_protobuf_wrappers_pb.Int64Value): void;

  hasDeleted(): boolean;
  clearDeleted(): void;
  getDeleted(): google_protobuf_wrappers_pb.BoolValue | undefined;
  setDeleted(value?: google_protobuf_wrappers_pb.BoolValue): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): VerminForActiveSubstanceFilter.AsObject;
  static toObject(includeInstance: boolean, msg: VerminForActiveSubstanceFilter): VerminForActiveSubstanceFilter.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: VerminForActiveSubstanceFilter, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): VerminForActiveSubstanceFilter;
  static deserializeBinaryFromReader(message: VerminForActiveSubstanceFilter, reader: jspb.BinaryReader): VerminForActiveSubstanceFilter;
}

export namespace VerminForActiveSubstanceFilter {
  export type AsObject = {
    id?: google_protobuf_wrappers_pb.Int64Value.AsObject,
    verminForPrepId?: google_protobuf_wrappers_pb.Int64Value.AsObject,
    verminId?: google_protobuf_wrappers_pb.Int64Value.AsObject,
    activeSubstanceId?: google_protobuf_wrappers_pb.Int64Value.AsObject,
    deleted?: google_protobuf_wrappers_pb.BoolValue.AsObject,
  }
}

export class ListVerminForActiveSubstanceRequest extends jspb.Message {
  hasFilter(): boolean;
  clearFilter(): void;
  getFilter(): VerminForActiveSubstanceFilter | undefined;
  setFilter(value?: VerminForActiveSubstanceFilter): void;

  clearSortingsList(): void;
  getSortingsList(): Array<v1_dictionary_common_dictionary_common_pb.Sorting>;
  setSortingsList(value: Array<v1_dictionary_common_dictionary_common_pb.Sorting>): void;
  addSortings(value?: v1_dictionary_common_dictionary_common_pb.Sorting, index?: number): v1_dictionary_common_dictionary_common_pb.Sorting;

  hasPagination(): boolean;
  clearPagination(): void;
  getPagination(): v1_dictionary_common_dictionary_common_pb.PaginationRequest | undefined;
  setPagination(value?: v1_dictionary_common_dictionary_common_pb.PaginationRequest): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListVerminForActiveSubstanceRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ListVerminForActiveSubstanceRequest): ListVerminForActiveSubstanceRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListVerminForActiveSubstanceRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListVerminForActiveSubstanceRequest;
  static deserializeBinaryFromReader(message: ListVerminForActiveSubstanceRequest, reader: jspb.BinaryReader): ListVerminForActiveSubstanceRequest;
}

export namespace ListVerminForActiveSubstanceRequest {
  export type AsObject = {
    filter?: VerminForActiveSubstanceFilter.AsObject,
    sortingsList: Array<v1_dictionary_common_dictionary_common_pb.Sorting.AsObject>,
    pagination?: v1_dictionary_common_dictionary_common_pb.PaginationRequest.AsObject,
  }
}

export class ListVerminForActiveSubstanceResponse extends jspb.Message {
  clearItemsList(): void;
  getItemsList(): Array<VerminForActiveSubstance>;
  setItemsList(value: Array<VerminForActiveSubstance>): void;
  addItems(value?: VerminForActiveSubstance, index?: number): VerminForActiveSubstance;

  hasPagination(): boolean;
  clearPagination(): void;
  getPagination(): v1_dictionary_common_dictionary_common_pb.PaginationResponse | undefined;
  setPagination(value?: v1_dictionary_common_dictionary_common_pb.PaginationResponse): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListVerminForActiveSubstanceResponse.AsObject;
  static toObject(includeInstance: boolean, msg: ListVerminForActiveSubstanceResponse): ListVerminForActiveSubstanceResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListVerminForActiveSubstanceResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListVerminForActiveSubstanceResponse;
  static deserializeBinaryFromReader(message: ListVerminForActiveSubstanceResponse, reader: jspb.BinaryReader): ListVerminForActiveSubstanceResponse;
}

export namespace ListVerminForActiveSubstanceResponse {
  export type AsObject = {
    itemsList: Array<VerminForActiveSubstance.AsObject>,
    pagination?: v1_dictionary_common_dictionary_common_pb.PaginationResponse.AsObject,
  }
}

export class GetVerminForActiveSubstanceRequest extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetVerminForActiveSubstanceRequest.AsObject;
  static toObject(includeInstance: boolean, msg: GetVerminForActiveSubstanceRequest): GetVerminForActiveSubstanceRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetVerminForActiveSubstanceRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetVerminForActiveSubstanceRequest;
  static deserializeBinaryFromReader(message: GetVerminForActiveSubstanceRequest, reader: jspb.BinaryReader): GetVerminForActiveSubstanceRequest;
}

export namespace GetVerminForActiveSubstanceRequest {
  export type AsObject = {
    id: number,
  }
}

export class GetVerminForActiveSubstanceResponse extends jspb.Message {
  hasItem(): boolean;
  clearItem(): void;
  getItem(): VerminForActiveSubstance | undefined;
  setItem(value?: VerminForActiveSubstance): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetVerminForActiveSubstanceResponse.AsObject;
  static toObject(includeInstance: boolean, msg: GetVerminForActiveSubstanceResponse): GetVerminForActiveSubstanceResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetVerminForActiveSubstanceResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetVerminForActiveSubstanceResponse;
  static deserializeBinaryFromReader(message: GetVerminForActiveSubstanceResponse, reader: jspb.BinaryReader): GetVerminForActiveSubstanceResponse;
}

export namespace GetVerminForActiveSubstanceResponse {
  export type AsObject = {
    item?: VerminForActiveSubstance.AsObject,
  }
}

