// package: fcp.dictionary.v1.dictionary
// file: v1/dictionary/type_of_pathogen.proto

import * as jspb from "google-protobuf";
import * as google_protobuf_wrappers_pb from "google-protobuf/google/protobuf/wrappers_pb";
import * as google_protobuf_timestamp_pb from "google-protobuf/google/protobuf/timestamp_pb";
import * as v1_dictionary_common_dictionary_common_pb from "../../v1/dictionary_common/dictionary_common_pb";

export class TypeOfPathogen extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  getName(): string;
  setName(value: string): void;

  hasHash(): boolean;
  clearHash(): void;
  getHash(): google_protobuf_wrappers_pb.StringValue | undefined;
  setHash(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasDeleted(): boolean;
  clearDeleted(): void;
  getDeleted(): google_protobuf_wrappers_pb.BoolValue | undefined;
  setDeleted(value?: google_protobuf_wrappers_pb.BoolValue): void;

  hasCreatedBy(): boolean;
  clearCreatedBy(): void;
  getCreatedBy(): google_protobuf_wrappers_pb.StringValue | undefined;
  setCreatedBy(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasCreatedAt(): boolean;
  clearCreatedAt(): void;
  getCreatedAt(): google_protobuf_timestamp_pb.Timestamp | undefined;
  setCreatedAt(value?: google_protobuf_timestamp_pb.Timestamp): void;

  hasUpdatedBy(): boolean;
  clearUpdatedBy(): void;
  getUpdatedBy(): google_protobuf_wrappers_pb.StringValue | undefined;
  setUpdatedBy(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasUpdatedAt(): boolean;
  clearUpdatedAt(): void;
  getUpdatedAt(): google_protobuf_timestamp_pb.Timestamp | undefined;
  setUpdatedAt(value?: google_protobuf_timestamp_pb.Timestamp): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): TypeOfPathogen.AsObject;
  static toObject(includeInstance: boolean, msg: TypeOfPathogen): TypeOfPathogen.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: TypeOfPathogen, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): TypeOfPathogen;
  static deserializeBinaryFromReader(message: TypeOfPathogen, reader: jspb.BinaryReader): TypeOfPathogen;
}

export namespace TypeOfPathogen {
  export type AsObject = {
    id: number,
    name: string,
    hash?: google_protobuf_wrappers_pb.StringValue.AsObject,
    deleted?: google_protobuf_wrappers_pb.BoolValue.AsObject,
    createdBy?: google_protobuf_wrappers_pb.StringValue.AsObject,
    createdAt?: google_protobuf_timestamp_pb.Timestamp.AsObject,
    updatedBy?: google_protobuf_wrappers_pb.StringValue.AsObject,
    updatedAt?: google_protobuf_timestamp_pb.Timestamp.AsObject,
  }
}

export class TypeOfPathogenFilter extends jspb.Message {
  hasId(): boolean;
  clearId(): void;
  getId(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setId(value?: google_protobuf_wrappers_pb.Int64Value): void;

  hasName(): boolean;
  clearName(): void;
  getName(): google_protobuf_wrappers_pb.StringValue | undefined;
  setName(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasDeleted(): boolean;
  clearDeleted(): void;
  getDeleted(): google_protobuf_wrappers_pb.BoolValue | undefined;
  setDeleted(value?: google_protobuf_wrappers_pb.BoolValue): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): TypeOfPathogenFilter.AsObject;
  static toObject(includeInstance: boolean, msg: TypeOfPathogenFilter): TypeOfPathogenFilter.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: TypeOfPathogenFilter, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): TypeOfPathogenFilter;
  static deserializeBinaryFromReader(message: TypeOfPathogenFilter, reader: jspb.BinaryReader): TypeOfPathogenFilter;
}

export namespace TypeOfPathogenFilter {
  export type AsObject = {
    id?: google_protobuf_wrappers_pb.Int64Value.AsObject,
    name?: google_protobuf_wrappers_pb.StringValue.AsObject,
    deleted?: google_protobuf_wrappers_pb.BoolValue.AsObject,
  }
}

export class ListTypeOfPathogenRequest extends jspb.Message {
  hasFilter(): boolean;
  clearFilter(): void;
  getFilter(): TypeOfPathogenFilter | undefined;
  setFilter(value?: TypeOfPathogenFilter): void;

  clearSortingsList(): void;
  getSortingsList(): Array<v1_dictionary_common_dictionary_common_pb.Sorting>;
  setSortingsList(value: Array<v1_dictionary_common_dictionary_common_pb.Sorting>): void;
  addSortings(value?: v1_dictionary_common_dictionary_common_pb.Sorting, index?: number): v1_dictionary_common_dictionary_common_pb.Sorting;

  hasPagination(): boolean;
  clearPagination(): void;
  getPagination(): v1_dictionary_common_dictionary_common_pb.PaginationRequest | undefined;
  setPagination(value?: v1_dictionary_common_dictionary_common_pb.PaginationRequest): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListTypeOfPathogenRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ListTypeOfPathogenRequest): ListTypeOfPathogenRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListTypeOfPathogenRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListTypeOfPathogenRequest;
  static deserializeBinaryFromReader(message: ListTypeOfPathogenRequest, reader: jspb.BinaryReader): ListTypeOfPathogenRequest;
}

export namespace ListTypeOfPathogenRequest {
  export type AsObject = {
    filter?: TypeOfPathogenFilter.AsObject,
    sortingsList: Array<v1_dictionary_common_dictionary_common_pb.Sorting.AsObject>,
    pagination?: v1_dictionary_common_dictionary_common_pb.PaginationRequest.AsObject,
  }
}

export class ListTypeOfPathogenResponse extends jspb.Message {
  clearItemsList(): void;
  getItemsList(): Array<TypeOfPathogen>;
  setItemsList(value: Array<TypeOfPathogen>): void;
  addItems(value?: TypeOfPathogen, index?: number): TypeOfPathogen;

  hasPagination(): boolean;
  clearPagination(): void;
  getPagination(): v1_dictionary_common_dictionary_common_pb.PaginationResponse | undefined;
  setPagination(value?: v1_dictionary_common_dictionary_common_pb.PaginationResponse): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListTypeOfPathogenResponse.AsObject;
  static toObject(includeInstance: boolean, msg: ListTypeOfPathogenResponse): ListTypeOfPathogenResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListTypeOfPathogenResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListTypeOfPathogenResponse;
  static deserializeBinaryFromReader(message: ListTypeOfPathogenResponse, reader: jspb.BinaryReader): ListTypeOfPathogenResponse;
}

export namespace ListTypeOfPathogenResponse {
  export type AsObject = {
    itemsList: Array<TypeOfPathogen.AsObject>,
    pagination?: v1_dictionary_common_dictionary_common_pb.PaginationResponse.AsObject,
  }
}

export class GetTypeOfPathogenRequest extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetTypeOfPathogenRequest.AsObject;
  static toObject(includeInstance: boolean, msg: GetTypeOfPathogenRequest): GetTypeOfPathogenRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetTypeOfPathogenRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetTypeOfPathogenRequest;
  static deserializeBinaryFromReader(message: GetTypeOfPathogenRequest, reader: jspb.BinaryReader): GetTypeOfPathogenRequest;
}

export namespace GetTypeOfPathogenRequest {
  export type AsObject = {
    id: number,
  }
}

export class GetTypeOfPathogenResponse extends jspb.Message {
  hasItem(): boolean;
  clearItem(): void;
  getItem(): TypeOfPathogen | undefined;
  setItem(value?: TypeOfPathogen): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetTypeOfPathogenResponse.AsObject;
  static toObject(includeInstance: boolean, msg: GetTypeOfPathogenResponse): GetTypeOfPathogenResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetTypeOfPathogenResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetTypeOfPathogenResponse;
  static deserializeBinaryFromReader(message: GetTypeOfPathogenResponse, reader: jspb.BinaryReader): GetTypeOfPathogenResponse;
}

export namespace GetTypeOfPathogenResponse {
  export type AsObject = {
    item?: TypeOfPathogen.AsObject,
  }
}

