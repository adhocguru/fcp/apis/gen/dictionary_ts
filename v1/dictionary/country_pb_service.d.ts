// package: fcp.dictionary.v1.dictionary
// file: v1/dictionary/country.proto

import * as v1_dictionary_country_pb from "../../v1/dictionary/country_pb";
import * as google_protobuf_empty_pb from "google-protobuf/google/protobuf/empty_pb";
import {grpc} from "@improbable-eng/grpc-web";

type CountryServiceListCountry = {
  readonly methodName: string;
  readonly service: typeof CountryService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_dictionary_country_pb.ListCountryRequest;
  readonly responseType: typeof v1_dictionary_country_pb.ListCountryResponse;
};

type CountryServiceGetCountry = {
  readonly methodName: string;
  readonly service: typeof CountryService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_dictionary_country_pb.GetCountryRequest;
  readonly responseType: typeof v1_dictionary_country_pb.GetCountryResponse;
};

type CountryServiceCreateCountry = {
  readonly methodName: string;
  readonly service: typeof CountryService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_dictionary_country_pb.CreateCountryRequest;
  readonly responseType: typeof v1_dictionary_country_pb.CreateCountryResponse;
};

type CountryServiceUpdateCountry = {
  readonly methodName: string;
  readonly service: typeof CountryService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_dictionary_country_pb.UpdateCountryRequest;
  readonly responseType: typeof v1_dictionary_country_pb.UpdateCountryResponse;
};

type CountryServiceDeleteCountry = {
  readonly methodName: string;
  readonly service: typeof CountryService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_dictionary_country_pb.DeleteCountryRequest;
  readonly responseType: typeof google_protobuf_empty_pb.Empty;
};

type CountryServiceRestoreCountry = {
  readonly methodName: string;
  readonly service: typeof CountryService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_dictionary_country_pb.RestoreCountryRequest;
  readonly responseType: typeof google_protobuf_empty_pb.Empty;
};

export class CountryService {
  static readonly serviceName: string;
  static readonly ListCountry: CountryServiceListCountry;
  static readonly GetCountry: CountryServiceGetCountry;
  static readonly CreateCountry: CountryServiceCreateCountry;
  static readonly UpdateCountry: CountryServiceUpdateCountry;
  static readonly DeleteCountry: CountryServiceDeleteCountry;
  static readonly RestoreCountry: CountryServiceRestoreCountry;
}

export type ServiceError = { message: string, code: number; metadata: grpc.Metadata }
export type Status = { details: string, code: number; metadata: grpc.Metadata }

interface UnaryResponse {
  cancel(): void;
}
interface ResponseStream<T> {
  cancel(): void;
  on(type: 'data', handler: (message: T) => void): ResponseStream<T>;
  on(type: 'end', handler: (status?: Status) => void): ResponseStream<T>;
  on(type: 'status', handler: (status: Status) => void): ResponseStream<T>;
}
interface RequestStream<T> {
  write(message: T): RequestStream<T>;
  end(): void;
  cancel(): void;
  on(type: 'end', handler: (status?: Status) => void): RequestStream<T>;
  on(type: 'status', handler: (status: Status) => void): RequestStream<T>;
}
interface BidirectionalStream<ReqT, ResT> {
  write(message: ReqT): BidirectionalStream<ReqT, ResT>;
  end(): void;
  cancel(): void;
  on(type: 'data', handler: (message: ResT) => void): BidirectionalStream<ReqT, ResT>;
  on(type: 'end', handler: (status?: Status) => void): BidirectionalStream<ReqT, ResT>;
  on(type: 'status', handler: (status: Status) => void): BidirectionalStream<ReqT, ResT>;
}

export class CountryServiceClient {
  readonly serviceHost: string;

  constructor(serviceHost: string, options?: grpc.RpcOptions);
  listCountry(
    requestMessage: v1_dictionary_country_pb.ListCountryRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_country_pb.ListCountryResponse|null) => void
  ): UnaryResponse;
  listCountry(
    requestMessage: v1_dictionary_country_pb.ListCountryRequest,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_country_pb.ListCountryResponse|null) => void
  ): UnaryResponse;
  getCountry(
    requestMessage: v1_dictionary_country_pb.GetCountryRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_country_pb.GetCountryResponse|null) => void
  ): UnaryResponse;
  getCountry(
    requestMessage: v1_dictionary_country_pb.GetCountryRequest,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_country_pb.GetCountryResponse|null) => void
  ): UnaryResponse;
  createCountry(
    requestMessage: v1_dictionary_country_pb.CreateCountryRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_country_pb.CreateCountryResponse|null) => void
  ): UnaryResponse;
  createCountry(
    requestMessage: v1_dictionary_country_pb.CreateCountryRequest,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_country_pb.CreateCountryResponse|null) => void
  ): UnaryResponse;
  updateCountry(
    requestMessage: v1_dictionary_country_pb.UpdateCountryRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_country_pb.UpdateCountryResponse|null) => void
  ): UnaryResponse;
  updateCountry(
    requestMessage: v1_dictionary_country_pb.UpdateCountryRequest,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_country_pb.UpdateCountryResponse|null) => void
  ): UnaryResponse;
  deleteCountry(
    requestMessage: v1_dictionary_country_pb.DeleteCountryRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: google_protobuf_empty_pb.Empty|null) => void
  ): UnaryResponse;
  deleteCountry(
    requestMessage: v1_dictionary_country_pb.DeleteCountryRequest,
    callback: (error: ServiceError|null, responseMessage: google_protobuf_empty_pb.Empty|null) => void
  ): UnaryResponse;
  restoreCountry(
    requestMessage: v1_dictionary_country_pb.RestoreCountryRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: google_protobuf_empty_pb.Empty|null) => void
  ): UnaryResponse;
  restoreCountry(
    requestMessage: v1_dictionary_country_pb.RestoreCountryRequest,
    callback: (error: ServiceError|null, responseMessage: google_protobuf_empty_pb.Empty|null) => void
  ): UnaryResponse;
}

