// package: fcp.dictionary.v1.dictionary
// file: v1/dictionary/preparative_form.proto

import * as jspb from "google-protobuf";
import * as google_protobuf_wrappers_pb from "google-protobuf/google/protobuf/wrappers_pb";
import * as google_protobuf_timestamp_pb from "google-protobuf/google/protobuf/timestamp_pb";
import * as v1_dictionary_common_dictionary_common_pb from "../../v1/dictionary_common/dictionary_common_pb";

export class PreparativeForm extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  getGuid(): string;
  setGuid(value: string): void;

  getName(): string;
  setName(value: string): void;

  hasHash(): boolean;
  clearHash(): void;
  getHash(): google_protobuf_wrappers_pb.StringValue | undefined;
  setHash(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasDeleted(): boolean;
  clearDeleted(): void;
  getDeleted(): google_protobuf_wrappers_pb.BoolValue | undefined;
  setDeleted(value?: google_protobuf_wrappers_pb.BoolValue): void;

  hasCreatedBy(): boolean;
  clearCreatedBy(): void;
  getCreatedBy(): google_protobuf_wrappers_pb.StringValue | undefined;
  setCreatedBy(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasCreatedAt(): boolean;
  clearCreatedAt(): void;
  getCreatedAt(): google_protobuf_timestamp_pb.Timestamp | undefined;
  setCreatedAt(value?: google_protobuf_timestamp_pb.Timestamp): void;

  hasUpdatedBy(): boolean;
  clearUpdatedBy(): void;
  getUpdatedBy(): google_protobuf_wrappers_pb.StringValue | undefined;
  setUpdatedBy(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasUpdatedAt(): boolean;
  clearUpdatedAt(): void;
  getUpdatedAt(): google_protobuf_timestamp_pb.Timestamp | undefined;
  setUpdatedAt(value?: google_protobuf_timestamp_pb.Timestamp): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): PreparativeForm.AsObject;
  static toObject(includeInstance: boolean, msg: PreparativeForm): PreparativeForm.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: PreparativeForm, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): PreparativeForm;
  static deserializeBinaryFromReader(message: PreparativeForm, reader: jspb.BinaryReader): PreparativeForm;
}

export namespace PreparativeForm {
  export type AsObject = {
    id: number,
    guid: string,
    name: string,
    hash?: google_protobuf_wrappers_pb.StringValue.AsObject,
    deleted?: google_protobuf_wrappers_pb.BoolValue.AsObject,
    createdBy?: google_protobuf_wrappers_pb.StringValue.AsObject,
    createdAt?: google_protobuf_timestamp_pb.Timestamp.AsObject,
    updatedBy?: google_protobuf_wrappers_pb.StringValue.AsObject,
    updatedAt?: google_protobuf_timestamp_pb.Timestamp.AsObject,
  }
}

export class PreparativeFormFilter extends jspb.Message {
  hasId(): boolean;
  clearId(): void;
  getId(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setId(value?: google_protobuf_wrappers_pb.Int64Value): void;

  hasGuid(): boolean;
  clearGuid(): void;
  getGuid(): google_protobuf_wrappers_pb.StringValue | undefined;
  setGuid(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasName(): boolean;
  clearName(): void;
  getName(): google_protobuf_wrappers_pb.StringValue | undefined;
  setName(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasDeleted(): boolean;
  clearDeleted(): void;
  getDeleted(): google_protobuf_wrappers_pb.BoolValue | undefined;
  setDeleted(value?: google_protobuf_wrappers_pb.BoolValue): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): PreparativeFormFilter.AsObject;
  static toObject(includeInstance: boolean, msg: PreparativeFormFilter): PreparativeFormFilter.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: PreparativeFormFilter, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): PreparativeFormFilter;
  static deserializeBinaryFromReader(message: PreparativeFormFilter, reader: jspb.BinaryReader): PreparativeFormFilter;
}

export namespace PreparativeFormFilter {
  export type AsObject = {
    id?: google_protobuf_wrappers_pb.Int64Value.AsObject,
    guid?: google_protobuf_wrappers_pb.StringValue.AsObject,
    name?: google_protobuf_wrappers_pb.StringValue.AsObject,
    deleted?: google_protobuf_wrappers_pb.BoolValue.AsObject,
  }
}

export class ListPreparativeFormRequest extends jspb.Message {
  hasFilter(): boolean;
  clearFilter(): void;
  getFilter(): PreparativeFormFilter | undefined;
  setFilter(value?: PreparativeFormFilter): void;

  clearSortingsList(): void;
  getSortingsList(): Array<v1_dictionary_common_dictionary_common_pb.Sorting>;
  setSortingsList(value: Array<v1_dictionary_common_dictionary_common_pb.Sorting>): void;
  addSortings(value?: v1_dictionary_common_dictionary_common_pb.Sorting, index?: number): v1_dictionary_common_dictionary_common_pb.Sorting;

  hasPagination(): boolean;
  clearPagination(): void;
  getPagination(): v1_dictionary_common_dictionary_common_pb.PaginationRequest | undefined;
  setPagination(value?: v1_dictionary_common_dictionary_common_pb.PaginationRequest): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListPreparativeFormRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ListPreparativeFormRequest): ListPreparativeFormRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListPreparativeFormRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListPreparativeFormRequest;
  static deserializeBinaryFromReader(message: ListPreparativeFormRequest, reader: jspb.BinaryReader): ListPreparativeFormRequest;
}

export namespace ListPreparativeFormRequest {
  export type AsObject = {
    filter?: PreparativeFormFilter.AsObject,
    sortingsList: Array<v1_dictionary_common_dictionary_common_pb.Sorting.AsObject>,
    pagination?: v1_dictionary_common_dictionary_common_pb.PaginationRequest.AsObject,
  }
}

export class ListPreparativeFormResponse extends jspb.Message {
  clearItemsList(): void;
  getItemsList(): Array<PreparativeForm>;
  setItemsList(value: Array<PreparativeForm>): void;
  addItems(value?: PreparativeForm, index?: number): PreparativeForm;

  hasPagination(): boolean;
  clearPagination(): void;
  getPagination(): v1_dictionary_common_dictionary_common_pb.PaginationResponse | undefined;
  setPagination(value?: v1_dictionary_common_dictionary_common_pb.PaginationResponse): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListPreparativeFormResponse.AsObject;
  static toObject(includeInstance: boolean, msg: ListPreparativeFormResponse): ListPreparativeFormResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListPreparativeFormResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListPreparativeFormResponse;
  static deserializeBinaryFromReader(message: ListPreparativeFormResponse, reader: jspb.BinaryReader): ListPreparativeFormResponse;
}

export namespace ListPreparativeFormResponse {
  export type AsObject = {
    itemsList: Array<PreparativeForm.AsObject>,
    pagination?: v1_dictionary_common_dictionary_common_pb.PaginationResponse.AsObject,
  }
}

export class GetPreparativeFormRequest extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetPreparativeFormRequest.AsObject;
  static toObject(includeInstance: boolean, msg: GetPreparativeFormRequest): GetPreparativeFormRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetPreparativeFormRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetPreparativeFormRequest;
  static deserializeBinaryFromReader(message: GetPreparativeFormRequest, reader: jspb.BinaryReader): GetPreparativeFormRequest;
}

export namespace GetPreparativeFormRequest {
  export type AsObject = {
    id: number,
  }
}

export class GetPreparativeFormResponse extends jspb.Message {
  hasItem(): boolean;
  clearItem(): void;
  getItem(): PreparativeForm | undefined;
  setItem(value?: PreparativeForm): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetPreparativeFormResponse.AsObject;
  static toObject(includeInstance: boolean, msg: GetPreparativeFormResponse): GetPreparativeFormResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetPreparativeFormResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetPreparativeFormResponse;
  static deserializeBinaryFromReader(message: GetPreparativeFormResponse, reader: jspb.BinaryReader): GetPreparativeFormResponse;
}

export namespace GetPreparativeFormResponse {
  export type AsObject = {
    item?: PreparativeForm.AsObject,
  }
}

