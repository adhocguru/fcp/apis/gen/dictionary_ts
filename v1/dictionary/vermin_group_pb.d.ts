// package: fcp.dictionary.v1.dictionary
// file: v1/dictionary/vermin_group.proto

import * as jspb from "google-protobuf";
import * as google_protobuf_wrappers_pb from "google-protobuf/google/protobuf/wrappers_pb";
import * as google_protobuf_timestamp_pb from "google-protobuf/google/protobuf/timestamp_pb";
import * as v1_dictionary_common_dictionary_common_pb from "../../v1/dictionary_common/dictionary_common_pb";

export class VerminGroup extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  getName(): string;
  setName(value: string): void;

  hasHash(): boolean;
  clearHash(): void;
  getHash(): google_protobuf_wrappers_pb.StringValue | undefined;
  setHash(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasDeleted(): boolean;
  clearDeleted(): void;
  getDeleted(): google_protobuf_wrappers_pb.BoolValue | undefined;
  setDeleted(value?: google_protobuf_wrappers_pb.BoolValue): void;

  hasCreatedBy(): boolean;
  clearCreatedBy(): void;
  getCreatedBy(): google_protobuf_wrappers_pb.StringValue | undefined;
  setCreatedBy(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasCreatedAt(): boolean;
  clearCreatedAt(): void;
  getCreatedAt(): google_protobuf_timestamp_pb.Timestamp | undefined;
  setCreatedAt(value?: google_protobuf_timestamp_pb.Timestamp): void;

  hasUpdatedBy(): boolean;
  clearUpdatedBy(): void;
  getUpdatedBy(): google_protobuf_wrappers_pb.StringValue | undefined;
  setUpdatedBy(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasUpdatedAt(): boolean;
  clearUpdatedAt(): void;
  getUpdatedAt(): google_protobuf_timestamp_pb.Timestamp | undefined;
  setUpdatedAt(value?: google_protobuf_timestamp_pb.Timestamp): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): VerminGroup.AsObject;
  static toObject(includeInstance: boolean, msg: VerminGroup): VerminGroup.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: VerminGroup, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): VerminGroup;
  static deserializeBinaryFromReader(message: VerminGroup, reader: jspb.BinaryReader): VerminGroup;
}

export namespace VerminGroup {
  export type AsObject = {
    id: number,
    name: string,
    hash?: google_protobuf_wrappers_pb.StringValue.AsObject,
    deleted?: google_protobuf_wrappers_pb.BoolValue.AsObject,
    createdBy?: google_protobuf_wrappers_pb.StringValue.AsObject,
    createdAt?: google_protobuf_timestamp_pb.Timestamp.AsObject,
    updatedBy?: google_protobuf_wrappers_pb.StringValue.AsObject,
    updatedAt?: google_protobuf_timestamp_pb.Timestamp.AsObject,
  }
}

export class VerminGroupFilter extends jspb.Message {
  hasId(): boolean;
  clearId(): void;
  getId(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setId(value?: google_protobuf_wrappers_pb.Int64Value): void;

  hasName(): boolean;
  clearName(): void;
  getName(): google_protobuf_wrappers_pb.StringValue | undefined;
  setName(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasDeleted(): boolean;
  clearDeleted(): void;
  getDeleted(): google_protobuf_wrappers_pb.BoolValue | undefined;
  setDeleted(value?: google_protobuf_wrappers_pb.BoolValue): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): VerminGroupFilter.AsObject;
  static toObject(includeInstance: boolean, msg: VerminGroupFilter): VerminGroupFilter.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: VerminGroupFilter, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): VerminGroupFilter;
  static deserializeBinaryFromReader(message: VerminGroupFilter, reader: jspb.BinaryReader): VerminGroupFilter;
}

export namespace VerminGroupFilter {
  export type AsObject = {
    id?: google_protobuf_wrappers_pb.Int64Value.AsObject,
    name?: google_protobuf_wrappers_pb.StringValue.AsObject,
    deleted?: google_protobuf_wrappers_pb.BoolValue.AsObject,
  }
}

export class ListVerminGroupRequest extends jspb.Message {
  hasFilter(): boolean;
  clearFilter(): void;
  getFilter(): VerminGroupFilter | undefined;
  setFilter(value?: VerminGroupFilter): void;

  clearSortingsList(): void;
  getSortingsList(): Array<v1_dictionary_common_dictionary_common_pb.Sorting>;
  setSortingsList(value: Array<v1_dictionary_common_dictionary_common_pb.Sorting>): void;
  addSortings(value?: v1_dictionary_common_dictionary_common_pb.Sorting, index?: number): v1_dictionary_common_dictionary_common_pb.Sorting;

  hasPagination(): boolean;
  clearPagination(): void;
  getPagination(): v1_dictionary_common_dictionary_common_pb.PaginationRequest | undefined;
  setPagination(value?: v1_dictionary_common_dictionary_common_pb.PaginationRequest): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListVerminGroupRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ListVerminGroupRequest): ListVerminGroupRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListVerminGroupRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListVerminGroupRequest;
  static deserializeBinaryFromReader(message: ListVerminGroupRequest, reader: jspb.BinaryReader): ListVerminGroupRequest;
}

export namespace ListVerminGroupRequest {
  export type AsObject = {
    filter?: VerminGroupFilter.AsObject,
    sortingsList: Array<v1_dictionary_common_dictionary_common_pb.Sorting.AsObject>,
    pagination?: v1_dictionary_common_dictionary_common_pb.PaginationRequest.AsObject,
  }
}

export class ListVerminGroupResponse extends jspb.Message {
  clearItemsList(): void;
  getItemsList(): Array<VerminGroup>;
  setItemsList(value: Array<VerminGroup>): void;
  addItems(value?: VerminGroup, index?: number): VerminGroup;

  hasPagination(): boolean;
  clearPagination(): void;
  getPagination(): v1_dictionary_common_dictionary_common_pb.PaginationResponse | undefined;
  setPagination(value?: v1_dictionary_common_dictionary_common_pb.PaginationResponse): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListVerminGroupResponse.AsObject;
  static toObject(includeInstance: boolean, msg: ListVerminGroupResponse): ListVerminGroupResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListVerminGroupResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListVerminGroupResponse;
  static deserializeBinaryFromReader(message: ListVerminGroupResponse, reader: jspb.BinaryReader): ListVerminGroupResponse;
}

export namespace ListVerminGroupResponse {
  export type AsObject = {
    itemsList: Array<VerminGroup.AsObject>,
    pagination?: v1_dictionary_common_dictionary_common_pb.PaginationResponse.AsObject,
  }
}

export class GetVerminGroupRequest extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetVerminGroupRequest.AsObject;
  static toObject(includeInstance: boolean, msg: GetVerminGroupRequest): GetVerminGroupRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetVerminGroupRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetVerminGroupRequest;
  static deserializeBinaryFromReader(message: GetVerminGroupRequest, reader: jspb.BinaryReader): GetVerminGroupRequest;
}

export namespace GetVerminGroupRequest {
  export type AsObject = {
    id: number,
  }
}

export class GetVerminGroupResponse extends jspb.Message {
  hasItem(): boolean;
  clearItem(): void;
  getItem(): VerminGroup | undefined;
  setItem(value?: VerminGroup): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetVerminGroupResponse.AsObject;
  static toObject(includeInstance: boolean, msg: GetVerminGroupResponse): GetVerminGroupResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetVerminGroupResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetVerminGroupResponse;
  static deserializeBinaryFromReader(message: GetVerminGroupResponse, reader: jspb.BinaryReader): GetVerminGroupResponse;
}

export namespace GetVerminGroupResponse {
  export type AsObject = {
    item?: VerminGroup.AsObject,
  }
}

