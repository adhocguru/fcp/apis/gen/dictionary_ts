// package: fcp.dictionary.v1.dictionary
// file: v1/dictionary/hybrids_groups_for_culture.proto

import * as jspb from "google-protobuf";
import * as google_protobuf_wrappers_pb from "google-protobuf/google/protobuf/wrappers_pb";
import * as google_protobuf_timestamp_pb from "google-protobuf/google/protobuf/timestamp_pb";
import * as v1_dictionary_common_dictionary_common_pb from "../../v1/dictionary_common/dictionary_common_pb";

export class HybridsGroupsForCulture extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  getCultureId(): number;
  setCultureId(value: number): void;

  getCultureName(): string;
  setCultureName(value: string): void;

  getHybridsGroupId(): number;
  setHybridsGroupId(value: number): void;

  getHybridsGroupName(): string;
  setHybridsGroupName(value: string): void;

  hasHash(): boolean;
  clearHash(): void;
  getHash(): google_protobuf_wrappers_pb.StringValue | undefined;
  setHash(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasDeleted(): boolean;
  clearDeleted(): void;
  getDeleted(): google_protobuf_wrappers_pb.BoolValue | undefined;
  setDeleted(value?: google_protobuf_wrappers_pb.BoolValue): void;

  hasCreatedBy(): boolean;
  clearCreatedBy(): void;
  getCreatedBy(): google_protobuf_wrappers_pb.StringValue | undefined;
  setCreatedBy(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasCreatedAt(): boolean;
  clearCreatedAt(): void;
  getCreatedAt(): google_protobuf_timestamp_pb.Timestamp | undefined;
  setCreatedAt(value?: google_protobuf_timestamp_pb.Timestamp): void;

  hasUpdatedBy(): boolean;
  clearUpdatedBy(): void;
  getUpdatedBy(): google_protobuf_wrappers_pb.StringValue | undefined;
  setUpdatedBy(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasUpdatedAt(): boolean;
  clearUpdatedAt(): void;
  getUpdatedAt(): google_protobuf_timestamp_pb.Timestamp | undefined;
  setUpdatedAt(value?: google_protobuf_timestamp_pb.Timestamp): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): HybridsGroupsForCulture.AsObject;
  static toObject(includeInstance: boolean, msg: HybridsGroupsForCulture): HybridsGroupsForCulture.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: HybridsGroupsForCulture, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): HybridsGroupsForCulture;
  static deserializeBinaryFromReader(message: HybridsGroupsForCulture, reader: jspb.BinaryReader): HybridsGroupsForCulture;
}

export namespace HybridsGroupsForCulture {
  export type AsObject = {
    id: number,
    cultureId: number,
    cultureName: string,
    hybridsGroupId: number,
    hybridsGroupName: string,
    hash?: google_protobuf_wrappers_pb.StringValue.AsObject,
    deleted?: google_protobuf_wrappers_pb.BoolValue.AsObject,
    createdBy?: google_protobuf_wrappers_pb.StringValue.AsObject,
    createdAt?: google_protobuf_timestamp_pb.Timestamp.AsObject,
    updatedBy?: google_protobuf_wrappers_pb.StringValue.AsObject,
    updatedAt?: google_protobuf_timestamp_pb.Timestamp.AsObject,
  }
}

export class HybridsGroupsForCultureFilter extends jspb.Message {
  hasId(): boolean;
  clearId(): void;
  getId(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setId(value?: google_protobuf_wrappers_pb.Int64Value): void;

  hasCultureId(): boolean;
  clearCultureId(): void;
  getCultureId(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setCultureId(value?: google_protobuf_wrappers_pb.Int64Value): void;

  hasHybridsGroupId(): boolean;
  clearHybridsGroupId(): void;
  getHybridsGroupId(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setHybridsGroupId(value?: google_protobuf_wrappers_pb.Int64Value): void;

  hasDeleted(): boolean;
  clearDeleted(): void;
  getDeleted(): google_protobuf_wrappers_pb.BoolValue | undefined;
  setDeleted(value?: google_protobuf_wrappers_pb.BoolValue): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): HybridsGroupsForCultureFilter.AsObject;
  static toObject(includeInstance: boolean, msg: HybridsGroupsForCultureFilter): HybridsGroupsForCultureFilter.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: HybridsGroupsForCultureFilter, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): HybridsGroupsForCultureFilter;
  static deserializeBinaryFromReader(message: HybridsGroupsForCultureFilter, reader: jspb.BinaryReader): HybridsGroupsForCultureFilter;
}

export namespace HybridsGroupsForCultureFilter {
  export type AsObject = {
    id?: google_protobuf_wrappers_pb.Int64Value.AsObject,
    cultureId?: google_protobuf_wrappers_pb.Int64Value.AsObject,
    hybridsGroupId?: google_protobuf_wrappers_pb.Int64Value.AsObject,
    deleted?: google_protobuf_wrappers_pb.BoolValue.AsObject,
  }
}

export class ListHybridsGroupsForCultureRequest extends jspb.Message {
  hasFilter(): boolean;
  clearFilter(): void;
  getFilter(): HybridsGroupsForCultureFilter | undefined;
  setFilter(value?: HybridsGroupsForCultureFilter): void;

  clearSortingsList(): void;
  getSortingsList(): Array<v1_dictionary_common_dictionary_common_pb.Sorting>;
  setSortingsList(value: Array<v1_dictionary_common_dictionary_common_pb.Sorting>): void;
  addSortings(value?: v1_dictionary_common_dictionary_common_pb.Sorting, index?: number): v1_dictionary_common_dictionary_common_pb.Sorting;

  hasPagination(): boolean;
  clearPagination(): void;
  getPagination(): v1_dictionary_common_dictionary_common_pb.PaginationRequest | undefined;
  setPagination(value?: v1_dictionary_common_dictionary_common_pb.PaginationRequest): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListHybridsGroupsForCultureRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ListHybridsGroupsForCultureRequest): ListHybridsGroupsForCultureRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListHybridsGroupsForCultureRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListHybridsGroupsForCultureRequest;
  static deserializeBinaryFromReader(message: ListHybridsGroupsForCultureRequest, reader: jspb.BinaryReader): ListHybridsGroupsForCultureRequest;
}

export namespace ListHybridsGroupsForCultureRequest {
  export type AsObject = {
    filter?: HybridsGroupsForCultureFilter.AsObject,
    sortingsList: Array<v1_dictionary_common_dictionary_common_pb.Sorting.AsObject>,
    pagination?: v1_dictionary_common_dictionary_common_pb.PaginationRequest.AsObject,
  }
}

export class ListHybridsGroupsForCultureResponse extends jspb.Message {
  clearItemsList(): void;
  getItemsList(): Array<HybridsGroupsForCulture>;
  setItemsList(value: Array<HybridsGroupsForCulture>): void;
  addItems(value?: HybridsGroupsForCulture, index?: number): HybridsGroupsForCulture;

  hasPagination(): boolean;
  clearPagination(): void;
  getPagination(): v1_dictionary_common_dictionary_common_pb.PaginationResponse | undefined;
  setPagination(value?: v1_dictionary_common_dictionary_common_pb.PaginationResponse): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListHybridsGroupsForCultureResponse.AsObject;
  static toObject(includeInstance: boolean, msg: ListHybridsGroupsForCultureResponse): ListHybridsGroupsForCultureResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListHybridsGroupsForCultureResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListHybridsGroupsForCultureResponse;
  static deserializeBinaryFromReader(message: ListHybridsGroupsForCultureResponse, reader: jspb.BinaryReader): ListHybridsGroupsForCultureResponse;
}

export namespace ListHybridsGroupsForCultureResponse {
  export type AsObject = {
    itemsList: Array<HybridsGroupsForCulture.AsObject>,
    pagination?: v1_dictionary_common_dictionary_common_pb.PaginationResponse.AsObject,
  }
}

export class GetHybridsGroupsForCultureRequest extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetHybridsGroupsForCultureRequest.AsObject;
  static toObject(includeInstance: boolean, msg: GetHybridsGroupsForCultureRequest): GetHybridsGroupsForCultureRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetHybridsGroupsForCultureRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetHybridsGroupsForCultureRequest;
  static deserializeBinaryFromReader(message: GetHybridsGroupsForCultureRequest, reader: jspb.BinaryReader): GetHybridsGroupsForCultureRequest;
}

export namespace GetHybridsGroupsForCultureRequest {
  export type AsObject = {
    id: number,
  }
}

export class GetHybridsGroupsForCultureResponse extends jspb.Message {
  hasItem(): boolean;
  clearItem(): void;
  getItem(): HybridsGroupsForCulture | undefined;
  setItem(value?: HybridsGroupsForCulture): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetHybridsGroupsForCultureResponse.AsObject;
  static toObject(includeInstance: boolean, msg: GetHybridsGroupsForCultureResponse): GetHybridsGroupsForCultureResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetHybridsGroupsForCultureResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetHybridsGroupsForCultureResponse;
  static deserializeBinaryFromReader(message: GetHybridsGroupsForCultureResponse, reader: jspb.BinaryReader): GetHybridsGroupsForCultureResponse;
}

export namespace GetHybridsGroupsForCultureResponse {
  export type AsObject = {
    item?: HybridsGroupsForCulture.AsObject,
  }
}

