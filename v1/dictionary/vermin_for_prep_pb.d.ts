// package: fcp.dictionary.v1.dictionary
// file: v1/dictionary/vermin_for_prep.proto

import * as jspb from "google-protobuf";
import * as google_protobuf_wrappers_pb from "google-protobuf/google/protobuf/wrappers_pb";
import * as google_protobuf_timestamp_pb from "google-protobuf/google/protobuf/timestamp_pb";
import * as v1_dictionary_common_dictionary_common_pb from "../../v1/dictionary_common/dictionary_common_pb";

export class VerminForPrep extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  getVerminIds(): string;
  setVerminIds(value: string): void;

  getActiveSubstanceId(): number;
  setActiveSubstanceId(value: number): void;

  getActiveSubstanceName(): string;
  setActiveSubstanceName(value: string): void;

  hasHash(): boolean;
  clearHash(): void;
  getHash(): google_protobuf_wrappers_pb.StringValue | undefined;
  setHash(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasDeleted(): boolean;
  clearDeleted(): void;
  getDeleted(): google_protobuf_wrappers_pb.BoolValue | undefined;
  setDeleted(value?: google_protobuf_wrappers_pb.BoolValue): void;

  hasCreatedBy(): boolean;
  clearCreatedBy(): void;
  getCreatedBy(): google_protobuf_wrappers_pb.StringValue | undefined;
  setCreatedBy(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasCreatedAt(): boolean;
  clearCreatedAt(): void;
  getCreatedAt(): google_protobuf_timestamp_pb.Timestamp | undefined;
  setCreatedAt(value?: google_protobuf_timestamp_pb.Timestamp): void;

  hasUpdatedBy(): boolean;
  clearUpdatedBy(): void;
  getUpdatedBy(): google_protobuf_wrappers_pb.StringValue | undefined;
  setUpdatedBy(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasUpdatedAt(): boolean;
  clearUpdatedAt(): void;
  getUpdatedAt(): google_protobuf_timestamp_pb.Timestamp | undefined;
  setUpdatedAt(value?: google_protobuf_timestamp_pb.Timestamp): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): VerminForPrep.AsObject;
  static toObject(includeInstance: boolean, msg: VerminForPrep): VerminForPrep.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: VerminForPrep, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): VerminForPrep;
  static deserializeBinaryFromReader(message: VerminForPrep, reader: jspb.BinaryReader): VerminForPrep;
}

export namespace VerminForPrep {
  export type AsObject = {
    id: number,
    verminIds: string,
    activeSubstanceId: number,
    activeSubstanceName: string,
    hash?: google_protobuf_wrappers_pb.StringValue.AsObject,
    deleted?: google_protobuf_wrappers_pb.BoolValue.AsObject,
    createdBy?: google_protobuf_wrappers_pb.StringValue.AsObject,
    createdAt?: google_protobuf_timestamp_pb.Timestamp.AsObject,
    updatedBy?: google_protobuf_wrappers_pb.StringValue.AsObject,
    updatedAt?: google_protobuf_timestamp_pb.Timestamp.AsObject,
  }
}

export class VerminForPrepFilter extends jspb.Message {
  hasId(): boolean;
  clearId(): void;
  getId(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setId(value?: google_protobuf_wrappers_pb.Int64Value): void;

  hasActiveSubstanceId(): boolean;
  clearActiveSubstanceId(): void;
  getActiveSubstanceId(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setActiveSubstanceId(value?: google_protobuf_wrappers_pb.Int64Value): void;

  hasDeleted(): boolean;
  clearDeleted(): void;
  getDeleted(): google_protobuf_wrappers_pb.BoolValue | undefined;
  setDeleted(value?: google_protobuf_wrappers_pb.BoolValue): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): VerminForPrepFilter.AsObject;
  static toObject(includeInstance: boolean, msg: VerminForPrepFilter): VerminForPrepFilter.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: VerminForPrepFilter, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): VerminForPrepFilter;
  static deserializeBinaryFromReader(message: VerminForPrepFilter, reader: jspb.BinaryReader): VerminForPrepFilter;
}

export namespace VerminForPrepFilter {
  export type AsObject = {
    id?: google_protobuf_wrappers_pb.Int64Value.AsObject,
    activeSubstanceId?: google_protobuf_wrappers_pb.Int64Value.AsObject,
    deleted?: google_protobuf_wrappers_pb.BoolValue.AsObject,
  }
}

export class ListVerminForPrepRequest extends jspb.Message {
  hasFilter(): boolean;
  clearFilter(): void;
  getFilter(): VerminForPrepFilter | undefined;
  setFilter(value?: VerminForPrepFilter): void;

  clearSortingsList(): void;
  getSortingsList(): Array<v1_dictionary_common_dictionary_common_pb.Sorting>;
  setSortingsList(value: Array<v1_dictionary_common_dictionary_common_pb.Sorting>): void;
  addSortings(value?: v1_dictionary_common_dictionary_common_pb.Sorting, index?: number): v1_dictionary_common_dictionary_common_pb.Sorting;

  hasPagination(): boolean;
  clearPagination(): void;
  getPagination(): v1_dictionary_common_dictionary_common_pb.PaginationRequest | undefined;
  setPagination(value?: v1_dictionary_common_dictionary_common_pb.PaginationRequest): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListVerminForPrepRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ListVerminForPrepRequest): ListVerminForPrepRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListVerminForPrepRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListVerminForPrepRequest;
  static deserializeBinaryFromReader(message: ListVerminForPrepRequest, reader: jspb.BinaryReader): ListVerminForPrepRequest;
}

export namespace ListVerminForPrepRequest {
  export type AsObject = {
    filter?: VerminForPrepFilter.AsObject,
    sortingsList: Array<v1_dictionary_common_dictionary_common_pb.Sorting.AsObject>,
    pagination?: v1_dictionary_common_dictionary_common_pb.PaginationRequest.AsObject,
  }
}

export class ListVerminForPrepResponse extends jspb.Message {
  clearItemsList(): void;
  getItemsList(): Array<VerminForPrep>;
  setItemsList(value: Array<VerminForPrep>): void;
  addItems(value?: VerminForPrep, index?: number): VerminForPrep;

  hasPagination(): boolean;
  clearPagination(): void;
  getPagination(): v1_dictionary_common_dictionary_common_pb.PaginationResponse | undefined;
  setPagination(value?: v1_dictionary_common_dictionary_common_pb.PaginationResponse): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListVerminForPrepResponse.AsObject;
  static toObject(includeInstance: boolean, msg: ListVerminForPrepResponse): ListVerminForPrepResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListVerminForPrepResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListVerminForPrepResponse;
  static deserializeBinaryFromReader(message: ListVerminForPrepResponse, reader: jspb.BinaryReader): ListVerminForPrepResponse;
}

export namespace ListVerminForPrepResponse {
  export type AsObject = {
    itemsList: Array<VerminForPrep.AsObject>,
    pagination?: v1_dictionary_common_dictionary_common_pb.PaginationResponse.AsObject,
  }
}

export class GetVerminForPrepRequest extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetVerminForPrepRequest.AsObject;
  static toObject(includeInstance: boolean, msg: GetVerminForPrepRequest): GetVerminForPrepRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetVerminForPrepRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetVerminForPrepRequest;
  static deserializeBinaryFromReader(message: GetVerminForPrepRequest, reader: jspb.BinaryReader): GetVerminForPrepRequest;
}

export namespace GetVerminForPrepRequest {
  export type AsObject = {
    id: number,
  }
}

export class GetVerminForPrepResponse extends jspb.Message {
  hasItem(): boolean;
  clearItem(): void;
  getItem(): VerminForPrep | undefined;
  setItem(value?: VerminForPrep): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetVerminForPrepResponse.AsObject;
  static toObject(includeInstance: boolean, msg: GetVerminForPrepResponse): GetVerminForPrepResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetVerminForPrepResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetVerminForPrepResponse;
  static deserializeBinaryFromReader(message: GetVerminForPrepResponse, reader: jspb.BinaryReader): GetVerminForPrepResponse;
}

export namespace GetVerminForPrepResponse {
  export type AsObject = {
    item?: VerminForPrep.AsObject,
  }
}

