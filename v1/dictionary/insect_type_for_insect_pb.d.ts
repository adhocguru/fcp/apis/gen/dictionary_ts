// package: fcp.dictionary.v1.dictionary
// file: v1/dictionary/insect_type_for_insect.proto

import * as jspb from "google-protobuf";
import * as google_protobuf_wrappers_pb from "google-protobuf/google/protobuf/wrappers_pb";
import * as google_protobuf_timestamp_pb from "google-protobuf/google/protobuf/timestamp_pb";
import * as v1_dictionary_common_dictionary_common_pb from "../../v1/dictionary_common/dictionary_common_pb";

export class InsectTypeForInsect extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  getVerminId(): number;
  setVerminId(value: number): void;

  getVerminName(): string;
  setVerminName(value: string): void;

  getInsectTypeId(): number;
  setInsectTypeId(value: number): void;

  getInsectTypeName(): string;
  setInsectTypeName(value: string): void;

  hasHash(): boolean;
  clearHash(): void;
  getHash(): google_protobuf_wrappers_pb.StringValue | undefined;
  setHash(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasDeleted(): boolean;
  clearDeleted(): void;
  getDeleted(): google_protobuf_wrappers_pb.BoolValue | undefined;
  setDeleted(value?: google_protobuf_wrappers_pb.BoolValue): void;

  hasCreatedBy(): boolean;
  clearCreatedBy(): void;
  getCreatedBy(): google_protobuf_wrappers_pb.StringValue | undefined;
  setCreatedBy(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasCreatedAt(): boolean;
  clearCreatedAt(): void;
  getCreatedAt(): google_protobuf_timestamp_pb.Timestamp | undefined;
  setCreatedAt(value?: google_protobuf_timestamp_pb.Timestamp): void;

  hasUpdatedBy(): boolean;
  clearUpdatedBy(): void;
  getUpdatedBy(): google_protobuf_wrappers_pb.StringValue | undefined;
  setUpdatedBy(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasUpdatedAt(): boolean;
  clearUpdatedAt(): void;
  getUpdatedAt(): google_protobuf_timestamp_pb.Timestamp | undefined;
  setUpdatedAt(value?: google_protobuf_timestamp_pb.Timestamp): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): InsectTypeForInsect.AsObject;
  static toObject(includeInstance: boolean, msg: InsectTypeForInsect): InsectTypeForInsect.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: InsectTypeForInsect, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): InsectTypeForInsect;
  static deserializeBinaryFromReader(message: InsectTypeForInsect, reader: jspb.BinaryReader): InsectTypeForInsect;
}

export namespace InsectTypeForInsect {
  export type AsObject = {
    id: number,
    verminId: number,
    verminName: string,
    insectTypeId: number,
    insectTypeName: string,
    hash?: google_protobuf_wrappers_pb.StringValue.AsObject,
    deleted?: google_protobuf_wrappers_pb.BoolValue.AsObject,
    createdBy?: google_protobuf_wrappers_pb.StringValue.AsObject,
    createdAt?: google_protobuf_timestamp_pb.Timestamp.AsObject,
    updatedBy?: google_protobuf_wrappers_pb.StringValue.AsObject,
    updatedAt?: google_protobuf_timestamp_pb.Timestamp.AsObject,
  }
}

export class InsectTypeForInsectFilter extends jspb.Message {
  hasId(): boolean;
  clearId(): void;
  getId(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setId(value?: google_protobuf_wrappers_pb.Int64Value): void;

  hasVerminId(): boolean;
  clearVerminId(): void;
  getVerminId(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setVerminId(value?: google_protobuf_wrappers_pb.Int64Value): void;

  hasInsectTypeId(): boolean;
  clearInsectTypeId(): void;
  getInsectTypeId(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setInsectTypeId(value?: google_protobuf_wrappers_pb.Int64Value): void;

  hasDeleted(): boolean;
  clearDeleted(): void;
  getDeleted(): google_protobuf_wrappers_pb.BoolValue | undefined;
  setDeleted(value?: google_protobuf_wrappers_pb.BoolValue): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): InsectTypeForInsectFilter.AsObject;
  static toObject(includeInstance: boolean, msg: InsectTypeForInsectFilter): InsectTypeForInsectFilter.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: InsectTypeForInsectFilter, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): InsectTypeForInsectFilter;
  static deserializeBinaryFromReader(message: InsectTypeForInsectFilter, reader: jspb.BinaryReader): InsectTypeForInsectFilter;
}

export namespace InsectTypeForInsectFilter {
  export type AsObject = {
    id?: google_protobuf_wrappers_pb.Int64Value.AsObject,
    verminId?: google_protobuf_wrappers_pb.Int64Value.AsObject,
    insectTypeId?: google_protobuf_wrappers_pb.Int64Value.AsObject,
    deleted?: google_protobuf_wrappers_pb.BoolValue.AsObject,
  }
}

export class ListInsectTypeForInsectRequest extends jspb.Message {
  hasFilter(): boolean;
  clearFilter(): void;
  getFilter(): InsectTypeForInsectFilter | undefined;
  setFilter(value?: InsectTypeForInsectFilter): void;

  clearSortingsList(): void;
  getSortingsList(): Array<v1_dictionary_common_dictionary_common_pb.Sorting>;
  setSortingsList(value: Array<v1_dictionary_common_dictionary_common_pb.Sorting>): void;
  addSortings(value?: v1_dictionary_common_dictionary_common_pb.Sorting, index?: number): v1_dictionary_common_dictionary_common_pb.Sorting;

  hasPagination(): boolean;
  clearPagination(): void;
  getPagination(): v1_dictionary_common_dictionary_common_pb.PaginationRequest | undefined;
  setPagination(value?: v1_dictionary_common_dictionary_common_pb.PaginationRequest): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListInsectTypeForInsectRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ListInsectTypeForInsectRequest): ListInsectTypeForInsectRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListInsectTypeForInsectRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListInsectTypeForInsectRequest;
  static deserializeBinaryFromReader(message: ListInsectTypeForInsectRequest, reader: jspb.BinaryReader): ListInsectTypeForInsectRequest;
}

export namespace ListInsectTypeForInsectRequest {
  export type AsObject = {
    filter?: InsectTypeForInsectFilter.AsObject,
    sortingsList: Array<v1_dictionary_common_dictionary_common_pb.Sorting.AsObject>,
    pagination?: v1_dictionary_common_dictionary_common_pb.PaginationRequest.AsObject,
  }
}

export class ListInsectTypeForInsectResponse extends jspb.Message {
  clearItemsList(): void;
  getItemsList(): Array<InsectTypeForInsect>;
  setItemsList(value: Array<InsectTypeForInsect>): void;
  addItems(value?: InsectTypeForInsect, index?: number): InsectTypeForInsect;

  hasPagination(): boolean;
  clearPagination(): void;
  getPagination(): v1_dictionary_common_dictionary_common_pb.PaginationResponse | undefined;
  setPagination(value?: v1_dictionary_common_dictionary_common_pb.PaginationResponse): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListInsectTypeForInsectResponse.AsObject;
  static toObject(includeInstance: boolean, msg: ListInsectTypeForInsectResponse): ListInsectTypeForInsectResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListInsectTypeForInsectResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListInsectTypeForInsectResponse;
  static deserializeBinaryFromReader(message: ListInsectTypeForInsectResponse, reader: jspb.BinaryReader): ListInsectTypeForInsectResponse;
}

export namespace ListInsectTypeForInsectResponse {
  export type AsObject = {
    itemsList: Array<InsectTypeForInsect.AsObject>,
    pagination?: v1_dictionary_common_dictionary_common_pb.PaginationResponse.AsObject,
  }
}

export class GetInsectTypeForInsectRequest extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetInsectTypeForInsectRequest.AsObject;
  static toObject(includeInstance: boolean, msg: GetInsectTypeForInsectRequest): GetInsectTypeForInsectRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetInsectTypeForInsectRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetInsectTypeForInsectRequest;
  static deserializeBinaryFromReader(message: GetInsectTypeForInsectRequest, reader: jspb.BinaryReader): GetInsectTypeForInsectRequest;
}

export namespace GetInsectTypeForInsectRequest {
  export type AsObject = {
    id: number,
  }
}

export class GetInsectTypeForInsectResponse extends jspb.Message {
  hasItem(): boolean;
  clearItem(): void;
  getItem(): InsectTypeForInsect | undefined;
  setItem(value?: InsectTypeForInsect): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetInsectTypeForInsectResponse.AsObject;
  static toObject(includeInstance: boolean, msg: GetInsectTypeForInsectResponse): GetInsectTypeForInsectResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetInsectTypeForInsectResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetInsectTypeForInsectResponse;
  static deserializeBinaryFromReader(message: GetInsectTypeForInsectResponse, reader: jspb.BinaryReader): GetInsectTypeForInsectResponse;
}

export namespace GetInsectTypeForInsectResponse {
  export type AsObject = {
    item?: InsectTypeForInsect.AsObject,
  }
}

