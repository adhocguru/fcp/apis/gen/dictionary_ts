// package: fcp.dictionary.v1.dictionary
// file: v1/dictionary/packaging_for_product.proto

import * as jspb from "google-protobuf";
import * as google_protobuf_wrappers_pb from "google-protobuf/google/protobuf/wrappers_pb";
import * as google_protobuf_timestamp_pb from "google-protobuf/google/protobuf/timestamp_pb";
import * as v1_dictionary_common_dictionary_common_pb from "../../v1/dictionary_common/dictionary_common_pb";

export class PackagingForProduct extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  getProductId(): number;
  setProductId(value: number): void;

  getPackagingId(): number;
  setPackagingId(value: number): void;

  getPackagingQuantity(): string;
  setPackagingQuantity(value: string): void;

  getPackagingUnitName(): string;
  setPackagingUnitName(value: string): void;

  getSeen(): number;
  setSeen(value: number): void;

  hasHash(): boolean;
  clearHash(): void;
  getHash(): google_protobuf_wrappers_pb.StringValue | undefined;
  setHash(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasDeleted(): boolean;
  clearDeleted(): void;
  getDeleted(): google_protobuf_wrappers_pb.BoolValue | undefined;
  setDeleted(value?: google_protobuf_wrappers_pb.BoolValue): void;

  hasCreatedBy(): boolean;
  clearCreatedBy(): void;
  getCreatedBy(): google_protobuf_wrappers_pb.StringValue | undefined;
  setCreatedBy(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasCreatedAt(): boolean;
  clearCreatedAt(): void;
  getCreatedAt(): google_protobuf_timestamp_pb.Timestamp | undefined;
  setCreatedAt(value?: google_protobuf_timestamp_pb.Timestamp): void;

  hasUpdatedBy(): boolean;
  clearUpdatedBy(): void;
  getUpdatedBy(): google_protobuf_wrappers_pb.StringValue | undefined;
  setUpdatedBy(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasUpdatedAt(): boolean;
  clearUpdatedAt(): void;
  getUpdatedAt(): google_protobuf_timestamp_pb.Timestamp | undefined;
  setUpdatedAt(value?: google_protobuf_timestamp_pb.Timestamp): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): PackagingForProduct.AsObject;
  static toObject(includeInstance: boolean, msg: PackagingForProduct): PackagingForProduct.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: PackagingForProduct, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): PackagingForProduct;
  static deserializeBinaryFromReader(message: PackagingForProduct, reader: jspb.BinaryReader): PackagingForProduct;
}

export namespace PackagingForProduct {
  export type AsObject = {
    id: number,
    productId: number,
    packagingId: number,
    packagingQuantity: string,
    packagingUnitName: string,
    seen: number,
    hash?: google_protobuf_wrappers_pb.StringValue.AsObject,
    deleted?: google_protobuf_wrappers_pb.BoolValue.AsObject,
    createdBy?: google_protobuf_wrappers_pb.StringValue.AsObject,
    createdAt?: google_protobuf_timestamp_pb.Timestamp.AsObject,
    updatedBy?: google_protobuf_wrappers_pb.StringValue.AsObject,
    updatedAt?: google_protobuf_timestamp_pb.Timestamp.AsObject,
  }
}

export class PackagingForProductFilter extends jspb.Message {
  hasId(): boolean;
  clearId(): void;
  getId(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setId(value?: google_protobuf_wrappers_pb.Int64Value): void;

  hasProductId(): boolean;
  clearProductId(): void;
  getProductId(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setProductId(value?: google_protobuf_wrappers_pb.Int64Value): void;

  hasPackagingId(): boolean;
  clearPackagingId(): void;
  getPackagingId(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setPackagingId(value?: google_protobuf_wrappers_pb.Int64Value): void;

  hasDeleted(): boolean;
  clearDeleted(): void;
  getDeleted(): google_protobuf_wrappers_pb.BoolValue | undefined;
  setDeleted(value?: google_protobuf_wrappers_pb.BoolValue): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): PackagingForProductFilter.AsObject;
  static toObject(includeInstance: boolean, msg: PackagingForProductFilter): PackagingForProductFilter.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: PackagingForProductFilter, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): PackagingForProductFilter;
  static deserializeBinaryFromReader(message: PackagingForProductFilter, reader: jspb.BinaryReader): PackagingForProductFilter;
}

export namespace PackagingForProductFilter {
  export type AsObject = {
    id?: google_protobuf_wrappers_pb.Int64Value.AsObject,
    productId?: google_protobuf_wrappers_pb.Int64Value.AsObject,
    packagingId?: google_protobuf_wrappers_pb.Int64Value.AsObject,
    deleted?: google_protobuf_wrappers_pb.BoolValue.AsObject,
  }
}

export class GetPackagingForProductRequest extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetPackagingForProductRequest.AsObject;
  static toObject(includeInstance: boolean, msg: GetPackagingForProductRequest): GetPackagingForProductRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetPackagingForProductRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetPackagingForProductRequest;
  static deserializeBinaryFromReader(message: GetPackagingForProductRequest, reader: jspb.BinaryReader): GetPackagingForProductRequest;
}

export namespace GetPackagingForProductRequest {
  export type AsObject = {
    id: number,
  }
}

export class GetPackagingForProductResponse extends jspb.Message {
  hasItem(): boolean;
  clearItem(): void;
  getItem(): PackagingForProduct | undefined;
  setItem(value?: PackagingForProduct): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetPackagingForProductResponse.AsObject;
  static toObject(includeInstance: boolean, msg: GetPackagingForProductResponse): GetPackagingForProductResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetPackagingForProductResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetPackagingForProductResponse;
  static deserializeBinaryFromReader(message: GetPackagingForProductResponse, reader: jspb.BinaryReader): GetPackagingForProductResponse;
}

export namespace GetPackagingForProductResponse {
  export type AsObject = {
    item?: PackagingForProduct.AsObject,
  }
}

export class ListPackagingForProductRequest extends jspb.Message {
  hasFilter(): boolean;
  clearFilter(): void;
  getFilter(): PackagingForProductFilter | undefined;
  setFilter(value?: PackagingForProductFilter): void;

  clearSortingsList(): void;
  getSortingsList(): Array<v1_dictionary_common_dictionary_common_pb.Sorting>;
  setSortingsList(value: Array<v1_dictionary_common_dictionary_common_pb.Sorting>): void;
  addSortings(value?: v1_dictionary_common_dictionary_common_pb.Sorting, index?: number): v1_dictionary_common_dictionary_common_pb.Sorting;

  hasPagination(): boolean;
  clearPagination(): void;
  getPagination(): v1_dictionary_common_dictionary_common_pb.PaginationRequest | undefined;
  setPagination(value?: v1_dictionary_common_dictionary_common_pb.PaginationRequest): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListPackagingForProductRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ListPackagingForProductRequest): ListPackagingForProductRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListPackagingForProductRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListPackagingForProductRequest;
  static deserializeBinaryFromReader(message: ListPackagingForProductRequest, reader: jspb.BinaryReader): ListPackagingForProductRequest;
}

export namespace ListPackagingForProductRequest {
  export type AsObject = {
    filter?: PackagingForProductFilter.AsObject,
    sortingsList: Array<v1_dictionary_common_dictionary_common_pb.Sorting.AsObject>,
    pagination?: v1_dictionary_common_dictionary_common_pb.PaginationRequest.AsObject,
  }
}

export class ListPackagingForProductResponse extends jspb.Message {
  clearItemsList(): void;
  getItemsList(): Array<PackagingForProduct>;
  setItemsList(value: Array<PackagingForProduct>): void;
  addItems(value?: PackagingForProduct, index?: number): PackagingForProduct;

  hasPagination(): boolean;
  clearPagination(): void;
  getPagination(): v1_dictionary_common_dictionary_common_pb.PaginationResponse | undefined;
  setPagination(value?: v1_dictionary_common_dictionary_common_pb.PaginationResponse): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListPackagingForProductResponse.AsObject;
  static toObject(includeInstance: boolean, msg: ListPackagingForProductResponse): ListPackagingForProductResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListPackagingForProductResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListPackagingForProductResponse;
  static deserializeBinaryFromReader(message: ListPackagingForProductResponse, reader: jspb.BinaryReader): ListPackagingForProductResponse;
}

export namespace ListPackagingForProductResponse {
  export type AsObject = {
    itemsList: Array<PackagingForProduct.AsObject>,
    pagination?: v1_dictionary_common_dictionary_common_pb.PaginationResponse.AsObject,
  }
}

