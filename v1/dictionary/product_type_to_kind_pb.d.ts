// package: fcp.dictionary.v1.dictionary
// file: v1/dictionary/product_type_to_kind.proto

import * as jspb from "google-protobuf";
import * as google_protobuf_wrappers_pb from "google-protobuf/google/protobuf/wrappers_pb";
import * as google_protobuf_timestamp_pb from "google-protobuf/google/protobuf/timestamp_pb";
import * as v1_dictionary_common_dictionary_common_pb from "../../v1/dictionary_common/dictionary_common_pb";

export class ProductTypeToKind extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  getProductTypeId(): number;
  setProductTypeId(value: number): void;

  getProductTypeName(): string;
  setProductTypeName(value: string): void;

  getProductKindId(): number;
  setProductKindId(value: number): void;

  getProductKindName(): string;
  setProductKindName(value: string): void;

  hasHash(): boolean;
  clearHash(): void;
  getHash(): google_protobuf_wrappers_pb.StringValue | undefined;
  setHash(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasDeleted(): boolean;
  clearDeleted(): void;
  getDeleted(): google_protobuf_wrappers_pb.BoolValue | undefined;
  setDeleted(value?: google_protobuf_wrappers_pb.BoolValue): void;

  hasCreatedBy(): boolean;
  clearCreatedBy(): void;
  getCreatedBy(): google_protobuf_wrappers_pb.StringValue | undefined;
  setCreatedBy(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasCreatedAt(): boolean;
  clearCreatedAt(): void;
  getCreatedAt(): google_protobuf_timestamp_pb.Timestamp | undefined;
  setCreatedAt(value?: google_protobuf_timestamp_pb.Timestamp): void;

  hasUpdatedBy(): boolean;
  clearUpdatedBy(): void;
  getUpdatedBy(): google_protobuf_wrappers_pb.StringValue | undefined;
  setUpdatedBy(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasUpdatedAt(): boolean;
  clearUpdatedAt(): void;
  getUpdatedAt(): google_protobuf_timestamp_pb.Timestamp | undefined;
  setUpdatedAt(value?: google_protobuf_timestamp_pb.Timestamp): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ProductTypeToKind.AsObject;
  static toObject(includeInstance: boolean, msg: ProductTypeToKind): ProductTypeToKind.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ProductTypeToKind, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ProductTypeToKind;
  static deserializeBinaryFromReader(message: ProductTypeToKind, reader: jspb.BinaryReader): ProductTypeToKind;
}

export namespace ProductTypeToKind {
  export type AsObject = {
    id: number,
    productTypeId: number,
    productTypeName: string,
    productKindId: number,
    productKindName: string,
    hash?: google_protobuf_wrappers_pb.StringValue.AsObject,
    deleted?: google_protobuf_wrappers_pb.BoolValue.AsObject,
    createdBy?: google_protobuf_wrappers_pb.StringValue.AsObject,
    createdAt?: google_protobuf_timestamp_pb.Timestamp.AsObject,
    updatedBy?: google_protobuf_wrappers_pb.StringValue.AsObject,
    updatedAt?: google_protobuf_timestamp_pb.Timestamp.AsObject,
  }
}

export class ProductTypeToKindFilter extends jspb.Message {
  hasId(): boolean;
  clearId(): void;
  getId(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setId(value?: google_protobuf_wrappers_pb.Int64Value): void;

  hasProductTypeId(): boolean;
  clearProductTypeId(): void;
  getProductTypeId(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setProductTypeId(value?: google_protobuf_wrappers_pb.Int64Value): void;

  hasProductKindId(): boolean;
  clearProductKindId(): void;
  getProductKindId(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setProductKindId(value?: google_protobuf_wrappers_pb.Int64Value): void;

  hasDeleted(): boolean;
  clearDeleted(): void;
  getDeleted(): google_protobuf_wrappers_pb.BoolValue | undefined;
  setDeleted(value?: google_protobuf_wrappers_pb.BoolValue): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ProductTypeToKindFilter.AsObject;
  static toObject(includeInstance: boolean, msg: ProductTypeToKindFilter): ProductTypeToKindFilter.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ProductTypeToKindFilter, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ProductTypeToKindFilter;
  static deserializeBinaryFromReader(message: ProductTypeToKindFilter, reader: jspb.BinaryReader): ProductTypeToKindFilter;
}

export namespace ProductTypeToKindFilter {
  export type AsObject = {
    id?: google_protobuf_wrappers_pb.Int64Value.AsObject,
    productTypeId?: google_protobuf_wrappers_pb.Int64Value.AsObject,
    productKindId?: google_protobuf_wrappers_pb.Int64Value.AsObject,
    deleted?: google_protobuf_wrappers_pb.BoolValue.AsObject,
  }
}

export class ListProductTypeToKindRequest extends jspb.Message {
  hasFilter(): boolean;
  clearFilter(): void;
  getFilter(): ProductTypeToKindFilter | undefined;
  setFilter(value?: ProductTypeToKindFilter): void;

  clearSortingsList(): void;
  getSortingsList(): Array<v1_dictionary_common_dictionary_common_pb.Sorting>;
  setSortingsList(value: Array<v1_dictionary_common_dictionary_common_pb.Sorting>): void;
  addSortings(value?: v1_dictionary_common_dictionary_common_pb.Sorting, index?: number): v1_dictionary_common_dictionary_common_pb.Sorting;

  hasPagination(): boolean;
  clearPagination(): void;
  getPagination(): v1_dictionary_common_dictionary_common_pb.PaginationRequest | undefined;
  setPagination(value?: v1_dictionary_common_dictionary_common_pb.PaginationRequest): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListProductTypeToKindRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ListProductTypeToKindRequest): ListProductTypeToKindRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListProductTypeToKindRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListProductTypeToKindRequest;
  static deserializeBinaryFromReader(message: ListProductTypeToKindRequest, reader: jspb.BinaryReader): ListProductTypeToKindRequest;
}

export namespace ListProductTypeToKindRequest {
  export type AsObject = {
    filter?: ProductTypeToKindFilter.AsObject,
    sortingsList: Array<v1_dictionary_common_dictionary_common_pb.Sorting.AsObject>,
    pagination?: v1_dictionary_common_dictionary_common_pb.PaginationRequest.AsObject,
  }
}

export class ListProductTypeToKindResponse extends jspb.Message {
  clearItemsList(): void;
  getItemsList(): Array<ProductTypeToKind>;
  setItemsList(value: Array<ProductTypeToKind>): void;
  addItems(value?: ProductTypeToKind, index?: number): ProductTypeToKind;

  hasPagination(): boolean;
  clearPagination(): void;
  getPagination(): v1_dictionary_common_dictionary_common_pb.PaginationResponse | undefined;
  setPagination(value?: v1_dictionary_common_dictionary_common_pb.PaginationResponse): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListProductTypeToKindResponse.AsObject;
  static toObject(includeInstance: boolean, msg: ListProductTypeToKindResponse): ListProductTypeToKindResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListProductTypeToKindResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListProductTypeToKindResponse;
  static deserializeBinaryFromReader(message: ListProductTypeToKindResponse, reader: jspb.BinaryReader): ListProductTypeToKindResponse;
}

export namespace ListProductTypeToKindResponse {
  export type AsObject = {
    itemsList: Array<ProductTypeToKind.AsObject>,
    pagination?: v1_dictionary_common_dictionary_common_pb.PaginationResponse.AsObject,
  }
}

export class GetProductTypeToKindRequest extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetProductTypeToKindRequest.AsObject;
  static toObject(includeInstance: boolean, msg: GetProductTypeToKindRequest): GetProductTypeToKindRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetProductTypeToKindRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetProductTypeToKindRequest;
  static deserializeBinaryFromReader(message: GetProductTypeToKindRequest, reader: jspb.BinaryReader): GetProductTypeToKindRequest;
}

export namespace GetProductTypeToKindRequest {
  export type AsObject = {
    id: number,
  }
}

export class GetProductTypeToKindResponse extends jspb.Message {
  hasItem(): boolean;
  clearItem(): void;
  getItem(): ProductTypeToKind | undefined;
  setItem(value?: ProductTypeToKind): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetProductTypeToKindResponse.AsObject;
  static toObject(includeInstance: boolean, msg: GetProductTypeToKindResponse): GetProductTypeToKindResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetProductTypeToKindResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetProductTypeToKindResponse;
  static deserializeBinaryFromReader(message: GetProductTypeToKindResponse, reader: jspb.BinaryReader): GetProductTypeToKindResponse;
}

export namespace GetProductTypeToKindResponse {
  export type AsObject = {
    item?: ProductTypeToKind.AsObject,
  }
}

