// package: fcp.dictionary.v1.dictionary
// file: v1/dictionary/zone_weed_for_weed.proto

import * as jspb from "google-protobuf";
import * as google_protobuf_wrappers_pb from "google-protobuf/google/protobuf/wrappers_pb";
import * as google_protobuf_timestamp_pb from "google-protobuf/google/protobuf/timestamp_pb";
import * as v1_dictionary_common_dictionary_common_pb from "../../v1/dictionary_common/dictionary_common_pb";

export class ZoneWeedforWeed extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  getVerminId(): number;
  setVerminId(value: number): void;

  getVerminName(): string;
  setVerminName(value: string): void;

  getZoneWeedId(): number;
  setZoneWeedId(value: number): void;

  getZoneWeedName(): string;
  setZoneWeedName(value: string): void;

  hasHash(): boolean;
  clearHash(): void;
  getHash(): google_protobuf_wrappers_pb.StringValue | undefined;
  setHash(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasDeleted(): boolean;
  clearDeleted(): void;
  getDeleted(): google_protobuf_wrappers_pb.BoolValue | undefined;
  setDeleted(value?: google_protobuf_wrappers_pb.BoolValue): void;

  hasCreatedBy(): boolean;
  clearCreatedBy(): void;
  getCreatedBy(): google_protobuf_wrappers_pb.StringValue | undefined;
  setCreatedBy(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasCreatedAt(): boolean;
  clearCreatedAt(): void;
  getCreatedAt(): google_protobuf_timestamp_pb.Timestamp | undefined;
  setCreatedAt(value?: google_protobuf_timestamp_pb.Timestamp): void;

  hasUpdatedBy(): boolean;
  clearUpdatedBy(): void;
  getUpdatedBy(): google_protobuf_wrappers_pb.StringValue | undefined;
  setUpdatedBy(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasUpdatedAt(): boolean;
  clearUpdatedAt(): void;
  getUpdatedAt(): google_protobuf_timestamp_pb.Timestamp | undefined;
  setUpdatedAt(value?: google_protobuf_timestamp_pb.Timestamp): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ZoneWeedforWeed.AsObject;
  static toObject(includeInstance: boolean, msg: ZoneWeedforWeed): ZoneWeedforWeed.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ZoneWeedforWeed, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ZoneWeedforWeed;
  static deserializeBinaryFromReader(message: ZoneWeedforWeed, reader: jspb.BinaryReader): ZoneWeedforWeed;
}

export namespace ZoneWeedforWeed {
  export type AsObject = {
    id: number,
    verminId: number,
    verminName: string,
    zoneWeedId: number,
    zoneWeedName: string,
    hash?: google_protobuf_wrappers_pb.StringValue.AsObject,
    deleted?: google_protobuf_wrappers_pb.BoolValue.AsObject,
    createdBy?: google_protobuf_wrappers_pb.StringValue.AsObject,
    createdAt?: google_protobuf_timestamp_pb.Timestamp.AsObject,
    updatedBy?: google_protobuf_wrappers_pb.StringValue.AsObject,
    updatedAt?: google_protobuf_timestamp_pb.Timestamp.AsObject,
  }
}

export class ZoneWeedforWeedFilter extends jspb.Message {
  hasId(): boolean;
  clearId(): void;
  getId(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setId(value?: google_protobuf_wrappers_pb.Int64Value): void;

  hasVerminId(): boolean;
  clearVerminId(): void;
  getVerminId(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setVerminId(value?: google_protobuf_wrappers_pb.Int64Value): void;

  hasZoneWeedId(): boolean;
  clearZoneWeedId(): void;
  getZoneWeedId(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setZoneWeedId(value?: google_protobuf_wrappers_pb.Int64Value): void;

  hasDeleted(): boolean;
  clearDeleted(): void;
  getDeleted(): google_protobuf_wrappers_pb.BoolValue | undefined;
  setDeleted(value?: google_protobuf_wrappers_pb.BoolValue): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ZoneWeedforWeedFilter.AsObject;
  static toObject(includeInstance: boolean, msg: ZoneWeedforWeedFilter): ZoneWeedforWeedFilter.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ZoneWeedforWeedFilter, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ZoneWeedforWeedFilter;
  static deserializeBinaryFromReader(message: ZoneWeedforWeedFilter, reader: jspb.BinaryReader): ZoneWeedforWeedFilter;
}

export namespace ZoneWeedforWeedFilter {
  export type AsObject = {
    id?: google_protobuf_wrappers_pb.Int64Value.AsObject,
    verminId?: google_protobuf_wrappers_pb.Int64Value.AsObject,
    zoneWeedId?: google_protobuf_wrappers_pb.Int64Value.AsObject,
    deleted?: google_protobuf_wrappers_pb.BoolValue.AsObject,
  }
}

export class ListZoneWeedforWeedRequest extends jspb.Message {
  hasFilter(): boolean;
  clearFilter(): void;
  getFilter(): ZoneWeedforWeedFilter | undefined;
  setFilter(value?: ZoneWeedforWeedFilter): void;

  clearSortingsList(): void;
  getSortingsList(): Array<v1_dictionary_common_dictionary_common_pb.Sorting>;
  setSortingsList(value: Array<v1_dictionary_common_dictionary_common_pb.Sorting>): void;
  addSortings(value?: v1_dictionary_common_dictionary_common_pb.Sorting, index?: number): v1_dictionary_common_dictionary_common_pb.Sorting;

  hasPagination(): boolean;
  clearPagination(): void;
  getPagination(): v1_dictionary_common_dictionary_common_pb.PaginationRequest | undefined;
  setPagination(value?: v1_dictionary_common_dictionary_common_pb.PaginationRequest): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListZoneWeedforWeedRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ListZoneWeedforWeedRequest): ListZoneWeedforWeedRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListZoneWeedforWeedRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListZoneWeedforWeedRequest;
  static deserializeBinaryFromReader(message: ListZoneWeedforWeedRequest, reader: jspb.BinaryReader): ListZoneWeedforWeedRequest;
}

export namespace ListZoneWeedforWeedRequest {
  export type AsObject = {
    filter?: ZoneWeedforWeedFilter.AsObject,
    sortingsList: Array<v1_dictionary_common_dictionary_common_pb.Sorting.AsObject>,
    pagination?: v1_dictionary_common_dictionary_common_pb.PaginationRequest.AsObject,
  }
}

export class ListZoneWeedforWeedResponse extends jspb.Message {
  clearItemsList(): void;
  getItemsList(): Array<ZoneWeedforWeed>;
  setItemsList(value: Array<ZoneWeedforWeed>): void;
  addItems(value?: ZoneWeedforWeed, index?: number): ZoneWeedforWeed;

  hasPagination(): boolean;
  clearPagination(): void;
  getPagination(): v1_dictionary_common_dictionary_common_pb.PaginationResponse | undefined;
  setPagination(value?: v1_dictionary_common_dictionary_common_pb.PaginationResponse): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListZoneWeedforWeedResponse.AsObject;
  static toObject(includeInstance: boolean, msg: ListZoneWeedforWeedResponse): ListZoneWeedforWeedResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListZoneWeedforWeedResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListZoneWeedforWeedResponse;
  static deserializeBinaryFromReader(message: ListZoneWeedforWeedResponse, reader: jspb.BinaryReader): ListZoneWeedforWeedResponse;
}

export namespace ListZoneWeedforWeedResponse {
  export type AsObject = {
    itemsList: Array<ZoneWeedforWeed.AsObject>,
    pagination?: v1_dictionary_common_dictionary_common_pb.PaginationResponse.AsObject,
  }
}

export class GetZoneWeedforWeedRequest extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetZoneWeedforWeedRequest.AsObject;
  static toObject(includeInstance: boolean, msg: GetZoneWeedforWeedRequest): GetZoneWeedforWeedRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetZoneWeedforWeedRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetZoneWeedforWeedRequest;
  static deserializeBinaryFromReader(message: GetZoneWeedforWeedRequest, reader: jspb.BinaryReader): GetZoneWeedforWeedRequest;
}

export namespace GetZoneWeedforWeedRequest {
  export type AsObject = {
    id: number,
  }
}

export class GetZoneWeedforWeedResponse extends jspb.Message {
  hasItem(): boolean;
  clearItem(): void;
  getItem(): ZoneWeedforWeed | undefined;
  setItem(value?: ZoneWeedforWeed): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetZoneWeedforWeedResponse.AsObject;
  static toObject(includeInstance: boolean, msg: GetZoneWeedforWeedResponse): GetZoneWeedforWeedResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetZoneWeedforWeedResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetZoneWeedforWeedResponse;
  static deserializeBinaryFromReader(message: GetZoneWeedforWeedResponse, reader: jspb.BinaryReader): GetZoneWeedforWeedResponse;
}

export namespace GetZoneWeedforWeedResponse {
  export type AsObject = {
    item?: ZoneWeedforWeed.AsObject,
  }
}

