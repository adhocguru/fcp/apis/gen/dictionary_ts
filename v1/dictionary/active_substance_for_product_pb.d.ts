// package: fcp.dictionary.v1.dictionary
// file: v1/dictionary/active_substance_for_product.proto

import * as jspb from "google-protobuf";
import * as google_protobuf_wrappers_pb from "google-protobuf/google/protobuf/wrappers_pb";
import * as google_protobuf_timestamp_pb from "google-protobuf/google/protobuf/timestamp_pb";
import * as v1_dictionary_common_dictionary_common_pb from "../../v1/dictionary_common/dictionary_common_pb";

export class ActiveSubstanceForProduct extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  getActiveSubstanceId(): number;
  setActiveSubstanceId(value: number): void;

  getActiveSubstanceName(): string;
  setActiveSubstanceName(value: string): void;

  getConcentrationType(): string;
  setConcentrationType(value: string): void;

  getQuantity(): string;
  setQuantity(value: string): void;

  getUnitName(): string;
  setUnitName(value: string): void;

  getBasicQuantity(): string;
  setBasicQuantity(value: string): void;

  getBasicUnitName(): string;
  setBasicUnitName(value: string): void;

  getProductId(): number;
  setProductId(value: number): void;

  getProductName(): string;
  setProductName(value: string): void;

  hasHash(): boolean;
  clearHash(): void;
  getHash(): google_protobuf_wrappers_pb.StringValue | undefined;
  setHash(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasDeleted(): boolean;
  clearDeleted(): void;
  getDeleted(): google_protobuf_wrappers_pb.BoolValue | undefined;
  setDeleted(value?: google_protobuf_wrappers_pb.BoolValue): void;

  hasCreatedBy(): boolean;
  clearCreatedBy(): void;
  getCreatedBy(): google_protobuf_wrappers_pb.StringValue | undefined;
  setCreatedBy(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasCreatedAt(): boolean;
  clearCreatedAt(): void;
  getCreatedAt(): google_protobuf_timestamp_pb.Timestamp | undefined;
  setCreatedAt(value?: google_protobuf_timestamp_pb.Timestamp): void;

  hasUpdatedBy(): boolean;
  clearUpdatedBy(): void;
  getUpdatedBy(): google_protobuf_wrappers_pb.StringValue | undefined;
  setUpdatedBy(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasUpdatedAt(): boolean;
  clearUpdatedAt(): void;
  getUpdatedAt(): google_protobuf_timestamp_pb.Timestamp | undefined;
  setUpdatedAt(value?: google_protobuf_timestamp_pb.Timestamp): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ActiveSubstanceForProduct.AsObject;
  static toObject(includeInstance: boolean, msg: ActiveSubstanceForProduct): ActiveSubstanceForProduct.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ActiveSubstanceForProduct, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ActiveSubstanceForProduct;
  static deserializeBinaryFromReader(message: ActiveSubstanceForProduct, reader: jspb.BinaryReader): ActiveSubstanceForProduct;
}

export namespace ActiveSubstanceForProduct {
  export type AsObject = {
    id: number,
    activeSubstanceId: number,
    activeSubstanceName: string,
    concentrationType: string,
    quantity: string,
    unitName: string,
    basicQuantity: string,
    basicUnitName: string,
    productId: number,
    productName: string,
    hash?: google_protobuf_wrappers_pb.StringValue.AsObject,
    deleted?: google_protobuf_wrappers_pb.BoolValue.AsObject,
    createdBy?: google_protobuf_wrappers_pb.StringValue.AsObject,
    createdAt?: google_protobuf_timestamp_pb.Timestamp.AsObject,
    updatedBy?: google_protobuf_wrappers_pb.StringValue.AsObject,
    updatedAt?: google_protobuf_timestamp_pb.Timestamp.AsObject,
  }
}

export class ActiveSubstanceForProductFilter extends jspb.Message {
  hasId(): boolean;
  clearId(): void;
  getId(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setId(value?: google_protobuf_wrappers_pb.Int64Value): void;

  hasActiveSubstanceId(): boolean;
  clearActiveSubstanceId(): void;
  getActiveSubstanceId(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setActiveSubstanceId(value?: google_protobuf_wrappers_pb.Int64Value): void;

  hasProductId(): boolean;
  clearProductId(): void;
  getProductId(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setProductId(value?: google_protobuf_wrappers_pb.Int64Value): void;

  hasDeleted(): boolean;
  clearDeleted(): void;
  getDeleted(): google_protobuf_wrappers_pb.BoolValue | undefined;
  setDeleted(value?: google_protobuf_wrappers_pb.BoolValue): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ActiveSubstanceForProductFilter.AsObject;
  static toObject(includeInstance: boolean, msg: ActiveSubstanceForProductFilter): ActiveSubstanceForProductFilter.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ActiveSubstanceForProductFilter, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ActiveSubstanceForProductFilter;
  static deserializeBinaryFromReader(message: ActiveSubstanceForProductFilter, reader: jspb.BinaryReader): ActiveSubstanceForProductFilter;
}

export namespace ActiveSubstanceForProductFilter {
  export type AsObject = {
    id?: google_protobuf_wrappers_pb.Int64Value.AsObject,
    activeSubstanceId?: google_protobuf_wrappers_pb.Int64Value.AsObject,
    productId?: google_protobuf_wrappers_pb.Int64Value.AsObject,
    deleted?: google_protobuf_wrappers_pb.BoolValue.AsObject,
  }
}

export class GetActiveSubstanceForProductRequest extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetActiveSubstanceForProductRequest.AsObject;
  static toObject(includeInstance: boolean, msg: GetActiveSubstanceForProductRequest): GetActiveSubstanceForProductRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetActiveSubstanceForProductRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetActiveSubstanceForProductRequest;
  static deserializeBinaryFromReader(message: GetActiveSubstanceForProductRequest, reader: jspb.BinaryReader): GetActiveSubstanceForProductRequest;
}

export namespace GetActiveSubstanceForProductRequest {
  export type AsObject = {
    id: number,
  }
}

export class GetActiveSubstanceForProductResponse extends jspb.Message {
  hasItem(): boolean;
  clearItem(): void;
  getItem(): ActiveSubstanceForProduct | undefined;
  setItem(value?: ActiveSubstanceForProduct): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetActiveSubstanceForProductResponse.AsObject;
  static toObject(includeInstance: boolean, msg: GetActiveSubstanceForProductResponse): GetActiveSubstanceForProductResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetActiveSubstanceForProductResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetActiveSubstanceForProductResponse;
  static deserializeBinaryFromReader(message: GetActiveSubstanceForProductResponse, reader: jspb.BinaryReader): GetActiveSubstanceForProductResponse;
}

export namespace GetActiveSubstanceForProductResponse {
  export type AsObject = {
    item?: ActiveSubstanceForProduct.AsObject,
  }
}

export class ListActiveSubstanceForProductRequest extends jspb.Message {
  hasFilter(): boolean;
  clearFilter(): void;
  getFilter(): ActiveSubstanceForProductFilter | undefined;
  setFilter(value?: ActiveSubstanceForProductFilter): void;

  clearSortingsList(): void;
  getSortingsList(): Array<v1_dictionary_common_dictionary_common_pb.Sorting>;
  setSortingsList(value: Array<v1_dictionary_common_dictionary_common_pb.Sorting>): void;
  addSortings(value?: v1_dictionary_common_dictionary_common_pb.Sorting, index?: number): v1_dictionary_common_dictionary_common_pb.Sorting;

  hasPagination(): boolean;
  clearPagination(): void;
  getPagination(): v1_dictionary_common_dictionary_common_pb.PaginationRequest | undefined;
  setPagination(value?: v1_dictionary_common_dictionary_common_pb.PaginationRequest): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListActiveSubstanceForProductRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ListActiveSubstanceForProductRequest): ListActiveSubstanceForProductRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListActiveSubstanceForProductRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListActiveSubstanceForProductRequest;
  static deserializeBinaryFromReader(message: ListActiveSubstanceForProductRequest, reader: jspb.BinaryReader): ListActiveSubstanceForProductRequest;
}

export namespace ListActiveSubstanceForProductRequest {
  export type AsObject = {
    filter?: ActiveSubstanceForProductFilter.AsObject,
    sortingsList: Array<v1_dictionary_common_dictionary_common_pb.Sorting.AsObject>,
    pagination?: v1_dictionary_common_dictionary_common_pb.PaginationRequest.AsObject,
  }
}

export class ListActiveSubstanceForProductResponse extends jspb.Message {
  clearItemsList(): void;
  getItemsList(): Array<ActiveSubstanceForProduct>;
  setItemsList(value: Array<ActiveSubstanceForProduct>): void;
  addItems(value?: ActiveSubstanceForProduct, index?: number): ActiveSubstanceForProduct;

  hasPagination(): boolean;
  clearPagination(): void;
  getPagination(): v1_dictionary_common_dictionary_common_pb.PaginationResponse | undefined;
  setPagination(value?: v1_dictionary_common_dictionary_common_pb.PaginationResponse): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListActiveSubstanceForProductResponse.AsObject;
  static toObject(includeInstance: boolean, msg: ListActiveSubstanceForProductResponse): ListActiveSubstanceForProductResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListActiveSubstanceForProductResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListActiveSubstanceForProductResponse;
  static deserializeBinaryFromReader(message: ListActiveSubstanceForProductResponse, reader: jspb.BinaryReader): ListActiveSubstanceForProductResponse;
}

export namespace ListActiveSubstanceForProductResponse {
  export type AsObject = {
    itemsList: Array<ActiveSubstanceForProduct.AsObject>,
    pagination?: v1_dictionary_common_dictionary_common_pb.PaginationResponse.AsObject,
  }
}

