// package: fcp.dictionary.v1.dictionary
// file: v1/dictionary/features_for_culture.proto

import * as jspb from "google-protobuf";
import * as google_protobuf_wrappers_pb from "google-protobuf/google/protobuf/wrappers_pb";
import * as google_protobuf_timestamp_pb from "google-protobuf/google/protobuf/timestamp_pb";
import * as v1_dictionary_common_dictionary_common_pb from "../../v1/dictionary_common/dictionary_common_pb";

export class FeaturesForCulture extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  getFeatureId(): number;
  setFeatureId(value: number): void;

  getFeatureName(): string;
  setFeatureName(value: string): void;

  getCultureId(): number;
  setCultureId(value: number): void;

  getCultureName(): string;
  setCultureName(value: string): void;

  hasHash(): boolean;
  clearHash(): void;
  getHash(): google_protobuf_wrappers_pb.StringValue | undefined;
  setHash(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasDeleted(): boolean;
  clearDeleted(): void;
  getDeleted(): google_protobuf_wrappers_pb.BoolValue | undefined;
  setDeleted(value?: google_protobuf_wrappers_pb.BoolValue): void;

  hasCreatedBy(): boolean;
  clearCreatedBy(): void;
  getCreatedBy(): google_protobuf_wrappers_pb.StringValue | undefined;
  setCreatedBy(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasCreatedAt(): boolean;
  clearCreatedAt(): void;
  getCreatedAt(): google_protobuf_timestamp_pb.Timestamp | undefined;
  setCreatedAt(value?: google_protobuf_timestamp_pb.Timestamp): void;

  hasUpdatedBy(): boolean;
  clearUpdatedBy(): void;
  getUpdatedBy(): google_protobuf_wrappers_pb.StringValue | undefined;
  setUpdatedBy(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasUpdatedAt(): boolean;
  clearUpdatedAt(): void;
  getUpdatedAt(): google_protobuf_timestamp_pb.Timestamp | undefined;
  setUpdatedAt(value?: google_protobuf_timestamp_pb.Timestamp): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): FeaturesForCulture.AsObject;
  static toObject(includeInstance: boolean, msg: FeaturesForCulture): FeaturesForCulture.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: FeaturesForCulture, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): FeaturesForCulture;
  static deserializeBinaryFromReader(message: FeaturesForCulture, reader: jspb.BinaryReader): FeaturesForCulture;
}

export namespace FeaturesForCulture {
  export type AsObject = {
    id: number,
    featureId: number,
    featureName: string,
    cultureId: number,
    cultureName: string,
    hash?: google_protobuf_wrappers_pb.StringValue.AsObject,
    deleted?: google_protobuf_wrappers_pb.BoolValue.AsObject,
    createdBy?: google_protobuf_wrappers_pb.StringValue.AsObject,
    createdAt?: google_protobuf_timestamp_pb.Timestamp.AsObject,
    updatedBy?: google_protobuf_wrappers_pb.StringValue.AsObject,
    updatedAt?: google_protobuf_timestamp_pb.Timestamp.AsObject,
  }
}

export class FeaturesForCultureFilter extends jspb.Message {
  hasId(): boolean;
  clearId(): void;
  getId(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setId(value?: google_protobuf_wrappers_pb.Int64Value): void;

  hasFeatureId(): boolean;
  clearFeatureId(): void;
  getFeatureId(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setFeatureId(value?: google_protobuf_wrappers_pb.Int64Value): void;

  hasCultureId(): boolean;
  clearCultureId(): void;
  getCultureId(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setCultureId(value?: google_protobuf_wrappers_pb.Int64Value): void;

  hasDeleted(): boolean;
  clearDeleted(): void;
  getDeleted(): google_protobuf_wrappers_pb.BoolValue | undefined;
  setDeleted(value?: google_protobuf_wrappers_pb.BoolValue): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): FeaturesForCultureFilter.AsObject;
  static toObject(includeInstance: boolean, msg: FeaturesForCultureFilter): FeaturesForCultureFilter.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: FeaturesForCultureFilter, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): FeaturesForCultureFilter;
  static deserializeBinaryFromReader(message: FeaturesForCultureFilter, reader: jspb.BinaryReader): FeaturesForCultureFilter;
}

export namespace FeaturesForCultureFilter {
  export type AsObject = {
    id?: google_protobuf_wrappers_pb.Int64Value.AsObject,
    featureId?: google_protobuf_wrappers_pb.Int64Value.AsObject,
    cultureId?: google_protobuf_wrappers_pb.Int64Value.AsObject,
    deleted?: google_protobuf_wrappers_pb.BoolValue.AsObject,
  }
}

export class ListFeaturesForCultureRequest extends jspb.Message {
  hasFilter(): boolean;
  clearFilter(): void;
  getFilter(): FeaturesForCultureFilter | undefined;
  setFilter(value?: FeaturesForCultureFilter): void;

  clearSortingsList(): void;
  getSortingsList(): Array<v1_dictionary_common_dictionary_common_pb.Sorting>;
  setSortingsList(value: Array<v1_dictionary_common_dictionary_common_pb.Sorting>): void;
  addSortings(value?: v1_dictionary_common_dictionary_common_pb.Sorting, index?: number): v1_dictionary_common_dictionary_common_pb.Sorting;

  hasPagination(): boolean;
  clearPagination(): void;
  getPagination(): v1_dictionary_common_dictionary_common_pb.PaginationRequest | undefined;
  setPagination(value?: v1_dictionary_common_dictionary_common_pb.PaginationRequest): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListFeaturesForCultureRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ListFeaturesForCultureRequest): ListFeaturesForCultureRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListFeaturesForCultureRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListFeaturesForCultureRequest;
  static deserializeBinaryFromReader(message: ListFeaturesForCultureRequest, reader: jspb.BinaryReader): ListFeaturesForCultureRequest;
}

export namespace ListFeaturesForCultureRequest {
  export type AsObject = {
    filter?: FeaturesForCultureFilter.AsObject,
    sortingsList: Array<v1_dictionary_common_dictionary_common_pb.Sorting.AsObject>,
    pagination?: v1_dictionary_common_dictionary_common_pb.PaginationRequest.AsObject,
  }
}

export class ListFeaturesForCultureResponse extends jspb.Message {
  clearItemsList(): void;
  getItemsList(): Array<FeaturesForCulture>;
  setItemsList(value: Array<FeaturesForCulture>): void;
  addItems(value?: FeaturesForCulture, index?: number): FeaturesForCulture;

  hasPagination(): boolean;
  clearPagination(): void;
  getPagination(): v1_dictionary_common_dictionary_common_pb.PaginationResponse | undefined;
  setPagination(value?: v1_dictionary_common_dictionary_common_pb.PaginationResponse): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListFeaturesForCultureResponse.AsObject;
  static toObject(includeInstance: boolean, msg: ListFeaturesForCultureResponse): ListFeaturesForCultureResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListFeaturesForCultureResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListFeaturesForCultureResponse;
  static deserializeBinaryFromReader(message: ListFeaturesForCultureResponse, reader: jspb.BinaryReader): ListFeaturesForCultureResponse;
}

export namespace ListFeaturesForCultureResponse {
  export type AsObject = {
    itemsList: Array<FeaturesForCulture.AsObject>,
    pagination?: v1_dictionary_common_dictionary_common_pb.PaginationResponse.AsObject,
  }
}

export class GetFeaturesForCultureRequest extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetFeaturesForCultureRequest.AsObject;
  static toObject(includeInstance: boolean, msg: GetFeaturesForCultureRequest): GetFeaturesForCultureRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetFeaturesForCultureRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetFeaturesForCultureRequest;
  static deserializeBinaryFromReader(message: GetFeaturesForCultureRequest, reader: jspb.BinaryReader): GetFeaturesForCultureRequest;
}

export namespace GetFeaturesForCultureRequest {
  export type AsObject = {
    id: number,
  }
}

export class GetFeaturesForCultureResponse extends jspb.Message {
  hasItem(): boolean;
  clearItem(): void;
  getItem(): FeaturesForCulture | undefined;
  setItem(value?: FeaturesForCulture): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetFeaturesForCultureResponse.AsObject;
  static toObject(includeInstance: boolean, msg: GetFeaturesForCultureResponse): GetFeaturesForCultureResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetFeaturesForCultureResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetFeaturesForCultureResponse;
  static deserializeBinaryFromReader(message: GetFeaturesForCultureResponse, reader: jspb.BinaryReader): GetFeaturesForCultureResponse;
}

export namespace GetFeaturesForCultureResponse {
  export type AsObject = {
    item?: FeaturesForCulture.AsObject,
  }
}

