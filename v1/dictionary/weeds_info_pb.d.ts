// package: fcp.dictionary.v1.dictionary
// file: v1/dictionary/weeds_info.proto

import * as jspb from "google-protobuf";
import * as google_protobuf_wrappers_pb from "google-protobuf/google/protobuf/wrappers_pb";
import * as google_protobuf_timestamp_pb from "google-protobuf/google/protobuf/timestamp_pb";
import * as v1_dictionary_common_dictionary_common_pb from "../../v1/dictionary_common/dictionary_common_pb";

export class WeedsInfo extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  getVerminId(): number;
  setVerminId(value: number): void;

  getVerminName(): string;
  setVerminName(value: string): void;

  getClassWeedId(): number;
  setClassWeedId(value: number): void;

  getClassWeedName(): string;
  setClassWeedName(value: string): void;

  getFamilyWeedsId(): number;
  setFamilyWeedsId(value: number): void;

  getFamilyWeedsName(): string;
  setFamilyWeedsName(value: string): void;

  getMethodSupplyId(): number;
  setMethodSupplyId(value: number): void;

  getMethodSupplyName(): string;
  setMethodSupplyName(value: string): void;

  getTxt(): string;
  setTxt(value: string): void;

  hasHash(): boolean;
  clearHash(): void;
  getHash(): google_protobuf_wrappers_pb.StringValue | undefined;
  setHash(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasDeleted(): boolean;
  clearDeleted(): void;
  getDeleted(): google_protobuf_wrappers_pb.BoolValue | undefined;
  setDeleted(value?: google_protobuf_wrappers_pb.BoolValue): void;

  hasCreatedBy(): boolean;
  clearCreatedBy(): void;
  getCreatedBy(): google_protobuf_wrappers_pb.StringValue | undefined;
  setCreatedBy(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasCreatedAt(): boolean;
  clearCreatedAt(): void;
  getCreatedAt(): google_protobuf_timestamp_pb.Timestamp | undefined;
  setCreatedAt(value?: google_protobuf_timestamp_pb.Timestamp): void;

  hasUpdatedBy(): boolean;
  clearUpdatedBy(): void;
  getUpdatedBy(): google_protobuf_wrappers_pb.StringValue | undefined;
  setUpdatedBy(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasUpdatedAt(): boolean;
  clearUpdatedAt(): void;
  getUpdatedAt(): google_protobuf_timestamp_pb.Timestamp | undefined;
  setUpdatedAt(value?: google_protobuf_timestamp_pb.Timestamp): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): WeedsInfo.AsObject;
  static toObject(includeInstance: boolean, msg: WeedsInfo): WeedsInfo.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: WeedsInfo, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): WeedsInfo;
  static deserializeBinaryFromReader(message: WeedsInfo, reader: jspb.BinaryReader): WeedsInfo;
}

export namespace WeedsInfo {
  export type AsObject = {
    id: number,
    verminId: number,
    verminName: string,
    classWeedId: number,
    classWeedName: string,
    familyWeedsId: number,
    familyWeedsName: string,
    methodSupplyId: number,
    methodSupplyName: string,
    txt: string,
    hash?: google_protobuf_wrappers_pb.StringValue.AsObject,
    deleted?: google_protobuf_wrappers_pb.BoolValue.AsObject,
    createdBy?: google_protobuf_wrappers_pb.StringValue.AsObject,
    createdAt?: google_protobuf_timestamp_pb.Timestamp.AsObject,
    updatedBy?: google_protobuf_wrappers_pb.StringValue.AsObject,
    updatedAt?: google_protobuf_timestamp_pb.Timestamp.AsObject,
  }
}

export class WeedsInfoFilter extends jspb.Message {
  hasId(): boolean;
  clearId(): void;
  getId(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setId(value?: google_protobuf_wrappers_pb.Int64Value): void;

  hasVerminId(): boolean;
  clearVerminId(): void;
  getVerminId(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setVerminId(value?: google_protobuf_wrappers_pb.Int64Value): void;

  hasClassWeedId(): boolean;
  clearClassWeedId(): void;
  getClassWeedId(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setClassWeedId(value?: google_protobuf_wrappers_pb.Int64Value): void;

  hasFamilyWeedsId(): boolean;
  clearFamilyWeedsId(): void;
  getFamilyWeedsId(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setFamilyWeedsId(value?: google_protobuf_wrappers_pb.Int64Value): void;

  hasMethodSupplyId(): boolean;
  clearMethodSupplyId(): void;
  getMethodSupplyId(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setMethodSupplyId(value?: google_protobuf_wrappers_pb.Int64Value): void;

  hasDeleted(): boolean;
  clearDeleted(): void;
  getDeleted(): google_protobuf_wrappers_pb.BoolValue | undefined;
  setDeleted(value?: google_protobuf_wrappers_pb.BoolValue): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): WeedsInfoFilter.AsObject;
  static toObject(includeInstance: boolean, msg: WeedsInfoFilter): WeedsInfoFilter.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: WeedsInfoFilter, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): WeedsInfoFilter;
  static deserializeBinaryFromReader(message: WeedsInfoFilter, reader: jspb.BinaryReader): WeedsInfoFilter;
}

export namespace WeedsInfoFilter {
  export type AsObject = {
    id?: google_protobuf_wrappers_pb.Int64Value.AsObject,
    verminId?: google_protobuf_wrappers_pb.Int64Value.AsObject,
    classWeedId?: google_protobuf_wrappers_pb.Int64Value.AsObject,
    familyWeedsId?: google_protobuf_wrappers_pb.Int64Value.AsObject,
    methodSupplyId?: google_protobuf_wrappers_pb.Int64Value.AsObject,
    deleted?: google_protobuf_wrappers_pb.BoolValue.AsObject,
  }
}

export class ListWeedsInfoRequest extends jspb.Message {
  hasFilter(): boolean;
  clearFilter(): void;
  getFilter(): WeedsInfoFilter | undefined;
  setFilter(value?: WeedsInfoFilter): void;

  clearSortingsList(): void;
  getSortingsList(): Array<v1_dictionary_common_dictionary_common_pb.Sorting>;
  setSortingsList(value: Array<v1_dictionary_common_dictionary_common_pb.Sorting>): void;
  addSortings(value?: v1_dictionary_common_dictionary_common_pb.Sorting, index?: number): v1_dictionary_common_dictionary_common_pb.Sorting;

  hasPagination(): boolean;
  clearPagination(): void;
  getPagination(): v1_dictionary_common_dictionary_common_pb.PaginationRequest | undefined;
  setPagination(value?: v1_dictionary_common_dictionary_common_pb.PaginationRequest): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListWeedsInfoRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ListWeedsInfoRequest): ListWeedsInfoRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListWeedsInfoRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListWeedsInfoRequest;
  static deserializeBinaryFromReader(message: ListWeedsInfoRequest, reader: jspb.BinaryReader): ListWeedsInfoRequest;
}

export namespace ListWeedsInfoRequest {
  export type AsObject = {
    filter?: WeedsInfoFilter.AsObject,
    sortingsList: Array<v1_dictionary_common_dictionary_common_pb.Sorting.AsObject>,
    pagination?: v1_dictionary_common_dictionary_common_pb.PaginationRequest.AsObject,
  }
}

export class ListWeedsInfoResponse extends jspb.Message {
  clearItemsList(): void;
  getItemsList(): Array<WeedsInfo>;
  setItemsList(value: Array<WeedsInfo>): void;
  addItems(value?: WeedsInfo, index?: number): WeedsInfo;

  hasPagination(): boolean;
  clearPagination(): void;
  getPagination(): v1_dictionary_common_dictionary_common_pb.PaginationResponse | undefined;
  setPagination(value?: v1_dictionary_common_dictionary_common_pb.PaginationResponse): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListWeedsInfoResponse.AsObject;
  static toObject(includeInstance: boolean, msg: ListWeedsInfoResponse): ListWeedsInfoResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListWeedsInfoResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListWeedsInfoResponse;
  static deserializeBinaryFromReader(message: ListWeedsInfoResponse, reader: jspb.BinaryReader): ListWeedsInfoResponse;
}

export namespace ListWeedsInfoResponse {
  export type AsObject = {
    itemsList: Array<WeedsInfo.AsObject>,
    pagination?: v1_dictionary_common_dictionary_common_pb.PaginationResponse.AsObject,
  }
}

export class GetWeedsInfoRequest extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetWeedsInfoRequest.AsObject;
  static toObject(includeInstance: boolean, msg: GetWeedsInfoRequest): GetWeedsInfoRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetWeedsInfoRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetWeedsInfoRequest;
  static deserializeBinaryFromReader(message: GetWeedsInfoRequest, reader: jspb.BinaryReader): GetWeedsInfoRequest;
}

export namespace GetWeedsInfoRequest {
  export type AsObject = {
    id: number,
  }
}

export class GetWeedsInfoResponse extends jspb.Message {
  hasItem(): boolean;
  clearItem(): void;
  getItem(): WeedsInfo | undefined;
  setItem(value?: WeedsInfo): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetWeedsInfoResponse.AsObject;
  static toObject(includeInstance: boolean, msg: GetWeedsInfoResponse): GetWeedsInfoResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetWeedsInfoResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetWeedsInfoResponse;
  static deserializeBinaryFromReader(message: GetWeedsInfoResponse, reader: jspb.BinaryReader): GetWeedsInfoResponse;
}

export namespace GetWeedsInfoResponse {
  export type AsObject = {
    item?: WeedsInfo.AsObject,
  }
}

