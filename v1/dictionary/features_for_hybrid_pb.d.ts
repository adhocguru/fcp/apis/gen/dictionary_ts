// package: fcp.dictionary.v1.dictionary
// file: v1/dictionary/features_for_hybrid.proto

import * as jspb from "google-protobuf";
import * as google_protobuf_wrappers_pb from "google-protobuf/google/protobuf/wrappers_pb";
import * as google_protobuf_timestamp_pb from "google-protobuf/google/protobuf/timestamp_pb";
import * as v1_dictionary_common_dictionary_common_pb from "../../v1/dictionary_common/dictionary_common_pb";

export class FeaturesForHybrid extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  getFeatureId(): number;
  setFeatureId(value: number): void;

  getFeatureName(): string;
  setFeatureName(value: string): void;

  getHybridId(): number;
  setHybridId(value: number): void;

  getHybridName(): string;
  setHybridName(value: string): void;

  getFeaturesValueId(): number;
  setFeaturesValueId(value: number): void;

  hasFeaturesHardValue(): boolean;
  clearFeaturesHardValue(): void;
  getFeaturesHardValue(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setFeaturesHardValue(value?: google_protobuf_wrappers_pb.Int64Value): void;

  hasHash(): boolean;
  clearHash(): void;
  getHash(): google_protobuf_wrappers_pb.StringValue | undefined;
  setHash(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasDeleted(): boolean;
  clearDeleted(): void;
  getDeleted(): google_protobuf_wrappers_pb.BoolValue | undefined;
  setDeleted(value?: google_protobuf_wrappers_pb.BoolValue): void;

  hasCreatedBy(): boolean;
  clearCreatedBy(): void;
  getCreatedBy(): google_protobuf_wrappers_pb.StringValue | undefined;
  setCreatedBy(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasCreatedAt(): boolean;
  clearCreatedAt(): void;
  getCreatedAt(): google_protobuf_timestamp_pb.Timestamp | undefined;
  setCreatedAt(value?: google_protobuf_timestamp_pb.Timestamp): void;

  hasUpdatedBy(): boolean;
  clearUpdatedBy(): void;
  getUpdatedBy(): google_protobuf_wrappers_pb.StringValue | undefined;
  setUpdatedBy(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasUpdatedAt(): boolean;
  clearUpdatedAt(): void;
  getUpdatedAt(): google_protobuf_timestamp_pb.Timestamp | undefined;
  setUpdatedAt(value?: google_protobuf_timestamp_pb.Timestamp): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): FeaturesForHybrid.AsObject;
  static toObject(includeInstance: boolean, msg: FeaturesForHybrid): FeaturesForHybrid.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: FeaturesForHybrid, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): FeaturesForHybrid;
  static deserializeBinaryFromReader(message: FeaturesForHybrid, reader: jspb.BinaryReader): FeaturesForHybrid;
}

export namespace FeaturesForHybrid {
  export type AsObject = {
    id: number,
    featureId: number,
    featureName: string,
    hybridId: number,
    hybridName: string,
    featuresValueId: number,
    featuresHardValue?: google_protobuf_wrappers_pb.Int64Value.AsObject,
    hash?: google_protobuf_wrappers_pb.StringValue.AsObject,
    deleted?: google_protobuf_wrappers_pb.BoolValue.AsObject,
    createdBy?: google_protobuf_wrappers_pb.StringValue.AsObject,
    createdAt?: google_protobuf_timestamp_pb.Timestamp.AsObject,
    updatedBy?: google_protobuf_wrappers_pb.StringValue.AsObject,
    updatedAt?: google_protobuf_timestamp_pb.Timestamp.AsObject,
  }
}

export class FeaturesForHybridFilter extends jspb.Message {
  hasId(): boolean;
  clearId(): void;
  getId(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setId(value?: google_protobuf_wrappers_pb.Int64Value): void;

  hasFeatureId(): boolean;
  clearFeatureId(): void;
  getFeatureId(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setFeatureId(value?: google_protobuf_wrappers_pb.Int64Value): void;

  hasHybridId(): boolean;
  clearHybridId(): void;
  getHybridId(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setHybridId(value?: google_protobuf_wrappers_pb.Int64Value): void;

  hasFeaturesValueId(): boolean;
  clearFeaturesValueId(): void;
  getFeaturesValueId(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setFeaturesValueId(value?: google_protobuf_wrappers_pb.Int64Value): void;

  hasDeleted(): boolean;
  clearDeleted(): void;
  getDeleted(): google_protobuf_wrappers_pb.BoolValue | undefined;
  setDeleted(value?: google_protobuf_wrappers_pb.BoolValue): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): FeaturesForHybridFilter.AsObject;
  static toObject(includeInstance: boolean, msg: FeaturesForHybridFilter): FeaturesForHybridFilter.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: FeaturesForHybridFilter, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): FeaturesForHybridFilter;
  static deserializeBinaryFromReader(message: FeaturesForHybridFilter, reader: jspb.BinaryReader): FeaturesForHybridFilter;
}

export namespace FeaturesForHybridFilter {
  export type AsObject = {
    id?: google_protobuf_wrappers_pb.Int64Value.AsObject,
    featureId?: google_protobuf_wrappers_pb.Int64Value.AsObject,
    hybridId?: google_protobuf_wrappers_pb.Int64Value.AsObject,
    featuresValueId?: google_protobuf_wrappers_pb.Int64Value.AsObject,
    deleted?: google_protobuf_wrappers_pb.BoolValue.AsObject,
  }
}

export class ListFeaturesForHybridRequest extends jspb.Message {
  hasFilter(): boolean;
  clearFilter(): void;
  getFilter(): FeaturesForHybridFilter | undefined;
  setFilter(value?: FeaturesForHybridFilter): void;

  clearSortingsList(): void;
  getSortingsList(): Array<v1_dictionary_common_dictionary_common_pb.Sorting>;
  setSortingsList(value: Array<v1_dictionary_common_dictionary_common_pb.Sorting>): void;
  addSortings(value?: v1_dictionary_common_dictionary_common_pb.Sorting, index?: number): v1_dictionary_common_dictionary_common_pb.Sorting;

  hasPagination(): boolean;
  clearPagination(): void;
  getPagination(): v1_dictionary_common_dictionary_common_pb.PaginationRequest | undefined;
  setPagination(value?: v1_dictionary_common_dictionary_common_pb.PaginationRequest): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListFeaturesForHybridRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ListFeaturesForHybridRequest): ListFeaturesForHybridRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListFeaturesForHybridRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListFeaturesForHybridRequest;
  static deserializeBinaryFromReader(message: ListFeaturesForHybridRequest, reader: jspb.BinaryReader): ListFeaturesForHybridRequest;
}

export namespace ListFeaturesForHybridRequest {
  export type AsObject = {
    filter?: FeaturesForHybridFilter.AsObject,
    sortingsList: Array<v1_dictionary_common_dictionary_common_pb.Sorting.AsObject>,
    pagination?: v1_dictionary_common_dictionary_common_pb.PaginationRequest.AsObject,
  }
}

export class ListFeaturesForHybridResponse extends jspb.Message {
  clearItemsList(): void;
  getItemsList(): Array<FeaturesForHybrid>;
  setItemsList(value: Array<FeaturesForHybrid>): void;
  addItems(value?: FeaturesForHybrid, index?: number): FeaturesForHybrid;

  hasPagination(): boolean;
  clearPagination(): void;
  getPagination(): v1_dictionary_common_dictionary_common_pb.PaginationResponse | undefined;
  setPagination(value?: v1_dictionary_common_dictionary_common_pb.PaginationResponse): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListFeaturesForHybridResponse.AsObject;
  static toObject(includeInstance: boolean, msg: ListFeaturesForHybridResponse): ListFeaturesForHybridResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListFeaturesForHybridResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListFeaturesForHybridResponse;
  static deserializeBinaryFromReader(message: ListFeaturesForHybridResponse, reader: jspb.BinaryReader): ListFeaturesForHybridResponse;
}

export namespace ListFeaturesForHybridResponse {
  export type AsObject = {
    itemsList: Array<FeaturesForHybrid.AsObject>,
    pagination?: v1_dictionary_common_dictionary_common_pb.PaginationResponse.AsObject,
  }
}

export class GetFeaturesForHybridRequest extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetFeaturesForHybridRequest.AsObject;
  static toObject(includeInstance: boolean, msg: GetFeaturesForHybridRequest): GetFeaturesForHybridRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetFeaturesForHybridRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetFeaturesForHybridRequest;
  static deserializeBinaryFromReader(message: GetFeaturesForHybridRequest, reader: jspb.BinaryReader): GetFeaturesForHybridRequest;
}

export namespace GetFeaturesForHybridRequest {
  export type AsObject = {
    id: number,
  }
}

export class GetFeaturesForHybridResponse extends jspb.Message {
  hasItem(): boolean;
  clearItem(): void;
  getItem(): FeaturesForHybrid | undefined;
  setItem(value?: FeaturesForHybrid): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetFeaturesForHybridResponse.AsObject;
  static toObject(includeInstance: boolean, msg: GetFeaturesForHybridResponse): GetFeaturesForHybridResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetFeaturesForHybridResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetFeaturesForHybridResponse;
  static deserializeBinaryFromReader(message: GetFeaturesForHybridResponse, reader: jspb.BinaryReader): GetFeaturesForHybridResponse;
}

export namespace GetFeaturesForHybridResponse {
  export type AsObject = {
    item?: FeaturesForHybrid.AsObject,
  }
}

