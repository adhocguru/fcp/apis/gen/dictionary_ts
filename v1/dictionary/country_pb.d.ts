// package: fcp.dictionary.v1.dictionary
// file: v1/dictionary/country.proto

import * as jspb from "google-protobuf";
import * as google_protobuf_empty_pb from "google-protobuf/google/protobuf/empty_pb";
import * as google_protobuf_wrappers_pb from "google-protobuf/google/protobuf/wrappers_pb";
import * as google_protobuf_timestamp_pb from "google-protobuf/google/protobuf/timestamp_pb";
import * as v1_dictionary_common_dictionary_common_pb from "../../v1/dictionary_common/dictionary_common_pb";

export class Country extends jspb.Message {
  getId(): string;
  setId(value: string): void;

  getName(): string;
  setName(value: string): void;

  getIso3(): string;
  setIso3(value: string): void;

  getDeleted(): boolean;
  setDeleted(value: boolean): void;

  getCreatedBy(): string;
  setCreatedBy(value: string): void;

  hasCreatedAt(): boolean;
  clearCreatedAt(): void;
  getCreatedAt(): google_protobuf_timestamp_pb.Timestamp | undefined;
  setCreatedAt(value?: google_protobuf_timestamp_pb.Timestamp): void;

  getUpdatedBy(): string;
  setUpdatedBy(value: string): void;

  hasUpdatedAt(): boolean;
  clearUpdatedAt(): void;
  getUpdatedAt(): google_protobuf_timestamp_pb.Timestamp | undefined;
  setUpdatedAt(value?: google_protobuf_timestamp_pb.Timestamp): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): Country.AsObject;
  static toObject(includeInstance: boolean, msg: Country): Country.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: Country, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): Country;
  static deserializeBinaryFromReader(message: Country, reader: jspb.BinaryReader): Country;
}

export namespace Country {
  export type AsObject = {
    id: string,
    name: string,
    iso3: string,
    deleted: boolean,
    createdBy: string,
    createdAt?: google_protobuf_timestamp_pb.Timestamp.AsObject,
    updatedBy: string,
    updatedAt?: google_protobuf_timestamp_pb.Timestamp.AsObject,
  }
}

export class CountryFilter extends jspb.Message {
  hasId(): boolean;
  clearId(): void;
  getId(): google_protobuf_wrappers_pb.StringValue | undefined;
  setId(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasName(): boolean;
  clearName(): void;
  getName(): google_protobuf_wrappers_pb.StringValue | undefined;
  setName(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasIso3(): boolean;
  clearIso3(): void;
  getIso3(): google_protobuf_wrappers_pb.StringValue | undefined;
  setIso3(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasShowDeleted(): boolean;
  clearShowDeleted(): void;
  getShowDeleted(): google_protobuf_wrappers_pb.BoolValue | undefined;
  setShowDeleted(value?: google_protobuf_wrappers_pb.BoolValue): void;

  hasCreatedBy(): boolean;
  clearCreatedBy(): void;
  getCreatedBy(): google_protobuf_wrappers_pb.StringValue | undefined;
  setCreatedBy(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasCreatedAt(): boolean;
  clearCreatedAt(): void;
  getCreatedAt(): v1_dictionary_common_dictionary_common_pb.TimeRange | undefined;
  setCreatedAt(value?: v1_dictionary_common_dictionary_common_pb.TimeRange): void;

  hasUpdatedBy(): boolean;
  clearUpdatedBy(): void;
  getUpdatedBy(): google_protobuf_wrappers_pb.StringValue | undefined;
  setUpdatedBy(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasUpdatedAt(): boolean;
  clearUpdatedAt(): void;
  getUpdatedAt(): v1_dictionary_common_dictionary_common_pb.TimeRange | undefined;
  setUpdatedAt(value?: v1_dictionary_common_dictionary_common_pb.TimeRange): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): CountryFilter.AsObject;
  static toObject(includeInstance: boolean, msg: CountryFilter): CountryFilter.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: CountryFilter, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): CountryFilter;
  static deserializeBinaryFromReader(message: CountryFilter, reader: jspb.BinaryReader): CountryFilter;
}

export namespace CountryFilter {
  export type AsObject = {
    id?: google_protobuf_wrappers_pb.StringValue.AsObject,
    name?: google_protobuf_wrappers_pb.StringValue.AsObject,
    iso3?: google_protobuf_wrappers_pb.StringValue.AsObject,
    showDeleted?: google_protobuf_wrappers_pb.BoolValue.AsObject,
    createdBy?: google_protobuf_wrappers_pb.StringValue.AsObject,
    createdAt?: v1_dictionary_common_dictionary_common_pb.TimeRange.AsObject,
    updatedBy?: google_protobuf_wrappers_pb.StringValue.AsObject,
    updatedAt?: v1_dictionary_common_dictionary_common_pb.TimeRange.AsObject,
  }
}

export class ListCountryRequest extends jspb.Message {
  hasFilter(): boolean;
  clearFilter(): void;
  getFilter(): CountryFilter | undefined;
  setFilter(value?: CountryFilter): void;

  clearSortingsList(): void;
  getSortingsList(): Array<v1_dictionary_common_dictionary_common_pb.Sorting>;
  setSortingsList(value: Array<v1_dictionary_common_dictionary_common_pb.Sorting>): void;
  addSortings(value?: v1_dictionary_common_dictionary_common_pb.Sorting, index?: number): v1_dictionary_common_dictionary_common_pb.Sorting;

  hasPagination(): boolean;
  clearPagination(): void;
  getPagination(): v1_dictionary_common_dictionary_common_pb.PaginationRequest | undefined;
  setPagination(value?: v1_dictionary_common_dictionary_common_pb.PaginationRequest): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListCountryRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ListCountryRequest): ListCountryRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListCountryRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListCountryRequest;
  static deserializeBinaryFromReader(message: ListCountryRequest, reader: jspb.BinaryReader): ListCountryRequest;
}

export namespace ListCountryRequest {
  export type AsObject = {
    filter?: CountryFilter.AsObject,
    sortingsList: Array<v1_dictionary_common_dictionary_common_pb.Sorting.AsObject>,
    pagination?: v1_dictionary_common_dictionary_common_pb.PaginationRequest.AsObject,
  }
}

export class ListCountryResponse extends jspb.Message {
  clearCountriesList(): void;
  getCountriesList(): Array<Country>;
  setCountriesList(value: Array<Country>): void;
  addCountries(value?: Country, index?: number): Country;

  hasPagination(): boolean;
  clearPagination(): void;
  getPagination(): v1_dictionary_common_dictionary_common_pb.PaginationResponse | undefined;
  setPagination(value?: v1_dictionary_common_dictionary_common_pb.PaginationResponse): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListCountryResponse.AsObject;
  static toObject(includeInstance: boolean, msg: ListCountryResponse): ListCountryResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListCountryResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListCountryResponse;
  static deserializeBinaryFromReader(message: ListCountryResponse, reader: jspb.BinaryReader): ListCountryResponse;
}

export namespace ListCountryResponse {
  export type AsObject = {
    countriesList: Array<Country.AsObject>,
    pagination?: v1_dictionary_common_dictionary_common_pb.PaginationResponse.AsObject,
  }
}

export class GetCountryRequest extends jspb.Message {
  getId(): string;
  setId(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetCountryRequest.AsObject;
  static toObject(includeInstance: boolean, msg: GetCountryRequest): GetCountryRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetCountryRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetCountryRequest;
  static deserializeBinaryFromReader(message: GetCountryRequest, reader: jspb.BinaryReader): GetCountryRequest;
}

export namespace GetCountryRequest {
  export type AsObject = {
    id: string,
  }
}

export class GetCountryResponse extends jspb.Message {
  hasCountry(): boolean;
  clearCountry(): void;
  getCountry(): Country | undefined;
  setCountry(value?: Country): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetCountryResponse.AsObject;
  static toObject(includeInstance: boolean, msg: GetCountryResponse): GetCountryResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetCountryResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetCountryResponse;
  static deserializeBinaryFromReader(message: GetCountryResponse, reader: jspb.BinaryReader): GetCountryResponse;
}

export namespace GetCountryResponse {
  export type AsObject = {
    country?: Country.AsObject,
  }
}

export class CreateCountryRequest extends jspb.Message {
  getId(): string;
  setId(value: string): void;

  getName(): string;
  setName(value: string): void;

  hasIso3(): boolean;
  clearIso3(): void;
  getIso3(): google_protobuf_wrappers_pb.StringValue | undefined;
  setIso3(value?: google_protobuf_wrappers_pb.StringValue): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): CreateCountryRequest.AsObject;
  static toObject(includeInstance: boolean, msg: CreateCountryRequest): CreateCountryRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: CreateCountryRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): CreateCountryRequest;
  static deserializeBinaryFromReader(message: CreateCountryRequest, reader: jspb.BinaryReader): CreateCountryRequest;
}

export namespace CreateCountryRequest {
  export type AsObject = {
    id: string,
    name: string,
    iso3?: google_protobuf_wrappers_pb.StringValue.AsObject,
  }
}

export class CreateCountryResponse extends jspb.Message {
  hasCountry(): boolean;
  clearCountry(): void;
  getCountry(): Country | undefined;
  setCountry(value?: Country): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): CreateCountryResponse.AsObject;
  static toObject(includeInstance: boolean, msg: CreateCountryResponse): CreateCountryResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: CreateCountryResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): CreateCountryResponse;
  static deserializeBinaryFromReader(message: CreateCountryResponse, reader: jspb.BinaryReader): CreateCountryResponse;
}

export namespace CreateCountryResponse {
  export type AsObject = {
    country?: Country.AsObject,
  }
}

export class UpdateCountryRequest extends jspb.Message {
  getId(): string;
  setId(value: string): void;

  getName(): string;
  setName(value: string): void;

  hasIso3(): boolean;
  clearIso3(): void;
  getIso3(): google_protobuf_wrappers_pb.StringValue | undefined;
  setIso3(value?: google_protobuf_wrappers_pb.StringValue): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): UpdateCountryRequest.AsObject;
  static toObject(includeInstance: boolean, msg: UpdateCountryRequest): UpdateCountryRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: UpdateCountryRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): UpdateCountryRequest;
  static deserializeBinaryFromReader(message: UpdateCountryRequest, reader: jspb.BinaryReader): UpdateCountryRequest;
}

export namespace UpdateCountryRequest {
  export type AsObject = {
    id: string,
    name: string,
    iso3?: google_protobuf_wrappers_pb.StringValue.AsObject,
  }
}

export class UpdateCountryResponse extends jspb.Message {
  hasCountry(): boolean;
  clearCountry(): void;
  getCountry(): Country | undefined;
  setCountry(value?: Country): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): UpdateCountryResponse.AsObject;
  static toObject(includeInstance: boolean, msg: UpdateCountryResponse): UpdateCountryResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: UpdateCountryResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): UpdateCountryResponse;
  static deserializeBinaryFromReader(message: UpdateCountryResponse, reader: jspb.BinaryReader): UpdateCountryResponse;
}

export namespace UpdateCountryResponse {
  export type AsObject = {
    country?: Country.AsObject,
  }
}

export class DeleteCountryRequest extends jspb.Message {
  getId(): string;
  setId(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): DeleteCountryRequest.AsObject;
  static toObject(includeInstance: boolean, msg: DeleteCountryRequest): DeleteCountryRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: DeleteCountryRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): DeleteCountryRequest;
  static deserializeBinaryFromReader(message: DeleteCountryRequest, reader: jspb.BinaryReader): DeleteCountryRequest;
}

export namespace DeleteCountryRequest {
  export type AsObject = {
    id: string,
  }
}

export class RestoreCountryRequest extends jspb.Message {
  getId(): string;
  setId(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): RestoreCountryRequest.AsObject;
  static toObject(includeInstance: boolean, msg: RestoreCountryRequest): RestoreCountryRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: RestoreCountryRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): RestoreCountryRequest;
  static deserializeBinaryFromReader(message: RestoreCountryRequest, reader: jspb.BinaryReader): RestoreCountryRequest;
}

export namespace RestoreCountryRequest {
  export type AsObject = {
    id: string,
  }
}

