// package: fcp.dictionary.v1.dictionary
// file: v1/dictionary/active_substance.proto

import * as jspb from "google-protobuf";
import * as google_protobuf_wrappers_pb from "google-protobuf/google/protobuf/wrappers_pb";
import * as google_protobuf_timestamp_pb from "google-protobuf/google/protobuf/timestamp_pb";
import * as v1_dictionary_common_dictionary_common_pb from "../../v1/dictionary_common/dictionary_common_pb";

export class ActiveSubstance extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  getGuid(): string;
  setGuid(value: string): void;

  getName(): string;
  setName(value: string): void;

  hasHash(): boolean;
  clearHash(): void;
  getHash(): google_protobuf_wrappers_pb.StringValue | undefined;
  setHash(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasDeleted(): boolean;
  clearDeleted(): void;
  getDeleted(): google_protobuf_wrappers_pb.BoolValue | undefined;
  setDeleted(value?: google_protobuf_wrappers_pb.BoolValue): void;

  hasCreatedBy(): boolean;
  clearCreatedBy(): void;
  getCreatedBy(): google_protobuf_wrappers_pb.StringValue | undefined;
  setCreatedBy(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasCreatedAt(): boolean;
  clearCreatedAt(): void;
  getCreatedAt(): google_protobuf_timestamp_pb.Timestamp | undefined;
  setCreatedAt(value?: google_protobuf_timestamp_pb.Timestamp): void;

  hasUpdatedBy(): boolean;
  clearUpdatedBy(): void;
  getUpdatedBy(): google_protobuf_wrappers_pb.StringValue | undefined;
  setUpdatedBy(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasUpdatedAt(): boolean;
  clearUpdatedAt(): void;
  getUpdatedAt(): google_protobuf_timestamp_pb.Timestamp | undefined;
  setUpdatedAt(value?: google_protobuf_timestamp_pb.Timestamp): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ActiveSubstance.AsObject;
  static toObject(includeInstance: boolean, msg: ActiveSubstance): ActiveSubstance.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ActiveSubstance, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ActiveSubstance;
  static deserializeBinaryFromReader(message: ActiveSubstance, reader: jspb.BinaryReader): ActiveSubstance;
}

export namespace ActiveSubstance {
  export type AsObject = {
    id: number,
    guid: string,
    name: string,
    hash?: google_protobuf_wrappers_pb.StringValue.AsObject,
    deleted?: google_protobuf_wrappers_pb.BoolValue.AsObject,
    createdBy?: google_protobuf_wrappers_pb.StringValue.AsObject,
    createdAt?: google_protobuf_timestamp_pb.Timestamp.AsObject,
    updatedBy?: google_protobuf_wrappers_pb.StringValue.AsObject,
    updatedAt?: google_protobuf_timestamp_pb.Timestamp.AsObject,
  }
}

export class ActiveSubstanceFilter extends jspb.Message {
  hasId(): boolean;
  clearId(): void;
  getId(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setId(value?: google_protobuf_wrappers_pb.Int64Value): void;

  hasGuid(): boolean;
  clearGuid(): void;
  getGuid(): google_protobuf_wrappers_pb.StringValue | undefined;
  setGuid(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasName(): boolean;
  clearName(): void;
  getName(): google_protobuf_wrappers_pb.StringValue | undefined;
  setName(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasDeleted(): boolean;
  clearDeleted(): void;
  getDeleted(): google_protobuf_wrappers_pb.BoolValue | undefined;
  setDeleted(value?: google_protobuf_wrappers_pb.BoolValue): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ActiveSubstanceFilter.AsObject;
  static toObject(includeInstance: boolean, msg: ActiveSubstanceFilter): ActiveSubstanceFilter.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ActiveSubstanceFilter, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ActiveSubstanceFilter;
  static deserializeBinaryFromReader(message: ActiveSubstanceFilter, reader: jspb.BinaryReader): ActiveSubstanceFilter;
}

export namespace ActiveSubstanceFilter {
  export type AsObject = {
    id?: google_protobuf_wrappers_pb.Int64Value.AsObject,
    guid?: google_protobuf_wrappers_pb.StringValue.AsObject,
    name?: google_protobuf_wrappers_pb.StringValue.AsObject,
    deleted?: google_protobuf_wrappers_pb.BoolValue.AsObject,
  }
}

export class GetActiveSubstanceRequest extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetActiveSubstanceRequest.AsObject;
  static toObject(includeInstance: boolean, msg: GetActiveSubstanceRequest): GetActiveSubstanceRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetActiveSubstanceRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetActiveSubstanceRequest;
  static deserializeBinaryFromReader(message: GetActiveSubstanceRequest, reader: jspb.BinaryReader): GetActiveSubstanceRequest;
}

export namespace GetActiveSubstanceRequest {
  export type AsObject = {
    id: number,
  }
}

export class GetActiveSubstanceResponse extends jspb.Message {
  hasItem(): boolean;
  clearItem(): void;
  getItem(): ActiveSubstance | undefined;
  setItem(value?: ActiveSubstance): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetActiveSubstanceResponse.AsObject;
  static toObject(includeInstance: boolean, msg: GetActiveSubstanceResponse): GetActiveSubstanceResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetActiveSubstanceResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetActiveSubstanceResponse;
  static deserializeBinaryFromReader(message: GetActiveSubstanceResponse, reader: jspb.BinaryReader): GetActiveSubstanceResponse;
}

export namespace GetActiveSubstanceResponse {
  export type AsObject = {
    item?: ActiveSubstance.AsObject,
  }
}

export class ListActiveSubstanceRequest extends jspb.Message {
  hasFilter(): boolean;
  clearFilter(): void;
  getFilter(): ActiveSubstanceFilter | undefined;
  setFilter(value?: ActiveSubstanceFilter): void;

  clearSortingsList(): void;
  getSortingsList(): Array<v1_dictionary_common_dictionary_common_pb.Sorting>;
  setSortingsList(value: Array<v1_dictionary_common_dictionary_common_pb.Sorting>): void;
  addSortings(value?: v1_dictionary_common_dictionary_common_pb.Sorting, index?: number): v1_dictionary_common_dictionary_common_pb.Sorting;

  hasPagination(): boolean;
  clearPagination(): void;
  getPagination(): v1_dictionary_common_dictionary_common_pb.PaginationRequest | undefined;
  setPagination(value?: v1_dictionary_common_dictionary_common_pb.PaginationRequest): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListActiveSubstanceRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ListActiveSubstanceRequest): ListActiveSubstanceRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListActiveSubstanceRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListActiveSubstanceRequest;
  static deserializeBinaryFromReader(message: ListActiveSubstanceRequest, reader: jspb.BinaryReader): ListActiveSubstanceRequest;
}

export namespace ListActiveSubstanceRequest {
  export type AsObject = {
    filter?: ActiveSubstanceFilter.AsObject,
    sortingsList: Array<v1_dictionary_common_dictionary_common_pb.Sorting.AsObject>,
    pagination?: v1_dictionary_common_dictionary_common_pb.PaginationRequest.AsObject,
  }
}

export class ListActiveSubstanceResponse extends jspb.Message {
  clearItemsList(): void;
  getItemsList(): Array<ActiveSubstance>;
  setItemsList(value: Array<ActiveSubstance>): void;
  addItems(value?: ActiveSubstance, index?: number): ActiveSubstance;

  hasPagination(): boolean;
  clearPagination(): void;
  getPagination(): v1_dictionary_common_dictionary_common_pb.PaginationResponse | undefined;
  setPagination(value?: v1_dictionary_common_dictionary_common_pb.PaginationResponse): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListActiveSubstanceResponse.AsObject;
  static toObject(includeInstance: boolean, msg: ListActiveSubstanceResponse): ListActiveSubstanceResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListActiveSubstanceResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListActiveSubstanceResponse;
  static deserializeBinaryFromReader(message: ListActiveSubstanceResponse, reader: jspb.BinaryReader): ListActiveSubstanceResponse;
}

export namespace ListActiveSubstanceResponse {
  export type AsObject = {
    itemsList: Array<ActiveSubstance.AsObject>,
    pagination?: v1_dictionary_common_dictionary_common_pb.PaginationResponse.AsObject,
  }
}

