// package: fcp.dictionary.v1.dictionary
// file: v1/dictionary/microelement_for_product.proto

import * as jspb from "google-protobuf";
import * as google_protobuf_wrappers_pb from "google-protobuf/google/protobuf/wrappers_pb";
import * as google_protobuf_timestamp_pb from "google-protobuf/google/protobuf/timestamp_pb";
import * as v1_dictionary_common_dictionary_common_pb from "../../v1/dictionary_common/dictionary_common_pb";

export class MicroelementForProduct extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  getMicroelementId(): number;
  setMicroelementId(value: number): void;

  getMicroelementName(): string;
  setMicroelementName(value: string): void;

  getConcentrationType(): string;
  setConcentrationType(value: string): void;

  getQuantity(): string;
  setQuantity(value: string): void;

  getUnitName(): string;
  setUnitName(value: string): void;

  getBasisQuantity(): string;
  setBasisQuantity(value: string): void;

  getBasisUnitName(): string;
  setBasisUnitName(value: string): void;

  getProductId(): number;
  setProductId(value: number): void;

  getProductName(): string;
  setProductName(value: string): void;

  hasHash(): boolean;
  clearHash(): void;
  getHash(): google_protobuf_wrappers_pb.StringValue | undefined;
  setHash(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasDeleted(): boolean;
  clearDeleted(): void;
  getDeleted(): google_protobuf_wrappers_pb.BoolValue | undefined;
  setDeleted(value?: google_protobuf_wrappers_pb.BoolValue): void;

  hasCreatedBy(): boolean;
  clearCreatedBy(): void;
  getCreatedBy(): google_protobuf_wrappers_pb.StringValue | undefined;
  setCreatedBy(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasCreatedAt(): boolean;
  clearCreatedAt(): void;
  getCreatedAt(): google_protobuf_timestamp_pb.Timestamp | undefined;
  setCreatedAt(value?: google_protobuf_timestamp_pb.Timestamp): void;

  hasUpdatedBy(): boolean;
  clearUpdatedBy(): void;
  getUpdatedBy(): google_protobuf_wrappers_pb.StringValue | undefined;
  setUpdatedBy(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasUpdatedAt(): boolean;
  clearUpdatedAt(): void;
  getUpdatedAt(): google_protobuf_timestamp_pb.Timestamp | undefined;
  setUpdatedAt(value?: google_protobuf_timestamp_pb.Timestamp): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): MicroelementForProduct.AsObject;
  static toObject(includeInstance: boolean, msg: MicroelementForProduct): MicroelementForProduct.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: MicroelementForProduct, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): MicroelementForProduct;
  static deserializeBinaryFromReader(message: MicroelementForProduct, reader: jspb.BinaryReader): MicroelementForProduct;
}

export namespace MicroelementForProduct {
  export type AsObject = {
    id: number,
    microelementId: number,
    microelementName: string,
    concentrationType: string,
    quantity: string,
    unitName: string,
    basisQuantity: string,
    basisUnitName: string,
    productId: number,
    productName: string,
    hash?: google_protobuf_wrappers_pb.StringValue.AsObject,
    deleted?: google_protobuf_wrappers_pb.BoolValue.AsObject,
    createdBy?: google_protobuf_wrappers_pb.StringValue.AsObject,
    createdAt?: google_protobuf_timestamp_pb.Timestamp.AsObject,
    updatedBy?: google_protobuf_wrappers_pb.StringValue.AsObject,
    updatedAt?: google_protobuf_timestamp_pb.Timestamp.AsObject,
  }
}

export class MicroelementForProductFilter extends jspb.Message {
  hasId(): boolean;
  clearId(): void;
  getId(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setId(value?: google_protobuf_wrappers_pb.Int64Value): void;

  hasMicroelementId(): boolean;
  clearMicroelementId(): void;
  getMicroelementId(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setMicroelementId(value?: google_protobuf_wrappers_pb.Int64Value): void;

  hasProductId(): boolean;
  clearProductId(): void;
  getProductId(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setProductId(value?: google_protobuf_wrappers_pb.Int64Value): void;

  hasDeleted(): boolean;
  clearDeleted(): void;
  getDeleted(): google_protobuf_wrappers_pb.BoolValue | undefined;
  setDeleted(value?: google_protobuf_wrappers_pb.BoolValue): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): MicroelementForProductFilter.AsObject;
  static toObject(includeInstance: boolean, msg: MicroelementForProductFilter): MicroelementForProductFilter.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: MicroelementForProductFilter, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): MicroelementForProductFilter;
  static deserializeBinaryFromReader(message: MicroelementForProductFilter, reader: jspb.BinaryReader): MicroelementForProductFilter;
}

export namespace MicroelementForProductFilter {
  export type AsObject = {
    id?: google_protobuf_wrappers_pb.Int64Value.AsObject,
    microelementId?: google_protobuf_wrappers_pb.Int64Value.AsObject,
    productId?: google_protobuf_wrappers_pb.Int64Value.AsObject,
    deleted?: google_protobuf_wrappers_pb.BoolValue.AsObject,
  }
}

export class GetMicroelementForProductRequest extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetMicroelementForProductRequest.AsObject;
  static toObject(includeInstance: boolean, msg: GetMicroelementForProductRequest): GetMicroelementForProductRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetMicroelementForProductRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetMicroelementForProductRequest;
  static deserializeBinaryFromReader(message: GetMicroelementForProductRequest, reader: jspb.BinaryReader): GetMicroelementForProductRequest;
}

export namespace GetMicroelementForProductRequest {
  export type AsObject = {
    id: number,
  }
}

export class GetMicroelementForProductResponse extends jspb.Message {
  hasItem(): boolean;
  clearItem(): void;
  getItem(): MicroelementForProduct | undefined;
  setItem(value?: MicroelementForProduct): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetMicroelementForProductResponse.AsObject;
  static toObject(includeInstance: boolean, msg: GetMicroelementForProductResponse): GetMicroelementForProductResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetMicroelementForProductResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetMicroelementForProductResponse;
  static deserializeBinaryFromReader(message: GetMicroelementForProductResponse, reader: jspb.BinaryReader): GetMicroelementForProductResponse;
}

export namespace GetMicroelementForProductResponse {
  export type AsObject = {
    item?: MicroelementForProduct.AsObject,
  }
}

export class ListMicroelementForProductRequest extends jspb.Message {
  hasFilter(): boolean;
  clearFilter(): void;
  getFilter(): MicroelementForProductFilter | undefined;
  setFilter(value?: MicroelementForProductFilter): void;

  clearSortingsList(): void;
  getSortingsList(): Array<v1_dictionary_common_dictionary_common_pb.Sorting>;
  setSortingsList(value: Array<v1_dictionary_common_dictionary_common_pb.Sorting>): void;
  addSortings(value?: v1_dictionary_common_dictionary_common_pb.Sorting, index?: number): v1_dictionary_common_dictionary_common_pb.Sorting;

  hasPagination(): boolean;
  clearPagination(): void;
  getPagination(): v1_dictionary_common_dictionary_common_pb.PaginationRequest | undefined;
  setPagination(value?: v1_dictionary_common_dictionary_common_pb.PaginationRequest): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListMicroelementForProductRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ListMicroelementForProductRequest): ListMicroelementForProductRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListMicroelementForProductRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListMicroelementForProductRequest;
  static deserializeBinaryFromReader(message: ListMicroelementForProductRequest, reader: jspb.BinaryReader): ListMicroelementForProductRequest;
}

export namespace ListMicroelementForProductRequest {
  export type AsObject = {
    filter?: MicroelementForProductFilter.AsObject,
    sortingsList: Array<v1_dictionary_common_dictionary_common_pb.Sorting.AsObject>,
    pagination?: v1_dictionary_common_dictionary_common_pb.PaginationRequest.AsObject,
  }
}

export class ListMicroelementForProductResponse extends jspb.Message {
  clearItemsList(): void;
  getItemsList(): Array<MicroelementForProduct>;
  setItemsList(value: Array<MicroelementForProduct>): void;
  addItems(value?: MicroelementForProduct, index?: number): MicroelementForProduct;

  hasPagination(): boolean;
  clearPagination(): void;
  getPagination(): v1_dictionary_common_dictionary_common_pb.PaginationResponse | undefined;
  setPagination(value?: v1_dictionary_common_dictionary_common_pb.PaginationResponse): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListMicroelementForProductResponse.AsObject;
  static toObject(includeInstance: boolean, msg: ListMicroelementForProductResponse): ListMicroelementForProductResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListMicroelementForProductResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListMicroelementForProductResponse;
  static deserializeBinaryFromReader(message: ListMicroelementForProductResponse, reader: jspb.BinaryReader): ListMicroelementForProductResponse;
}

export namespace ListMicroelementForProductResponse {
  export type AsObject = {
    itemsList: Array<MicroelementForProduct.AsObject>,
    pagination?: v1_dictionary_common_dictionary_common_pb.PaginationResponse.AsObject,
  }
}

