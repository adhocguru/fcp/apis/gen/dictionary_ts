// package: fcp.dictionary.v1.dictionary
// file: v1/dictionary/product.proto

import * as jspb from "google-protobuf";
import * as google_protobuf_wrappers_pb from "google-protobuf/google/protobuf/wrappers_pb";
import * as google_protobuf_timestamp_pb from "google-protobuf/google/protobuf/timestamp_pb";
import * as v1_dictionary_common_dictionary_common_pb from "../../v1/dictionary_common/dictionary_common_pb";
import * as v1_dictionary_common_enum_product_pb from "../../v1/dictionary_common/enum_product_pb";

export class Product extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  getName(): string;
  setName(value: string): void;

  hasProductClassId(): boolean;
  clearProductClassId(): void;
  getProductClassId(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setProductClassId(value?: google_protobuf_wrappers_pb.Int64Value): void;

  hasProductGroupId(): boolean;
  clearProductGroupId(): void;
  getProductGroupId(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setProductGroupId(value?: google_protobuf_wrappers_pb.Int64Value): void;

  getShortDescription(): string;
  setShortDescription(value: string): void;

  getDescription(): string;
  setDescription(value: string): void;

  getBenefits(): string;
  setBenefits(value: string): void;

  getToxic(): string;
  setToxic(value: string): void;

  hasPreparativeFormId(): boolean;
  clearPreparativeFormId(): void;
  getPreparativeFormId(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setPreparativeFormId(value?: google_protobuf_wrappers_pb.Int64Value): void;

  getChemicalGroup(): string;
  setChemicalGroup(value: string): void;

  getCompatibility(): string;
  setCompatibility(value: string): void;

  getActionMethod(): string;
  setActionMethod(value: string): void;

  getActionSpectrum(): string;
  setActionSpectrum(value: string): void;

  getConsumptionNorm(): string;
  setConsumptionNorm(value: string): void;

  getTreatmentsFrequencyRate(): string;
  setTreatmentsFrequencyRate(value: string): void;

  getRegistrationTerms(): string;
  setRegistrationTerms(value: string): void;

  getAdditionalInformationText(): string;
  setAdditionalInformationText(value: string): void;

  getAdditionalInformationLink(): string;
  setAdditionalInformationLink(value: string): void;

  getGuid(): string;
  setGuid(value: string): void;

  getUsageSpecification(): string;
  setUsageSpecification(value: string): void;

  getUsageRateInformation(): string;
  setUsageRateInformation(value: string): void;

  hasProductLabelId(): boolean;
  clearProductLabelId(): void;
  getProductLabelId(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setProductLabelId(value?: google_protobuf_wrappers_pb.Int64Value): void;

  hasBrandId(): boolean;
  clearBrandId(): void;
  getBrandId(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setBrandId(value?: google_protobuf_wrappers_pb.Int64Value): void;

  getSeoUrl(): string;
  setSeoUrl(value: string): void;

  getOnlyInTdn(): number;
  setOnlyInTdn(value: number): void;

  hasHash(): boolean;
  clearHash(): void;
  getHash(): google_protobuf_wrappers_pb.StringValue | undefined;
  setHash(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasDeleted(): boolean;
  clearDeleted(): void;
  getDeleted(): google_protobuf_wrappers_pb.BoolValue | undefined;
  setDeleted(value?: google_protobuf_wrappers_pb.BoolValue): void;

  hasCreatedBy(): boolean;
  clearCreatedBy(): void;
  getCreatedBy(): google_protobuf_wrappers_pb.StringValue | undefined;
  setCreatedBy(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasCreatedAt(): boolean;
  clearCreatedAt(): void;
  getCreatedAt(): google_protobuf_timestamp_pb.Timestamp | undefined;
  setCreatedAt(value?: google_protobuf_timestamp_pb.Timestamp): void;

  hasUpdatedBy(): boolean;
  clearUpdatedBy(): void;
  getUpdatedBy(): google_protobuf_wrappers_pb.StringValue | undefined;
  setUpdatedBy(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasUpdatedAt(): boolean;
  clearUpdatedAt(): void;
  getUpdatedAt(): google_protobuf_timestamp_pb.Timestamp | undefined;
  setUpdatedAt(value?: google_protobuf_timestamp_pb.Timestamp): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): Product.AsObject;
  static toObject(includeInstance: boolean, msg: Product): Product.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: Product, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): Product;
  static deserializeBinaryFromReader(message: Product, reader: jspb.BinaryReader): Product;
}

export namespace Product {
  export type AsObject = {
    id: number,
    name: string,
    productClassId?: google_protobuf_wrappers_pb.Int64Value.AsObject,
    productGroupId?: google_protobuf_wrappers_pb.Int64Value.AsObject,
    shortDescription: string,
    description: string,
    benefits: string,
    toxic: string,
    preparativeFormId?: google_protobuf_wrappers_pb.Int64Value.AsObject,
    chemicalGroup: string,
    compatibility: string,
    actionMethod: string,
    actionSpectrum: string,
    consumptionNorm: string,
    treatmentsFrequencyRate: string,
    registrationTerms: string,
    additionalInformationText: string,
    additionalInformationLink: string,
    guid: string,
    usageSpecification: string,
    usageRateInformation: string,
    productLabelId?: google_protobuf_wrappers_pb.Int64Value.AsObject,
    brandId?: google_protobuf_wrappers_pb.Int64Value.AsObject,
    seoUrl: string,
    onlyInTdn: number,
    hash?: google_protobuf_wrappers_pb.StringValue.AsObject,
    deleted?: google_protobuf_wrappers_pb.BoolValue.AsObject,
    createdBy?: google_protobuf_wrappers_pb.StringValue.AsObject,
    createdAt?: google_protobuf_timestamp_pb.Timestamp.AsObject,
    updatedBy?: google_protobuf_wrappers_pb.StringValue.AsObject,
    updatedAt?: google_protobuf_timestamp_pb.Timestamp.AsObject,
  }
}

export class ProductFilter extends jspb.Message {
  hasId(): boolean;
  clearId(): void;
  getId(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setId(value?: google_protobuf_wrappers_pb.Int64Value): void;

  hasName(): boolean;
  clearName(): void;
  getName(): google_protobuf_wrappers_pb.StringValue | undefined;
  setName(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasProductClassId(): boolean;
  clearProductClassId(): void;
  getProductClassId(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setProductClassId(value?: google_protobuf_wrappers_pb.Int64Value): void;

  hasProductGroupId(): boolean;
  clearProductGroupId(): void;
  getProductGroupId(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setProductGroupId(value?: google_protobuf_wrappers_pb.Int64Value): void;

  hasPreparativeFormId(): boolean;
  clearPreparativeFormId(): void;
  getPreparativeFormId(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setPreparativeFormId(value?: google_protobuf_wrappers_pb.Int64Value): void;

  hasProductLabelId(): boolean;
  clearProductLabelId(): void;
  getProductLabelId(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setProductLabelId(value?: google_protobuf_wrappers_pb.Int64Value): void;

  hasBrandId(): boolean;
  clearBrandId(): void;
  getBrandId(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setBrandId(value?: google_protobuf_wrappers_pb.Int64Value): void;

  hasDeleted(): boolean;
  clearDeleted(): void;
  getDeleted(): google_protobuf_wrappers_pb.BoolValue | undefined;
  setDeleted(value?: google_protobuf_wrappers_pb.BoolValue): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ProductFilter.AsObject;
  static toObject(includeInstance: boolean, msg: ProductFilter): ProductFilter.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ProductFilter, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ProductFilter;
  static deserializeBinaryFromReader(message: ProductFilter, reader: jspb.BinaryReader): ProductFilter;
}

export namespace ProductFilter {
  export type AsObject = {
    id?: google_protobuf_wrappers_pb.Int64Value.AsObject,
    name?: google_protobuf_wrappers_pb.StringValue.AsObject,
    productClassId?: google_protobuf_wrappers_pb.Int64Value.AsObject,
    productGroupId?: google_protobuf_wrappers_pb.Int64Value.AsObject,
    preparativeFormId?: google_protobuf_wrappers_pb.Int64Value.AsObject,
    productLabelId?: google_protobuf_wrappers_pb.Int64Value.AsObject,
    brandId?: google_protobuf_wrappers_pb.Int64Value.AsObject,
    deleted?: google_protobuf_wrappers_pb.BoolValue.AsObject,
  }
}

export class GetProductRequest extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetProductRequest.AsObject;
  static toObject(includeInstance: boolean, msg: GetProductRequest): GetProductRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetProductRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetProductRequest;
  static deserializeBinaryFromReader(message: GetProductRequest, reader: jspb.BinaryReader): GetProductRequest;
}

export namespace GetProductRequest {
  export type AsObject = {
    id: number,
  }
}

export class GetProductResponse extends jspb.Message {
  hasItem(): boolean;
  clearItem(): void;
  getItem(): Product | undefined;
  setItem(value?: Product): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetProductResponse.AsObject;
  static toObject(includeInstance: boolean, msg: GetProductResponse): GetProductResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetProductResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetProductResponse;
  static deserializeBinaryFromReader(message: GetProductResponse, reader: jspb.BinaryReader): GetProductResponse;
}

export namespace GetProductResponse {
  export type AsObject = {
    item?: Product.AsObject,
  }
}

export class ListProductRequest extends jspb.Message {
  hasFilter(): boolean;
  clearFilter(): void;
  getFilter(): ProductFilter | undefined;
  setFilter(value?: ProductFilter): void;

  clearSortingsList(): void;
  getSortingsList(): Array<v1_dictionary_common_dictionary_common_pb.Sorting>;
  setSortingsList(value: Array<v1_dictionary_common_dictionary_common_pb.Sorting>): void;
  addSortings(value?: v1_dictionary_common_dictionary_common_pb.Sorting, index?: number): v1_dictionary_common_dictionary_common_pb.Sorting;

  hasPagination(): boolean;
  clearPagination(): void;
  getPagination(): v1_dictionary_common_dictionary_common_pb.PaginationRequest | undefined;
  setPagination(value?: v1_dictionary_common_dictionary_common_pb.PaginationRequest): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListProductRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ListProductRequest): ListProductRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListProductRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListProductRequest;
  static deserializeBinaryFromReader(message: ListProductRequest, reader: jspb.BinaryReader): ListProductRequest;
}

export namespace ListProductRequest {
  export type AsObject = {
    filter?: ProductFilter.AsObject,
    sortingsList: Array<v1_dictionary_common_dictionary_common_pb.Sorting.AsObject>,
    pagination?: v1_dictionary_common_dictionary_common_pb.PaginationRequest.AsObject,
  }
}

export class ListProductOfPlantProtectionByCultureRequest extends jspb.Message {
  hasFilter(): boolean;
  clearFilter(): void;
  getFilter(): ProductFilter | undefined;
  setFilter(value?: ProductFilter): void;

  getCultureId(): number;
  setCultureId(value: number): void;

  clearSortingsList(): void;
  getSortingsList(): Array<v1_dictionary_common_dictionary_common_pb.Sorting>;
  setSortingsList(value: Array<v1_dictionary_common_dictionary_common_pb.Sorting>): void;
  addSortings(value?: v1_dictionary_common_dictionary_common_pb.Sorting, index?: number): v1_dictionary_common_dictionary_common_pb.Sorting;

  hasPagination(): boolean;
  clearPagination(): void;
  getPagination(): v1_dictionary_common_dictionary_common_pb.PaginationRequest | undefined;
  setPagination(value?: v1_dictionary_common_dictionary_common_pb.PaginationRequest): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListProductOfPlantProtectionByCultureRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ListProductOfPlantProtectionByCultureRequest): ListProductOfPlantProtectionByCultureRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListProductOfPlantProtectionByCultureRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListProductOfPlantProtectionByCultureRequest;
  static deserializeBinaryFromReader(message: ListProductOfPlantProtectionByCultureRequest, reader: jspb.BinaryReader): ListProductOfPlantProtectionByCultureRequest;
}

export namespace ListProductOfPlantProtectionByCultureRequest {
  export type AsObject = {
    filter?: ProductFilter.AsObject,
    cultureId: number,
    sortingsList: Array<v1_dictionary_common_dictionary_common_pb.Sorting.AsObject>,
    pagination?: v1_dictionary_common_dictionary_common_pb.PaginationRequest.AsObject,
  }
}

export class ListProductResponse extends jspb.Message {
  clearItemsList(): void;
  getItemsList(): Array<Product>;
  setItemsList(value: Array<Product>): void;
  addItems(value?: Product, index?: number): Product;

  hasPagination(): boolean;
  clearPagination(): void;
  getPagination(): v1_dictionary_common_dictionary_common_pb.PaginationResponse | undefined;
  setPagination(value?: v1_dictionary_common_dictionary_common_pb.PaginationResponse): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListProductResponse.AsObject;
  static toObject(includeInstance: boolean, msg: ListProductResponse): ListProductResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListProductResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListProductResponse;
  static deserializeBinaryFromReader(message: ListProductResponse, reader: jspb.BinaryReader): ListProductResponse;
}

export namespace ListProductResponse {
  export type AsObject = {
    itemsList: Array<Product.AsObject>,
    pagination?: v1_dictionary_common_dictionary_common_pb.PaginationResponse.AsObject,
  }
}

export class ListProductAlternativeRequest extends jspb.Message {
  hasFilter(): boolean;
  clearFilter(): void;
  getFilter(): ListProductAlternativeRequest.ProductAlternativeFilter | undefined;
  setFilter(value?: ListProductAlternativeRequest.ProductAlternativeFilter): void;

  clearSortingList(): void;
  getSortingList(): Array<v1_dictionary_common_dictionary_common_pb.Sorting>;
  setSortingList(value: Array<v1_dictionary_common_dictionary_common_pb.Sorting>): void;
  addSorting(value?: v1_dictionary_common_dictionary_common_pb.Sorting, index?: number): v1_dictionary_common_dictionary_common_pb.Sorting;

  hasPagination(): boolean;
  clearPagination(): void;
  getPagination(): v1_dictionary_common_dictionary_common_pb.PaginationRequest | undefined;
  setPagination(value?: v1_dictionary_common_dictionary_common_pb.PaginationRequest): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListProductAlternativeRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ListProductAlternativeRequest): ListProductAlternativeRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListProductAlternativeRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListProductAlternativeRequest;
  static deserializeBinaryFromReader(message: ListProductAlternativeRequest, reader: jspb.BinaryReader): ListProductAlternativeRequest;
}

export namespace ListProductAlternativeRequest {
  export type AsObject = {
    filter?: ListProductAlternativeRequest.ProductAlternativeFilter.AsObject,
    sortingList: Array<v1_dictionary_common_dictionary_common_pb.Sorting.AsObject>,
    pagination?: v1_dictionary_common_dictionary_common_pb.PaginationRequest.AsObject,
  }

  export class ProductAlternativeFilter extends jspb.Message {
    getProcess(): v1_dictionary_common_enum_product_pb.ProductProcessMap[keyof v1_dictionary_common_enum_product_pb.ProductProcessMap];
    setProcess(value: v1_dictionary_common_enum_product_pb.ProductProcessMap[keyof v1_dictionary_common_enum_product_pb.ProductProcessMap]): void;

    getType(): v1_dictionary_common_enum_product_pb.ProductTypeMap[keyof v1_dictionary_common_enum_product_pb.ProductTypeMap];
    setType(value: v1_dictionary_common_enum_product_pb.ProductTypeMap[keyof v1_dictionary_common_enum_product_pb.ProductTypeMap]): void;

    getCategoryId(): number;
    setCategoryId(value: number): void;

    hasName(): boolean;
    clearName(): void;
    getName(): google_protobuf_wrappers_pb.StringValue | undefined;
    setName(value?: google_protobuf_wrappers_pb.StringValue): void;

    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): ProductAlternativeFilter.AsObject;
    static toObject(includeInstance: boolean, msg: ProductAlternativeFilter): ProductAlternativeFilter.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: ProductAlternativeFilter, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): ProductAlternativeFilter;
    static deserializeBinaryFromReader(message: ProductAlternativeFilter, reader: jspb.BinaryReader): ProductAlternativeFilter;
  }

  export namespace ProductAlternativeFilter {
    export type AsObject = {
      process: v1_dictionary_common_enum_product_pb.ProductProcessMap[keyof v1_dictionary_common_enum_product_pb.ProductProcessMap],
      type: v1_dictionary_common_enum_product_pb.ProductTypeMap[keyof v1_dictionary_common_enum_product_pb.ProductTypeMap],
      categoryId: number,
      name?: google_protobuf_wrappers_pb.StringValue.AsObject,
    }
  }
}

export class ListProductAlternativeResponse extends jspb.Message {
  clearItemsList(): void;
  getItemsList(): Array<ListProductAlternativeResponse.Item>;
  setItemsList(value: Array<ListProductAlternativeResponse.Item>): void;
  addItems(value?: ListProductAlternativeResponse.Item, index?: number): ListProductAlternativeResponse.Item;

  hasPagination(): boolean;
  clearPagination(): void;
  getPagination(): v1_dictionary_common_dictionary_common_pb.PaginationResponse | undefined;
  setPagination(value?: v1_dictionary_common_dictionary_common_pb.PaginationResponse): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListProductAlternativeResponse.AsObject;
  static toObject(includeInstance: boolean, msg: ListProductAlternativeResponse): ListProductAlternativeResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListProductAlternativeResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListProductAlternativeResponse;
  static deserializeBinaryFromReader(message: ListProductAlternativeResponse, reader: jspb.BinaryReader): ListProductAlternativeResponse;
}

export namespace ListProductAlternativeResponse {
  export type AsObject = {
    itemsList: Array<ListProductAlternativeResponse.Item.AsObject>,
    pagination?: v1_dictionary_common_dictionary_common_pb.PaginationResponse.AsObject,
  }

  export class Item extends jspb.Message {
    getId(): number;
    setId(value: number): void;

    getName(): string;
    setName(value: string): void;

    getBrandId(): number;
    setBrandId(value: number): void;

    getBrandName(): string;
    setBrandName(value: string): void;

    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): Item.AsObject;
    static toObject(includeInstance: boolean, msg: Item): Item.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: Item, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): Item;
    static deserializeBinaryFromReader(message: Item, reader: jspb.BinaryReader): Item;
  }

  export namespace Item {
    export type AsObject = {
      id: number,
      name: string,
      brandId: number,
      brandName: string,
    }
  }
}

