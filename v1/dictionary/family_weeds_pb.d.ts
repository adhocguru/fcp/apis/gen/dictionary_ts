// package: fcp.dictionary.v1.dictionary
// file: v1/dictionary/family_weeds.proto

import * as jspb from "google-protobuf";
import * as google_protobuf_wrappers_pb from "google-protobuf/google/protobuf/wrappers_pb";
import * as google_protobuf_timestamp_pb from "google-protobuf/google/protobuf/timestamp_pb";
import * as v1_dictionary_common_dictionary_common_pb from "../../v1/dictionary_common/dictionary_common_pb";

export class FamilyWeeds extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  getName(): string;
  setName(value: string): void;

  hasHash(): boolean;
  clearHash(): void;
  getHash(): google_protobuf_wrappers_pb.StringValue | undefined;
  setHash(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasDeleted(): boolean;
  clearDeleted(): void;
  getDeleted(): google_protobuf_wrappers_pb.BoolValue | undefined;
  setDeleted(value?: google_protobuf_wrappers_pb.BoolValue): void;

  hasCreatedBy(): boolean;
  clearCreatedBy(): void;
  getCreatedBy(): google_protobuf_wrappers_pb.StringValue | undefined;
  setCreatedBy(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasCreatedAt(): boolean;
  clearCreatedAt(): void;
  getCreatedAt(): google_protobuf_timestamp_pb.Timestamp | undefined;
  setCreatedAt(value?: google_protobuf_timestamp_pb.Timestamp): void;

  hasUpdatedBy(): boolean;
  clearUpdatedBy(): void;
  getUpdatedBy(): google_protobuf_wrappers_pb.StringValue | undefined;
  setUpdatedBy(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasUpdatedAt(): boolean;
  clearUpdatedAt(): void;
  getUpdatedAt(): google_protobuf_timestamp_pb.Timestamp | undefined;
  setUpdatedAt(value?: google_protobuf_timestamp_pb.Timestamp): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): FamilyWeeds.AsObject;
  static toObject(includeInstance: boolean, msg: FamilyWeeds): FamilyWeeds.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: FamilyWeeds, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): FamilyWeeds;
  static deserializeBinaryFromReader(message: FamilyWeeds, reader: jspb.BinaryReader): FamilyWeeds;
}

export namespace FamilyWeeds {
  export type AsObject = {
    id: number,
    name: string,
    hash?: google_protobuf_wrappers_pb.StringValue.AsObject,
    deleted?: google_protobuf_wrappers_pb.BoolValue.AsObject,
    createdBy?: google_protobuf_wrappers_pb.StringValue.AsObject,
    createdAt?: google_protobuf_timestamp_pb.Timestamp.AsObject,
    updatedBy?: google_protobuf_wrappers_pb.StringValue.AsObject,
    updatedAt?: google_protobuf_timestamp_pb.Timestamp.AsObject,
  }
}

export class FamilyWeedsFilter extends jspb.Message {
  hasId(): boolean;
  clearId(): void;
  getId(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setId(value?: google_protobuf_wrappers_pb.Int64Value): void;

  hasName(): boolean;
  clearName(): void;
  getName(): google_protobuf_wrappers_pb.StringValue | undefined;
  setName(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasDeleted(): boolean;
  clearDeleted(): void;
  getDeleted(): google_protobuf_wrappers_pb.BoolValue | undefined;
  setDeleted(value?: google_protobuf_wrappers_pb.BoolValue): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): FamilyWeedsFilter.AsObject;
  static toObject(includeInstance: boolean, msg: FamilyWeedsFilter): FamilyWeedsFilter.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: FamilyWeedsFilter, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): FamilyWeedsFilter;
  static deserializeBinaryFromReader(message: FamilyWeedsFilter, reader: jspb.BinaryReader): FamilyWeedsFilter;
}

export namespace FamilyWeedsFilter {
  export type AsObject = {
    id?: google_protobuf_wrappers_pb.Int64Value.AsObject,
    name?: google_protobuf_wrappers_pb.StringValue.AsObject,
    deleted?: google_protobuf_wrappers_pb.BoolValue.AsObject,
  }
}

export class ListFamilyWeedsRequest extends jspb.Message {
  hasFilter(): boolean;
  clearFilter(): void;
  getFilter(): FamilyWeedsFilter | undefined;
  setFilter(value?: FamilyWeedsFilter): void;

  clearSortingsList(): void;
  getSortingsList(): Array<v1_dictionary_common_dictionary_common_pb.Sorting>;
  setSortingsList(value: Array<v1_dictionary_common_dictionary_common_pb.Sorting>): void;
  addSortings(value?: v1_dictionary_common_dictionary_common_pb.Sorting, index?: number): v1_dictionary_common_dictionary_common_pb.Sorting;

  hasPagination(): boolean;
  clearPagination(): void;
  getPagination(): v1_dictionary_common_dictionary_common_pb.PaginationRequest | undefined;
  setPagination(value?: v1_dictionary_common_dictionary_common_pb.PaginationRequest): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListFamilyWeedsRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ListFamilyWeedsRequest): ListFamilyWeedsRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListFamilyWeedsRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListFamilyWeedsRequest;
  static deserializeBinaryFromReader(message: ListFamilyWeedsRequest, reader: jspb.BinaryReader): ListFamilyWeedsRequest;
}

export namespace ListFamilyWeedsRequest {
  export type AsObject = {
    filter?: FamilyWeedsFilter.AsObject,
    sortingsList: Array<v1_dictionary_common_dictionary_common_pb.Sorting.AsObject>,
    pagination?: v1_dictionary_common_dictionary_common_pb.PaginationRequest.AsObject,
  }
}

export class ListFamilyWeedsResponse extends jspb.Message {
  clearItemsList(): void;
  getItemsList(): Array<FamilyWeeds>;
  setItemsList(value: Array<FamilyWeeds>): void;
  addItems(value?: FamilyWeeds, index?: number): FamilyWeeds;

  hasPagination(): boolean;
  clearPagination(): void;
  getPagination(): v1_dictionary_common_dictionary_common_pb.PaginationResponse | undefined;
  setPagination(value?: v1_dictionary_common_dictionary_common_pb.PaginationResponse): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListFamilyWeedsResponse.AsObject;
  static toObject(includeInstance: boolean, msg: ListFamilyWeedsResponse): ListFamilyWeedsResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListFamilyWeedsResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListFamilyWeedsResponse;
  static deserializeBinaryFromReader(message: ListFamilyWeedsResponse, reader: jspb.BinaryReader): ListFamilyWeedsResponse;
}

export namespace ListFamilyWeedsResponse {
  export type AsObject = {
    itemsList: Array<FamilyWeeds.AsObject>,
    pagination?: v1_dictionary_common_dictionary_common_pb.PaginationResponse.AsObject,
  }
}

export class GetFamilyWeedsRequest extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetFamilyWeedsRequest.AsObject;
  static toObject(includeInstance: boolean, msg: GetFamilyWeedsRequest): GetFamilyWeedsRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetFamilyWeedsRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetFamilyWeedsRequest;
  static deserializeBinaryFromReader(message: GetFamilyWeedsRequest, reader: jspb.BinaryReader): GetFamilyWeedsRequest;
}

export namespace GetFamilyWeedsRequest {
  export type AsObject = {
    id: number,
  }
}

export class GetFamilyWeedsResponse extends jspb.Message {
  hasItem(): boolean;
  clearItem(): void;
  getItem(): FamilyWeeds | undefined;
  setItem(value?: FamilyWeeds): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetFamilyWeedsResponse.AsObject;
  static toObject(includeInstance: boolean, msg: GetFamilyWeedsResponse): GetFamilyWeedsResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetFamilyWeedsResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetFamilyWeedsResponse;
  static deserializeBinaryFromReader(message: GetFamilyWeedsResponse, reader: jspb.BinaryReader): GetFamilyWeedsResponse;
}

export namespace GetFamilyWeedsResponse {
  export type AsObject = {
    item?: FamilyWeeds.AsObject,
  }
}

