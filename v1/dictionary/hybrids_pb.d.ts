// package: fcp.dictionary.v1.dictionary
// file: v1/dictionary/hybrids.proto

import * as jspb from "google-protobuf";
import * as google_protobuf_wrappers_pb from "google-protobuf/google/protobuf/wrappers_pb";
import * as google_protobuf_timestamp_pb from "google-protobuf/google/protobuf/timestamp_pb";
import * as v1_dictionary_common_dictionary_common_pb from "../../v1/dictionary_common/dictionary_common_pb";

export class Hybrids extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  getCultureId(): number;
  setCultureId(value: number): void;

  getCultureName(): string;
  setCultureName(value: string): void;

  getBrandId(): number;
  setBrandId(value: number): void;

  getBrandName(): string;
  setBrandName(value: string): void;

  getName(): string;
  setName(value: string): void;

  getNameEng(): string;
  setNameEng(value: string): void;

  getHybridsGroupId(): number;
  setHybridsGroupId(value: number): void;

  getHybridsGroupName(): string;
  setHybridsGroupName(value: string): void;

  getCharactersTxt(): string;
  setCharactersTxt(value: string): void;

  getUniqueTxt(): string;
  setUniqueTxt(value: string): void;

  getShitField(): string;
  setShitField(value: string): void;

  getRecomendedTxt(): string;
  setRecomendedTxt(value: string): void;

  getHybridsDescript(): string;
  setHybridsDescript(value: string): void;

  getAdvantages(): string;
  setAdvantages(value: string): void;

  getOnlyInTdn(): number;
  setOnlyInTdn(value: number): void;

  hasHash(): boolean;
  clearHash(): void;
  getHash(): google_protobuf_wrappers_pb.StringValue | undefined;
  setHash(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasDeleted(): boolean;
  clearDeleted(): void;
  getDeleted(): google_protobuf_wrappers_pb.BoolValue | undefined;
  setDeleted(value?: google_protobuf_wrappers_pb.BoolValue): void;

  hasCreatedBy(): boolean;
  clearCreatedBy(): void;
  getCreatedBy(): google_protobuf_wrappers_pb.StringValue | undefined;
  setCreatedBy(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasCreatedAt(): boolean;
  clearCreatedAt(): void;
  getCreatedAt(): google_protobuf_timestamp_pb.Timestamp | undefined;
  setCreatedAt(value?: google_protobuf_timestamp_pb.Timestamp): void;

  hasUpdatedBy(): boolean;
  clearUpdatedBy(): void;
  getUpdatedBy(): google_protobuf_wrappers_pb.StringValue | undefined;
  setUpdatedBy(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasUpdatedAt(): boolean;
  clearUpdatedAt(): void;
  getUpdatedAt(): google_protobuf_timestamp_pb.Timestamp | undefined;
  setUpdatedAt(value?: google_protobuf_timestamp_pb.Timestamp): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): Hybrids.AsObject;
  static toObject(includeInstance: boolean, msg: Hybrids): Hybrids.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: Hybrids, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): Hybrids;
  static deserializeBinaryFromReader(message: Hybrids, reader: jspb.BinaryReader): Hybrids;
}

export namespace Hybrids {
  export type AsObject = {
    id: number,
    cultureId: number,
    cultureName: string,
    brandId: number,
    brandName: string,
    name: string,
    nameEng: string,
    hybridsGroupId: number,
    hybridsGroupName: string,
    charactersTxt: string,
    uniqueTxt: string,
    shitField: string,
    recomendedTxt: string,
    hybridsDescript: string,
    advantages: string,
    onlyInTdn: number,
    hash?: google_protobuf_wrappers_pb.StringValue.AsObject,
    deleted?: google_protobuf_wrappers_pb.BoolValue.AsObject,
    createdBy?: google_protobuf_wrappers_pb.StringValue.AsObject,
    createdAt?: google_protobuf_timestamp_pb.Timestamp.AsObject,
    updatedBy?: google_protobuf_wrappers_pb.StringValue.AsObject,
    updatedAt?: google_protobuf_timestamp_pb.Timestamp.AsObject,
  }
}

export class HybridsFilter extends jspb.Message {
  hasId(): boolean;
  clearId(): void;
  getId(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setId(value?: google_protobuf_wrappers_pb.Int64Value): void;

  hasCultureId(): boolean;
  clearCultureId(): void;
  getCultureId(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setCultureId(value?: google_protobuf_wrappers_pb.Int64Value): void;

  hasBrandId(): boolean;
  clearBrandId(): void;
  getBrandId(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setBrandId(value?: google_protobuf_wrappers_pb.Int64Value): void;

  hasName(): boolean;
  clearName(): void;
  getName(): google_protobuf_wrappers_pb.StringValue | undefined;
  setName(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasNameEng(): boolean;
  clearNameEng(): void;
  getNameEng(): google_protobuf_wrappers_pb.StringValue | undefined;
  setNameEng(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasHybridsGroupId(): boolean;
  clearHybridsGroupId(): void;
  getHybridsGroupId(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setHybridsGroupId(value?: google_protobuf_wrappers_pb.Int64Value): void;

  hasDeleted(): boolean;
  clearDeleted(): void;
  getDeleted(): google_protobuf_wrappers_pb.BoolValue | undefined;
  setDeleted(value?: google_protobuf_wrappers_pb.BoolValue): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): HybridsFilter.AsObject;
  static toObject(includeInstance: boolean, msg: HybridsFilter): HybridsFilter.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: HybridsFilter, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): HybridsFilter;
  static deserializeBinaryFromReader(message: HybridsFilter, reader: jspb.BinaryReader): HybridsFilter;
}

export namespace HybridsFilter {
  export type AsObject = {
    id?: google_protobuf_wrappers_pb.Int64Value.AsObject,
    cultureId?: google_protobuf_wrappers_pb.Int64Value.AsObject,
    brandId?: google_protobuf_wrappers_pb.Int64Value.AsObject,
    name?: google_protobuf_wrappers_pb.StringValue.AsObject,
    nameEng?: google_protobuf_wrappers_pb.StringValue.AsObject,
    hybridsGroupId?: google_protobuf_wrappers_pb.Int64Value.AsObject,
    deleted?: google_protobuf_wrappers_pb.BoolValue.AsObject,
  }
}

export class ListHybridsRequest extends jspb.Message {
  hasFilter(): boolean;
  clearFilter(): void;
  getFilter(): HybridsFilter | undefined;
  setFilter(value?: HybridsFilter): void;

  clearSortingsList(): void;
  getSortingsList(): Array<v1_dictionary_common_dictionary_common_pb.Sorting>;
  setSortingsList(value: Array<v1_dictionary_common_dictionary_common_pb.Sorting>): void;
  addSortings(value?: v1_dictionary_common_dictionary_common_pb.Sorting, index?: number): v1_dictionary_common_dictionary_common_pb.Sorting;

  hasPagination(): boolean;
  clearPagination(): void;
  getPagination(): v1_dictionary_common_dictionary_common_pb.PaginationRequest | undefined;
  setPagination(value?: v1_dictionary_common_dictionary_common_pb.PaginationRequest): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListHybridsRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ListHybridsRequest): ListHybridsRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListHybridsRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListHybridsRequest;
  static deserializeBinaryFromReader(message: ListHybridsRequest, reader: jspb.BinaryReader): ListHybridsRequest;
}

export namespace ListHybridsRequest {
  export type AsObject = {
    filter?: HybridsFilter.AsObject,
    sortingsList: Array<v1_dictionary_common_dictionary_common_pb.Sorting.AsObject>,
    pagination?: v1_dictionary_common_dictionary_common_pb.PaginationRequest.AsObject,
  }
}

export class ListHybridsResponse extends jspb.Message {
  clearItemsList(): void;
  getItemsList(): Array<Hybrids>;
  setItemsList(value: Array<Hybrids>): void;
  addItems(value?: Hybrids, index?: number): Hybrids;

  hasPagination(): boolean;
  clearPagination(): void;
  getPagination(): v1_dictionary_common_dictionary_common_pb.PaginationResponse | undefined;
  setPagination(value?: v1_dictionary_common_dictionary_common_pb.PaginationResponse): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListHybridsResponse.AsObject;
  static toObject(includeInstance: boolean, msg: ListHybridsResponse): ListHybridsResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListHybridsResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListHybridsResponse;
  static deserializeBinaryFromReader(message: ListHybridsResponse, reader: jspb.BinaryReader): ListHybridsResponse;
}

export namespace ListHybridsResponse {
  export type AsObject = {
    itemsList: Array<Hybrids.AsObject>,
    pagination?: v1_dictionary_common_dictionary_common_pb.PaginationResponse.AsObject,
  }
}

export class GetHybridsRequest extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetHybridsRequest.AsObject;
  static toObject(includeInstance: boolean, msg: GetHybridsRequest): GetHybridsRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetHybridsRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetHybridsRequest;
  static deserializeBinaryFromReader(message: GetHybridsRequest, reader: jspb.BinaryReader): GetHybridsRequest;
}

export namespace GetHybridsRequest {
  export type AsObject = {
    id: number,
  }
}

export class GetHybridsResponse extends jspb.Message {
  hasItem(): boolean;
  clearItem(): void;
  getItem(): Hybrids | undefined;
  setItem(value?: Hybrids): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetHybridsResponse.AsObject;
  static toObject(includeInstance: boolean, msg: GetHybridsResponse): GetHybridsResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetHybridsResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetHybridsResponse;
  static deserializeBinaryFromReader(message: GetHybridsResponse, reader: jspb.BinaryReader): GetHybridsResponse;
}

export namespace GetHybridsResponse {
  export type AsObject = {
    item?: Hybrids.AsObject,
  }
}

