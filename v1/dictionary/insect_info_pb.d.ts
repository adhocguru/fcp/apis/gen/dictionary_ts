// package: fcp.dictionary.v1.dictionary
// file: v1/dictionary/insect_info.proto

import * as jspb from "google-protobuf";
import * as google_protobuf_wrappers_pb from "google-protobuf/google/protobuf/wrappers_pb";
import * as google_protobuf_timestamp_pb from "google-protobuf/google/protobuf/timestamp_pb";
import * as v1_dictionary_common_dictionary_common_pb from "../../v1/dictionary_common/dictionary_common_pb";

export class InsectInfo extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  getName(): string;
  setName(value: string): void;

  getVerminId(): number;
  setVerminId(value: number): void;

  getVerminName(): string;
  setVerminName(value: string): void;

  getInsectRowId(): number;
  setInsectRowId(value: number): void;

  getInsectRowName(): string;
  setInsectRowName(value: string): void;

  getInsectFamilyId(): number;
  setInsectFamilyId(value: number): void;

  getInsectFamilyName(): string;
  setInsectFamilyName(value: string): void;

  hasHash(): boolean;
  clearHash(): void;
  getHash(): google_protobuf_wrappers_pb.StringValue | undefined;
  setHash(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasDeleted(): boolean;
  clearDeleted(): void;
  getDeleted(): google_protobuf_wrappers_pb.BoolValue | undefined;
  setDeleted(value?: google_protobuf_wrappers_pb.BoolValue): void;

  hasCreatedBy(): boolean;
  clearCreatedBy(): void;
  getCreatedBy(): google_protobuf_wrappers_pb.StringValue | undefined;
  setCreatedBy(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasCreatedAt(): boolean;
  clearCreatedAt(): void;
  getCreatedAt(): google_protobuf_timestamp_pb.Timestamp | undefined;
  setCreatedAt(value?: google_protobuf_timestamp_pb.Timestamp): void;

  hasUpdatedBy(): boolean;
  clearUpdatedBy(): void;
  getUpdatedBy(): google_protobuf_wrappers_pb.StringValue | undefined;
  setUpdatedBy(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasUpdatedAt(): boolean;
  clearUpdatedAt(): void;
  getUpdatedAt(): google_protobuf_timestamp_pb.Timestamp | undefined;
  setUpdatedAt(value?: google_protobuf_timestamp_pb.Timestamp): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): InsectInfo.AsObject;
  static toObject(includeInstance: boolean, msg: InsectInfo): InsectInfo.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: InsectInfo, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): InsectInfo;
  static deserializeBinaryFromReader(message: InsectInfo, reader: jspb.BinaryReader): InsectInfo;
}

export namespace InsectInfo {
  export type AsObject = {
    id: number,
    name: string,
    verminId: number,
    verminName: string,
    insectRowId: number,
    insectRowName: string,
    insectFamilyId: number,
    insectFamilyName: string,
    hash?: google_protobuf_wrappers_pb.StringValue.AsObject,
    deleted?: google_protobuf_wrappers_pb.BoolValue.AsObject,
    createdBy?: google_protobuf_wrappers_pb.StringValue.AsObject,
    createdAt?: google_protobuf_timestamp_pb.Timestamp.AsObject,
    updatedBy?: google_protobuf_wrappers_pb.StringValue.AsObject,
    updatedAt?: google_protobuf_timestamp_pb.Timestamp.AsObject,
  }
}

export class InsectInfoFilter extends jspb.Message {
  hasId(): boolean;
  clearId(): void;
  getId(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setId(value?: google_protobuf_wrappers_pb.Int64Value): void;

  hasName(): boolean;
  clearName(): void;
  getName(): google_protobuf_wrappers_pb.StringValue | undefined;
  setName(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasVerminId(): boolean;
  clearVerminId(): void;
  getVerminId(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setVerminId(value?: google_protobuf_wrappers_pb.Int64Value): void;

  hasInsectRowId(): boolean;
  clearInsectRowId(): void;
  getInsectRowId(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setInsectRowId(value?: google_protobuf_wrappers_pb.Int64Value): void;

  hasInsectFamilyId(): boolean;
  clearInsectFamilyId(): void;
  getInsectFamilyId(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setInsectFamilyId(value?: google_protobuf_wrappers_pb.Int64Value): void;

  hasDeleted(): boolean;
  clearDeleted(): void;
  getDeleted(): google_protobuf_wrappers_pb.BoolValue | undefined;
  setDeleted(value?: google_protobuf_wrappers_pb.BoolValue): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): InsectInfoFilter.AsObject;
  static toObject(includeInstance: boolean, msg: InsectInfoFilter): InsectInfoFilter.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: InsectInfoFilter, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): InsectInfoFilter;
  static deserializeBinaryFromReader(message: InsectInfoFilter, reader: jspb.BinaryReader): InsectInfoFilter;
}

export namespace InsectInfoFilter {
  export type AsObject = {
    id?: google_protobuf_wrappers_pb.Int64Value.AsObject,
    name?: google_protobuf_wrappers_pb.StringValue.AsObject,
    verminId?: google_protobuf_wrappers_pb.Int64Value.AsObject,
    insectRowId?: google_protobuf_wrappers_pb.Int64Value.AsObject,
    insectFamilyId?: google_protobuf_wrappers_pb.Int64Value.AsObject,
    deleted?: google_protobuf_wrappers_pb.BoolValue.AsObject,
  }
}

export class ListInsectInfoRequest extends jspb.Message {
  hasFilter(): boolean;
  clearFilter(): void;
  getFilter(): InsectInfoFilter | undefined;
  setFilter(value?: InsectInfoFilter): void;

  clearSortingsList(): void;
  getSortingsList(): Array<v1_dictionary_common_dictionary_common_pb.Sorting>;
  setSortingsList(value: Array<v1_dictionary_common_dictionary_common_pb.Sorting>): void;
  addSortings(value?: v1_dictionary_common_dictionary_common_pb.Sorting, index?: number): v1_dictionary_common_dictionary_common_pb.Sorting;

  hasPagination(): boolean;
  clearPagination(): void;
  getPagination(): v1_dictionary_common_dictionary_common_pb.PaginationRequest | undefined;
  setPagination(value?: v1_dictionary_common_dictionary_common_pb.PaginationRequest): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListInsectInfoRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ListInsectInfoRequest): ListInsectInfoRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListInsectInfoRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListInsectInfoRequest;
  static deserializeBinaryFromReader(message: ListInsectInfoRequest, reader: jspb.BinaryReader): ListInsectInfoRequest;
}

export namespace ListInsectInfoRequest {
  export type AsObject = {
    filter?: InsectInfoFilter.AsObject,
    sortingsList: Array<v1_dictionary_common_dictionary_common_pb.Sorting.AsObject>,
    pagination?: v1_dictionary_common_dictionary_common_pb.PaginationRequest.AsObject,
  }
}

export class ListInsectInfoResponse extends jspb.Message {
  clearItemsList(): void;
  getItemsList(): Array<InsectInfo>;
  setItemsList(value: Array<InsectInfo>): void;
  addItems(value?: InsectInfo, index?: number): InsectInfo;

  hasPagination(): boolean;
  clearPagination(): void;
  getPagination(): v1_dictionary_common_dictionary_common_pb.PaginationResponse | undefined;
  setPagination(value?: v1_dictionary_common_dictionary_common_pb.PaginationResponse): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListInsectInfoResponse.AsObject;
  static toObject(includeInstance: boolean, msg: ListInsectInfoResponse): ListInsectInfoResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListInsectInfoResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListInsectInfoResponse;
  static deserializeBinaryFromReader(message: ListInsectInfoResponse, reader: jspb.BinaryReader): ListInsectInfoResponse;
}

export namespace ListInsectInfoResponse {
  export type AsObject = {
    itemsList: Array<InsectInfo.AsObject>,
    pagination?: v1_dictionary_common_dictionary_common_pb.PaginationResponse.AsObject,
  }
}

export class GetInsectInfoRequest extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetInsectInfoRequest.AsObject;
  static toObject(includeInstance: boolean, msg: GetInsectInfoRequest): GetInsectInfoRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetInsectInfoRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetInsectInfoRequest;
  static deserializeBinaryFromReader(message: GetInsectInfoRequest, reader: jspb.BinaryReader): GetInsectInfoRequest;
}

export namespace GetInsectInfoRequest {
  export type AsObject = {
    id: number,
  }
}

export class GetInsectInfoResponse extends jspb.Message {
  hasItem(): boolean;
  clearItem(): void;
  getItem(): InsectInfo | undefined;
  setItem(value?: InsectInfo): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetInsectInfoResponse.AsObject;
  static toObject(includeInstance: boolean, msg: GetInsectInfoResponse): GetInsectInfoResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetInsectInfoResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetInsectInfoResponse;
  static deserializeBinaryFromReader(message: GetInsectInfoResponse, reader: jspb.BinaryReader): GetInsectInfoResponse;
}

export namespace GetInsectInfoResponse {
  export type AsObject = {
    item?: InsectInfo.AsObject,
  }
}

