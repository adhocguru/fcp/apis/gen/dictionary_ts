// package: fcp.dictionary.v1.dictionary
// file: v1/dictionary/culture.proto

import * as jspb from "google-protobuf";
import * as google_protobuf_wrappers_pb from "google-protobuf/google/protobuf/wrappers_pb";
import * as google_protobuf_timestamp_pb from "google-protobuf/google/protobuf/timestamp_pb";
import * as v1_dictionary_common_dictionary_common_pb from "../../v1/dictionary_common/dictionary_common_pb";
import * as v1_dictionary_common_enum_product_pb from "../../v1/dictionary_common/enum_product_pb";

export class Culture extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  getGuid(): string;
  setGuid(value: string): void;

  getName(): string;
  setName(value: string): void;

  getPicture(): string;
  setPicture(value: string): void;

  getDescription(): string;
  setDescription(value: string): void;

  hasCulturesGroupId(): boolean;
  clearCulturesGroupId(): void;
  getCulturesGroupId(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setCulturesGroupId(value?: google_protobuf_wrappers_pb.Int64Value): void;

  getEmptySchemaImage(): string;
  setEmptySchemaImage(value: string): void;

  getType(): number;
  setType(value: number): void;

  getOrd(): number;
  setOrd(value: number): void;

  hasHash(): boolean;
  clearHash(): void;
  getHash(): google_protobuf_wrappers_pb.StringValue | undefined;
  setHash(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasDeleted(): boolean;
  clearDeleted(): void;
  getDeleted(): google_protobuf_wrappers_pb.BoolValue | undefined;
  setDeleted(value?: google_protobuf_wrappers_pb.BoolValue): void;

  hasCreatedBy(): boolean;
  clearCreatedBy(): void;
  getCreatedBy(): google_protobuf_wrappers_pb.StringValue | undefined;
  setCreatedBy(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasCreatedAt(): boolean;
  clearCreatedAt(): void;
  getCreatedAt(): google_protobuf_timestamp_pb.Timestamp | undefined;
  setCreatedAt(value?: google_protobuf_timestamp_pb.Timestamp): void;

  hasUpdatedBy(): boolean;
  clearUpdatedBy(): void;
  getUpdatedBy(): google_protobuf_wrappers_pb.StringValue | undefined;
  setUpdatedBy(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasUpdatedAt(): boolean;
  clearUpdatedAt(): void;
  getUpdatedAt(): google_protobuf_timestamp_pb.Timestamp | undefined;
  setUpdatedAt(value?: google_protobuf_timestamp_pb.Timestamp): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): Culture.AsObject;
  static toObject(includeInstance: boolean, msg: Culture): Culture.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: Culture, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): Culture;
  static deserializeBinaryFromReader(message: Culture, reader: jspb.BinaryReader): Culture;
}

export namespace Culture {
  export type AsObject = {
    id: number,
    guid: string,
    name: string,
    picture: string,
    description: string,
    culturesGroupId?: google_protobuf_wrappers_pb.Int64Value.AsObject,
    emptySchemaImage: string,
    type: number,
    ord: number,
    hash?: google_protobuf_wrappers_pb.StringValue.AsObject,
    deleted?: google_protobuf_wrappers_pb.BoolValue.AsObject,
    createdBy?: google_protobuf_wrappers_pb.StringValue.AsObject,
    createdAt?: google_protobuf_timestamp_pb.Timestamp.AsObject,
    updatedBy?: google_protobuf_wrappers_pb.StringValue.AsObject,
    updatedAt?: google_protobuf_timestamp_pb.Timestamp.AsObject,
  }
}

export class CultureFilter extends jspb.Message {
  hasId(): boolean;
  clearId(): void;
  getId(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setId(value?: google_protobuf_wrappers_pb.Int64Value): void;

  hasGuid(): boolean;
  clearGuid(): void;
  getGuid(): google_protobuf_wrappers_pb.StringValue | undefined;
  setGuid(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasName(): boolean;
  clearName(): void;
  getName(): google_protobuf_wrappers_pb.StringValue | undefined;
  setName(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasCulturesGroupId(): boolean;
  clearCulturesGroupId(): void;
  getCulturesGroupId(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setCulturesGroupId(value?: google_protobuf_wrappers_pb.Int64Value): void;

  hasType(): boolean;
  clearType(): void;
  getType(): google_protobuf_wrappers_pb.Int32Value | undefined;
  setType(value?: google_protobuf_wrappers_pb.Int32Value): void;

  hasOrd(): boolean;
  clearOrd(): void;
  getOrd(): google_protobuf_wrappers_pb.Int32Value | undefined;
  setOrd(value?: google_protobuf_wrappers_pb.Int32Value): void;

  hasProductType(): boolean;
  clearProductType(): void;
  getProductType(): v1_dictionary_common_enum_product_pb.ProductTypeValue | undefined;
  setProductType(value?: v1_dictionary_common_enum_product_pb.ProductTypeValue): void;

  hasDeleted(): boolean;
  clearDeleted(): void;
  getDeleted(): google_protobuf_wrappers_pb.BoolValue | undefined;
  setDeleted(value?: google_protobuf_wrappers_pb.BoolValue): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): CultureFilter.AsObject;
  static toObject(includeInstance: boolean, msg: CultureFilter): CultureFilter.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: CultureFilter, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): CultureFilter;
  static deserializeBinaryFromReader(message: CultureFilter, reader: jspb.BinaryReader): CultureFilter;
}

export namespace CultureFilter {
  export type AsObject = {
    id?: google_protobuf_wrappers_pb.Int64Value.AsObject,
    guid?: google_protobuf_wrappers_pb.StringValue.AsObject,
    name?: google_protobuf_wrappers_pb.StringValue.AsObject,
    culturesGroupId?: google_protobuf_wrappers_pb.Int64Value.AsObject,
    type?: google_protobuf_wrappers_pb.Int32Value.AsObject,
    ord?: google_protobuf_wrappers_pb.Int32Value.AsObject,
    productType?: v1_dictionary_common_enum_product_pb.ProductTypeValue.AsObject,
    deleted?: google_protobuf_wrappers_pb.BoolValue.AsObject,
  }
}

export class ListCultureRequest extends jspb.Message {
  hasFilter(): boolean;
  clearFilter(): void;
  getFilter(): CultureFilter | undefined;
  setFilter(value?: CultureFilter): void;

  clearSortingsList(): void;
  getSortingsList(): Array<v1_dictionary_common_dictionary_common_pb.Sorting>;
  setSortingsList(value: Array<v1_dictionary_common_dictionary_common_pb.Sorting>): void;
  addSortings(value?: v1_dictionary_common_dictionary_common_pb.Sorting, index?: number): v1_dictionary_common_dictionary_common_pb.Sorting;

  hasPagination(): boolean;
  clearPagination(): void;
  getPagination(): v1_dictionary_common_dictionary_common_pb.PaginationRequest | undefined;
  setPagination(value?: v1_dictionary_common_dictionary_common_pb.PaginationRequest): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListCultureRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ListCultureRequest): ListCultureRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListCultureRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListCultureRequest;
  static deserializeBinaryFromReader(message: ListCultureRequest, reader: jspb.BinaryReader): ListCultureRequest;
}

export namespace ListCultureRequest {
  export type AsObject = {
    filter?: CultureFilter.AsObject,
    sortingsList: Array<v1_dictionary_common_dictionary_common_pb.Sorting.AsObject>,
    pagination?: v1_dictionary_common_dictionary_common_pb.PaginationRequest.AsObject,
  }
}

export class ListCultureResponse extends jspb.Message {
  clearItemsList(): void;
  getItemsList(): Array<Culture>;
  setItemsList(value: Array<Culture>): void;
  addItems(value?: Culture, index?: number): Culture;

  hasPagination(): boolean;
  clearPagination(): void;
  getPagination(): v1_dictionary_common_dictionary_common_pb.PaginationResponse | undefined;
  setPagination(value?: v1_dictionary_common_dictionary_common_pb.PaginationResponse): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListCultureResponse.AsObject;
  static toObject(includeInstance: boolean, msg: ListCultureResponse): ListCultureResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListCultureResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListCultureResponse;
  static deserializeBinaryFromReader(message: ListCultureResponse, reader: jspb.BinaryReader): ListCultureResponse;
}

export namespace ListCultureResponse {
  export type AsObject = {
    itemsList: Array<Culture.AsObject>,
    pagination?: v1_dictionary_common_dictionary_common_pb.PaginationResponse.AsObject,
  }
}

export class GetCultureRequest extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetCultureRequest.AsObject;
  static toObject(includeInstance: boolean, msg: GetCultureRequest): GetCultureRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetCultureRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetCultureRequest;
  static deserializeBinaryFromReader(message: GetCultureRequest, reader: jspb.BinaryReader): GetCultureRequest;
}

export namespace GetCultureRequest {
  export type AsObject = {
    id: number,
  }
}

export class GetCultureResponse extends jspb.Message {
  hasItem(): boolean;
  clearItem(): void;
  getItem(): Culture | undefined;
  setItem(value?: Culture): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetCultureResponse.AsObject;
  static toObject(includeInstance: boolean, msg: GetCultureResponse): GetCultureResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetCultureResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetCultureResponse;
  static deserializeBinaryFromReader(message: GetCultureResponse, reader: jspb.BinaryReader): GetCultureResponse;
}

export namespace GetCultureResponse {
  export type AsObject = {
    item?: Culture.AsObject,
  }
}

