// package: fcp.dictionary.v1.dictionary
// file: v1/dictionary/culture_for_crop_processing.proto

import * as jspb from "google-protobuf";
import * as google_protobuf_wrappers_pb from "google-protobuf/google/protobuf/wrappers_pb";
import * as google_protobuf_timestamp_pb from "google-protobuf/google/protobuf/timestamp_pb";
import * as v1_dictionary_common_dictionary_common_pb from "../../v1/dictionary_common/dictionary_common_pb";

export class CultureForCropProcessing extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  getTechnologyCropProcessingId(): number;
  setTechnologyCropProcessingId(value: number): void;

  getCultureId(): number;
  setCultureId(value: number): void;

  getCultureName(): string;
  setCultureName(value: string): void;

  getCulturesGroupId(): number;
  setCulturesGroupId(value: number): void;

  getCulturesGroupName(): string;
  setCulturesGroupName(value: string): void;

  getOrd(): number;
  setOrd(value: number): void;

  hasHash(): boolean;
  clearHash(): void;
  getHash(): google_protobuf_wrappers_pb.StringValue | undefined;
  setHash(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasDeleted(): boolean;
  clearDeleted(): void;
  getDeleted(): google_protobuf_wrappers_pb.BoolValue | undefined;
  setDeleted(value?: google_protobuf_wrappers_pb.BoolValue): void;

  hasCreatedBy(): boolean;
  clearCreatedBy(): void;
  getCreatedBy(): google_protobuf_wrappers_pb.StringValue | undefined;
  setCreatedBy(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasCreatedAt(): boolean;
  clearCreatedAt(): void;
  getCreatedAt(): google_protobuf_timestamp_pb.Timestamp | undefined;
  setCreatedAt(value?: google_protobuf_timestamp_pb.Timestamp): void;

  hasUpdatedBy(): boolean;
  clearUpdatedBy(): void;
  getUpdatedBy(): google_protobuf_wrappers_pb.StringValue | undefined;
  setUpdatedBy(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasUpdatedAt(): boolean;
  clearUpdatedAt(): void;
  getUpdatedAt(): google_protobuf_timestamp_pb.Timestamp | undefined;
  setUpdatedAt(value?: google_protobuf_timestamp_pb.Timestamp): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): CultureForCropProcessing.AsObject;
  static toObject(includeInstance: boolean, msg: CultureForCropProcessing): CultureForCropProcessing.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: CultureForCropProcessing, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): CultureForCropProcessing;
  static deserializeBinaryFromReader(message: CultureForCropProcessing, reader: jspb.BinaryReader): CultureForCropProcessing;
}

export namespace CultureForCropProcessing {
  export type AsObject = {
    id: number,
    technologyCropProcessingId: number,
    cultureId: number,
    cultureName: string,
    culturesGroupId: number,
    culturesGroupName: string,
    ord: number,
    hash?: google_protobuf_wrappers_pb.StringValue.AsObject,
    deleted?: google_protobuf_wrappers_pb.BoolValue.AsObject,
    createdBy?: google_protobuf_wrappers_pb.StringValue.AsObject,
    createdAt?: google_protobuf_timestamp_pb.Timestamp.AsObject,
    updatedBy?: google_protobuf_wrappers_pb.StringValue.AsObject,
    updatedAt?: google_protobuf_timestamp_pb.Timestamp.AsObject,
  }
}

export class CultureForCropProcessingFilter extends jspb.Message {
  hasId(): boolean;
  clearId(): void;
  getId(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setId(value?: google_protobuf_wrappers_pb.Int64Value): void;

  hasTechnologyCropProcessingId(): boolean;
  clearTechnologyCropProcessingId(): void;
  getTechnologyCropProcessingId(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setTechnologyCropProcessingId(value?: google_protobuf_wrappers_pb.Int64Value): void;

  hasCultureId(): boolean;
  clearCultureId(): void;
  getCultureId(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setCultureId(value?: google_protobuf_wrappers_pb.Int64Value): void;

  hasCulturesGroupId(): boolean;
  clearCulturesGroupId(): void;
  getCulturesGroupId(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setCulturesGroupId(value?: google_protobuf_wrappers_pb.Int64Value): void;

  hasDeleted(): boolean;
  clearDeleted(): void;
  getDeleted(): google_protobuf_wrappers_pb.BoolValue | undefined;
  setDeleted(value?: google_protobuf_wrappers_pb.BoolValue): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): CultureForCropProcessingFilter.AsObject;
  static toObject(includeInstance: boolean, msg: CultureForCropProcessingFilter): CultureForCropProcessingFilter.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: CultureForCropProcessingFilter, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): CultureForCropProcessingFilter;
  static deserializeBinaryFromReader(message: CultureForCropProcessingFilter, reader: jspb.BinaryReader): CultureForCropProcessingFilter;
}

export namespace CultureForCropProcessingFilter {
  export type AsObject = {
    id?: google_protobuf_wrappers_pb.Int64Value.AsObject,
    technologyCropProcessingId?: google_protobuf_wrappers_pb.Int64Value.AsObject,
    cultureId?: google_protobuf_wrappers_pb.Int64Value.AsObject,
    culturesGroupId?: google_protobuf_wrappers_pb.Int64Value.AsObject,
    deleted?: google_protobuf_wrappers_pb.BoolValue.AsObject,
  }
}

export class ListCultureForCropProcessingRequest extends jspb.Message {
  hasFilter(): boolean;
  clearFilter(): void;
  getFilter(): CultureForCropProcessingFilter | undefined;
  setFilter(value?: CultureForCropProcessingFilter): void;

  clearSortingsList(): void;
  getSortingsList(): Array<v1_dictionary_common_dictionary_common_pb.Sorting>;
  setSortingsList(value: Array<v1_dictionary_common_dictionary_common_pb.Sorting>): void;
  addSortings(value?: v1_dictionary_common_dictionary_common_pb.Sorting, index?: number): v1_dictionary_common_dictionary_common_pb.Sorting;

  hasPagination(): boolean;
  clearPagination(): void;
  getPagination(): v1_dictionary_common_dictionary_common_pb.PaginationRequest | undefined;
  setPagination(value?: v1_dictionary_common_dictionary_common_pb.PaginationRequest): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListCultureForCropProcessingRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ListCultureForCropProcessingRequest): ListCultureForCropProcessingRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListCultureForCropProcessingRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListCultureForCropProcessingRequest;
  static deserializeBinaryFromReader(message: ListCultureForCropProcessingRequest, reader: jspb.BinaryReader): ListCultureForCropProcessingRequest;
}

export namespace ListCultureForCropProcessingRequest {
  export type AsObject = {
    filter?: CultureForCropProcessingFilter.AsObject,
    sortingsList: Array<v1_dictionary_common_dictionary_common_pb.Sorting.AsObject>,
    pagination?: v1_dictionary_common_dictionary_common_pb.PaginationRequest.AsObject,
  }
}

export class ListCultureForCropProcessingResponse extends jspb.Message {
  clearItemsList(): void;
  getItemsList(): Array<CultureForCropProcessing>;
  setItemsList(value: Array<CultureForCropProcessing>): void;
  addItems(value?: CultureForCropProcessing, index?: number): CultureForCropProcessing;

  hasPagination(): boolean;
  clearPagination(): void;
  getPagination(): v1_dictionary_common_dictionary_common_pb.PaginationResponse | undefined;
  setPagination(value?: v1_dictionary_common_dictionary_common_pb.PaginationResponse): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListCultureForCropProcessingResponse.AsObject;
  static toObject(includeInstance: boolean, msg: ListCultureForCropProcessingResponse): ListCultureForCropProcessingResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListCultureForCropProcessingResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListCultureForCropProcessingResponse;
  static deserializeBinaryFromReader(message: ListCultureForCropProcessingResponse, reader: jspb.BinaryReader): ListCultureForCropProcessingResponse;
}

export namespace ListCultureForCropProcessingResponse {
  export type AsObject = {
    itemsList: Array<CultureForCropProcessing.AsObject>,
    pagination?: v1_dictionary_common_dictionary_common_pb.PaginationResponse.AsObject,
  }
}

export class GetCultureForCropProcessingRequest extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetCultureForCropProcessingRequest.AsObject;
  static toObject(includeInstance: boolean, msg: GetCultureForCropProcessingRequest): GetCultureForCropProcessingRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetCultureForCropProcessingRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetCultureForCropProcessingRequest;
  static deserializeBinaryFromReader(message: GetCultureForCropProcessingRequest, reader: jspb.BinaryReader): GetCultureForCropProcessingRequest;
}

export namespace GetCultureForCropProcessingRequest {
  export type AsObject = {
    id: number,
  }
}

export class GetCultureForCropProcessingResponse extends jspb.Message {
  hasItem(): boolean;
  clearItem(): void;
  getItem(): CultureForCropProcessing | undefined;
  setItem(value?: CultureForCropProcessing): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetCultureForCropProcessingResponse.AsObject;
  static toObject(includeInstance: boolean, msg: GetCultureForCropProcessingResponse): GetCultureForCropProcessingResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetCultureForCropProcessingResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetCultureForCropProcessingResponse;
  static deserializeBinaryFromReader(message: GetCultureForCropProcessingResponse, reader: jspb.BinaryReader): GetCultureForCropProcessingResponse;
}

export namespace GetCultureForCropProcessingResponse {
  export type AsObject = {
    item?: CultureForCropProcessing.AsObject,
  }
}

