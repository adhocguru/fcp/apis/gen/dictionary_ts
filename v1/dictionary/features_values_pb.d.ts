// package: fcp.dictionary.v1.dictionary
// file: v1/dictionary/features_values.proto

import * as jspb from "google-protobuf";
import * as google_protobuf_wrappers_pb from "google-protobuf/google/protobuf/wrappers_pb";
import * as google_protobuf_timestamp_pb from "google-protobuf/google/protobuf/timestamp_pb";
import * as v1_dictionary_common_dictionary_common_pb from "../../v1/dictionary_common/dictionary_common_pb";

export class FeaturesValues extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  getFeatureId(): number;
  setFeatureId(value: number): void;

  getFeatureName(): string;
  setFeatureName(value: string): void;

  getValue(): string;
  setValue(value: string): void;

  getSortOrder(): number;
  setSortOrder(value: number): void;

  hasHash(): boolean;
  clearHash(): void;
  getHash(): google_protobuf_wrappers_pb.StringValue | undefined;
  setHash(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasDeleted(): boolean;
  clearDeleted(): void;
  getDeleted(): google_protobuf_wrappers_pb.BoolValue | undefined;
  setDeleted(value?: google_protobuf_wrappers_pb.BoolValue): void;

  hasCreatedBy(): boolean;
  clearCreatedBy(): void;
  getCreatedBy(): google_protobuf_wrappers_pb.StringValue | undefined;
  setCreatedBy(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasCreatedAt(): boolean;
  clearCreatedAt(): void;
  getCreatedAt(): google_protobuf_timestamp_pb.Timestamp | undefined;
  setCreatedAt(value?: google_protobuf_timestamp_pb.Timestamp): void;

  hasUpdatedBy(): boolean;
  clearUpdatedBy(): void;
  getUpdatedBy(): google_protobuf_wrappers_pb.StringValue | undefined;
  setUpdatedBy(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasUpdatedAt(): boolean;
  clearUpdatedAt(): void;
  getUpdatedAt(): google_protobuf_timestamp_pb.Timestamp | undefined;
  setUpdatedAt(value?: google_protobuf_timestamp_pb.Timestamp): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): FeaturesValues.AsObject;
  static toObject(includeInstance: boolean, msg: FeaturesValues): FeaturesValues.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: FeaturesValues, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): FeaturesValues;
  static deserializeBinaryFromReader(message: FeaturesValues, reader: jspb.BinaryReader): FeaturesValues;
}

export namespace FeaturesValues {
  export type AsObject = {
    id: number,
    featureId: number,
    featureName: string,
    value: string,
    sortOrder: number,
    hash?: google_protobuf_wrappers_pb.StringValue.AsObject,
    deleted?: google_protobuf_wrappers_pb.BoolValue.AsObject,
    createdBy?: google_protobuf_wrappers_pb.StringValue.AsObject,
    createdAt?: google_protobuf_timestamp_pb.Timestamp.AsObject,
    updatedBy?: google_protobuf_wrappers_pb.StringValue.AsObject,
    updatedAt?: google_protobuf_timestamp_pb.Timestamp.AsObject,
  }
}

export class FeaturesValuesFilter extends jspb.Message {
  hasId(): boolean;
  clearId(): void;
  getId(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setId(value?: google_protobuf_wrappers_pb.Int64Value): void;

  hasFeatureId(): boolean;
  clearFeatureId(): void;
  getFeatureId(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setFeatureId(value?: google_protobuf_wrappers_pb.Int64Value): void;

  hasDeleted(): boolean;
  clearDeleted(): void;
  getDeleted(): google_protobuf_wrappers_pb.BoolValue | undefined;
  setDeleted(value?: google_protobuf_wrappers_pb.BoolValue): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): FeaturesValuesFilter.AsObject;
  static toObject(includeInstance: boolean, msg: FeaturesValuesFilter): FeaturesValuesFilter.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: FeaturesValuesFilter, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): FeaturesValuesFilter;
  static deserializeBinaryFromReader(message: FeaturesValuesFilter, reader: jspb.BinaryReader): FeaturesValuesFilter;
}

export namespace FeaturesValuesFilter {
  export type AsObject = {
    id?: google_protobuf_wrappers_pb.Int64Value.AsObject,
    featureId?: google_protobuf_wrappers_pb.Int64Value.AsObject,
    deleted?: google_protobuf_wrappers_pb.BoolValue.AsObject,
  }
}

export class ListFeaturesValuesRequest extends jspb.Message {
  hasFilter(): boolean;
  clearFilter(): void;
  getFilter(): FeaturesValuesFilter | undefined;
  setFilter(value?: FeaturesValuesFilter): void;

  clearSortingsList(): void;
  getSortingsList(): Array<v1_dictionary_common_dictionary_common_pb.Sorting>;
  setSortingsList(value: Array<v1_dictionary_common_dictionary_common_pb.Sorting>): void;
  addSortings(value?: v1_dictionary_common_dictionary_common_pb.Sorting, index?: number): v1_dictionary_common_dictionary_common_pb.Sorting;

  hasPagination(): boolean;
  clearPagination(): void;
  getPagination(): v1_dictionary_common_dictionary_common_pb.PaginationRequest | undefined;
  setPagination(value?: v1_dictionary_common_dictionary_common_pb.PaginationRequest): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListFeaturesValuesRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ListFeaturesValuesRequest): ListFeaturesValuesRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListFeaturesValuesRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListFeaturesValuesRequest;
  static deserializeBinaryFromReader(message: ListFeaturesValuesRequest, reader: jspb.BinaryReader): ListFeaturesValuesRequest;
}

export namespace ListFeaturesValuesRequest {
  export type AsObject = {
    filter?: FeaturesValuesFilter.AsObject,
    sortingsList: Array<v1_dictionary_common_dictionary_common_pb.Sorting.AsObject>,
    pagination?: v1_dictionary_common_dictionary_common_pb.PaginationRequest.AsObject,
  }
}

export class ListFeaturesValuesResponse extends jspb.Message {
  clearItemsList(): void;
  getItemsList(): Array<FeaturesValues>;
  setItemsList(value: Array<FeaturesValues>): void;
  addItems(value?: FeaturesValues, index?: number): FeaturesValues;

  hasPagination(): boolean;
  clearPagination(): void;
  getPagination(): v1_dictionary_common_dictionary_common_pb.PaginationResponse | undefined;
  setPagination(value?: v1_dictionary_common_dictionary_common_pb.PaginationResponse): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListFeaturesValuesResponse.AsObject;
  static toObject(includeInstance: boolean, msg: ListFeaturesValuesResponse): ListFeaturesValuesResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListFeaturesValuesResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListFeaturesValuesResponse;
  static deserializeBinaryFromReader(message: ListFeaturesValuesResponse, reader: jspb.BinaryReader): ListFeaturesValuesResponse;
}

export namespace ListFeaturesValuesResponse {
  export type AsObject = {
    itemsList: Array<FeaturesValues.AsObject>,
    pagination?: v1_dictionary_common_dictionary_common_pb.PaginationResponse.AsObject,
  }
}

export class GetFeaturesValuesRequest extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetFeaturesValuesRequest.AsObject;
  static toObject(includeInstance: boolean, msg: GetFeaturesValuesRequest): GetFeaturesValuesRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetFeaturesValuesRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetFeaturesValuesRequest;
  static deserializeBinaryFromReader(message: GetFeaturesValuesRequest, reader: jspb.BinaryReader): GetFeaturesValuesRequest;
}

export namespace GetFeaturesValuesRequest {
  export type AsObject = {
    id: number,
  }
}

export class GetFeaturesValuesResponse extends jspb.Message {
  hasItem(): boolean;
  clearItem(): void;
  getItem(): FeaturesValues | undefined;
  setItem(value?: FeaturesValues): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetFeaturesValuesResponse.AsObject;
  static toObject(includeInstance: boolean, msg: GetFeaturesValuesResponse): GetFeaturesValuesResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetFeaturesValuesResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetFeaturesValuesResponse;
  static deserializeBinaryFromReader(message: GetFeaturesValuesResponse, reader: jspb.BinaryReader): GetFeaturesValuesResponse;
}

export namespace GetFeaturesValuesResponse {
  export type AsObject = {
    item?: FeaturesValues.AsObject,
  }
}

