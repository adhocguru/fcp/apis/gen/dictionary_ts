// package: fcp.dictionary.v1.dictionary
// file: v1/dictionary/zone_for_hybrids.proto

import * as jspb from "google-protobuf";
import * as google_protobuf_wrappers_pb from "google-protobuf/google/protobuf/wrappers_pb";
import * as google_protobuf_timestamp_pb from "google-protobuf/google/protobuf/timestamp_pb";
import * as v1_dictionary_common_dictionary_common_pb from "../../v1/dictionary_common/dictionary_common_pb";

export class ZoneForHybrids extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  getHybridId(): number;
  setHybridId(value: number): void;

  getHybridName(): string;
  setHybridName(value: string): void;

  getRecomendedZoneId(): number;
  setRecomendedZoneId(value: number): void;

  getRecomendedZoneName(): string;
  setRecomendedZoneName(value: string): void;

  hasHash(): boolean;
  clearHash(): void;
  getHash(): google_protobuf_wrappers_pb.StringValue | undefined;
  setHash(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasDeleted(): boolean;
  clearDeleted(): void;
  getDeleted(): google_protobuf_wrappers_pb.BoolValue | undefined;
  setDeleted(value?: google_protobuf_wrappers_pb.BoolValue): void;

  hasCreatedBy(): boolean;
  clearCreatedBy(): void;
  getCreatedBy(): google_protobuf_wrappers_pb.StringValue | undefined;
  setCreatedBy(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasCreatedAt(): boolean;
  clearCreatedAt(): void;
  getCreatedAt(): google_protobuf_timestamp_pb.Timestamp | undefined;
  setCreatedAt(value?: google_protobuf_timestamp_pb.Timestamp): void;

  hasUpdatedBy(): boolean;
  clearUpdatedBy(): void;
  getUpdatedBy(): google_protobuf_wrappers_pb.StringValue | undefined;
  setUpdatedBy(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasUpdatedAt(): boolean;
  clearUpdatedAt(): void;
  getUpdatedAt(): google_protobuf_timestamp_pb.Timestamp | undefined;
  setUpdatedAt(value?: google_protobuf_timestamp_pb.Timestamp): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ZoneForHybrids.AsObject;
  static toObject(includeInstance: boolean, msg: ZoneForHybrids): ZoneForHybrids.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ZoneForHybrids, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ZoneForHybrids;
  static deserializeBinaryFromReader(message: ZoneForHybrids, reader: jspb.BinaryReader): ZoneForHybrids;
}

export namespace ZoneForHybrids {
  export type AsObject = {
    id: number,
    hybridId: number,
    hybridName: string,
    recomendedZoneId: number,
    recomendedZoneName: string,
    hash?: google_protobuf_wrappers_pb.StringValue.AsObject,
    deleted?: google_protobuf_wrappers_pb.BoolValue.AsObject,
    createdBy?: google_protobuf_wrappers_pb.StringValue.AsObject,
    createdAt?: google_protobuf_timestamp_pb.Timestamp.AsObject,
    updatedBy?: google_protobuf_wrappers_pb.StringValue.AsObject,
    updatedAt?: google_protobuf_timestamp_pb.Timestamp.AsObject,
  }
}

export class ZoneForHybridsFilter extends jspb.Message {
  hasId(): boolean;
  clearId(): void;
  getId(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setId(value?: google_protobuf_wrappers_pb.Int64Value): void;

  hasHybridId(): boolean;
  clearHybridId(): void;
  getHybridId(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setHybridId(value?: google_protobuf_wrappers_pb.Int64Value): void;

  hasRecomendedZoneId(): boolean;
  clearRecomendedZoneId(): void;
  getRecomendedZoneId(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setRecomendedZoneId(value?: google_protobuf_wrappers_pb.Int64Value): void;

  hasDeleted(): boolean;
  clearDeleted(): void;
  getDeleted(): google_protobuf_wrappers_pb.BoolValue | undefined;
  setDeleted(value?: google_protobuf_wrappers_pb.BoolValue): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ZoneForHybridsFilter.AsObject;
  static toObject(includeInstance: boolean, msg: ZoneForHybridsFilter): ZoneForHybridsFilter.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ZoneForHybridsFilter, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ZoneForHybridsFilter;
  static deserializeBinaryFromReader(message: ZoneForHybridsFilter, reader: jspb.BinaryReader): ZoneForHybridsFilter;
}

export namespace ZoneForHybridsFilter {
  export type AsObject = {
    id?: google_protobuf_wrappers_pb.Int64Value.AsObject,
    hybridId?: google_protobuf_wrappers_pb.Int64Value.AsObject,
    recomendedZoneId?: google_protobuf_wrappers_pb.Int64Value.AsObject,
    deleted?: google_protobuf_wrappers_pb.BoolValue.AsObject,
  }
}

export class ListZoneForHybridsRequest extends jspb.Message {
  hasFilter(): boolean;
  clearFilter(): void;
  getFilter(): ZoneForHybridsFilter | undefined;
  setFilter(value?: ZoneForHybridsFilter): void;

  clearSortingsList(): void;
  getSortingsList(): Array<v1_dictionary_common_dictionary_common_pb.Sorting>;
  setSortingsList(value: Array<v1_dictionary_common_dictionary_common_pb.Sorting>): void;
  addSortings(value?: v1_dictionary_common_dictionary_common_pb.Sorting, index?: number): v1_dictionary_common_dictionary_common_pb.Sorting;

  hasPagination(): boolean;
  clearPagination(): void;
  getPagination(): v1_dictionary_common_dictionary_common_pb.PaginationRequest | undefined;
  setPagination(value?: v1_dictionary_common_dictionary_common_pb.PaginationRequest): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListZoneForHybridsRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ListZoneForHybridsRequest): ListZoneForHybridsRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListZoneForHybridsRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListZoneForHybridsRequest;
  static deserializeBinaryFromReader(message: ListZoneForHybridsRequest, reader: jspb.BinaryReader): ListZoneForHybridsRequest;
}

export namespace ListZoneForHybridsRequest {
  export type AsObject = {
    filter?: ZoneForHybridsFilter.AsObject,
    sortingsList: Array<v1_dictionary_common_dictionary_common_pb.Sorting.AsObject>,
    pagination?: v1_dictionary_common_dictionary_common_pb.PaginationRequest.AsObject,
  }
}

export class ListZoneForHybridsResponse extends jspb.Message {
  clearItemsList(): void;
  getItemsList(): Array<ZoneForHybrids>;
  setItemsList(value: Array<ZoneForHybrids>): void;
  addItems(value?: ZoneForHybrids, index?: number): ZoneForHybrids;

  hasPagination(): boolean;
  clearPagination(): void;
  getPagination(): v1_dictionary_common_dictionary_common_pb.PaginationResponse | undefined;
  setPagination(value?: v1_dictionary_common_dictionary_common_pb.PaginationResponse): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListZoneForHybridsResponse.AsObject;
  static toObject(includeInstance: boolean, msg: ListZoneForHybridsResponse): ListZoneForHybridsResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListZoneForHybridsResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListZoneForHybridsResponse;
  static deserializeBinaryFromReader(message: ListZoneForHybridsResponse, reader: jspb.BinaryReader): ListZoneForHybridsResponse;
}

export namespace ListZoneForHybridsResponse {
  export type AsObject = {
    itemsList: Array<ZoneForHybrids.AsObject>,
    pagination?: v1_dictionary_common_dictionary_common_pb.PaginationResponse.AsObject,
  }
}

export class GetZoneForHybridsRequest extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetZoneForHybridsRequest.AsObject;
  static toObject(includeInstance: boolean, msg: GetZoneForHybridsRequest): GetZoneForHybridsRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetZoneForHybridsRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetZoneForHybridsRequest;
  static deserializeBinaryFromReader(message: GetZoneForHybridsRequest, reader: jspb.BinaryReader): GetZoneForHybridsRequest;
}

export namespace GetZoneForHybridsRequest {
  export type AsObject = {
    id: number,
  }
}

export class GetZoneForHybridsResponse extends jspb.Message {
  hasItem(): boolean;
  clearItem(): void;
  getItem(): ZoneForHybrids | undefined;
  setItem(value?: ZoneForHybrids): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetZoneForHybridsResponse.AsObject;
  static toObject(includeInstance: boolean, msg: GetZoneForHybridsResponse): GetZoneForHybridsResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetZoneForHybridsResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetZoneForHybridsResponse;
  static deserializeBinaryFromReader(message: GetZoneForHybridsResponse, reader: jspb.BinaryReader): GetZoneForHybridsResponse;
}

export namespace GetZoneForHybridsResponse {
  export type AsObject = {
    item?: ZoneForHybrids.AsObject,
  }
}

